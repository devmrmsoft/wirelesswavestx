<?php

namespace FleetCart;

use Illuminate\Database\Eloquent\Model;

class productPhoneModel extends Model
{
    //

    protected $fillable = ['product_id', 'cat_id', 'status'];
}
