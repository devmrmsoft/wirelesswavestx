<?php

Route::post('cookie-bar', 'CookieBarController@accepted')->name('cookie_bar.accepted');
Route::post('newsletter-removed', 'CookieBarController@newsletterRemoved')->name('newsletter.removed');
Route::get('products/{slug}/quick-view', 'ProductQuickViewController@show')->name('products.quick_view.show');
Route::get('/our-newsletter', 'ProductQuickViewController@newsLetter')->name('our-newsletter');