<?php

    $order = (array) DB::table('orders')->select()->where('id',$text[1])->first();
    $role =(array) DB::table('users')->select('ret_list')->where('id',$order['customer_id'])->first();
    
         
?>
<!DOCTYPE html>
<html lang="en" style="-ms-text-size-adjust: 100%;
                    -webkit-text-size-adjust: 100%;
                    -webkit-print-color-adjust: exact;"
>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet">
    </head>

    <body style="font-family: 'Open Sans', sans-serif;
                font-size: 15px;
                min-width: 320px;
                margin: 0;"
    >
        <table style="border-collapse: collapse; width: 100%;">
            <tbody>
                <tr>
                    <td style="padding: 0;">
                        <table style="border-collapse: collapse; width: 100%;">
                            <tbody>
                                <tr>
                                    <td align="center" style="background: #dadce0;">
                                        @if (is_null($logo))
                                            <h5 style="font-size: 30px;
                                                    line-height: 36px;
                                                    margin: 0;
                                                    padding: 30px 15px;
                                                    text-align: center;"
                                            >
                                                <a href="{{ route('home') }}" style="font-family: 'Open Sans', sans-serif;
                                                                                    font-weight: 400;
                                                                                    color: #ffffff;
                                                                                    text-decoration: none;"
                                                >
                                                    {{ setting('store_name') }}
                                                </a>
                                            </h5>
                                        @else
                                            <div style="display: flex;
                                                        height: 64px;
                                                        width: 200px;
                                                        padding: 16px 15px;
                                                        align-items: center;"
                                            >
                                                <img src="{{ $logo }}" style="max-height: 100%; max-width: 100%;">
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="padding: 40px 15px;">
                        <table style="border-collapse: collapse;
                                    min-width: 320px;
                                    max-width: 600px;
                                    width: 100%;
                                    margin: auto;"
                        >

                            @isset($heading)
                                <tr>
                                    <td style="padding: 0;" class='Test'>
                                        <h4 style="font-family: 'Open Sans', sans-serif;
                                                font-weight: 400;
                                                font-size: 21px;
                                                line-height: 26px;
                                                margin: 0 0 15px;
                                                color: #555555;"
                                        >
                                            {{ $heading }}
                                        </h4>
                                    </td>
                                </tr>
                            @endisset
                            @if($text[0]=='completed' && $role['ret_list'] == 4)
                            
                            <tr>
                                <td style="padding: 0;">
                                    <span style="font-family: 'Open Sans', sans-serif;
                                                font-weight: 400;
                                                font-size: 16px;
                                                line-height: 26px;
                                                color: #666666;
                                                display: block;"
                                    >
                                    
                                    
                                    <p>We got you! Your order is now complete and has been shipped. Below are tracking details </p>
                                    </span><br><span>
                                        <p>Shipping Carrier: {{$order['shipping_courier']}}</p>
                                        @if($order['shipping_address_1'] == Null)
                                        <p>Shipping Address: {{$order['billing_address_1']}}, {{$order['billing_city']}}, {{$order['billing_state']}}, {{$order['billing_country']}}</p>
                                        @else
                                        <p>Shipping Address: {{$order['shipping_address_1']}}, {{$order['shipping_city']}}, {{$order['shipping_state']}}, {{$order['shipping_country']}}</p>
                                        @endif
                                        <p>Tracking Number: {{$order['tracking_number']}}</p>
                                    </span><br><span>
                                        <p>We receive new inventory every day, be sure to check out our product catalog here so you don’t miss an incredible deal.</p>
                                    
                                    Regards</span><br><span>
                                    Team Wireless Waves</span>
                                    
                                    
                                </td>
                            </tr>
                        
                            @elseif($text[0]=='processing' && $role['ret_list'] == 4)
                            <tr>
                                <td style="padding: 0;">
                                    <span style="font-family: 'Open Sans', sans-serif;
                                                font-weight: 400;
                                                font-size: 16px;
                                                line-height: 26px;
                                                color: #666666;
                                                display: block;"
                                    >
                                         
                                    <p>You did it! Thank you for shopping at Wireless Waves. Your order confirmation number is {{$text[1]}}. We know you are excited to receive your order and display it for your customers so here is what to expect: </p>
                                    </span><br><span>
                                    <p>• Your order will be shipped in the next 24 hours.</p>
                                    <p>• Once it ships, you will receive a confirmation email with your tracking number.</p>

                                    Regards</span><br><span>
                                    Team Wireless Waves</span>
                                    
                                    
                                </td>
                            </tr>
                            @else
                            <tr>
                                <td style="padding: 0;">
                                    <span style="font-family: 'Open Sans', sans-serif;
                                                font-weight: 400;
                                                font-size: 16px;
                                                line-height: 26px;
                                                color: #666666;
                                                display: block;"
                                    >
                                         
                                        <p>Thank you for your submmission. One of our account
                                            managers will be in touch with you within 24-48 hours to verify your information and walk
                                            through our product catalog </p>
                                    </span><br><span>

                                    Regards</span><br><span>
                                    Team Wireless Waves</span>
                                    
                                    
                                </td>
                            </tr>
                            @endif
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" style="padding: 15px 0; background: #f1f3f7;">
                        <span style="font-family: 'Open Sans', sans-serif;
                                    font-weight: 400;
                                    font-size: 16px;
                                    line-height: 26px;
                                    display: inline-block;
                                    color: #555555;
                                    padding: 0 15px;"
                        >
                            &copy; {{ date('Y') }}
                            <a target="_blank" href="{{ route('home') }}" style="text-decoration: none; color: #31629f;">
                                {{ setting('store_name') }}.
                            </a>
                            {{ trans('storefront::mail.all_rights_reserved') }}
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
