@extends('public.layout')

@section('title', 'Newsletter')

@push('styles')
   
   <style>
       .newsletter {
            position: relative;
            padding-top: 87px;
            padding-left: 220px;
            flex-grow: 1;
            min-height: 600px;
        }
        
        .newsletter svg {
            position: absolute;
            z-index: 0;
            top: 0;
            left: 60px;
            fill: #fe420b;
        }
        
        .newsletter .newsletter-title {
            margin-bottom: 40px;
            font-family: IMFell DW,Times,Georgia,serif;
            font-weight: 400;
            font-style: italic;
            color: #ffffff;
            font-size: 4.0625rem;
            letter-spacing: -.02em;
            line-height: .9230769231;
            position: relative;
            z-index: 999;
        }
        
        .newsletter .newsletter-text {
            color: rgb(255, 255, 255);
            padding-right: 140px;
            position: relative;
            z-index: 999;
            max-width: 450px;
        }
        
        .newsletter .newsletter-form {
            position: relative;
            max-width: 355px;
            margin-top: 23px;
            z-index: 999;
        }
        
        .newsletter .newsletter-form input {
            height: 55px;
            width: 100%;
            padding: 18px 135px 17px 30px;
            font-family: IM Fell English,Times,Georgia,serif;
            font-weight: 400;
            font-style: italic;
            border: 0 none;
            border-radius: 20px;
            outline: none;
            background-color: #fff;
            color: #1107ff;
            font-size: 1.125rem;
            line-height: 1;
        }
        
        .newsletter .newsletter-form button {
            font-family: Sofia Pro,Helvetica,Arial,sans-serif;
            font-weight: 700;
            font-style: normal;
            width: 116px;
            height: 36px;
            transition: background-color .3s cubic-bezier(.165,.84,.44,1);
            border-radius: 15px;
            background-color: #fe420b;
            color: #fff;
            font-size: .875rem;
            line-height: 1;
            position: absolute;
            right: 9px;
            bottom: 9px;
            display: inline-block;
            margin: 0;
            padding: 0;
            border: 0px;
            outline: none;
            text-decoration: none;
            cursor: pointer;
        }

    @media only screen and (max-width: 600px) {
        .newsletter{
            padding-left: 100px;
        }
        .newsletter svg{
            left:-50px;
        }
        .newsletter .newsletter-text{
            padding-right:0px;
        }
    }

   </style>
   
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@endpush

@section('content')
<div class="container">
	<div class="newsletter js-rollover" data-radius="50">
      <svg viewBox="0 0 694 549" width="694" height="549">
        <path d="M74.7,439.6c58.9,76.3,152.2,113.1,224.6,109C585.8,532.3,734.2,295,684.7,94.2C658.7-11.3,508.1-8.6,433.6,9.8C384.1,22,329.1,47.8,277.8,60.4c-42.5,10.5-90,7.2-130.4,17.1C60.4,99-21.1,201.9,4.9,307.6C16.7,355.3,45.2,401.4,74.7,439.6z"></path>
      </svg>

      <h2 class="newsletter-title">Get the latest<br> info about Tech</h2>
      <p class="newsletter-text">Register to our newsletter and don’t miss anything anymore. We promise we will not spam you!</p>

      <form method="GET" action="https://kikk.us6.list-manage.com/subscribe" class="newsletter-form" target="_blank">
        <input type="hidden" name="u" value="d08fe605a9149dc54a3c13f44">
        <input type="hidden" name="id" value="96f67efdeb">
        <input type="email" name="EMAIL" id="email" placeholder="Enter your email">
        <button type="submit" class="button">Subscribe</button>
      </form>
    </div>
</div>
@endsection
