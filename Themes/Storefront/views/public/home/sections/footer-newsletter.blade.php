    <style type="text/css">        
        /*************Subscribe Section********************/
        .subscribe-section {
            padding: 80px 0 ;
        }
        figure {
            padding: 15px 0;
        }
        ._promo-detail {
            background: linear-gradient(163deg,#f8f8f8 0,#F8F8F8 60%,#f1f1f1 60%,#f1f1f1 100%);
            position: relative;
            display: block;
            font-size: 26px;
            transition: 1s all;
        }
        ._promo-detail figure h1 {
            text-transform: uppercase;
            font-size: 25px;
            font-weight: 300;
            margin: 0;
            padding: 4px 0;
            letter-spacing: .5px;
        }
        ._promo-detail figure h3 {
            text-transform: uppercase;
            font-size: 25px;
            margin: 0;
            letter-spacing: .5px;
            padding: 4px 0;
        }
        ._promo-detail figure p {
            display: inline-block;
            padding: 5px 0;
        }
        ._promo-detail figure p span sup {
            font-size: 20px;
        }
        ._promo-detail figure h6 {
            font-size: 30px;
            display: inline-block;
            margin: 0;
            margin-left: -5px;
        }
        ._promo-detail figure button {
            background: #f5c400;
            color: #323f48;
            font-size: 13px;
            padding: 10px 32px;
            vertical-align: middle;
            border-radius: 25px;
        }
        ._promo-detail article a img {
            bottom: 0;
            max-width: 300px;
            position: absolute;
            right: 0;
        }
        ._news-letter {
            background-color: #f4f6f5;
            padding: 38px 28px 20px;
        }
        ._news-letter h4 {
            margin: 0;
            font: 18px/1.3 "Open Sans",sans-serif;
            text-transform: uppercase;
            letter-spacing: 1px;
            margin-bottom: 2rem;
        }
        ._news-letter .newsletters__text {
            border: 1px solid #ffffff;
            /*border-radius: 35px;*/
            color: #8c8c8c;
            font-size: 13px;
            height: 36px;
            padding-right: 112px;
            position: relative;
            box-shadow: none;
            border-bottom-right-radius: 20px !important;
            border-top-right-radius: 20px !important;
        }
        ._news-letter .newsletters__btn {
            border-radius: 0 35px 35px 0;
            font-size: 13px;
            padding: 12px 30px;
            position: absolute;
            right: 43px;
            bottom: 36px;
            background: #e9420b;
            color: #ffffff;
        }
        .btn:active:focus, .btn:focus {
            outline-offset: -2px;
            outline: none;
        }
        ._promo-detail:hover {
            transform: translate(20px);
        } 
    </style>
    <section class="subscribe-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 _subscribe-col">
                    <div class="_promo-detail">
                        <a href="#"><img src="https://wirelesswavestx.com/storage/media/750x183-newsletter.png" class="img-responsive"></a>
                        {{-- <figure>
                            <h1>samsung galaxy s7 edge</h1>
                            <h3><strong> at a super price!</strong></h3>
                            <p>
                                <span>
                                    <sup>$</sup>
                                </span>
                            </p>                                    
                            <h6>916</h6>
                            <button class="btn" type="button">Buying Now</button>
                        </figure>
                        <article>
                        </article> --}}
                    </div>
                </div>
                <div class="col-md-4 _subscribe-col">
                    <div class="_news-letter">
                        <h4> Subscribe to our news and be aware of <strong>all sales!</strong></h4>
                        <div class="form-group"> <input id="newsletter-input" class="newsletters__text form-control form-control--skin-one" type="text" name="email" size="18" placeholder="Your e-mail"> <button type="submit" name="submitNewsletter" class="newsletters__btn btn"> <span>Subscribe</span> </button> <input type="hidden" name="action" value="0"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
