
<style type="text/css">
    li.item {
    list-style-type: none;
}
hr {
    border-color: #ededed;
}

.btn {
    font: 500 13px/22px 'Poppins', sans-serif;
    text-transform: uppercase;
    display: inline-block;
    text-align: center;
    vertical-align: middle;
    cursor: pointer;
    user-select: none;
    border-radius: 0;
    white-space: nowrap;
    letter-spacing: 0.5px;
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
}
.btn-primary {
    border: 1px solid #e86236;
    color: #fff;
    background: #e86236;
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
}
.btn-primary:hover {
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
}


.btn-primary,
.btn-secondary,
.btn-tertiary {
    text-transform: uppercase;
    padding: 8px 20px;
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
     -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    -o-border-radius: 3px;
    border-radius: 3px;
}
.btn-primary .material-icons,
.btn-secondary .material-icons,
.btn-tertiary .material-icons {
    margin-right: 5px;
    font-size: 20px;
    height: 19px;
}
.btn-tertiary {
    padding: 8px 20px;
    margin: 0.25rem 0;
    font-weight: 400;
    font-size: 0.875rem;
}
.btn-tertiary .material-icons {
    font-size: 14px;
}

.btn-warning {
    -webkit-transition: all 0.4s ease-in-out;
    -moz-transition: all 0.4s ease-in-out;
    -o-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
}
.btn-tertiary-outline {
    color: #6C868E;
    background-image: none;
    background-color: transparent;
    border-color: #6C868E;
    border: 0.15rem solid #6C868E;
    -webkit-transition: all 0.4s ease-in-out;
    -moz-transition: all 0.4s ease-in-out;
    -o-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
}
.btn-tertiary-outline:hover {
    border-color: #BBCDD2;
    color: #BBCDD2;
}



.product-tabcontent {
    margin: 50px 0;
}
.product-tabcontent .tabs .nav-tabs {
    width: 100%;
    float: none;
    margin: 0;
}
.special-products {
    padding: 0 0 20px;
}
.special-products .special-products-wrapper,
.product-accessories .product-accessories-wrapper,
.productscategory-products .productscategory-products-wrapper {
    margin: 0 -14px;
}
.featured-products,
.newproducts,
.bestseller-products,
.special-products,
.viewed-products,
.crosssell-products,
.productscategory-products,
.product-accessories {
    float: left;
    width: 100%;
    position: relative;
    text-align: center;
}
.featured-products .products,
.newproducts .products,
.bestseller-products .products,
.special-products .products,
.viewed-products .products,
.crosssell-products .products,
.productscategory-products .products,
.product-accessories .products,
.brands .products {
    float: left;
    width: 100%;
    position: relative;
}


.product-accessories, 
.productscategory-products {
    margin-bottom: 40px;
}
.products-section-title {
    border: medium none;
    display: inline-block;
    width: 100%;
    padding: 0 0 15px 0;
    margin: 0 0px 20px 28;
    color: #222222;
    position: relative;
    text-align: left;
    font: 600 20px/30px 'Poppins', sans-serif;
    text-transform: capitalize !important;
    border-bottom: 1px solid #e6e6e6;
        letter-spacing: 0.2px;
}

/***** Products Design ****/

.products .products {
    display: -webkit-box;
    display: -moz-box;
    display: box;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-lines: multiple;
    -moz-box-lines: multiple;
    box-lines: multiple;
    -webkit-flex-wrap: wrap;
    -moz-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-pack: start;
    -moz-box-pack: start;
    box-pack: start;
    -webkit-justify-content: flex-start;
    -moz-justify-content: flex-start;
    -ms-justify-content: flex-start;
    -o-justify-content: flex-start;
    justify-content: flex-start;
    -ms-flex-pack: start;
}
.products .product-thumbnail {
    background: #fff;
    text-align: center;
    display: block;
}
.products .product-thumbnail img {
    width: 100%;
    margin: 0 auto;
    position: relative;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}
.products .product-thumbnail img.fliper_image {
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    opacity: 0;
    filter: alpha(opacity=0);
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
    -webkit-transform: translateX(-110%);
    -moz-transform: translateX(-110%);
    -ms-transform: translateX(-110%);
    -o-transform: translateX(-110%);
    transform: translateX(-110%);
}
.products .product_list li.item:hover .thumbnail-container .product-thumbnail .fliper_image,
.products .product_list li.product_item:hover .thumbnail-container .product-thumbnail .fliper_image {
    opacity: 1;
    filter: alpha(opacity=100);
    -webkit-transform: translate(0, 0);
    -moz-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    -o-transform: translate(0, 0);
    transform: translate(0, 0);
    left: 0;
}
.products .product-title {
    text-transform: uppercase;
    display: block;
    margin-bottom: 3px;
    
    text-overflow: ellipsis;
    width: 100%;
}
.products .product-title a {
    color: #000000;
    font-size: 18px;
    text-decoration: none;
    font-weight: 400;
    text-transform: capitalize;
    font-family: 'Poppins', sans-serif;
    text-overflow: ellipsis;
    clear: both;
    display: block;
    text-align: left;
    line-height: 24px;
}
#footer .footer-before {
    margin: 0 -30px;
}
.products .product-title a:hover {
    color: #888888;
}
.products .thumbnail-container {
    position: relative;
    margin-bottom: 10px;
    overflow: hidden;
    padding: 0;
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
}
.products .product_list li:hover .thumbnail-container {
    border-color: #e86236;
}
.products .thumbnail-container:hover .highlighted-informations:after {
    opacity: 1;
    filter: alpha(opacity=100);
}
.products .thumbnail-container:hover .highlighted-informations.no-variants {
    bottom: 4.375rem;
}
.products .product_list li.item .outer-functional,
.products .product_list li.product_item .outer-functional {
    position: absolute;
    top: 45%;
    width: 100%;
    left: 0;
    right: 0;
    margin: 0 auto;
    opacity: 0;
    filter: alpha(opacity=0);
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
    transform: scale(0);
}
.products .product_list li .outer-functional .functional-buttons {
    display: inline-block;
    position: relative;
    width: 100%;
    text-align: center;
}
.products .product_list li.item:hover .outer-functional,
.products .product_list li.product_item:hover .outer-functional {
    opacity: 1;
    filter: alpha(opacity=100);
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
    transform: scale(1);
}
.products .product_list {
    width: 100%;
    margin: 0;
}
.products .customNavigation {
    top: -68px;
    right: 15px;
}

.products .product_list li.product_item{
    float: none;
    display: inline-block;
    vertical-align: top;
}
.products .product_list li.product_item,
.products .product_list li.item {
   /* padding: 10px 15px 10px 15px;*/
}
.products .product_list li .outer-functional .functional-buttons .wishlist a {
    font-size: 0;
    height: 34px;
    width: 34px;
    background: #dc3c08 url(../img/codezeel/action.png) no-repeat scroll center -102px;
    display: inline-block;
    vertical-align: top;
   -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
        border-radius: 3px;
}
.products .product_list li .outer-functional .functional-buttons .wishlist {
    display: inline-block;
}
.products .product_list li .outer-functional .functional-buttons .wishlist a:hover {
    background-color: #e86236;
}
.products .product_list li .quick-view {
    font-size: 0px;
    position: relative;
    height: 34px;
    width: 34px;
    background: #dc3c08 url(../img/codezeel/action.png) no-repeat scroll 9px -46px;
    transition: none;
    opacity: 1;
    filter: alpha(opacity=0);
    margin: 0 3px;
    display: inline-block;
    vertical-align: top;
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
    border-radius: 3px;
}
.products .product_list li .quick-view:before {
    display: inline-block;
    font-family: "FontAwesome";
    padding: 4px 12px;
    font-size: 16px;
    color: #000;
}
.products .product_list li .quick-view .material-icons {
    display: none;
    font-size: 20px;
    margin: 7px 10px;
    color: #ffffff;
}
.products .product_list li .quick-view:hover{
    background-color: #e86236;
}
.products .product_list li.item .product-flags .new {
    opacity: 0;
    filter: alpha(opacity=0);
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
.products .product_list li.item .on-sale {
    opacity: 1;
    filter: alpha(opacity=100);
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;

}
.products .product_list li.item:hover .thumbnail-container .product-thumbnail .fliper_image {
    opacity: 1;
    filter: alpha(opacity=100);
}
.products .product_list li.item:hover .thumbnail-container .product-thumbnail, 
.products .product_list li.product_item:hover .thumbnail-container .product-thumbnail {
    opacity: 0.7;
    filter: alpha(opacity=70);
}
.special-products .products .product_list li.item:hover .thumbnail-container .product-thumbnail, 
.special-products .products .product_list li.product_item:hover .thumbnail-container .product-thumbnail {
    opacity: 1;
    filter: alpha(opacity=100);
}
.products .product_list li.item:hover .product-title, .products .product_list li.item:hover .product-title a, .products .product_list li.product_item:hover .product-title, .products .product_list li.product_item:hover .product-title a{
    color: #e86236;
}
.products .product_list li .product-actions {
    
    margin: 0 5px 5px ;
}
.products .list li:hover .product-actions {
    opacity: 1;
    margin: 0;
}

.products .product_list li.item:hover .quick-view {
    opacity: 1;
    filter: alpha(opacity=100);
    -webkit-transform: scale(1, 1);
    -ms-transform: scale(1, 1);
    transform: scale(1, 1);
}
.products .product_list li.item:hover .product-flags .new {
    opacity: 1;
    filter: alpha(opacity=100);
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
.products .product_list li.item:hover .on-sale {
    opacity: 0;
    filter: alpha(opacity=0);
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
}
.products .product_list.list li .product-actions{
    position: static;
    opacity: 1;
    margin: 0;
    filter: alpha(opacity=100);
    
}
.products .product_list.list li .btn.add-to-cart {
    width: auto;
    height: auto;
    font-size: 12px;
    background: #e86236;
    padding: 8px 25px;
}
.products .product_list.list li:hover .btn.add-to-cart {
    border-color: #dc3c08;
    background: #dc3c08;
}
.products .product-price-and-shipping {
    color: #000000;
    font-weight: 600;
    font-size: 18px;
    margin-bottom: 10px;
}
.products .variant-links {
    width: 100%;
    padding-top: 8px;
}
.products .variant-links .color {
    width: 20px;
    height: 20px;
}
.products .highlighted-informations {
    display: none;
}
.hurryup-text {
    color: #f00;
    font-size: 13px;
    font-weight: 500;
}
.products .product-detail {
    display: none;
}
.products .product-description {
    text-align: left;
        position: relative;
}
.products .product-miniature {
    margin: 0;
    position: relative;
}
.products .product-miniature .product-actions {
    margin: 0;
}
.products .product_list li .product-miniature .product-actions {
    margin: 0 ;
    display: inline-block;
    vertical-align: top;
}
.products .product-miniature .discount {
    display: none;
}
.products .product-miniature .discount-percentage, 
.products .product-miniature .discount-product {
    z-index: 2;
    color: #f00;
    vertical-align: top;
    display: inline-block;
    padding: 5px 3px;
    font-size: 13px;
    font-weight: 500;
    position: absolute;
    left: auto;
    right: 0;
    top: 3px;
    bottom: auto;
    border-radius: 3px;
    line-height: 15px;
}
.products .comments_note {
    text-align: left;
    color: #878787;
    padding-bottom: 5px;
    display: block;
}
.products .regular-price {
    color: #999999;
    text-decoration: line-through;
    font-size: 13px;
    display: inline-block;
    vertical-align: top;
    font-weight: 300;
}

.products .count {
    color: #808080;
    font-weight: 700;
    position: relative;
    bottom: 0.5rem;
}
.products .view_more {
    margin: 20px 0 0;
}
.products .all-product-link {
    clear: both;
    display: inline-block;
    text-align: center;
    font-size: 16px;
    padding: 8px 30px;
    background-color: #e86236;
    border: 1px solid #e86236;
    border-radius: 5px;
    font-weight: 500;
    letter-spacing: 0.5px;
    color: #fff;
    text-transform: uppercase;
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
.products .all-product-link:hover {
    background: #dc3c08;
    border-color: #dc3c08;
    color: #fff;
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
.product-title a {
    color: #262626;
    font-size: 14px;
    text-decoration: none;
    text-align: center;
    font-weight: 400;
    text-transform: capitalize;
}
.product-title a:hover {
    color: #888888;
}
.featured-products .featured-products-wrapper{
    margin: 0 -14px;
}
.featured-products {
    padding: 20px 0 0 0;
}
.special-products .products .thumbnail-container {
    width: 47.9%;
    float: left;
    margin:0;
}
.special-products .products .product-description {
    width: 52.1%;
    padding: 39px 20px;
}
.special-products .products li.item {
    float: left;
    width: 100%;
}
.special-products .products .product-miniature {
    border: unset;
    float: left;
    width: 100%;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    -o-border-radius: 5px;
}
.special-products .products .product_list li .btn.add-to-cart {
    font-size: 12px;
    font-weight: 500;
    color: #ffffff;
    background: #e86236;
    text-transform: uppercase;
    padding: 9px 17px 8px;
    margin: 0;
    height: auto;
    width: auto;
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
}
.special-products .products .product_list li .btn.add-to-cart:hover {
    background-color: #dc3c08;
}
.special-products .products .product_list li .product-miniature .product-actions {
    opacity: 1;
}
.special-products .products .product_list li.product_item .product-flags .new, 
.special-products .products .product_list li.item .product-flags .new, 
.special-products .products .product_list li.item .on-sale, 
.special-products .products .product_list li.product_item .on-sale {
    display: none;
}
.special-products .products .product-miniature .discount-percentage, .special-products .products .product-miniature .discount-product {
    position: static;
}
@media (max-width: 1349px) {
    .cz-hometabcontent .customNavigation a.next {
    right: 0;
}
    .cz-hometabcontent .customNavigation a.prev {
    left: 0;
}
}
@media (max-width: 1199px) {
    .cz-hometabcontent {
        padding: 30px 0;
    }
    
    .special-products .products .product-description {
    padding: 5px 10px;
    }
    .featured-products {
       padding-top: 10px;
    }
}
@media (max-width: 991px) {
    .special-products .products .product-description {
        padding: 40px 15px;
    }
}
@media (min-width: 768px) {
    
    .products .product_list li .product-actions {
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transition: all 500ms ease 0s;
        -moz-transition: all 500ms ease 0s;
        -o-transition: all 500ms ease 0s;
        transition: all 500ms ease 0s;
    }
    .products .product_list li:hover .product-actions {
        opacity: 1;
        filter: alpha(opacity=100);
    }
}
.products .product_list.list li .comments_note { opacity: 1; filter: alpha(opacity=100);}
/**** List view ****/

#products .products {
    margin-bottom: 15px;
    position: relative;
}
#products .products .product_list.list li.product_item {
    padding: 0px 15px;
    margin-bottom: 15px;
}
#products .products .product_list.list li.product_item .product-miniature {
    background: #fff;
    padding: 10px 0px 10px;
    overflow: hidden;
    width: 100%;
    max-width: 100%;
}
#products .products .product_list.list li.product_item .thumbnail-container .product-thumbnail {
    margin-bottom: 0px;
}
#products .products .product_list.list li.product_item .thumbnail-container .product-actions {
    display: none !important;
}
#products .products .product_list.list li ul.product-flags li.new {
    left: 20px;
}
#products .products .product_list.list li ul.product-flags li.on-sale {
    right: 20px;
}
#products .products .product_list.list li .highlighted-informations {
    display: block !important;
    margin-bottom: 12px;
}
#products .products .product_list.list .thumbnail-container {
    margin-bottom: 0;
    width: 30.6%;
}
#products .products .product_list.list li .center-block {
    text-align: left;
    padding-right: 0;
    padding-left: 25px; 
    padding-top: 10px;
    width: 69.4%;
}
#products .products .product_list.list li .variant-links {
    padding-top: 0px;
    display: inline-block;
    width: auto;
    vertical-align: text-top;
}
#products .products .product_list.list li .button-container {
    margin-top: 5px;
}
#products .products .product_list.list li .product-title {
    border-bottom: 1px solid #ededed;
    padding-bottom: 10px;
}
.products .product_list.list .product-miniature .discount-percentage, 
.products .product_list.list .product-miniature .discount-product {
    position: static;
}
#products .products .product_list.list li .product-detail {
    margin-bottom: 15px;
    line-height: 22px;
    display: block;
   
}
@media (max-width: 1199px) and (min-width: 576px){
    #products .products .product_list.list .thumbnail-container {
        width: 33.33%;
    }
    #products .products .product_list.list li .center-block {
        width: 66.66%;
    }
}
@media (max-width: 1199px) {
    .products {
        margin-bottom: 0px;
    }
    
    .brands .products {
        margin: 20px auto 10px;
    }
}
@media (max-width: 991px) {
    
    
}
@media (max-width: 767px) {
    .products .product_list li .quick-view {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: scale(1, 1);
        -ms-transform: scale(1, 1);
        transform: scale(1, 1);
    }
    .products .product_list li .thumbnail-container .product-actions {
        opacity: 1;
        filter: alpha(opacity=100);
        
       
    }
    .products .product_list li .add-to-cart {
        
    }
    
    
    .products .product-price-and-shipping {
        margin-bottom: 5px;
    }
    .products .product_list li .product-actions {
        position: static;
    }
}

@media (max-width: 575px) {
    #products .products .list li.product_item .center-block {
        width: 100%;
        text-align: center;
        padding-left: 0;
    }
    .cz-hometabcontent {
    padding-top: 30px;
    padding-bottom: 30px;
}
    #products .products .list li.product_item.col-sm-6,
    #products .products .list li.product_item.col-md-6 {
        width: 50%;
    }
    #products .products .list li.item.col-sm-6 {
        width: 50%;
    }
    #products .products .product_list.list li .thumbnail-container {
        max-width: 277px;
        width: 100%;
        margin: 0 auto 10px;
        float: none;
        display: block;
    }
    #products .products .product_list.list li .product-title a {
    text-align: center;
}
    
    .products .cz-carousel li.item {
        width: 100%;
    }
    .products .product_list li .thumbnail-container {
        max-width: 277px;
        
    }
    
}
@media (max-width: 599px) and (min-width: 480px) {
    .products .product_list.list li.product_item {
        width: 100%;
    }
    .products .product_list li.product_item {
        width: 49%;
    }
    
}
@media (min-width: 600px) {
    .products .product_list li.product_item.col-md-4,
    .products .product_list li.product_item.col-md-6 {
        width: 32.8%;
    }
    .products .product_list li.item.col-md-4 {
        width: 32.8%;
    }
}
@media (min-width: 992px) {
    .products .product_list li.product_item.col-lg-3 {
        width: 24.7%;
    }
    .products .product_list li.item.col-lg-3 {
        width: 24.7%;
    }
}
/************ Manufacture list page ****************/

.brands {
    text-align: center;
    clear: both;
    padding: 0 0 20px 0;
}
.brands .products {
    float: left;
    width: 100%;
    position: relative;
    text-align: center;
    margin: 10px auto 20px;
}
.brands .products .product_list li {
    padding: 10px 15px 20px;
    float: left;
}
.brands .products .product_list li.item {
    padding: 10px 15px 10px;
    float: none;
}
.brands .products .product_list li img {
    max-width: 100%;
    opacity: 0.5;
}
.brands .products .product_list li:hover img {
    opacity: 1;
    filter: alpha(opacity=100);
}
.brands .product-title {
    margin-top: 15px;
}
.brands .customNavigation {
    top: 40px;
    opacity: 0;
    filter: alpha(opacity=0);
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
    width: 100%;
    display: none;
    right: 0;
}
.brands:hover .customNavigation {
    opacity: 1;
    filter: alpha(opacity=100);
}
#brand_list li.brand .brand-inner {
    border-bottom: 1px solid #ededed;
    padding: 31px 0 30px 0;
}
#brand_list li.brand .brand-inner .brand-img {
    text-align: center;
    margin-bottom: 10px;
}
#brand_list li.brand .brand-inner .brand-img img {
    max-width: 100%;
}
#brand_list li.brand .brand-inner .brand-infos {
    padding-left: 0;
}
#brand_list li.brand .brand-inner .brand-products {
    border-left: 1px solid #ededed;
    padding: 0 0 15px 30px;
    min-height: 108px;
}
#custom-text {
    background: white;
    border-radius: 2px;
    margin-bottom: 1.5rem;
    padding: 3.125rem 3.125rem;
    text-align: center;
}
#custom-text h3 {
    text-transform: uppercase;
    color: #262626;
    font-size: 1.5625rem;
    font-weight: 700;
}
#custom-text p {
    color: #262626;
    font-weight: 400;
    font-size: 1.1em;
}
#custom-text p .dark {
    color: #878787;
    font-weight: 600;
}
.page-content.page-cms {
    text-align: justify;
}
.page-content.page-cms .cms-box img {
    max-width: 100%;
}

@media (max-width: 1199px) {
    
}
@media (max-width: 991px) {
    #block-cmsinfo {
        padding: 1.25rem 1.875rem;
    }
    
}
@media (max-width: 767px){
    
}
#products {
    color: #808080;
}
#products .products-select {
    display: -webkit-box;
    display: -moz-box;
    display: box;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-align: baseline;
    -moz-box-align: baseline;
    box-align: baseline;
    -webkit-align-items: baseline;
    -moz-align-items: baseline;
    -ms-align-items: baseline;
    -o-align-items: baseline;
    align-items: baseline;
    -ms-flex-align: baseline;
    -webkit-box-pack: justify;
    -moz-box-pack: justify;
    box-pack: justify;
    -webkit-justify-content: space-between;
    -moz-justify-content: space-between;
    -ms-justify-content: space-between;
    -o-justify-content: space-between;
    justify-content: space-between;
    -ms-flex-pack: justify;
}
#products .up {
    margin-bottom: 1rem;
}
#products .up .btn-secondary,
#products .up .btn-tertiary {
    color: #878787;
    text-transform: inherit;
}
#products .up .btn-secondary .material-icons,
#products .up .btn-tertiary .material-icons {
    margin-right: 0;
}
.block-category {
    margin-bottom: 25px;
    background: none;
    box-shadow: none;
    border: none;
    padding: 0;
}
.block-category h1 {
    padding-bottom: 15px;
    border-bottom: 1px solid #ededed;
}
.block-category #category-description p {
    margin: 0 0 5px 0;
}
.block-category .category-cover {
    margin: 0 0 20px 0;
}
.block-category .category-cover img {
    max-width: 100%;
}
.products-selection .sort-by {
    padding-top: 0.4rem;
    white-space: nowrap;
    margin-right: -0.9375rem;
    margin-left: 0.9375rem;
    text-align: right;
    color: #262626;
}
.products-selection .total-products {
    padding: 2px;
    color: #262626;
}
.products-selection .total-products p {
    margin-bottom: 0px;
    padding-top: 5px;
}
.products-selection h1 {
    padding-top: 0.625rem;
}
.products-selection .display {
    float: left;
    margin: 0 15px 0 0;
}
.products-selection .display li {
    float: left;
    text-align: center;
    padding: 0 3px;
    border: 1px solid #dc3c08;
}
.products-selection .display li#grid {
    margin: 5px 5px 0 0px;
}
.products-selection .display li#grid a {
    background: url(../img/codezeel/sprite.png) no-repeat scroll -6px -33px transparent;
    height: 24px;
    float: left;
    width: 20px;
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
.products-selection .display li#grid:hover,
.products-selection .display li#grid.selected {
    border-color: #e86236;
}
.products-selection .display li#grid:hover a,
.products-selection .display li#grid.selected a {
    background-position: -6px -3px !important;
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
.products-selection .display li#list {
    margin: 5px 0 0 0;
}
.products-selection .display li#list a {
    background: url(../img/codezeel/sprite.png) no-repeat scroll -43px -33px transparent;
    height: 24px;
    width: 20px;
    float: right;
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
.products-selection .display li#list:hover,
.products-selection .display li#list.selected {
    border-color: #e86236;
}
.products-selection .display li#list:hover a,
.products-selection .display li#list.selected a {
    background-position: -43px -3px !important;
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
.products-selection .display li a {
    font-size: 0px;
    line-height: 14px;
    cursor: pointer;
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
.products-selection .display li.selected a {
    cursor: default;
}
.products-sort-order {
    color: #262626;
}
.products-sort-order .select-title {
    display: inline-block;
    margin: 0 0px 0 15px;
    width: 100%;
    color: #262626;
    background: #FFFFFF;
    padding: 2px 10px;
    cursor: pointer;
    border: 1px solid #ededed;
}
.products-sort-order .select-list {
    display: block;
    color: #262626;
    padding: 5px 15px;
    font-size: 13px;
}
.products-sort-order .select-list:hover {
    background: #e86236;
    color: #FFFFFF;
    text-decoration: none;
}
.products-sort-order .dropdown-menu {
    right: 0;
    left: auto;
    width: 90.8%;
    border: 1px solid #ededed;
    background: #FFFFFF;
    border-radius: 0;
    border-top: 0;
    margin-top: 0;
}
#search_filters .facet {
    padding-bottom: 0.625rem;
}
#search_filters .facet .collapse {
    display: block;
}
#search_filters .facet .facet-title {
    color: #000000;
    font-weight: 600;
    margin: 5px 0 5px;
    font-size: 14px;
    text-transform: capitalize;
    line-height: 20px;
}
#search_filters .facet .facet-label {
    margin-bottom: 0;
    font-weight: normal;
    text-align: left;

}
#search_filters .facet .facet-label a {
    margin-top: 5px;
    display: inline-block;
     line-height: 20px;
}
#search_filters .js-search-filters-clear-all {
    padding: 3px 8px;
    margin: 0px;
    font-weight: normal;
    width: 100%;
    text-align: left;
    font-size: 12px;
    background-color: #f0f0f0;
    border-color: #f0f0f0;
    color: #262626;
}
.pagination {
    width: 100%;
    border-top: 1px solid #ededed;
    padding: 15px 0px 10px;
}
.pagination > div:first-child {
    line-height: 2.5rem;
}
.pagination .page-list {
    margin-bottom: 0;
    float: right;
    margin-right: 0px;
}
.pagination .page-list li {
    background: #FFFFFF;
    display: inline-block;
    float: left;
    margin: 0 0 0 2px;
    padding: 0px;
}
.pagination a {
    height: 35px;
    margin: 0;
    padding: 4px 13px;
    width: 35px;
    color: #fff;
    background: #dc3c08;
    display: block;
    font-weight: 600;
    border: 1px solid #dc3c08;
    border-radius: 3px;
}
.pagination a .material-icons {
    font-size: 28px;
    margin-left: -4px;
}
.pagination a:hover {
    color: #FFFFFF;
    border-color: #e86236;
    background-color: #e86236;
    text-decoration: none;
}
.pagination .previous {
    padding: 4px 10px;
}
.pagination .next {
    padding: 4px 10px;
}
.pagination .disabled {
    cursor: not-allowed;
}
.pagination .current a {
    color: #FFFFFF;
    border-color: #e86236;
    background-color: #e86236;
    text-decoration: none;
}
.active_filters {
    background: #f6f6f6;
    padding: 10px 15px;
    margin-bottom: 10px;
}
.active_filters .active-filter-title {
    display: inline;
    margin-right: 10px;
    font-weight: 400;
}
.active_filters ul {
    display: inline;
}
.active_filters .filter-block {
    color: #262626;
    margin-right: 5px;
    margin-bottom: 2px;
    background: #FFFFFF;
    padding: 4px 10px;
    display: inline-block;
    font-size: 13px;
}
.active_filters .filter-block .close {
    color: #262626;
    font-size: 15px;
    opacity: 1;
    filter: alpha(opacity=100);
    margin-top: 6px;
    margin-left: 5px;
}
.block-categories .category-top-menu {
    margin-bottom: 0px;
}
.block-categories .category-sub-menu {
    margin-top: 0;
}

.block-categories .category-sub-menu li {
    position: relative;
    padding: 3px 0;
}
.block-categories .category-sub-menu li[data-depth="1"] {
    margin-left: 15px;
}
.block-categories .category-sub-menu li[data-depth="0"] > a {
    width: 100%;
    display: inline-block;
}
.block-categories .category-sub-menu li:not([data-depth="0"]):not([data-depth="1"]) {
    padding-left: 0.3125rem;
}
.block-categories .category-sub-menu li:not([data-depth="0"]):not([data-depth="1"])::before {
    content: "-";
    margin-right: 0.3125rem;
}
.block-categories .block_content .collapse-icons {
    position: absolute;
    right: 0;
    top: 5px;
    padding: 0;
    cursor: pointer;
    border: 0;
    width: auto;
    height: auto;
}
.block-categories .block_content .collapse-icons[aria-expanded="true"] .add {
    display: none;
}
.block-categories .block_content .collapse-icons[aria-expanded="true"] .remove {
    display: block;
}
.block-categories .block_content .collapse-icons .add {
    background: url(../img/codezeel/sprite.png) no-repeat scroll -4px -72px transparent;
    width: 16px;
    height: 16px;
    float: left;
}
.block-categories .block_content .collapse-icons .remove {
    background: url(../img/codezeel/sprite.png) no-repeat scroll -4px -101px transparent;
    width: 16px;
    height: 16px;
    float: left;
    display: none;
}
.block-categories .arrows .arrow-right,
.block-categories .arrows .arrow-down {
    font-size: 0.875rem;
    cursor: pointer;
    margin-left: 2px;
}
.block-categories .arrows .arrow-right:hover,
.block-categories .arrows .arrow-down:hover {
    color: #000;
}
.block-categories .arrows .arrow-down {
    display: none;
}
.block-categories .arrows[aria-expanded="true"] .arrow-right {
    display: none;
}
.block-categories .arrows[aria-expanded="true"] .arrow-down {
    display: inline-block;
}
.facets-title {
    color: #262626;
}
.products-selection .filter-button .btn-secondary,
.products-selection .filter-button .btn-tertiary {
    padding: 5px;
}
.layout-left-column #content-wrapper {
    padding-right: 0px;
}
.layout-right-column #content-wrapper {
    padding-left: 0px;
}
#left-column {
    padding-left: 0;
}
#right-column {
    padding-right: 0;
}
#left-column a,
#right-column a {
    color: #555555;
}
#left-column a:hover,
#right-column a:hover {
    color: #e86236;
}
#left-column .links,
#right-column .links {
    margin: 0 0 25px;
    background: #FFFFFF;
    border: 1px solid #ededed;
    width: 100%;
    float: none;
}
#left-column .links .wrapper,
#right-column .links .wrapper {
    width: 100%;
    float: none;
    padding: 0px;
}
#left-column .links .h3,
#right-column .links .h3 {
    color: #000000;
    text-transform: uppercase;
    padding: 0 0 13px;
    font-size: 18px;
    font-weight: 700;
    line-height: 24px;
    margin: 0 0 10px 0;
    border-bottom: 2px solid #888888;
}
#left-column .block,
#right-column .block {
    margin: 0 0 25px;
    background: #FFFFFF;
    padding: 5px;
    border: 1px solid #ededed;
    border-radius: 5px;
}
#left-column .block-categories .block .block_title {
    border-bottom: 2px solid #e86236;
}
#left-column .block .block_title,
#right-column .block .block_title {
    color: #000;
    text-transform: capitalize;
    padding: 10px 15px;
    font: 500 15px/24px 'Poppins', sans-serif;
    background: #f0f0f0;
    margin: 0;
    border-radius: 5px ;
    cursor: pointer;
}
#left-column .block-categories .block_title a,
#left-column .block-categories .block_title .collapse-icons i{
    color: #fff;
}
#left-column .block-categories.block .block_title, 
#right-column .block-categories.block .block_title {
    background: #e86236;
}
#left-column .block .block_content,
#right-column .block .block_content {
    padding: 15px 15px;
    background: #FFFFFF;
    border-radius: 0 0 5px 5px;
}
#left-column .block .block_content.collapse,
#right-column .block .block_content.collapse {
    display: inherit;
}
#left-column .block .block_content ul,
#right-column .block .block_content ul {
    margin-bottom: 0px;
}
#left-column .products-block ul li,
#right-column .products-block ul li {
    padding: 7px 0;
    margin: 0;
    overflow: hidden;
}
#left-column .products-block ul li .product_thumbnail,
#right-column .products-block ul li .product_thumbnail {
    float: left;
    margin-right: 10px;
    overflow: hidden;
}
#left-column .products-block ul li .product-info,
#right-column .products-block ul li .product-info {
    overflow: hidden;

}
#left-column .products-block ul li .product-title,
#right-column .products-block ul li .product-title {
    margin-bottom: 5px;
}
#left-column .products-block ul li .product-price-and-shipping,
#right-column .products-block ul li .product-price-and-shipping {
    margin-bottom: 2px;
}
#left-column .products-block .view_more,
#right-column .products-block .view_more {
    text-align: right;
    margin-top: 15px;
}
#left-column .products-block .view_more a,
#right-column .products-block .view_more a {
    color: #fff;
}
#left-column .products .product-miniature .discount-percentage, 
#left-column .products .product-miniature .discount-product,
#right-column .products .product-miniature .discount-percentage, 
#right-column .products .product-miniature .discount-product  {
    position: static;
}
/*** Responsive part ***/

@media (max-width: 1199px) and (min-width: 992px){

    #left-column .products-block ul li .product-info, #right-column .products-block ul li .product-info {
        display: block;
        width: 100%;
        padding-top: 10px;
    }
}
@media (max-width: 991px) {
    .products-sort-order .dropdown-menu {
        width: 92.75%;
    }
}
@media (max-width: 991px) {
    #left-column .block .block_content.collapse,
    #right-column .block .block_content.collapse {
        display: none;
    }
    #left-column .block .block_content.collapse.in,
    #right-column .block .block_content.collapse.in {
        display: block;
    }
    #left-column .block .block_title .collapse-icons .remove,
    #right-column .block .block_title .collapse-icons .remove {
        display: none;
    }
    #left-column .block .block_title[aria-expanded="true"] .collapse-icons .add,
    #right-column .block .block_title[aria-expanded="true"] .collapse-icons .add {
        display: none;
    }
    #left-column .block .block_title[aria-expanded="true"] .collapse-icons .remove,
    #right-column .block .block_title[aria-expanded="true"] .collapse-icons .remove {
        display: block;
    }
    #left-column .block .block_title .navbar-toggler,
    #right-column .block .block_title .navbar-toggler {
        display: inline-block;
        padding: 0;
        color: #000;
        width: auto;
    }
    #left-column .block .block_title .navbar-toggler .fa-icon:before,
    #right-column .block .block_title .navbar-toggler .fa-icon:before {
        line-height: 20px;
    }
    #category #left-column #search_filter_controls,
    #category #right-column #search_filter_controls {
        text-align: center;
        margin-bottom: 1rem;
    }
    #category #left-column #search_filter_controls button,
    #category #right-column #search_filter_controls button {
        margin: 0 5px;
    }
    #category #left-column #search_filters,
    #category #right-column #search_filters {
        margin-bottom: 0;
        box-shadow: none;
        padding: 0;
    }
    #category #left-column #search_filters .facet,
    #category #right-column #search_filters .facet {
        padding-top: 0;
        border-bottom: 1px solid #ededed;
        padding-top: 0;
        border-bottom: 1px solid #f6f6f6;
    }
    #category #left-column #search_filters .facet .title,
    #category #right-column #search_filters .facet .title {
        cursor: pointer;
        padding: 13px 0px;
    }
    #category #left-column #search_filters .facet .title .collapse-icons .remove,
    #category #right-column #search_filters .facet .title .collapse-icons .remove {
        display: none;
    }
    #category #left-column #search_filters .facet .title[aria-expanded="true"] .collapse-icons .add,
    #category #right-column #search_filters .facet .title[aria-expanded="true"] .collapse-icons .add {
        display: none;
    }
    #category #left-column #search_filters .facet .title[aria-expanded="true"] .collapse-icons .remove,
    #category #right-column #search_filters .facet .title[aria-expanded="true"] .collapse-icons .remove {
        display: block;
    }
    #category #left-column #search_filters .facet .facet-title,
    #category #right-column #search_filters .facet .facet-title {
        color: #262626;
        text-transform: uppercase;
    }
    #category #left-column #search_filters .facet .h6,
    #category #right-column #search_filters .facet .h6 {
        margin: 0;
        padding: 0;
        display: inline-block;
    }
    #category #left-column #search_filters .facet .navbar-toggler,
    #category #right-column #search_filters .facet .navbar-toggler {
        display: inline-block;
        padding: 0;
        width: auto;
    }
    #category #left-column #search_filters .facet .navbar-toggler .fa-icon:before,
    #category #right-column #search_filters .facet .navbar-toggler .fa-icon:before {
        font-size: 30px;
        line-height: 20px;
    }
    #category #left-column #search_filters .facet .collapse,
    #category #right-column #search_filters .facet .collapse {
        display: none;
    }
    #category #left-column #search_filters .facet .collapse.in,
    #category #right-column #search_filters .facet .collapse.in {
        display: block;
        margin-bottom: 10px;
    }
    #category #left-column #search_filters .facet .facet-label a,
    #category #right-column #search_filters .facet .facet-label a {
        margin-top: 0;
    }
    #category #left-column #search_filters .facet ul,
    #category #right-column #search_filters .facet ul {
        margin-bottom: 0;
    }
    #category #left-column #search_filters .facet ul li,
    #category #right-column #search_filters .facet ul li {
        padding: 4px 0;
    }
    #category #content-wrapper {
        width: 100%;
    }
    #category #search_filter_toggler {
        width: 100%;
        background-color: #dc3c08;
        color: #ffffff;
    }
    #category #search_filter_toggler:hover {
        background-color: #e86236;
    }
    .pagination .page-list {
        float: none;
    }
}
@media only screen and (min-width: 768px) and (max-width: 991px) {
    .filter-button {
        margin: 10px 0;
        min-width: 130px;
    }
}
@media (max-width: 767px) {
    .products-sort-order .select-title {
        margin-left: 0;
    }
    .products-selection h1 {
        padding-top: 0;
        text-align: center;
        margin-bottom: 1rem;
    }
    .products-selection .showing {
        padding-top: 1rem;
    }
    #prices-drop #content-wrapper,
    #new-products #content-wrapper,
    #best-sales #content-wrapper {
        width: 100%;
    }
    .products-sort-order .dropdown-menu {
        width: 96.3%;
    }
}
/**** Category page Subcategory ****/

.subcategory-heading {
    color: #000000;
    font-size: 14px;
    font-weight: 600;
    text-transform: capitalize;
}
#subcategories ul {
    margin: 0 0 15px 0;
    width: 100%;
    text-align: center;
}
#subcategories ul li {
    float: left;
    width: 162px;
    margin: 5px 10px 5px 0;
    text-align: center;
}
@media (max-width: 480px) {
    #subcategories ul li {
        float: none;
        display: inline-block;
    }
}
#subcategories ul li .subcategory-image {
    margin-bottom: 13px;
}
#subcategories ul li .subcategory-image a {
    display: block;
    padding: 5px;
    border: 1px solid #ededed;
    border-radius: 5px;
}
#subcategories ul li .subcategory-image a:hover {
    border-color: #e86236;
}
#subcategories ul li .subcategory-image a img {
    max-width: 100%;
    vertical-align: top;
    height: auto;
}
#subcategories ul li .subcategory-name {
    font-weight: normal;
    font-size: 14px;
}
#product #content {
    position: relative;
    max-width: 100%;
}
#product #content .product-leftside {
    position: relative;
}
.products .product-price-and-shipping,
.product-price {
    color: #000000;
    font-weight: 600;
    margin-bottom: 5px;
    font-size: 14px;
    letter-spacing: 0;
}
.product-price {
    display: inline-block;
}

.productpage_title,
.quickview h1.h1 {
    margin-top: 10px;
    text-transform: capitalize;
    font-weight: normal;
    color: #000;
    padding-bottom: 15px;
    margin-bottom: 15px;
    border-bottom: 1px solid #dfdfdf;
    font-size: 20px;
    line-height: 32px;
}
#product-description-short {
    
}
.product-information .manufacturer-logo {
    height: 35px;
}
.input-color {
    position: absolute;
    opacity: 0;
    filter: alpha(opacity=0);
    cursor: pointer;
    height: 1.25rem;
    width: 1.25rem;
}
.input-container {
    position: relative;
}
.input-radio {
    position: absolute;
    top: 0;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
    width: 100%;
    height: 100%;
}
.input-color:checked + span,
.input-color:hover + span,
.input-radio:checked + span,
.input-radio:hover + span {
    border: 2px solid #262626;
}
.radio-label {
    background: #FFFFFF;
    display: inline-block;
    padding: 0.125rem 0.625rem;
    font-weight: 600;
    border: 2px solid #FFFFFF;
}
.product-actions .control-label {
    width: 100%;
    margin-bottom: 5px;
    display: block;
    width: 100%;
    font-weight: 600;
    color: #262626;
}
.products .product_list li .btn.add-to-cart.view_page {
    background-position: center -159px;
}
.product-actions .add-to-cart {
    padding: 6px 25px;
}
.products .product_list li .btn.add-to-cart {
    background: #dc3c08 url(../img/codezeel/action.png) no-repeat scroll center 11px;
    border: 0;
    padding: 0;
    font-size: 0;
    height: 34px;
    width: 34px;
    display: inline-block;
    border-radius: 3px;
}
.products .product_list li .btn.add-to-cart:hover {
    background-color: #e86236;
}
.product-actions .add-to-cart .material-icons {
    line-height: inherit;
    display: none;
}
.product-quantity .wishlist {
    display: inline-block;
}
.product-quantity .wishlist a {
    font-size: 0;
    height: 44px;
    width: 44px;
    background: #dc3c08 url(../img/codezeel/action.png) no-repeat scroll center -96px;
    display: inline-block;
    vertical-align: top;
    margin-left: 5px;
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
     -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    -o-border-radius: 3px;
    border-radius: 3px;
}
.product-quantity .wishlist a:hover {
    background-color: #e86236;
}
.quickview .product-quantity .wishlist {
     display: none; 
}
.product-quantity .qty {
    float: left;
    width: 80px;
    margin-bottom: 0.5rem;
}
.product-quantity .add {
    float: left;
    margin-bottom: 0.5rem;
    margin-left: 10px;
}
.product-quantity .add .add-to-cart {
    padding: 10px 40px;
    color: #fff;
    background-color: #e86236;
    border-color: #e86236;
}
.product-quantity .add .add-to-cart:hover {
    color: #fff;
    background-color: #dc3c08;
    border-color: #dc3c08;
}
.product-quantity #quantity_wanted {
    color: #262626;
    background-color: #FFFFFF;
    height: 2.75rem;
    padding: 0.175rem 0.5rem;
    width: 3rem;
}
.product-quantity .input-group-btn-vertical {
    float: left;
}
.product-quantity .input-group-btn-vertical .btn {
    padding: 0.5rem 0.6875rem;
}
.product-quantity .input-group-btn-vertical .btn i {
    color: #262626;
    font-size: 1rem;
    top: 0.125rem;
    left: 0.1875rem;
}
.product-quantity .btn-touchspin {
    height: 1.4375rem;
}
.product-discounts {
    margin-bottom: 15px;
}
.product-discounts > .product-discounts-title {
    font-weight: normal;
    font-size: 0.875rem;
}
.product-discounts > .table-product-discounts thead tr th {
    width: 33%;
    padding: 0.625rem 1.25rem;
    background: #FFFFFF;
    border: 0.3125rem #ebebeb solid;
    text-align: center;
}
.product-discounts > .table-product-discounts tbody tr {
    background: #f6f6f6;
}
.product-discounts > .table-product-discounts tbody tr:nth-of-type(even) {
    background: #FFFFFF;
}
.product-discounts > .table-product-discounts tbody tr td {
    padding: 0.625rem 1.25rem;
    text-align: center;
    border: 0.3125rem #ebebeb solid;
}
.product-prices {
    margin-top: 0px;
    margin-bottom: 10px;
}
.product-prices div {
    margin-bottom: 6px;
    font-size: 22px;
    font-weight: 600;
    display: inline-block;
}
.product-prices .tax-shipping-delivery-label {
    font-size: 0.8125rem;
    color: #808080;
}
.product-discount {
    color: #878787;
}
.product-discount .regular-price {
    text-decoration: line-through;
    font-weight: normal;
    margin-right: 5px;
    font-size: 18px;
}
.has-discount.product-price,
.has-discount p {
    /* color: $brand-secondary;*/
}
.has-discount .discount {
    background: #f39d72;
    color: #FFFFFF;
    font-weight: normal;
    padding: 5px 11px;
    font-size: 14px;
    margin-left: 10px;
    text-transform: uppercase;
    display: inline-block;
}
.product-unit-price {
    font-size: 0.8125rem;
    margin-bottom: 0;
}
.tabs {
    margin-top: 10px;
}
.cz-hometabcontent .tabs {
    margin-top: 0;
}
.tabs .tab-pane {
    padding: 30px;
    background: #f8f8f8;
}
.tabs .nav-tabs {
    margin-bottom: 0;
    border: none;
    display: inline-block;
    width: auto;
    text-align: center;
    float: right;
}
.tabs .nav-tabs .nav-link {
    color: #000000;
    display: block;
    text-transform: capitalize;
    float: left;
    font-weight: 500;
    font-size: 14px;
    line-height: 22px;
    padding: 9px 20px;
    text-align: center;
    position: relative;
    letter-spacing: 0.5px;
    border: none;
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
}
.tabs .nav-tabs .nav-item:last-child .nav-link {
    
}
.clearfix::after {
    content: "";
    display: table;
    clear: both;
}

.tabs .nav-tabs .nav-link:hover, 
.tabs .nav-tabs .nav-link.active {
    color: #ffffff;
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
    background: #e86236;
    border-radius: 5px 5px 0 0;
    -webkit-border-radius: 5px 5px 0 0;
    -moz-border-radius: 5px 5px 0 0;
    -ms-border-radius: 5px 5px 0 0;
    -o-border-radius: 5px 5px 0 0;
}
.tabs .nav-tabs .nav-item {
        float: left;
    /* display: inline-block; */
    margin: 0 6px -10px 0;
}
.product-cover {
    margin-bottom: 15px;
    position: relative;

}
.product-cover img {
    background: #FFFFFF;
}
.product-cover .layer {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    padding: 14px;
    cursor: pointer;
    z-index: 1;
    opacity: 0;
    filter: alpha(opacity=0);
    background: rgba(255, 255, 255, 0.3);
    -webkit-transition: opacity 0.7s ease-in-out;
    -moz-transition: opacity 0.7s ease-in-out;
    -o-transition: opacity 0.7s ease-in-out;
    transition: opacity 0.7s ease-in-out;
}
.product-cover .layer:hover {
    opacity: 1;
    filter: alpha(opacity=100);
}
.product-cover .layer .zoom-in {
    color: #fff;
    background-color: #888888;
    position: absolute;
    bottom: 1px;
    right: 1px;
    padding: 10px 13px;
    height: auto;
    width: auto;
    font-size: 20px;
}
#product-modal .modal-content {
    background: transparent;
    border: none;
    padding: 0;
    background: #fff;
    border-radius: 0px;
}
#product-modal .modal-content .modal-body {
    display: -webkit-box;
    display: -moz-box;
    display: box;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    margin-left: 0;
}
#product-modal .modal-content .modal-body .product-cover-modal {
    background: #FFFFFF;
    width: 100%;
    height: auto;
}
#product-modal .modal-content .modal-body figure {
    margin-bottom: 0px;
}
#product-modal .modal-content .modal-body .image-caption {
    padding: 0.625rem 1.25rem;
}
#product-modal .modal-content .modal-body .image-caption p {
    margin-bottom: 0;
}
#product-modal .modal-content .modal-body .thumbnails {
    position: relative;
}
#product-modal .modal-content .modal-body .mask {
    position: relative;
    overflow: hidden;
    max-height: 710px;
    margin-top: 30px;
    z-index: 1;
}
#product-modal .modal-content .modal-body .mask.nomargin {
    margin-top: 0;
}
#product-modal .modal-content .modal-body .product-images {
    margin-left: 20px;
}
#product-modal .modal-content .modal-body .product-images img {
    width: 120px;
    cursor: pointer;
    background: #FFFFFF;
}
#product-modal .modal-content .modal-body .product-images img:hover {
    border: #e86236 1px solid;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
#product-modal .modal-content .modal-body .arrows {
    height: 100%;
    width: 100%;
    text-align: center;
    position: absolute;
    top: 0;
    color: #FFFFFF;
    right: 0px;
    z-index: 0;
    cursor: pointer;
}
#product-modal .modal-content .modal-body .arrows .arrow-up {
    position: absolute;
    top: -20px;
    right: 20px;
    opacity: 0.2;
    filter: alpha(opacity=20);
}
#product-modal .modal-content .modal-body .arrows .arrow-down {
    position: absolute;
    bottom: -20px;
    right: 20px;
}
#product-modal .modal-content .modal-body .arrows i {
    font-size: 75px;
    display: inline;
    color: #000;
}
#blockcart-modal .modal-header {
    color: #4cbb6c;
}
#blockcart-modal .modal-body {
    padding: 35px 25px;
}
#blockcart-modal .modal-body .divide-right span {
    display: inline-block;
    margin-bottom: 10px;
}
#blockcart-modal .modal-dialog {
    max-width: 1140px;
    width: 100%;
}
#blockcart-modal .product-image {
    width: 240px;
}
#blockcart-modal .modal-title {
    line-height: 24px;
    color: #4cbb6c;
    font-size: 13px;
    text-align: left !important;
    text-transform: uppercase;
    font-weight: 600;
}
#blockcart-modal .modal-title:before {
    content: "\F00C";
    font-family: "FontAwesome";
    font-size: 18px;
    color: #4cbb6c;
    padding-right: 5px;
    font-weight: normal;
}
#blockcart-modal .product-name {
    color: #262626;
    font-size: 16px;
    text-transform: capitalize;
    font-weight: normal;
}
#blockcart-modal .cart-products-count {
    font-size: 15px;
    font-weight: 600;
    text-transform: capitalize;
    color: #262626;
    padding-bottom: 13px;
    border-bottom: 1px solid #ededed;
    margin-bottom: 15px;
}
#blockcart-modal .cart-content {
    padding-left: 2.5rem;
}
#blockcart-modal .cart-content button {
    margin-right: 5px;
    background-color: #dc3c08;
    border-color: #dc3c08;
    color: #ffffff;
}
#blockcart-modal .cart-content button:hover {
    background-color: #e86236;
    border-color: #e86236;
}
#blockcart-modal .divide-right {
    border-right: 1px solid #ededed;
}
.product-images > li.thumb-container > .thumb {
    border: 1px solid transparent;
    cursor: pointer;
    margin: 0 0 10px;
    width: 95px;
}
.images-container .product-images > li.thumb-container > .thumb {
    margin: 0px;
}
.product-images > li.thumb-container > .thumb.selected,
.product-images > li.thumb-container > .thumb:hover {
    border: 1px solid #888888;
}
.images-container .product-images > li.thumb-container {
    display: inline;
    margin-right: 10px;
}
.images-container .js-qv-mask {
    margin: 0 30px;
    white-space: nowrap;
    position: relative;
}
.images-container .js-qv-mask .thumb-container .thumb {
    border: 1px solid transparent
    margin: 0 0 10px;
    width: 95px;
}
.images-container .js-qv-mask .thumb-container .thumb.selected,
.images-container .js-qv-mask .thumb-container .thumb:hover {
    border: 1px solid #e86236;
}
.images-container .js-qv-mask .customNavigation {
    top: 40px;
    width: 100%;
    right: 0 !important;
}
.images-container .js-qv-mask .customNavigation a:before {
    font-size: 18px;
}
.images-container .js-qv-mask .customNavigation a.prev {
    left: -30px;
}
.images-container .js-qv-mask .customNavigation a.next {
    right: -30px;
}
.scroll-box-arrows {
    display: none;
}
.scroll-box-arrows.scroll {
    display: block;
}
.scroll-box-arrows i {
    position: absolute;
    bottom: 40px;
    cursor: pointer;
    font-size: 40px;
    color: #000000;
    height: 35px;
    width: 35px;
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
}
.scroll-box-arrows i:hover {
    border-color: #888888;
    color: #888888;
}
.scroll-box-arrows .left {
    left: -10px;
}
.scroll-box-arrows .right {
    right: -5px;
}
.product-quantity {
    display: inline-block;
    margin-right: 5px;
    margin-bottom: 0px;
}
#product-availability,
.product-availability {
    display: inline-block;
    font-weight: normal;
    margin-left: 5px;
    color: #fff;
}
#product-availability .material-icons,
.product-availability .material-icons {
    display: none;
    line-height: inherit;
}
#product-availability .product-available,
.product-availability .product-available {
    background-color: #4cbb6c;
    padding: 3px 12px 5px;
}
#product-availability .product-unavailable,
.product-availability .product-unavailable {
    background-color: #ff9a52;
    padding: 3px 12px 5px;
}
#product-availability .product-last-items,
.product-availability .product-last-items {
    background-color: #ff9a52;
    padding: 3px 12px 5px;
}
#product-details .label {
    font-weight: 600;
    margin-right: 10px;
    margin-bottom: 12px;
    color: #262626;
}
.product-manufacturer {
    margin-bottom: 15px;
}
.product-features {
    margin-top: 5px;
    margin-left: 0;
}
.product-features h3 {
    font-weight: 600;
    margin-right: 10px;
    margin-bottom: 12px;
    color: #262626;
    font-size: 12px;
}
.product-features > dl.data-sheet dd.value,
.product-features > dl.data-sheet dt.name {
    width: 49%;
    display: inline-table;
    font-weight: normal;
    background: #fff;
    padding: 10px 20px;
    margin-right: 0px;
    min-height: 2.5rem;
    word-break: break-all;
    vertical-align: top;
    color: #262626;
}
.product-features > dl.data-sheet dd.value:nth-of-type(even),
.product-features > dl.data-sheet dt.name:nth-of-type(even) {
    background: #fff;
}
.product-features > dl.data-sheet dt.name {
    text-transform: capitalize;
}
.product-features > dl.data-sheet dd {
    margin-bottom: 3px;
}
.product-variants > .product-variants-item {
    margin: 10px 0;
}
.product-variants > .product-variants-item select {
    border: 1px solid #ebebeb;
    height: 2.5rem;
    width: auto;
    min-width: 75px;
    padding-left: 8px;
}
.product-variants > .product-variants-item ul li {
    margin-right: 0.625rem;
}
.product-variants > .product-variants-item .color {
    margin: 0;
    width: 20px;
    height: 20px;
}
.product-flags {
    margin: 0px;
}
ul.product-flags li {
    display: block;
    position: absolute;
    padding: 2px 5px 0;
    font-size: 11px;
    font-weight: 600;
    text-transform: capitalize;
    color: #000;
    z-index: 2;
    line-height: 18px;
}
ul.product-flags li.online-only {
    top: 25rem;
    right: 0;
    font-size: 0.8125rem;
}
ul.product-flags li.discount {
    display: none;
}
ul.product-flags li.on-sale {
    text-align: center;
    margin: 0;
    right: 15px;
    top: 15px;
    color: #e86236;
    border: 1px solid #e86236;
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}

ul.product-flags li.on-sale.discount-percentage {
    z-index: 2;
    color: #000000;
    vertical-align: top;
    display: inline;
    padding: 0px;
    font-size: 14px;
}
ul.product-flags li.new {
    color: #dc3c08;
    left: 15px;
    top: 15px;
    opacity: 0;
    filter: alpha(opacity=0);
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
    border: 1px solid #dc3c08;
}
.products .product_list li.item:hover ul.product-flags li.new, 
.products .product_list li.product_item:hover ul.product-flags li.new {
    opacity: 1;
    filter: alpha(opacity=100);
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
}
.product-customization {
    margin: 25px 0;
}
.product-customization .product-customization-item {
    margin: 15px 0;
}
.product-customization .product-message {
    background: #ebebeb;
    border: none;
    width: 100%;
    height: 3.125rem;
    resize: none;
    padding: 0.625rem;
}
.product-customization .product-message::-webkit-input-placeholder {
    color: #262626;
}
.product-customization .product-message::-moz-placeholder {
    color: #262626;
}
.product-customization .product-message:-moz-placeholder {
    color: #262626;
}
.product-customization .product-message:-ms-input-placeholder {
    color: #262626;
}
.product-customization .file-input {
    width: 100%;
    opacity: 0;
    filter: alpha(opacity=0);
    left: 0;
    z-index: 1;
    cursor: pointer;
    height: 2.625rem;
    overflow: hidden;
    position: absolute;
}
.product-customization .custom-file {
    position: relative;
    background: #ebebeb;
    width: 100%;
    height: 2.625rem;
    line-height: 2.625rem;
    text-indent: 0.625rem;
    display: block;
    color: #878787;
    margin-top: 1.25rem;
}
.product-customization .custom-file button {
    z-index: 0;
    position: absolute;
    right: 0;
    top: 0;
}
.product-customization small {
    color: #878787;
}
.product-pack {
    margin-top: 2.5rem;
}
.product-pack .pack-product-container {
    display: -webkit-box;
    display: -moz-box;
    display: box;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    justify-content: space-around;
}
.product-pack .pack-product-container .pack-product-name {
    -webkit-box-flex: 0;
    -moz-box-flex: 0;
    box-flex: 0;
    -webkit-flex: 0 0 50%;
    -moz-flex: 0 0 50%;
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    font-size: 0.875rem;
    color: #878787;
}
.product-pack .pack-product-container .pack-product-quantity {
    border-left: #ebebeb 2px solid;
    padding-left: 0.625rem;
}
.product-pack .pack-product-container .pack-product-name,
.product-pack .pack-product-container .pack-product-price,
.product-pack .pack-product-container .pack-product-quantity {
    display: -webkit-box;
    display: -moz-box;
    display: box;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    align-items: center;
}
.product-refresh {
    margin-top: 1.25rem;
}
.social-sharing {
    width: 100%;
    clear: both;
    margin-top: 10px;
}
.social-sharing > span {
    display: none;
}
.social-sharing ul {
    margin: 0;
}
.social-sharing ul li {
    height: 35px;
    width: 60px;
    display: inline-block;
    background-color: #FFFFFF;
    cursor: pointer;
    margin-right: 4px;
    border: 1px solid;
    -webkit-transition: all 0.2s ease-in;
    -moz-transition: all 0.2s ease-in;
    -o-transition: all 0.2s ease-in;
    transition: all 0.2s ease-in;
}
.social-sharing ul li a {
    display: block;
    width: 100%;
    height: 100%;
    white-space: nowrap;
    text-align: center;
    line-height: 35px;
    overflow: hidden;
}
.social-sharing ul li a:hover {
    color: transparent;
}
.social-sharing ul li a:before {
    font-size: 18px;
    font-family: "FontAwesome";
    display: inline-block;
}
.social-sharing ul li.facebook {
    border-color: #435f9f;
}
.social-sharing ul li.facebook a:before {
    content: "\F09A";
    color: #435f9f;
}
.social-sharing ul li.facebook:hover {
    background-color: #435f9f;
}
.social-sharing ul li.facebook:hover a:before {
    color: #ffffff;
}
.social-sharing ul li.twitter {
    border-color: #00aaf0;
}
.social-sharing ul li.twitter a:before {
    content: "\F099";
    color: #00aaf0;
}
.social-sharing ul li.twitter:hover {
    background-color: #00aaf0;
}
.social-sharing ul li.twitter:hover a:before {
    color: #ffffff;
}
.social-sharing ul li.googleplus {
    border-color: #e04b34;
}
.social-sharing ul li.googleplus a:before {
    content: "\F0D5";
    color: #e04b34;
}
.social-sharing ul li.googleplus:hover {
    background-color: #e04b34;
}
.social-sharing ul li.googleplus:hover a:before {
    color: #ffffff;
}
.social-sharing ul li.pinterest {
    border-color: #ce1f21;
}
.social-sharing ul li.pinterest a:before {
    content: "\F0D2";
    color: #ce1f21;
}
.social-sharing ul li.pinterest:hover {
    background-color: #ce1f21;
}
.social-sharing ul li.pinterest:hover a:before {
    color: #ffffff;
}
.products-selection {
    margin-bottom: 10px;
    display: inline-block;
    width: 100%;
    padding: 10px 0px 15px 0px;
    border-bottom: 1px solid #ededed;
}
.products-selection .title {
    color: #808080;
}
@media (max-width: 991px) {
    .product-cover img {
        width: 100%;
    }
    #product-modal .modal-content .modal-body {
        -webkit-box-orient: vertical;
        -moz-box-orient: vertical;
        box-orient: vertical;
        -webkit-box-direction: normal;
        -moz-box-direction: normal;
        box-direction: normal;
        -webkit-flex-direction: column;
        -moz-flex-direction: column;
        flex-direction: column;
        -ms-flex-direction: column;
        margin-left: 0;
    }
    #product-modal .modal-content .modal-body img.product-cover-modal {
        width: 100%;
    }
    #product-modal .modal-content .modal-body .arrows {
        display: none;
    }
    #product-modal .modal-content .modal-body .mask {
        margin-top: 20px;
    }
    #product-modal .modal-content .modal-body .product-images > li.thumb-container {
        display: inline-block;
    }
    #product-modal .modal-content .modal-body .image-caption {
        width: 100%;
    }
    #blockcart-modal .modal-dialog {
        width: calc(100% - 20px);
    }
    #blockcart-modal .modal-body {
        padding: 1.875rem;
    }
    .social-sharing {
        padding: 15px 0;
        text-align: left;
    }
    .product-quantity .add {
        margin-left: 0px;
    }
    .cz-hometabcontent .tabs { margin: 0px; }
    .cz-hometabcontent .tabs .nav-tabs { margin-bottom: 10px; }
}
@media (max-width: 767px) {
    #blockcart-modal .divide-right {
        border-right: none;
    }
    #blockcart-modal .modal-body {
        padding: 1rem;
    }
    #product .page-content .product-leftside {
        width: 280px;
        margin: 0 auto 25px;
        clear: both;
        overflow: hidden;
        height: 100%;
    }
}
@media (max-width: 575px) {
    .tabs .nav-tabs .nav-item a {
        width: 100%;
        padding-left: 15px;
        padding-right: 15px;
    }
    .tabs .nav-tabs .nav-link:before{
        display: none;
    }
    
    .products-section-title:after{
        display: none;
    }
    .products .product_list li .product-miniature {
    max-width: 277px;
    }
    .special-products .products .product_list li .product-miniature {
    float: none;
    max-width: 100%;
    
}
    .special-products .products .comments_note div.star {
    float: none;
}
    .special-products .products .product-description a {
    text-align: center;
}
    .special-products .products .thumbnail-container {
    float: none;
    width: 100%;
}
    .special-products .products .product-description {
    width: 100%;
    float: none;
    text-align: center;
    padding-top: 10px;
    padding-left: 0;
    padding-right: 0;
}
    
}
@media (max-width: 479px) {
    .tabs .nav-tabs .nav-item {
        width: 100%;
        margin: 0;
        margin-bottom: 5px;
    }
    .products .customNavigation {
        left: 0;
        right: 0;
        margin: 0 auto;
        top: -30px;
    }
    .products-section-title {
        text-align: center;
        border: 0;
    }
    
}
@media (max-width: 380px) {
    #product .page-content .product-leftside {
        width: auto;
    }
}
#product-modal .modal-dialog {
    max-width: 800px;
}
.cart-grid {
    margin-bottom: 2rem;
}
.cart-items {
    margin-bottom: 0;
}
.cart-item {
    border-bottom: 1px solid #ededed;
    padding: 1rem 0;
}
.cart-item:last-child {
    border-bottom: 0px;
}
.cart-summary-line {
    clear: both;
    margin-top: 10px;
}
.cart-summary-line::after {
    content: "";
    display: table;
    clear: both;
}
.cart-summary-line .label {
    padding-left: 0;
    white-space: inherit;
}
.cart-summary-line .value {
    color: #262626;
    float: right !important;
    font-size: 16px;
    font-weight: 600;
}
/** CART BODY **/

.cart-grid-body {
    margin-bottom: 0.75rem;
}
.cart-grid-body a.label:hover {
    color: #e86236;
}
.cart-grid-body .card-block {
    padding: 1rem;
}
.cart-grid-body .card-block h1 {
    margin-bottom: 0;
    font-size: 16px;
    font-weight: 600;
}
.cart-grid-body hr {
    margin: 0;
}
.cart-grid-body .cart-overview {
    padding: 1rem;
}
/** CART RIGHT **/

.cart-grid-right hr {
    margin: 0;
}
.cart-grid-right .promo-discounts {
    margin-bottom: 0;
}
.cart-grid-right .promo-discounts .cart-summary-line .label {
    color: #808080;
}
.cart-grid-right .promo-discounts .cart-summary-line .label .code {
    text-decoration: underline;
    cursor: pointer;
}
.block-promo .promo-code {
    padding: 1.60rem;
    background: #ebebeb;
}
.block-promo .promo-code .alert-danger {
    position: relative;
    margin-top: 1.25rem;
    background: #ff4c4c;
    color: #FFFFFF;
    display: none;
}
.block-promo .promo-code .alert-danger::after {
    bottom: 100%;
    left: 10%;
    border: solid transparent;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
    border-bottom-color: #ff4c4c;
    border-width: 10px;
    margin-left: -10px;
}
.block-promo .promo-input {
    color: #262626;
    border: #878787 1px solid;
    height: 2.5rem;
    text-indent: 0.625rem;
    width: 60%;
}
.block-promo .promo-input + button {
    margin-top: -4px;
    text-transform: capitalize;
}
.block-promo .cart-summary-line .label,
.block-promo .promo-name {
    color: #ff9a52;
    font-weight: 600;
}
.block-promo .cart-summary-line .label a,
.block-promo .promo-name a {
    font-weight: normal;
    color: #262626;
    display: inline-block;
}
.block-promo .promo-code-button {
    padding-left: 1.25rem;
    margin-bottom: 1.25rem;
    display: inline-block;
}
.block-promo.promo-highlighted {
    padding: 1.25rem;
    padding-bottom: 0;
    margin-bottom: 0;
}
/** CONTENT LEFT **/

.product-line-grid-left img {
    max-width: 100%;
}
/** CONTENT BODY **/

.product-line-grid-body > .product-line-info > .label {
    padding: 0;
    line-height: inherit;
    text-align: left;
    white-space: inherit;
    font-weight: normal;
}
.product-line-grid-body > .product-line-info > .out-of-stock {
    color: red;
}
.product-line-grid-body > .product-line-info > .available {
    color: #4cbb6c;
}
.product-line-grid-body > .product-line-info > .unit-price-cart {
    padding-left: 0.3125rem;
    font-size: 0.875rem;
    color: #808080;
}
/** CONTENT LEFT **/

.product-line-grid-right .bootstrap-touchspin {
    width: 4.25rem;
}
.product-line-grid-right .bootstrap-touchspin > .form-control,
.product-line-grid-right .bootstrap-touchspin > .input-group {
    color: #262626;
    background-color: #FFFFFF;
    height: 2.5rem;
    padding: 0.175rem 0.5rem;
    width: 3rem;
}
.product-line-grid-right .bootstrap-touchspin > .input-group-btn-vertical {
    width: auto;
}
.product-line-grid-right .cart-line-product-actions,
.product-line-grid-right .product-price {
    color: #262626;
    line-height: 36px;
}
.product-line-grid-right .cart-line-product-actions .remove-from-cart,
.product-line-grid-right .product-price .remove-from-cart {
    color: #262626;
    display: inline-block;
    margin-top: 0.3125rem;
}
.product-line-grid-right .cart-line-product-actions strong,
.product-line-grid-right .product-price strong {
    font-size: 18px;
    font-weight: 600;
}
/*** Responsive part ***/

@media (max-width: 767px) {
    .product-line-grid-body {
        margin-bottom: 1rem;
    }
}
@media (max-width: 575px) {
    .cart-items {
        padding: 1rem 0;
    }
    .cart-item {
        border-bottom: 1px solid #ebebeb;
    }
    .cart-item:last-child {
        border-bottom: 0;
    }
    .cart-grid-body .cart-overview {
        padding: 0;
    }
    .cart-grid-body .no-items {
        padding: 1rem;
        display: inline-block;
    }
    .product-line-grid-left {
        padding-right: 0 !important;
    }
}
@media (max-width: 360px) {
    .product-line-grid-right .qty {
        width: 100%;
    }
    .product-line-grid-right .price {
        width: 100%;
    }
}
#block-reassurance {
    margin-top: 20px;
}
#block-reassurance img {
    opacity: 0.7;
    filter: alpha(opacity=70);
    width: 1.5625rem;
    margin-right: 10px;
}
#block-reassurance ul {
    margin-bottom: 0px;
}
#block-reassurance ul li {
    margin-bottom: 5px;
    color: #000;
}
#block-reassurance ul li .block-reassurance-item {
    padding: 12px 15px;
    margin-bottom: 0;
    border-left: 3px solid #e86236;
    background: #f6f6f6;
}
#block-reassurance span {
    font-weight: 400;
    font-size: 13px;
}
.quickview .modal-dialog {
    width: calc(100% - 30px);
    max-width: 64rem;
}
.quickview .modal-content {
    min-height: 28.125rem;
}
.quickview .modal-header {
    border: none;
    padding: 0;
    position: relative;
    z-index: 9;
}
.quickview .modal-header .close {
    margin-top: 8px;
}
.quickview .modal-body {
    min-height: 23.75rem;
}
.quickview .modal-footer {
    border-top: 1px solid #ededed;
    padding: 20px 0 10px;
    text-align: left;
    margin-top: 15px;
}
.quickview .layer {
    display: none;
}
.quickview .product-cover img {
    width: 95%;
}
.quickview .images-container {
    z-index: 1;
    min-height: 21.875rem;
}
.quickview .images-container .product-images > li.thumb-container {
    display: inline-block;
}
.quickview .images-container .product-images > li.thumb-container > .thumb {
    margin-bottom: 0.8125rem;
    background: #FFFFFF;
}
.quickview .mask {
    position: relative;
}
.quickview .arrows {
    position: absolute;
    top: 0;
    height: 22.5rem;
    right: 5rem;
    z-index: 0;
}
.quickview .arrows .arrow-up {
    margin-top: -3.125rem;
    cursor: pointer;
    opacity: 0.2;
    filter: alpha(opacity=20);
}
.quickview .arrows .arrow-down {
    position: absolute;
    bottom: -1.875rem;
    cursor: pointer;
}
.quickview .social-sharing {
    margin-top: 0;
    margin-left: 0;
}
#stores .page-stores {
    margin: 0 auto;
}
#stores .page-stores .store-item {
    padding-left: 0.75rem;
    padding-right: 0.75rem;
}
#stores .page-stores .store-picture img {
    max-width: 100%;
}
#stores .page-stores .store-item-container {
    display: -webkit-box;
    display: -moz-box;
    display: box;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-pack: distribute;
    -moz-box-pack: distribute;
    box-pack: distribute;
    -webkit-justify-content: space-around;
    -moz-justify-content: space-around;
    -ms-justify-content: space-around;
    -o-justify-content: space-around;
    justify-content: space-around;
    -ms-flex-pack: distribute;
    -webkit-box-align: center;
    -moz-box-align: center;
    box-align: center;
    -webkit-align-items: center;
    -moz-align-items: center;
    -ms-align-items: center;
    -o-align-items: center;
    align-items: center;
    -ms-flex-align: center;
    padding: 1.875rem 0;
}
#stores .page-stores .store-item-container ul {
    margin-bottom: 0;
    font-size: 0.9375rem;
}
#stores .page-stores .store-item-container .divide-left {
    border-left: #ebebeb 1px solid;
}
#stores .page-stores .store-item-container .divide-left tr {
    height: 1.5625rem;
}
#stores .page-stores .store-item-container .divide-left td {
    padding-left: 0.375rem;
}
#stores .page-stores .store-item-container .divide-left th {
    text-align: right;
}
#stores .page-stores .store-item-container .store-description {
    font-size: 14px;
}
#stores .page-stores .store-item-footer {
    margin-top: 0.5rem;
    padding-top: 0.5rem;
    display: -webkit-box;
    display: -moz-box;
    display: box;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    -js-display: flex;
    display: flex;
    -webkit-box-pack: distribute;
    -moz-box-pack: distribute;
    box-pack: distribute;
    -webkit-justify-content: space-around;
    -moz-justify-content: space-around;
    -ms-justify-content: space-around;
    -o-justify-content: space-around;
    justify-content: space-around;
    -ms-flex-pack: distribute;
}
#stores .page-stores .store-item-footer.divide-top {
    border-top: #ebebeb 1px solid;
}
#stores .page-stores .store-item-footer div:first-child {
    -webkit-box-flex: 0;
    -moz-box-flex: 0;
    box-flex: 0;
    -webkit-flex: 0 0 65%;
    -moz-flex: 0 0 65%;
    -ms-flex: 0 0 65%;
    flex: 0 0 65%;
}
#stores .page-stores .store-item-footer i.material-icons {
    margin-right: 0.625rem;
    color: #878787;
    font-size: 14px;
}
#stores .page-stores .store-item-footer li {
    margin-bottom: 0.625rem;
}
/*** Responsive part ***/

@media (max-width: 767px) {
    #stores .page-stores {
        width: 100%;
    }
    #stores .page-stores .store-item-container {
        padding: 1rem 0;
    }
}
@media (max-width: 575px) {
    #stores .page-stores .store-item-container {
        display: block;
    }
    #stores .page-stores .store-item-container .divide-left {
        border-left: none;
    }
    #stores .page-stores .store-item-container .store-description a {
        margin-bottom: 0.5rem;
    }
    #stores .page-stores .store-item-container .store-description address {
        margin-bottom: 0.5rem;
    }
    #stores .page-stores .store-item-footer {
        display: block;
    }
    #stores .page-stores .store-item-footer.divide-top {
        border-top: #ebebeb 1px solid;
    }
    #stores .page-stores .store-item-footer li {
        margin-bottom: 0.625rem;
    }
    #stores .page-stores .store-item-footer .card-block {
        padding: 0.75rem 0.75rem 0;
    }
}
.cz_newsletterdiv {
    display: inline-block;
    width: 67%;
    padding: 25px 115px 33px;
    background-color: #143848;
    margin-top: 0px;
}
.cz_newsletterdiv h4.sub_heading {
    height: 35px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    width: auto;
    margin-bottom: 10px;
}
.block_newsletter {
    margin-bottom: 0.625rem;
}
.block_newsletter form {
    position: relative;
}
.block_newsletter form input[type=text] {
    padding: 9px 50px 9px 9px;
    min-width: 250px;
    outline: 0;
    border: 1px solid #ddd;
    color: #262626;
    font-size: 12px;
    font-weight: 300;
}
.block_newsletter form input[type=text]:focus {
    border-color: #aaa;
    background-color: #FFFFFF;
    color: #262626;
}
.block_newsletter form button[type=submit] {
    position: absolute;
    background: #888888;
    border: none;
    top: 0px;
    right: 0;
    padding: 10px 15px 9px;
    outline: 0;
    cursor: pointer;
}
.block_newsletter form button[type=submit]:before {
    content: "\F002";
    display: block;
    font-family: "FontAwesome";
    font-size: 16px;
    padding: 0;
    width: 100%;
    text-align: center;
    color: #fff;
}
.block_newsletter form button[type=submit]:hover {
    background: #34373c;
    color: #fff;
}
.block_newsletter .title {
    color: #fff;
    font-size: 20px;
    font-weight: 700;
    line-height: 45px;
    /*margin-bottom: 10px;*/
    text-transform: uppercase;
}
.block_newsletter p.alert {
    margin-top: 3px;
    margin-bottom: 0;
    position: absolute;
    width: 100%;
    z-index: 9;
    background: none;
    border: none;
    color: #e86236;
    padding: 0;
    text-transform: capitalize;
}

.block_newsletter form .col-xs-12 {
    padding-left: 0px;
}
.block_newsletter form .input-wrapper {
    overflow: hidden;
}
.block_newsletter form input[type=text] {
    border: none;
    min-width: 255px;
    padding: 10px 15px 10px 15px;
    width: 100%;
}
.block_newsletter form input[type=text]:focus {
    color: #262626;
}
.block_newsletter form input {
    height: 42px;
    padding: 7px 26px;
    outline: 0;
    font-size: 12px;
    font-weight: 500;
    letter-spacing: .7px;
    border-radius: 3px 0 0 3px;
}
.block_newsletter form input.btn {
    float: right;
    margin-left: 0px;
    background: #e86236;
    color: #fff;
    border: 0;
    text-transform: uppercase;
    font-weight: 600;
    border-radius: 0 3px 3px 0;
}
.block_newsletter form input.btn:hover {
    color: #000000;
}
.block_newsletter form .newsletter-message {
    display: none;
}
.block-contact li {
    overflow: hidden;
    line-height: 26px;
}
.block-contact li i {
    color: #e86236;
    float: left;
    font-size: 18px;
    height: 25px;
    margin-top: 5px;
    width: 25px;
}
.block-contact li i.fa-envelope-o {
    font-size: 16px;
}
#contact-rich .block_content .icon {
    float: left;
    padding-top: 6px;
}
#contact-rich .block_content .icon i {
    font-size: 24px;
}
#contact-rich .block_content .icon i.fa-envelope-o {
    font-size: 20px;
}
#contact-rich .block_content .data {
    margin-left: 30px;
}
.linklist .blockcms-title a {
    color: #262626;
}
.account-list a {
    color: #878787;
}
.account-list a:hover {
    color: #000;
}
.blockcms-title,
.myaccount-title,
.myaccount-title a,
.block-contact-title,
.footer-container h3.h3 {
    font-weight: 600;
    font-size: 16px;
    margin-bottom: 10px;
    color: #ffffff;
    line-height: 26px;
}
.block-social {
    position: absolute;
    float: left;
    padding: 0 15px;
    left: 0;
    right: auto;
    top: auto;
    bottom: -5px;
}

.block-social ul {
    margin-bottom: 0px;
}
.footer-container .block-social li {
    text-align: center;
    display: inline-block;
    cursor: pointer;
    margin-right: 20px;
    margin-bottom: 0;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}
.block-social li:hover a:before {
    color: #FFF;
}
.footer-container .block-social li a {
    display: block;
    height: 100%;
    white-space: nowrap;
    overflow: hidden;
    padding: 0;
}
.footer-container .block-social li a:after{
    display: none;
}
.footer-container .block-social li a:hover {
    text-decoration: none;
}
.block-social li a:before {
    font-size: 14px;
    font-family: "FontAwesome";
    display: inline-block;
    font-weight: normal;
    padding-right: 10px;
    color: #d9d9d9;
}
.block-social li:hover a:before {
    color: #e86236;
}
.facebook a:before {
    content: "\F09A";
}
.twitter a:before {
    content: "\F099";
}
.rss a:before {
    content: "\F09E";
}
.youtube a:before {
    content: "\F16A";
}
.googleplus a:before {
    content: "\F0D5";
}
.pinterest a:before {
    content: "\F231";
}
.vimeo a:before {
    content: "\F27D";
}
.instagram a:before {
    content: "\F16D";
}
#footer {
    
}
#footer .block_newsletter {
    margin-bottom: 0px;
    padding: 20px 30px;
    background: #dc3c08;
    float: left;
    width: 100%;
}
#footer .block_newsletter .title {
   display: inline-block;
}
#footer .block_newsletter .block_content {
   width: 47.2%;
    display: inline-block;
   padding: 0;
    margin-left: 85px;
}
.footer-container {
    padding: 65px 30px 59px;
    overflow: hidden;
    color: #c6c6c6;
    background: #46702c;
    margin: 0 -30px;
    
}
#footer .block_newsletter .newsletter-title {
    float: left;
    color: #f7f7f7;
    font-size: 22px;
    text-transform: capitalize;
   display: inline-block;
    font-weight: 600;
    letter-spacing: .4px;
    line-height: 28px;
    text-align: center;
    position: relative;
    padding: 17px 0 6px 60px;
    background: url(../img/codezeel/newsletter.png) no-repeat scroll 0 8px transparent;
}

.footer-container .footer {
    position: relative;
}
.footer-container li {
    display: block;
    text-align: left;
    margin-bottom: 5px;
    color: #d9d9d9;
    line-height: 32px;
}
.footer-container li a {
    color: #d9d9d9;
    cursor: pointer;
    letter-spacing: .6px;
    position: relative;
     padding: 0 0 0 20px;
}
.footer-container li a:after {
    cursor: pointer;
    padding-top: 0;
    font-family: 'FontAwesome';
    content: '\f105';
    font-size: 12px;
    position: absolute;
    right: auto;
    top: -6px;
    left: 0;
    bottom: auto;
}

.footer-container li a:hover {
    color: #e86236;
    text-decoration: underline;
}

/*.facebook:hover {
    background-color: #435f9f;
}
.twitter:hover {
    background-color: #00aced;
}
.youtube:hover {
    background-color: #b00;
}
.googleplus:hover {
    background-color: #dd4b39;
}
.instagram:hover {
    background-color: #fb3958;
}
.pinterest:hover {
    background-color: #dd4b39;
}*/
.footer-container .links {
    width: 21.33%;
    
}
.footer-container .links .h3,
.footer-container .links h3,
.footer-container .links h3 a {
    color: #ffffff;
    font: 600 16px/26px 'Poppins', sans-serif;
    text-transform: capitalize !important;
    margin: 0 0 10px 0;
    cursor: pointer;
    letter-spacing: 0.5px;
}

.footer-container .links .collapse {
    display: inherit;
    margin-bottom: 0;
}
.footer-container .links.block-contact {
    width: 24%;
}
.copyright ._blank:hover {
    color: #e86236;
}
.footer-container #czfootercmsblock {
    width: 36%;
    
}
.footer-container #czfootercmsblock .footertitle {
    color: #ffffff;
    font: 600 16px/26px 'Poppins', sans-serif;
    text-transform: capitalize !important;
    margin: 0 0 10px 0;
    cursor: pointer;
    letter-spacing: 0.5px;
}
.footer-container #czfootercmsblock li {
    margin: 0;
}
.footer-container #czfootercmsblock .footerlogo {
    margin-bottom: 20px;
    background: transparent url(../img/codezeel/footer-logo.png) no-repeat scroll 0 0;
    margin-bottom: 25px;
    max-width: 314px;
    min-height: 65px;
}
.footer-after {
    min-height: 36px;
    color: #fff;
    padding: 15px 0 15px;
    border-top: 1px solid #6b8d56;
    background-color: #46702c;
    margin: 0 -30px;
}
.footer-after a {
    color: #d9d9d9;
}
#footer .footer-after > .container {
    padding: 0 30px;
}
.footer-after .copyright {
    padding: 0;
    float: none;
    text-align: left;
}
.footer-after .payement_logo_block {
    padding: 0;
    float: right;
}
.footer-after .payement_logo_block a img {
    margin: 0 2px 0px 0;
    /*filter: grayscale(100%);
    -webkit-filter: grayscale(100%);*/
    vertical-align: text-top;
}
.footer-after .payement_logo_block a img:hover {
    filter: grayscale(0);
    -webkit-filter: grayscale(0);
}

@media (max-width: 1289px) {
    
    #footer .block_newsletter .newsletter-title {
        font-size: 20px;
    }
    #footer .block_newsletter .block_content {
        margin-left: 80px;
    }
    .footer-container .block-social li {
        margin-right: 15px;
    }
}
@media (max-width: 1199px) {
    
    #footer .block_newsletter .block_content {
        margin-left: 34px;
    }
    #footer .block_newsletter .newsletter-title {
        font-size: 17px;
    }
    .footer-container .block-social li {
        margin-right: 5px;
    }
    .block-social li a:before {
        padding-right: 5px;
    }
    .footer-container {
        padding-top: 40px;
        padding-bottom: 40px;
    }
}
@media (max-width: 991px) {
    #footer .block_newsletter .title {
    width: 100%;
    float: none;
    text-align: center;
    }
    .footer-container .block-social li {
        margin-right: 20px;
    }
    #footer .block_newsletter {
        padding: 0 25px;
    }
    #footer .footer-before {
        margin: 0 -25px;
    }
    #footer .block_newsletter .block_content {
        width: 100%;
        padding: 10px 0 20px;
        margin:0;
    }
    .footer-container {
        box-shadow: none;
        margin-top: 0;
        padding: 25px 25px;
        margin: 0 -25px;
    }
    .footer-container #czfootercmsblock {
        width: 100%;
    }
    .footer-container .links {
        width: 100%;
        margin: 0px;
    }
    .footer-container .links.block-contact,
    .footer-container .links .h3 {
        width: 100%;
    
    }
    .footer-container .links .title {
        padding: 7px 0;
        margin-bottom: 0px;
        cursor: pointer;
    }
    .footer-container .links .collapse {
    display: none;
}
    .footer-container .links .collapse.in {
    display: block;
}
    .footer-container #czfootercmsblock .footertitle {
    display: none !important;
}
    .footer-container .links .title .collapse-icons .remove {
        display: none;
    }
    .footer-container .links .title[aria-expanded="true"] .collapse-icons .add {
        display: none;
    }
    .footer-container .links .title[aria-expanded="true"] .collapse-icons .remove {
        display: block;
    }
    .footer-container .links .navbar-toggler {
        display: inline-block;
        padding: 0;
        width: auto;
    }
    .footer-container .links .navbar-toggler .fa-icon:before {
        font-size: 24px;
        line-height: 20px;
        color: #ffffff;
    }
    .footer-container li {
    margin-bottom: 0;
}
    .footer-container .links .collapse.in {
        display: block;
    }
    .footer-container .links ul {
        margin-bottom: 0;
        padding: 0px 0;
    }
    .footer-container .col-md-6 {
        width: 100%;
    }
    .footer-after {
        padding: 12px 0 12px;
        margin: 0 -25px;
    }
    .footer-after .copyright {
        text-align: center;
        float: none;
    }
    .footer-after .payement_logo_block {
        text-align: center;
        padding-top: 0px;
        float: none;
        margin-bottom: 5px;
    }
    .footer-container .hidden-sm-down,
    #search_filters .facet .facet-title.hidden-sm-down {
        display: none !important;
    }
    .footer-container .title.hidden-md-up,
    #search_filters .facet .facet-title.hidden-md-up {
        display: block !important;
    }
    .block-social {
        bottom: 0px;
        text-align: left;
        clear: both;
        display: inline-block;
        width: 100%;
        position: static;
        padding-top: 0;
    }
    .footer-container .block-social h3 {
        margin-bottom: 0;
        padding: 5px 0;
    }
    #content-wrapper,
    .layout-left-column #content-wrapper,
    .layout-right-column #content-wrapper,
    #left-column,
    #right-column {
        width: 100% !important;
        padding-left: 0px;
        padding-right: 0px;
    }
}
@media (max-width: 767px) {
    .footer-container .footer {
        padding-bottom: 0px;
    }
    #footer .block_newsletter {
        padding: 0 20px;
    }
    #footer .footer-before {
        margin: 0 -20px;
    }
    .footer-container {
        padding: 25px 20px;
        margin: 0 -20px;
    }
    .footer-after {
        margin: 0 -20px;
    }
    
}
@media (max-width: 575px) {
    #footer .block_newsletter .block_content {
    width: 100%;
    padding: 0 0 20px 0;
}
    #footer .block_newsletter .newsletter-title {
    background: none;
    float: left;
    text-align: left;
    padding-left: 0;
    padding-top: 15px;
}
    .block_newsletter form input[type=text] {
    padding-right: 5px;
    padding-left: 10px;
    min-width: 100%;
}
    .block_newsletter form input {
    padding: 7px 15px;
}
}
@media (max-width: 480px) {
    #footer .block_newsletter {
        padding: 0 15px;
    }
    #footer .footer-before {
        margin: 0 -15px;
    }
    .footer-container {
        padding: 25px 15px;
        margin: 0 -15px;
    }
    .footer-after {
        margin: 0 -15px;
    }
}
.contact-rich .block .icon {
    float: left;
    width: 45px;
}
.contact-rich .block .icon i {
    font-size: 2rem;
}
.contact-rich .block .data {
    width: auto;
    overflow: hidden;
}
.contact-form {
    background: #FFFFFF;
    padding: 1rem;
    color: #808080;
    width: 100%;
}
.contact-form h3 {
    text-transform: uppercase;
    color: #000000;
    font-weight: 600;
}
#products #main .page-header,
#pagenotfound #main .page-header {
    margin: 2rem 0 3rem;
}
#products #main .page-content,
#pagenotfound #main .page-content {
    margin-bottom: 10rem;
}
#products .page-not-found,
#pagenotfound .page-not-found {
    background: #FFFFFF;
    padding: 30px;
    border: 1px solid #ededed;
}
#products .page-not-found h4,
#pagenotfound .page-not-found h4 {
    margin: 0 0 10px;
}
#products .page-not-found .search-widget,
#pagenotfound .page-not-found .search-widget {
    float: none;
    padding: 0;
    width: 100%;
}
#products .page-not-found .search-widget input,
#pagenotfound .page-not-found .search-widget input {
    width: 100%;
}
.customization-modal .modal-content {
    border-radius: 0;
    border: 1px solid #ededed;
}
.customization-modal .modal-content .modal-body {
    padding-top: 0;
}
.customization-modal .modal-content .modal-body .product-customization-line {
    padding-bottom: 0.9375rem;
    padding-top: 0.9375rem;
    border-bottom: 1px solid #ededed;
}
.customization-modal .modal-content .modal-body .product-customization-line .label {
    font-weight: bold;
    text-align: right;
}
.customization-modal .modal-content .modal-body .product-customization-line:last-child {
    padding-bottom: 0;
    border-bottom: 0;
}
/*** HEADER ***/

#page {
    max-width: 100%;
    margin: 0 auto;
    box-shadow: 0 0 20px rgba(154, 145, 145, 0.35);
    width: 1260px;
}
#header .header-nav #language-selector, #header .header-nav #currency-selector {
    display: none;
}
#index #header {
    border-bottom: 0px;
}

#header .header_logo {
    padding: 25px 0 31px;
    float: left; 
}
#header .logo {
    max-width: 100%;
}
#header ul.dropdown-menu {
    margin: 0px 0px;
    padding: 5px 0;
    border: none;
    box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1);
}
#header .user-info .language-selector li, #header .user-info .currency-selector li {
    display: inline-block;
}
#header .user-info .currency-selector li a,
#header .user-info .language-selector li a{
    padding: 0px 9px;
    margin: 0 0 3px;
    border: 1px solid transparent;
}
#header .user-info .language-selector li a:hover,
#header .user-info .language-selector li.current a,
#header .user-info .currency-selector li a:hover,
#header .user-info .currency-selector li.current a{
    border-color: #e86236;
}
#header .user-info li a{
    font-size: 12px;
}
#header .user-info .language-selector .language-title, #header .user-info .currency-selector .currency-title {
    font-size: 12px;
    color: #000;
    font-weight: 500;
    letter-spacing: 0.5px;
        text-transform: uppercase;
    padding-bottom: 8px;
    display: inline-block;
}
 
#header .user-info .language-selector {
    display: block;
    border-top: 1px solid #e5e5e5;
    padding: 10px 0;
    margin: 10px 0px 0px;
}

#header .user-info .currency-selector {
    display: block;
    padding-top: 10px;
    border-top: 1px solid #e5e5e5;
}

#header ul.dropdown-menu li a {
    padding: 4px 0;
    display: block;
    color: #555;
    font-size: 12px;
    margin: 0 8px 0 0;
}
#header ul.dropdown-menu li a:hover {
    color: #e86236;
}
#header .header-nav {
    line-height: initial;
    color: #c6c6c6;
    font-size: 12px;
    font-weight: 400;
    display: none;
}
#header .header-nav a {
    color: #f5f5f5;
}
#header .header-nav a:hover {
    color: #808080;
}
#header .header-nav .left-nav {
    display: block;
}
#header .header-nav .right-nav {
    float: right;
    display: none;
}
#header .header-nav .currency-selector {
    padding: 12px 0 12px 0px;
    white-space: nowrap;
    display: inline-block;
}
#header .header-nav .currency-selector .dropdown-arrow:before {
    font-family: "FontAwesome";
    content: "\F107";
    font-size: 16px;
    padding-left: 3px;
}
#header .header-nav .currency-selector .expand-more {
    padding: 12px 0 12px 0px;
}
#header .header-nav .currency-selector.dropdown:hover .expand-more,
#header .header-nav .currency-selector.dropdown:hover .dropdown-arrow:before {
    text-decoration: none;
    color: #e86236;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
#header .header-nav .currency-selector.open .dropdown-arrow:before {
    content: "\F106";
}
#header .header-nav .language-selector {
    padding: 12px 12px 12px 0px;
    white-space: nowrap;
    display: inline-block;
}
#header .header-nav .language-selector .dropdown-arrow:before {
    font-family: "FontAwesome";
    content: "\F107";
    font-size: 16px;
    padding-left: 3px;
}
#header .header-nav .language-selector .expand-more {
    padding: 12px 0px 12px 0px;
}
#header .header-nav .language-selector.dropdown:hover .expand-more,
#header .header-nav .language-selector.dropdown:hover .dropdown-arrow:before {
    text-decoration: none;
    color: #e86236;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
#header .header-nav .language-selector.open .dropdown-arrow:before {
    content: "\F106";
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
#header .header-nav .cart-preview .shopping-cart {
    vertical-align: middle;
}
#header .header-nav .cart-preview .body {
    display: none;
}
#header .header-nav #contact-link {
    padding: 15px 0 15px 0px;
    float: right;
    margin-right: 10px;
}
#header .header-nav .material-icons {
    line-height: 22px;
    font-size: 20px;
}
#header .header-nav .material-icons.expand-more {
    margin-left: -3px;
}

#header .header-top {
    text-align: center;
    background-color: #46702c;
    margin: 0 -30px;
}
#header .header-top > .container {
    position: relative;
    padding: 0 30px;
}
#header .header-top-inner {
    background-color: #dc3c08;
    margin: 0 -30px;
    text-align: center;
}
#header .header-top-inner > .container {
    padding: 0 30px;
    position: relative;
}
#header .top-menu-link {
    margin-left: 1.25rem;
}
#header .user-info {
    float: right;
    position: relative;
    margin-right: 20px;
    margin-top: 41px;
}
#header .user-info .user-info-title {
    float: left;
    padding: 0 ;
    cursor: pointer;
    background: url(../img/codezeel/top-icon.png) no-repeat scroll 0px 0px transparent;
    width: 30px;
    height: 30px;
}
#header .user-info .user-info-title:hover,
#header .user-info.open .user-info-title {
    background-position: 0px -67px;
    color: #e86236;
}
#header .user-info .user-info-title .account_text:after {
    font-family: "FontAwesome";
    content: "\F107";
    font-size: 16px;
    padding-left: 7px;
}
#header .language-selector.open .expand-more,
#header .currency-selector.open .expand-more {
    color: #e86236;
}

#header .user-info.open .user-info-title .account_text:after {
    font-family: "FontAwesome";
    content: "\F106";
    font-size: 16px;
    padding-left: 7px;
}
#header .user-info .material-icons {
    display: none;
}
#header .user-info .dropdown-menu {
    left: auto;
    right: 0;
    top: 65px;
    bottom: auto;
    min-width: 260px;
    padding: 10px 20px;
}
#header .blockcart {
    text-align: center;
    white-space: nowrap;
    position: relative;
    float: right;
    padding: 0px;
    width: auto;
    margin-top: 30px;
    margin-bottom: 0;
    margin-right: 25px;
}
#header .blockcart .blockcart-header>a.shopping-cart {
    text-decoration: none;
    color: #e86236;
    text-transform: uppercase;
    font-weight: 600;
    font-size: 14px;
    padding-left: 43px;
    display: block;
    text-align: left;
    line-height: 24px;
    background: url(../img/codezeel/top-icon.png) no-repeat scroll 0px -135px transparent;
    transition: none;
    width: auto;
    height: 45px;
}
#header .blockcart .blockcart-header>a.shopping-cart span.value {
    color: #ffffff;
    font-weight: 300;
    font-size: 13px;
}
#header .blockcart .blockcart-header>a.shopping-cart .cart-headding {
    text-align: left;
    display: block;
    color: #ffffff;
    line-height: 20px;
    text-transform: capitalize;
}
#header .blockcart .blockcart-header>a.shopping-cart:hover .cart-headding {
    color: #e86236;
}
#header .blockcart .blockcart-header > a.shopping-cart .mobile_count {
   color: #fff;
    background: #e86236;
    line-height: 16px;
    position: absolute;
    height: 18px;
    width: 18px;
    font-size: 12px;
    top: 4px;
    right: auto;
    left: 18px;
    padding: 1px 5px;
    text-transform: lowercase;
    display: block;
    font-weight: 400;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
    
}
#header .blockcart .blockcart-header:hover, 
#header .blockcart .blockcart-header:focus {
    
}
#header .blockcart .blockcart-header>a.shopping-cart:hover,
#header .blockcart .blockcart-header>a.shopping-cart:focus {
    transition: none;
    color: #000000;
    background-position: 0px -216px;
}

#header .blockcart .blockcart-header>a.shopping-cart .cart-products-count {
    color: #808080;
    text-transform: lowercase;
    display: block;
    font-weight: 400;
    font-size: 13px;
    line-height: 16px;
}

#header .blockcart .material-icons.expand-more {
    display: none;
}

#header .blockcart a {
    color: #292929;
}

#header .blockcart a:hover {
    color: #e86236;
}
#header .cart_block {
    position: absolute;
    top: 76px;
    right: -20px;
    margin-top: 0px;
    z-index: 100;
    height: auto;
    background: white;
    border: none;
    width: 300px;
    padding: 5px 0;
    left: auto;
    box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1);
}
#header .cart_block .cart_block_list {
    max-height: 255px;
    overflow: auto;
}
#header .cart_block .cart-item {
    margin-bottom: 5px;
    padding: 10px;
    overflow: hidden;
    border-bottom: 1px solid #ededed;
}
#header .cart_block .cart-item .cart-image {
    float: left;
    margin-right: 12px;
    text-align: center;
}
#header .cart_block .cart-item .cart-image img {
    width: 100%;
}
#header .cart_block .cart-item .cart-info {
    overflow: hidden;
    position: relative;
    padding-right: 20px;
}
#header .cart_block .cart-item .cart-info .product-quantity {
    display: inline-block;
    text-transform: uppercase;
    color: #262626;
}
#header .cart_block .cart-item .cart-info .product-name {
    font-size: 13px;
    line-height: 18px;
    white-space: pre-line;
}
#header .cart_block .cart-item .cart-info .product-price {
    display: block;
    font-size: 15px;
}
#header .cart_block .cart-item .cart-info .remove-from-cart {
    position: absolute;
    right: 0px;
    top: 4px;
    background: url(../img/codezeel/delete.png) right center no-repeat;
    width: 12px;
    height: 12px;
    margin-right: 3px;
    float: right;
    outline: none;
    text-decoration: none;
}
#header .cart_block .cart-item .cart-info .remove-from-cart .material-icons {
    display: none;
}
#header .cart_block .cart-item .cart-info .customizations {
    padding-bottom: 10px;
    font-size: 11px;
}
#header .cart_block .cart-summary {
    border: 0px;
    margin: 0px;
    font-size: 13px;
}
#header .cart_block .block_content .card-block {
    padding: 10px 15px 15px;
    border-bottom: 1px solid #ededed;
}
#header .cart_block .block_content .checkout.card-block {
    text-align: right;
    padding: 15px;
    border-bottom: none;
}
#header .search-widget {
    padding: 32px 0;
    width: 50%;
    float: left;
   margin-left: 80px;
}
#header .search-widget .search_button {
    display: block;
    font-size: 0;
    padding: 0;
    text-align: center;
    cursor: pointer;
    z-index: 9;
    background: url(../img/codezeel/top-icon.png) no-repeat scroll 0px 6px transparent;
    height: 30px;
    width: 30px;
    transition: none;
}
#header .search-widget .search_button {
    display: none;
}
#header .search-widget .search_button:hover {
    background-position: 0 -52px;
    transition: none;
}
#header .search-widget .search_button.active {
    background-position: 0px -448px;
    transition: none;
    
}
#header .search-widget .search_button.active:hover {
   background-position: 0px -519px;
}

#header .search-widget .search_button.hover {
    background-position: 0 -97px;
}
#header .search-widget .search_toggle {
    display: block;
    position: relative;
    z-index: 9;
}
#header .search-widget form button[type=submit] {
   padding: 10px 30px;
    top: 0px;
    height: 43px;
    font-size: 12px;
    font-weight: 600;
    letter-spacing: 0.6px;
    color: #fff;
    background: #e86236;
    text-transform: uppercase;
    -webkit-transition: all 0.5s ease;
    -webkit-transition-delay: 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    border-radius: 0 5px 5px 0;
    -webkit-border-radius: 0 5px 5px 0;
    -moz-border-radius: 0 5px 5px 0;
    -ms-border-radius: 0 5px 5px 0;
    -o-border-radius: 0 5px 5px 0;
}
#header .search-widget form button[type=submit]:hover {
    color: #000000;
}
#header .search-widget form button[type=submit]:before {
    display: none;
}
#header .search-widget form input[type="text"] {
    min-width: inherit;
    width: 100%;
    padding: 10px 110px 10px 13px;
    border: 0;
    height: 43px;
    background: #fff;
    font-size: 12px;
    color: #888888;
    letter-spacing: 0.4px;
    font-weight: 400;
    border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    border: 2px solid #e86236;
}
#header .search-widget form input[type="text"]:-internal-autofill-selected {
    background-color: transparent !important;
}
.popover {
    font-family: inherit;
}
/*** WRAPPER ***/

.page-container {
    background: #FFF;
    margin: 0px auto;
    position: relative;
    padding: 0 30px;
}
#wrapper .page-container {
    padding-bottom: 30px;
    padding-top: 30px;
}

#index #wrapper .page-container {
    padding-bottom: 0;
    padding-top: 0;
}
#index #wrapper {
    padding-top: 0px;
    padding-bottom: 0px;
}
#wrapper {
    padding-top: 0;
    padding-bottom: 0;
}
#wrapper .banner {
    margin-bottom: 30px;
    display: block;
}
.breadcrumb {
    margin: 0 0 30px 0;
    width: 100%;
    padding: 15px 0;
    text-align: center;
    background: #f0f0f0;
}
.breadcrumb[data-depth="1"] {
    display: none;
}
#index .breadcrumb[data-depth="1"] {
    display: none;
}
.breadcrumb ol {
    padding-left: 0;
    margin-bottom: 0;
}
.breadcrumb li {
    display: inline;
}
.breadcrumb li::after {
    content: "|";
    color: #262626;
    margin: 0.3125em;
}
.breadcrumb li:last-child {
    content: "|";
    color: #262626;
    margin: 0.3125rem;
}
.breadcrumb li:last-child::after {
    content: "";
}
.breadcrumb li a {
    color: #262626;
}
.breadcrumb li a:hover {
    text-decoration: underline;
    color: #e86236
}
/*** MAIN ***/

#index #main .page-footer {
    margin-bottom: 0;
}
#main .page-header {
    margin-bottom: 1.5625rem;
}
#main .page-content {
    margin-bottom: 1.5625rem;
}
#main .page-content h6 {
    margin-bottom: 10px;
    font-weight: normal;
}
#main .page-content #notifications {
    margin-left: -20px;
    margin-right: -20px;
}
#main .page-footer {
    margin-bottom: 1.5625rem;
}
#main > h1,
#main h2.h2,
#main .page-header h1,
.block-category h1 {
    font: 600 20px/26px 'Poppins', sans-serif;
    color: #000000;
    text-transform: capitalize;
    margin-bottom: 25px;
}
#notifications ul {
    margin-bottom: 0;
}
/*** FOOTER ***/

/*** Responsive part ***/

@media (max-width: 575px) {
    #header .header-top .header_logo {
        width: 100%;
        padding: 20px 0;
    }
    #header .header-top .header-cms-block {
        display: none;
    }
    #header .cart_block {
        width: 270px;
    }
    .products-sort-order .dropdown-menu {
        width: 90%;
    }
    #contact-link .callus {
        display: none;
    }
    .footer-container .links .h3,
    .footer-container .links h3,
    .footer-container .links h3 a {
        
    }
    .viewed-products .products-section-title, 
    .crosssell-products .products-section-title, 
    .productscategory-products .products-section-title, 
    .product-accessories .products-section-title, 
    #productCommentsBlock .products-section-title {
        font-size: 16px;
    }
}
@media (max-width: 1289px) {
    #header .search-widget {
    margin-left: 60px;
}
}
@media (max-width: 1199px) {
    #header .logo {
        max-height: 40px;
        width: auto;
    }
    #header .search-widget {
        margin-left: 20px;
    }
}
@media (max-width: 991px) {
    #header .header_logo {
    padding-top: 20px;
    padding-bottom: 15px;
    float: none;
    width: auto;
}
    .container {
        max-width: 100%;
    }
    .page-container{
        padding: 0 25px;
    }
    #header .header-nav .language-selector {
        padding: 14px 12px 13px 0px;
    }
    #header .header-nav .currency-selector {
        padding: 12px 0 13px 0px;
    }
    #header .logo {
        width: auto;
        max-height: 45px;
    }
    #products .product-miniature,
    .products .product-miniature {
        margin: 0 auto;
    }
    .sub-menu {
        left: 0;
        min-width: 100%;
    }
    #blockcart-modal .product-image {
        width: 100%;
        display: block;
        max-width: 250px;
        margin: 0 auto 0.9375rem;
    }
    #blockcart-modal .cart-content {
        padding-left: 0;
    }
    #blockcart-modal .product-name,
    #product-availability {
        margin-top: 0.625rem;
    }
    #search_filters .facet .facet-label {
        text-align: left;
    }
    .block-category .category-cover {
        position: relative;
        text-align: center;
    }
    .block-category {
        padding-bottom: 0;
    }
    #header .header-top {
        padding: 0px;
        min-height: 100%;
        margin: 0 -25px;
    }
    #header .header-top > .container {
        padding: 0 25px;
    }
    #header .header-top-inner {
        margin: 0 -25px;
    }
    #header .header-top-inner > .container {
        padding: 0 25px;
    }
    #header .user-info {
        margin-bottom: -39px;
        z-index: 9;
        margin-right: 15px;
        margin-top: 16px;
        padding: 0;
    }
    #header .blockcart {
        margin-bottom: -49px;
        z-index: 9;
        margin-right: 0;
        margin-top: 5px;
    }
    #header .blockcart .blockcart-header>a.shopping-cart {
        font-size: 0;
        padding-left: 30px;
    }
    #header .blockcart .blockcart-header>a.shopping-cart span.value {
        font-size: 0;
    }
    #header .user-info .user-info-title .account_text {
        display: none;
    }
    #header .search-widget .search_toggle {
        display: none;
        position: absolute;
        padding: 0px;
        width: 250px;
        left: auto;
        right: 0;
        top: 37px;
        border-radius: 0;
        margin:0;
    }
    #header .search-widget form input[type="text"] {
        border: none;
        padding: 5px 40px 5px 10px;
        height: 40px;
        background: #ffffff;
        color: #000000;
        border-radius: 0;
        box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1);
        -webkit-box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1);
    }
    #header .search-widget form button[type=submit] {
        background: url(../img/codezeel/top-icon.png) no-repeat scroll 7px -295px #e86236;
        font-size: 0;
        padding: 0;
        border-radius: 0;
        width: 40px;
        height: 40px;
    }
    #header .search-widget form button[type="submit"]:hover {
        background-color: #000000;
    }
    #header .search-widget {
        float: right;
        z-index: 9;
        margin-left: 0;
        margin-top: 14px;
        margin-bottom: -37px;
        margin-right: 20px;
        padding: 0;
        width: auto;
    }
    #header .search-widget .search_button {
        display: block;
        float: right;
        background-position: 0 -299px;
    }
    #header .search-widget .search_button:hover {
        background-position: 0 -372px;
    }
    #header .user-info .dropdown-menu {
        top: 35px;
    }
    #header .cart_block {
        top: 46px;
        right: 0;
    }
    #header .blockcart .blockcart-header > a.shopping-cart .mobile_count {
        width: 16px;
        height: 16px;
        top: 6px;
    }
}
@media (max-width: 767px) {
    
    .page-container {
        padding: 0 20px;
    }
    #header .header-top-inner > .container {
        padding: 0 20px;
    }
    #header .header-top-inner {
        margin: 0 -20px;
    }
    #header .header-top > .container {
        padding: 0 20px;
    }
    #header .header-top {
        margin: 0 -20px;
    }
    #checkout-cart-summary {
        float: none;
        width: 100%;
        margin-top: 1rem;
    }
    #header .header-nav .top-logo {
        line-height: 45px;
        vertical-align: middle;
        margin: 0 auto;
    }
    #header .header-nav .top-logo a img {
        max-height: 50px;
        max-width: 100%;
    }
    #header .header-nav .user-info {
        text-align: left;
    }
    
    #header .header-nav .blockcart {
        margin-left: 0;
        background: inherit;
    }
    #header .header-nav .blockcart.inactive .cart-products-count {
        display: none;
    }
    section.checkout-step {
        width: 100%;
    }
    .default-input {
        min-width: 100%;
    }
    label {
        clear: both;
    }
    #products .product-miniature,
    .products .product-miniature {
        margin: 0 auto;
    }
    .block-contact {
        border: none;
    }
    .container {
        max-width: 100%;
    }
    .pp-left-column,
    .pp-right-column {
        float: none;
        width: 100%;
    }
    #header .header-nav .right-nav {
        float: right;
        margin-right: 5px;
    }
    #header .header-top .header_logo {
        padding: 15px 0 10px;
        float: none;
        max-width: none;
    }
    
    
}
@media (max-width: 480px) {
    #header .user-info .dropdown-menu {
        right: -20px;
    }
    #header .search-widget .search_toggle {
        right: -20px;
    }
    .page-container {
        padding: 0 15px;
    }
    #header .header-top > .container {
        padding: 0 15px;
    }
    #header .header-top {
        margin: 0 -15px;
    }
    #header .header-top-inner > .container {
        padding: 0 15px;
    }
    #header .header-top-inner {
        margin: 0 -15px;
    }
    #header .logo {
        max-height: 35px;
    }
}
@media (max-width: 380px) {
    #header .search-widget .search_toggle {
        right: -100px;
    }
    #header .user-info .dropdown-menu {
        right: -60px;
    }
    #header .cart_block {
        right: -10px;
    }
}
.sitemap .tree {
    padding-left: 20px;
}
.sitemap .tree li {
    padding: 5px 0;
}
.sitemap .tree li a {
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}
.sitemap .tree li a:hover {
    margin-left: 5px;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}
.sitemap .tree li a:before {
    content: "\F105";
    display: inline-block;
    font-family: "FontAwesome";
    padding-right: 13px;
}
.sitemap .tree li ul {
    margin-left: 20px;
}
/*-------------------------- Start Home page Service CMS Block -------------------------------*/

@-webkit-keyframes hvr-icon-push {
  50% {
    -webkit-transform: scale(0.5);
    transform: scale(0.5);
  }
}
@keyframes hvr-icon-push {
  50% {
    -webkit-transform: scale(0.5);
    transform: scale(0.5);
  }
}

#czservicecmsblock {
    clear: both;
    margin: 0px;
    padding: 0;
    float: left;
    width: 100%;
}
#czservicecmsblock .service_container {
    color: #FFF;
    padding-top: 0px;
    padding-bottom: 0px;
    margin: 0px auto;
}
#czservicecmsblock .service_container .service-area {
    padding: 42px 0 42px;
    float: left;
    width: 100%;
 }
#czservicecmsblock .service_container .service-area .service-fourth {
    float: left;
    width: 25%;
    line-height: 20px;
    color: #333333;
    padding: 0px 50px;
    position:relative;
}
#czservicecmsblock .service_container .service-area .service-fourth.service1 {
    padding-left: 0;
}
#czservicecmsblock .service_container .service-area .service-fourth.service4 {
    padding-right: 0;
}
#czservicecmsblock .service_container .service-area .service-fourth:after {
    content: "";
    position: absolute;
    border-right: 1px solid #eeeeee;
    height: 40px;
    top: 0;
    right: 15px;
}
#czservicecmsblock .service_container .service-area .service-fourth:last-child:after {
    border: 0;
}
#czservicecmsblock .service_container .service-area .service-fourth .service-icon {
    position: relative;
    background: url(../img/codezeel/service-icon.png) no-repeat scroll;
    float: left;
    margin-top: 0px;
    height: 40px;
    width: 30px;
}
#czservicecmsblock .service_container .service-area .service-fourth:hover .service-icon {
    -webkit-animation-name: hvr-icon-push;
    animation-name: hvr-icon-push;
    -webkit-animation-duration: 0.4s;
    animation-duration: 0.4s;
    -webkit-animation-timing-function: linear;
    animation-timing-function: linear;
    -webkit-animation-iteration-count: 1;
    animation-iteration-count: 1;
 }

#czservicecmsblock .service_container .service-area .service-fourth.service1 .icon1 {
    background-position:0px 6px;
}
#czservicecmsblock .service_container .service-area .service-fourth.service1:hover .icon1 {
    background-position:0px -113px;
}
#czservicecmsblock .service_container .service-area .service-fourth.service2 .icon2 {
    background-position: 0px -231px;
}
#czservicecmsblock .service_container .service-area .service-fourth.service2:hover .icon2 {
    background-position: 0px -350px;
}
#czservicecmsblock .service_container .service-area .service-fourth.service3 .icon3 {
    background-position: 0px -468px;
}
#czservicecmsblock .service_container .service-area .service-fourth.service3:hover .icon3 {
    background-position: 0px -587px;
}
#czservicecmsblock .service_container .service-area .service-fourth.service4 .icon4 {
    background-position: 0px -706px;
}
#czservicecmsblock .service_container .service-area .service-fourth.service4:hover .icon4 {
    background-position: 0px -825px;
}

#czservicecmsblock .service_container .service-area .service-fourth .service-content {
    margin-left: 35px;
    text-align: left;
}
#czservicecmsblock .service_container .service-area .service-fourth .service-content .service-heading {
    font-weight: 500;
    font-size: 14px;
    line-height: 22px;
    color: #000000;
    letter-spacing: 0.5px;
    text-transform: capitalize;
}
#czservicecmsblock .service_container .service-area .service-fourth:hover .service-content .service-heading {
    
}
#czservicecmsblock .service_container .service-area .service-fourth .service-content .service-description {
    font-weight: 400;
    color: #888888;
    margin-top: 0;
}


@media (max-width: 1459px) {
    #czservicecmsblock .service_container .service-area .service-fourth {
        padding: 0 30px;
    }
}
@media (max-width: 1199px) {
    
    #czservicecmsblock .service_container .service-area .service-fourth { text-align: center; } 
    #czservicecmsblock .service_container .service-area .service-fourth .service-icon{
       float:none;
       display:inline-block;
    }
    #czservicecmsblock .service_container .service-area .service-fourth .service-content {
         margin-left: 0px; 
         text-align: center;
    }
    #czservicecmsblock .service_container .service-area .service-fourth .service-content .service-heading{
        text-align:center;
    }
    #czservicecmsblock .service_container .service-area .service-fourth:after {
        top: 25px;
        right: 5px;
    }
    #czservicecmsblock .service_container .service-area .service-fourth .service-icon:after{
        display: none;
    }
    #czservicecmsblock .service_container .service-area {
        padding: 35px 0;
    }
    
}
@media (max-width: 991px) {
    
    
    #czservicecmsblock .service_container .service-area .service-fourth:after {
        border: 0;
    } 
    #czservicecmsblock .service_container .service-area .service-fourth .service-content .service-description {
        font-size: 12px;
    }
    #czservicecmsblock .service_container .service-area .service-fourth {
        width: 50%;
        padding-left: 0;
        padding-right: 0;
    }
    #czservicecmsblock .service_container .service-area .service-fourth.service1,
    #czservicecmsblock .service_container .service-area .service-fourth.service2 {
        margin-bottom: 20px;
    }
    #czservicecmsblock .service_container .service-area {
        padding-bottom: 30px;
        padding-top: 30px;
    }
}
@media (max-width: 767px) {
    #czservicecmsblock { padding-bottom: 0px; }
    #czservicecmsblock .service_container .service-area .service-third .service-content .service-heading {
        margin-bottom: 0;   
    }
    #czservicecmsblock .service_container .service-area .service-third {
        width: 100%;
        margin-bottom: 35px;
    }
}

@media (max-width: 575px) {
    
}
@media (max-width: 399px) {
    #czservicecmsblock .service_container .service-area .service-fourth{
       width:100%;
       margin-bottom:30px;
       padding:0;
    }
    #czservicecmsblock .service_container .service-area .service-fourth.service1,
    #czservicecmsblock .service_container .service-area .service-fourth.service4,
    #czservicecmsblock .service_container .service-area .service-fourth.service3 ,
    #czservicecmsblock .service_container .service-area .service-fourth.service4 {
           width:100%;
    }
    #czservicecmsblock .service_container .service-area {
        padding-bottom: 10px;
    }
}
/*-------------------------- Start Home page Parallax Block -------------------------------*/

#cztestimonialcmsblock {
    padding: 25px 0 50px;
    position: relative;
    clear: both;
    width: 100%;
    float: left;
}
.testimonial_wrapper {
    background: #e86236;
    float: left;
    width: 100%;
    border-radius: 3px;
}
#cztestimonialcmsblock .parallax.czparallax_2 {
    background: #f7f7f7;
}
#cztestimonialcmsblock .testimonial_container {
    color: #808080;
    margin: 0px auto;
    padding:0;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area {
    padding: 0;
    float: left;
    width: 100%;
    position: relative;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel {

}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item {
    overflow: hidden;
    padding: 48px 40px 70px;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face {
    float: left;
    width: 100%;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face .testimonial-image {
    float: none;
    border: 5px solid #a1c2bf;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
    display: inline-block;
    text-align: center;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face .testimonial-image img {
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face .title {
    text-align: center;
    margin-top: 20px;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .testimonial-detail .quote_img {
    background: transparent url(../img/codezeel/quote.png) no-repeat scroll left 10px;
    height: 50px;
    width: 50px;
    display: inline-block;
    vertical-align: top;
    float: left;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face .name {
    float: right;
    text-align: center;
    position: relative;
    padding-top: 5px;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .description::after {
    background: url(../img/codezeel/bracket.png) no-repeat scroll right 0; 
    content: "";
    position: absolute;
    right: 0px;
    top: 0;
    height: 70px;
    width: 20px;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .description::before {
    background: transparent url(../img/codezeel/bracket.png) no-repeat scroll 0 0;
    content: "";
    position: absolute;
    left: 50px;
    top: 0;
    height: 70px;
    width: 20px;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .description {
    font-size: 12px;
    font-weight: 400;
    color: #fff;
    margin-top: 0px;
    text-align: left;
    width: 81%;
    float: left;
    padding-left: 100px;
    padding-right: 50px;
    position: relative;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face .name a {
    color: #fff;
    display: block;
    text-transform: capitalize;
    font-size: 16px;
    font-weight: 600;
    line-height: 20px;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face .name span{
    color: #fff;
    font-size: 12px;
    font-weight: 300;
    letter-spacing: 1px;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .product_inner_cms {
    padding-top: 0;
    text-align: center;
}
#cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .product_inner_cms .des {
    line-height: 28px;
    color: #999999;
    position: relative;
    padding: 0 20%;
    
}
#cztestimonialcmsblock .customNavigation {
    filter: alpha(opacity=0);
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
    width: 30px;
    bottom: 45px;
    left: 0;
    top: auto;
    right: 0;
    z-index: 9;
    text-align: center;
    margin: 0 auto;
    background: url(../img/codezeel/testimonial-arrow.png) no-repeat 0 0;
}
#cztestimonialcmsblock .customNavigation a {
     background: url(../img/codezeel/testimonial-arrow.png) no-repeat 0 0;
}
#cztestimonialcmsblock .customNavigation a.prev {
    left: -20px;
    background-position: center -66px;
}
#cztestimonialcmsblock .customNavigation a.next {
    right: -20px;
    background-position: center -239px;
}
#cztestimonialcmsblock .customNavigation a.prev:hover {
    background-position: center 10px;
}
#cztestimonialcmsblock .customNavigation a.next:hover {
    background-position: center -146px
}
#cztestimonialcmsblock .customNavigation a.prev:before,
#cztestimonialcmsblock .customNavigation a.next:before{
    display: none;
}
#cztestimonialcmsblock:hover .customNavigation {
    opacity: 1;
    filter: alpha(opacity=100);
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
}
/*#cztestimonialcmsblock .owl-controls {
    position: absolute;
    margin: 0;
    bottom: -60px;
    right: 0;
    z-index: 9;
    left: 0;
    display: block;
    opacity: 0;
    text-align: center;
}
#cztestimonialcmsblock:hover .owl-controls {
    opacity: 1;
}
#cztestimonialcmsblock .testimonial_container .owl-controls .owl-page span:hover, 
#cztestimonialcmsblock .testimonial_container .owl-controls .owl-page.active span{
    border-color: #e86236;
    -webkit-transform: scale(1.1);
    -ms-transform: scale(1.1);
    transform: scale(1.1);
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
    background: #e86236;
}

#cztestimonialcmsblock .testimonial_container .owl-controls .owl-page {
    margin: 10px 4px;
    display: inline-block;
    zoom: 1;
    cursor: pointer;
}

#cztestimonialcmsblock .testimonial_container .owl-controls .owl-page span {
    width: 10px;
    height: 10px;
    display: block;
    border: 0;
    background: #000;
    cursor: pointer;
    margin: 0;
    text-indent: -9999px;
    -webkit-transition: transform 0.3s cubic-bezier(0.7, -0.2, 0.3, 2.5), color 0.15s ease-out;
    -moz-transition: transform 0.3s cubic-bezier(0.7, -0.2, 0.3, 2.5), color 0.15s ease-out;
    -o-transition: transform 0.3s cubic-bezier(0.7, -0.2, 0.3, 2.5), color 0.15s ease-out;
    transition: transform 0.3s cubic-bezier(0.7, -0.2, 0.3, 2.5), color 0.15s ease-out;
}*/

@media (max-width: 1289px) {
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .description {
        padding-left: 80px;
        padding-right: 20px;
    }
}
@media (max-width: 1199px) {
    #cztestimonialcmsblock {
        padding-bottom: 40px;
    }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .description {
    padding-left: 70px;
    padding-right: 15px;
    width: 75%;
    }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .description::before{
        left: 30px;
    }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .description::after{
        right: -20px;
    }
}
@media (max-width: 991px) {
    
    #cztestimonialcmsblock .testimonial_container {
        margin: 0px auto;
    }
    
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face {
        float: none;
        width: 100%;
    }
    
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .product_inner_cms {
        float: none;
        width: 100%;
        clear: both;
        margin-left: 0;
        padding-top:0px;
    }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .product_inner_cms .des {
        padding: 0;
    }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item {
        padding: 30px 20px 50px;
    }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .testimonial-detail .quote_img {
        float: none;
        background-position: center 0;
    }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face .name {
        float: none;
    }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .description::before,
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .description::after{
        display: none;
    }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .description {
        padding-left: 0;
        width: 100%;
        float: none;
        text-align: center;
        padding-top: 15px;
        padding-right: 0;
    }
}
@media (max-width: 480px) {

    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face .title {
        clear: both;
    }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face .title .quote_img {
        display: none;
    }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face .title { text-align: center; }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .cms_face .testimonial-image{ float: none; margin-right: 0px; border: 0; }
    #cztestimonialcmsblock .testimonial_container .testimonial-area ul#testimonial-carousel li.item .product_inner_cms{ text-align:center; margin-top: 15px; }
}
/****** Social icon in parralax ********/

.news-social {
    background: #888888;
    width: 33%;
    text-align: center;
    float: left;
    display: inline-block;
    padding: 39px 20px 40px;
}
.news-social .social-title {
    margin-top: 0px;
    margin-bottom: 10px;
}
.news-social .social-title .social-heading {
    color: #FFF;
    font-size: 20px;
    font-weight: 700;
    line-height: 35px;
    margin-bottom: 10px;
    text-transform: uppercase;
}
.news-social ul.social-links {
    display: inline-block;
    float: none;
    margin: 0;
    padding: 0px;
    background: none;
    position: relative;
}
.news-social ul.social-links li.social-link {
    display: inline-block;
    text-align: center;
    margin: 0 3px 0 0;
}
.news-social ul.social-links li.social-link a {
    display: inline-block;
    padding: 5px;
    background-color: #add880;
    color: #5bb000;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
.news-social ul.social-links li.social-link a span {
    display: none;
}
.news-social ul.social-links li.social-link a:hover {
    background-color: #000000;
    color: #fff;
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
.news-social ul.social-links li.social-link a:before {
    font-family: "FontAwesome";
    display: inline-block;
    font-weight: normal;
    width: 25px;
}
.news-social ul.social-links li.social-link.facebook a:before {
    content: "\F09A";
}
.news-social ul.social-links li.social-link.twitter a:before {
    content: "\F099";
}
.news-social ul.social-links li.social-link.google a:before {
    content: "\F0D5";
}
.news-social ul.social-links li.social-link.rss a:before {
    content: "\F09E";
}
.news-social ul.social-links li.social-link.vimeo a:before {
    content: "\F27D";
}
.news-social ul.social-links li.social-link.tumblr a:before {
    content: "\F173";
}
@media (max-width: 991px) and (min-width: 768px) {
    .news-social {
        width: 42%;
        padding: 40px 20px 39px;
    }
}
@media (max-width: 991px) {
    .news-social .social-title .social-heading {
        font-size: 18px;
    }
}
@media (max-width: 767px) {
    .news-social {
        width: 100%;
        padding: 20px 20px 25px;
    }
    .cz_newsletterdiv {
        width: 100%;
        padding: 10px;
    }
    .cz_newsletterdiv .block_newsletter {
        padding: 20px 15px;
    }
}
/*------------- Header CMS Block-------------*/

#header .header-top .header-cms-block {
    float: left;
    text-align: left;
    margin: 40px 0 40px;
    position: relative;
}

#header .header-top .header-cms-block .header-cms {
    padding-left: 42px;
    background: url(../img/codezeel/top-icon.png) no-repeat scroll 0px -2px transparent;
    line-height: 24px;
}
#header .header-top .header-cms-block .header-cms:hover {
    background-position: 0px -61px;
}
#header .header-top .header-cms-block .header-cms .contact-info {
    color: #e86236;
    font-size: 14px;
    font-weight: 600;
    display: inline-block;
}
#header .header-top .header-cms-block .header-cms:hover .contact-info {
    color: #000000;
}
#header .header-top .header-cms-block .header-cms .shop-time {
    font-size: 13px;
    color: #808080;
    line-height: 16px;
}

@media (max-width: 767px) {
    #header .header-top .header-cms-block {
        display: none;
    }
}


/*********************  CMS About Block homepage    ********************/


#czaboutcmsblock {
    padding: 90px 0 0;
}
#czaboutcmsblock .left-part {
    width: 42.4%;
    float: left;
    padding-top: 30px;
}
#czaboutcmsblock .right-part {
    width: 57.6%;
    display: inline-block;
    overflow: hidden;
    margin: 0 -10px;
    float: right;
}
#czaboutcmsblock .right-part .about-image {
    width: 100%;
}
#czaboutcmsblock .right-part .right-image {
    float: left;
    width: 53.8%;
    padding: 0 10px;
}
#czaboutcmsblock .right-part .right-image.right-part2 {
    width: 46.2%;
}
#czaboutcmsblock .right-part .right-image .rightbanner.aboutcms-image1 {
    margin-bottom: 20px;
}
#czaboutcmsblock .right-part .right-image .rightbanner {
    position: relative;
    overflow: hidden;
    vertical-align: middle;
}
#czaboutcmsblock .right-part .right-image .rightbanner img {
    width: 100%;
}
#czaboutcmsblock .right-part .right-image .rightbanner a {
    display: inline-block;
    position: relative;
}
#czaboutcmsblock .left-part .mainheading {
    font-size: 30px;
    color: #e86236;
    letter-spacing: .2px;
}
#czaboutcmsblock .left-part .title {
    font-size: 40px;
    font-weight: 700;
    color: #000;
    line-height: 30px;
    letter-spacing: .2px;
    padding: 20px 0;
}
#czaboutcmsblock .left-part .storedetail {
    color: #888888;
    font-size: 13px;
   
}
#czaboutcmsblock .left-part a.shop-now {
    padding: 7px 23px;
    background: #e86236;
    color: #fff;
    font-size: 16px;
    font-weight: 500;
    letter-spacing: .2px;
    float: left;
    -webkit-transition: all 0.5s ease 0s;
    -moz-transition: all 0.5s ease 0s;
    -o-transition: all 0.5s ease 0s;
    transition: all 0.5s ease 0s;
}
#czaboutcmsblock .left-part a.shop-now:hover {
    background: #000;
}
#czaboutcmsblock .left-part .shop.now {
    display: inline-block;
    margin-top: 70px;
}
#czaboutcmsblock .right-part .right-image .rightbanner a:after,
#czaboutcmsblock .right-part .right-image .rightbanner a:before {
    bottom: 0;
    content: "";
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    -webkit-transition: all 0.6s ease-in-out;
    transition: all 0.6s ease-in-out;
}
#czaboutcmsblock .right-part .right-image .rightbanner:hover a:before{
    background: rgba(255, 255, 255, 0.5) none repeat scroll 0 0;
    left: 50%;
    right: 50%;
}
#czaboutcmsblock .right-part .right-image .rightbanner:hover a:after{
    background: rgba(255, 255, 255, 0.5) none repeat scroll 0 0;
    bottom: 50%;
    top: 50%;
}


@media (max-width: 1229px) {
    #czaboutcmsblock .left-part {
    padding-top: 20px;
}
    #czaboutcmsblock .left-part .title {
    font-size: 32px;
    padding: 15px 0;
}
    #czaboutcmsblock .left-part .shop.now {
    margin-top: 40px;
}
}
@media (max-width: 991px) {
    #czaboutcmsblock .left-part {
    width: 100%;
    text-align: center;
    padding: 0;
}
    #czaboutcmsblock .right-part {
    width: 100%;
    text-align: center;
    float: none;
    margin: 15px 0 0 0;
}
    
    #czaboutcmsblock {
    padding: 50px 0 0;
}
    #czaboutcmsblock .left-part .title {
    font-size: 32px;
}
    #czaboutcmsblock .left-part .shop.now {
    margin-top: 30px;
}
}
@media (max-width: 480px) {
    #czaboutcmsblock .right-part .right-image,
    #czaboutcmsblock .right-part .right-image.right-part2 {
    float: none;
    width: auto;
    text-align: center;
    display: inline-block;
    padding: 0;
    }
    #czaboutcmsblock .right-part .right-image.right-part1{
        margin-bottom: 10px;
    }
}
/*********************  CMS Sub Banner homepage    ********************/
#czsubbannercmsblock {
    padding: 30px 0px 0;
    clear: both;
    overflow: hidden;
    position: relative;
}
#czsubbannercmsblock .subbanners {
    margin: 0 -15px;
}
#czsubbannercmsblock .subbanners .one-half {
    float:left;
    width: 50%;
    position: relative;
    overflow: hidden;
    padding: 0 15px;
}

#czsubbannercmsblock .subbanners .one-half .subbanner{
    position:relative;
    overflow:hidden;
    display: inline-block;
}
#czsubbannercmsblock .subbanners .one-half .subbanner img {
    max-width: 100%;
    -webkit-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
#czsubbannercmsblock .subbanners .one-half .subbanner:hover img {
    -webkit-transform: scale(1.05);
    -ms-transform: scale(1.05);
    transform: scale(1.05);
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
#czsubbannercmsblock .subbanners .one-half .subbanner a.banner-anchor {
    display: block;
    position:relative;
    overflow: hidden;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    -o-border-radius: 5px;
    border-radius: 5px;
}
#czsubbannercmsblock .subbanners .one-half .subbanner a.banner-anchor:after {
    background: rgba(117, 62, 47, 0.42);
    bottom: 0;
    content: "";
    height: 0;
    opacity: 1;
    filter: alpha(opacity=100);
    position: absolute;
    right: 0;
    width: 100%;
}
#czsubbannercmsblock .subbanners .one-half .subbanner:hover a.banner-anchor:after {
    height: 100%;
    -webkit-transition-duration: 1.3s;
    transition-duration: 1.3s;
    opacity: 0;
    filter: alpha(opacity=0);
    width: 100%;
}
#czsubbannercmsblock .subbanners .subbanner .subbanner-text {
    position: absolute;
    top: 25%;
    text-align: left;
    text-transform: capitalize;
    padding: 0 25px 0 0;
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
    color: #262626;
    left: auto;
    right: 0;
}

#czsubbannercmsblock .subbanners .subbanner .subbanner-text .main-title{
    color: #000000;
    display: block;
    font-size: 20px;
    font-weight: 300;
    padding-bottom: 12px;
    letter-spacing: 0.2px;
}
#czsubbannercmsblock .subbanners .subbanner .subbanner-text .sub-title {
   font-size: 32px;
    font-weight: 600;
    color: #000000;
    display: block;
    letter-spacing: 0.2px;
    padding-bottom: 70px;
}
#czsubbannercmsblock .subbanners .subbanner .subbanner-text .shop-now {
    color: #e86236;
    font-size: 12px;
    font-weight: 600;
    line-height: 20px;
    padding: 0;
    letter-spacing: 0.5px;
    text-decoration: underline;
}
#czsubbannercmsblock .subbanners .subbanner .subbanner-text .shop-now:hover{ color: #000; text-decoration: none; }

@media (max-width: 1199px) {
    
    #czsubbannercmsblock .subbanners .subbanner .subbanner-text .main-title {
        font-size: 18px;
    }
    #czsubbannercmsblock .subbanners .subbanner .subbanner-text .sub-title {
        font-size: 23px;
        padding-bottom: 30px;
    }
}

@media (max-width: 991px) {
    
    #czsubbannercmsblock .subbanners .one-half.subbanner-part3 .subbanner .subbanner-text .main-title { padding-bottom: 0px; }
    #czsubbannercmsblock .subbanners .subbanner .subbanner-text .main-title { font-size: 16px; padding-bottom: 5px; }
    #czsubbannercmsblock .subbanners .subbanner .subbanner-text .sub-title {
    font-size: 20px;
}
    #czsubbannercmsblock .subbanners .subbanner .subbanner-text {
    padding-right: 10px;
    top: 20%;
}
}
@media screen and (max-width: 767px) {
    
}
@media screen and (max-width: 650px) {
    #czsubbannercmsblock { width:100%; text-align:center; padding-top: 20px; }
    #czsubbannercmsblock .subbanners .one-half {
        width: 100%;
        float: none;
        text-align: center;
        margin-bottom: 10px;
        padding: 0px;
    }
    #czsubbannercmsblock .subbanners .one-half.subbanner-part3{ margin-bottom: 0; }
    #czsubbannercmsblock .subbanners .subbanner .subbanner-text {
        padding-right: 20px;
    }
    #czsubbannercmsblock .subbanners .subbanner .subbanner-text .main-title {
    font-size: 20px;
}
    #czsubbannercmsblock .subbanners .subbanner .subbanner-text .sub-title {
        font-size: 25px;
    }
    #czsubbannercmsblock .subbanners .one-half.subbanner-part2 {
    margin-bottom: 0;
}
}
@media screen and (max-width: 480px) {
    #czsubbannercmsblock {
    padding-top: 15px;
}
}
@media screen and (max-width: 479px) {
    #czsubbannercmsblock .subbanners .subbanner .subbanner-text .main-title {
    font-size: 18px;
}
    #czsubbannercmsblock .subbanners .subbanner .subbanner-text .sub-title {
        font-size: 20px;
    }
    
}

/*********************  CMS Banner homepage    ********************/

#czbannercmsblock {
    padding: 0 0 50px;
    clear: both;
    overflow: hidden;
    margin: 0px;
}
#czbannercmsblock .cmsbanners {
    margin: 0 -15px;
}
#czbannercmsblock .cmsbanners .one-half {
    float: left;
    width: 50%;
    padding:0 15px;
    overflow: hidden;
}
#czbannercmsblock .cmsbanners .one-half .cmsbanner {
    position: relative;
    overflow: hidden;
    vertical-align: middle;
    display: inline-block;
}

#czbannercmsblock .cmsbanners .cmsbanner img {
    max-width: 100%;
    -webkit-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
#czbannercmsblock .cmsbanners .cmsbanner:hover img {
    -webkit-transform: scale(1.05);
    -ms-transform: scale(1.05);
    transform: scale(1.05);
    -webkit-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}
#czbannercmsblock .cmsbanners .cmsbanner a.banner-anchor {
    position: relative;
    display: block;
}
#czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text {
    position: absolute;
    top: 60px;
    text-align: left;
    right: 0;
    color: #ffffff;
    width: 50%;
    padding: 0 25px;
}
#czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .offer-title {
    display: inline-block;
    color: #fd3232;
    line-height: 16px;
    padding: 0;
    font-size: 16px;
    font-weight: 500;
}
#czbannercmsblock .cmsbanners .one-half .cmsbanner.cmsbanner2 .cmsbanner-text .offer-title {
    color: #fff;
    position: relative;
    background: #fd3232;
    font-size: 12px;
    font-weight: 400;
    padding: 1px 3px;
    letter-spacing: 0.7px;
}
#czbannercmsblock .cmsbanners .one-half .cmsbanner.cmsbanner2 .cmsbanner-text .offer-title:after {
    color: #000;
    width: 0;
    height: 0;
    border-top: 9px solid transparent;
    border-bottom: 9px solid transparent;
    border-right: 0px solid transparent;
    border-left: 9px solid #fd3232;
    position: absolute;
    right: auto;
    top: 0;
    content: "";
    left: 37px;
}
#czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .main-title {
    color: #000000;
    font-size: 26px;
    font-weight: 500;
    line-height: 40px;
    letter-spacing: 0px;
    text-transform: capitalize;
    margin-top: 12px;
}
#czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .main-title .title1 {
    font-weight: 300;
}
#czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .sub-title {
    color: #000000;
    font-size: 20px;
    font-weight: 400;
    line-height: 36px;
    letter-spacing: 0.3px;
    text-transform: capitalize;
    margin-top: 3px;
}
#czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .shop-button {
    margin-top: 25px;
}
#czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text a {
    font-weight: 600;
    color: #e86236;
    font-size: 12px;
    position: relative;
    text-transform: uppercase;
}
#czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text a:after {
    position: absolute;
    width: 100%;
    height: 2px;
    background: #e86236;
    left: 0;
    right: 0;
    content: "";
    top: auto;
    bottom: 2px;
    opacity: 0.3;
}

#czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text a:hover::after{
    background: none;
}
@media (max-width: 1289px) {
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text {
        padding: 0 20px;
    }
}
@media (max-width: 1239px) {
    
}
@media (max-width: 1199px) {
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .main-title {
        font-size: 20px;
        line-height: 30px;
    }
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text {
        top: 40px;
    }
    #czbannercmsblock {
        padding-bottom: 40px;
    }
}
@media (max-width: 991px) {
    
    #czbannercmsblock .cmsbanners {
        margin-left: -10px;
        margin-right: -10px;
    }
    #czbannercmsblock .cmsbanners .one-half {
        padding-left: 10px;
        padding-right: 10px;
    }
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .main-title {
        font-size: 16px;
        line-height: 26px;
    }
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text {
        top: 25px;
        padding: 0 7px;
    }
    
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .shop-button {
        margin-top: 15px;
    }
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .offer-title {
        font-size: 14px;
    }
}
@media (max-width: 700px) {
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .main-title {
        font-size: 14px;
        line-height: 26px;
    }
    
}
@media (max-width: 650px) {
    
    #czbannercmsblock .cmsbanners .one-half.cmsbanner-part2 {
        margin-bottom: 0;
    }
    #czbannercmsblock .cmsbanners { margin: 0px; }
    #czbannercmsblock .cmsbanners .one-half {
        width: 100%;
        text-align: center;
        float: none;
        padding: 0;
        margin-bottom: 20px;
    }
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .main-title {
        font-size: 22px;
        line-height: 30px;
    }
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text {
        padding: 0 10px;
    }
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text {
        top: 45px;
    }
}
@media (max-width: 479px) {
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .main-title {
        font-size: 16px;
        line-height: 25px;
    }
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text {
        padding: 0 15px;
    }
}
@media (max-width: 389px) { 
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .main-title{
        font-size: 12px;
        line-height: 24px;
        margin-top: 0;
    }
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text {
        top: 30px;
        padding: 0 10px;
    }
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .shop-button {
        margin-top: 5px;
    }
    #czbannercmsblock .cmsbanners .one-half .cmsbanner .cmsbanner-text .offer-title {
        font-size: 12px;
        letter-spacing: 0;
    }
}
/**** CSS update ***/

.ps-alert-error {
    margin-bottom: 0
}
.ps-alert-error .item,
.ps-alert-success .item {
    align-items: center;
    border: 2px solid #ff4c4c;
    -js-display: flex;
    display: flex;
    background-color: #ff4c4c;
    margin-bottom: 1rem
}
.ps-alert-error .item i,
.ps-alert-success .item i {
    border: 15px solid #ff4c4c;
    -js-display: flex;
    display: flex
}
.ps-alert-error .item i svg,
.ps-alert-success .item i svg {
    background-color: #ff4c4c;
    width: 24px;
    height: 24px
}
.ps-alert-error .item p,
.ps-alert-success .item p {
    background-color: #fff;
    margin: 0;
    padding: 18px 20px;
    width: 100%
}
.ps-alert-success {
    padding: .25rem .25rem 2.75rem
}
.ps-alert-success .item {
    border-color: #4cbb6c;
    background-color: #4cbb6c
}
.ps-alert-success .item i {
    border-color: #4cbb6c
}
.ps-alert-success .item i svg {
    background-color: #4cbb6c
}
#delivery_message {
    width: 100%;
}
#delivery textarea {
    border-color: #ededed;
}
#productCommentsBlock {
    clear: both;
}
.container { 
/*border: 1px solid #f00;*/
}









#products .psproductcountdown .count.curr, #products .psproductcountdown .count.bottom,
.psproductcountdown .count.curr, .psproductcountdown .count.bottom {
    display: none;
}
.psproductcountdown .hidden {
    display: none;
}
.psproductcountdown { position: relative; }
.product-counter {
    padding: 10px 0;
}
.product-miniature .psproductcountdown, #product .psproductcountdown {
    display: inline-block;
    padding: 0;
    /*padding-bottom: 0;
    width: auto;
    padding: 10px 10px 15px;
    background: #dd959b;
    border: 10px solid #fff;*/
}
#product .product-actions .psproductcountdown {
    margin-bottom: 30px;    
    display: inline-block;
}


.product-miniature:hover .psproductcountdown {
    
}
#left-column .products .product_list li.item {
    padding-right: 0;
    padding-left: 0;
    padding-bottom: 0;
    padding-top: 0;
}


.psproductcountdown {
    padding-bottom: 30px;
    padding-top: 30px;
}
.pspc-main .time {
    margin: 0 5px;
    display: inline-block;
    text-align: center;
    position: relative;
    height: 54px;
    width: 54px;
    max-width: 7em;
    background: #fdf9f4;
    border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    -o-border-radius: 5px;
}
#products .pspc-main .count, .pspc-main .count {
    color: #fff;
    display: block;
    font-size: 24px;
    font-weight: 500;
    letter-spacing: .2px;
    line-height: 30px;
    overflow: hidden;
    position: absolute;
    text-align: center;
    top: 0;
    left: 0;
    right: 0;
    bottom: auto;
}
/*.pspc-main .time:after {
    content: ":";
    font-size: 30px;
    position: absolute;
    right: -18px;
    color: #92bb64;
    top: 10px;
}*/
.psproductcountdown .time:last-child:after { display: none; }
.psproductcountdown .time::before {
 

}
#products .pspc-main .count,
.pspc-main .count {
    color: #202020;
    display: block;
    font-size: 18px;
    font-weight: 600;
    letter-spacing: 0.6px;
    line-height: 40px;
    overflow: hidden;
    position: absolute;
    text-align: center;
    top: 0;
    left: 0;
    right: 0;
    bottom: auto;
}
.pspc-main .seconds .count,
#products .pspc-main .seconds .count {
    color: #e86236;
}
.pspc-main .label {
    font-size: 11px;
    text-transform: capitalize;
    position: absolute;
    top: 28px;
    left: 0;
    width: 100%;
    color: #9d9d9d;
    font-weight: 400;
    letter-spacing: 0.6px;
}
/*.pspc-main .time:after {
    content: ":";
    font-size: 30px;
    position: absolute;
    right: -11px;
    color: #e4adb2;
    top: 10px;
}*/
@media (max-width: 1199px) {
    .pspc-main .time {
        margin-left: 3px;
        margin-right: 3px;
        width: 45px;
        height: 45px;
    }
    #products .pspc-main .count, .pspc-main .count{
        font-size: 16px;
        line-height: 30px;
    }
    .pspc-main .label{
        top: 20px;
    }
    /*.product-miniature .psproductcountdown, #product .psproductcountdown {
        margin-left: -12px;
    }*/
}
@media (max-width: 991px) and (min-width: 651px) {
    /*.pspc-main .time { margin: 0px; }
    .product-miniature .psproductcountdown { padding-left: 0; padding-right: 0;}
    #products .pspc-main .count, .pspc-main .count { font-size: 17px; }
    .psproductcountdown .time::before { left: 3px; }
    .pspc-main .time:after { right: -5px; }
    .pspc-main .label { font-size: 11px; }*/
}
@media (max-width: 991px){
    .pspc-main .time {
        margin-left: 5px;
        margin-right: 5px;
        width: 54px;
        height: 54px;
    }
     #products .pspc-main .count, .pspc-main .count{
        font-size: 18px;
        line-height: 35px;
    }
    .pspc-main .label {
    top: 28px;
}
}
@media (max-width: 767px) {
    .product-miniature .psproductcountdown, #product .psproductcountdown {
        margin-left: 0px;
    }
}
@media (max-width: 480px) {
}
@media (max-width: 380px) {
    .pspc-main .time:after { display: none; }
    .pspc-main .time {
        margin-left: 3px;
        margin-right: 3px;
    }
    
}






/********************************************************
            Codezeel Custom Styles
********************************************************/
.cz-carousel {
  display: block;
  position: relative;
  float: left;
  width: 100%;
  margin: 0;
  -ms-touch-action: pan-y;
}

.owl-carousel .owl-wrapper {
  display: block;
  position: relative;
  -webkit-transform: translate3d(0px, 0px, 0px);
}
.owl-carousel .owl-wrapper:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
.owl-carousel .owl-wrapper-outer {
  overflow: hidden;
  position: relative;
  float: left;
  width: 100%;
}
.owl-carousel .owl-wrapper-outer.autoHeight {
  -webkit-transition: height 500ms ease-in-out;
  -moz-transition: height 500ms ease-in-out;
  -ms-transition: height 500ms ease-in-out;
  -o-transition: height 500ms ease-in-out;
  transition: height 500ms ease-in-out;
}
.owl-carousel .owl-item {
  float: left;
}
.owl-carousel .owl-item .manu_image a {
  display: inline-block;
}
.owl-carousel .owl-item .manu_image a img {
  max-width: 100%;
}
.owl-carousel .owl-item:hover a img {
  border-color: blue;
}
.owl-carousel .owl-wrapper,
.owl-carousel .owl-item {
  /* fix */
  -webkit-backface-visibility: hidden;
  -moz-backface-visibility: hidden;
  -ms-backface-visibility: hidden;
  -webkit-transform: translate3d(0, 0, 0);
  -moz-transform: translate3d(0, 0, 0);
  -ms-transform: translate3d(0, 0, 0);
  text-align: center;
}

/* mouse grab icon */
.grabbing {
  cursor: url("../img/codezeel/grabbing.png") 8 8, move;
}
 
.customNavigation {
  position: absolute;
  width: 65px;
  top: 0;
  direction: ltr !important;
  left: auto;
  right: 15px;
  z-index: 9;
}
.lastest_block .customNavigation {
    top: -68px;
}
.customNavigation a {
    font-size: 0px;
    position: absolute;
    font-weight: 300;
    height: 30px;
    width: 30px;
    line-height: 30px;
    padding: 0px;
    color: #ffffff;
    cursor: pointer;
    overflow: hidden;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    -o-border-radius: 3px;
    border-radius: 3px;
    border: 0;
    background: #dc3c08;
}
.customNavigation a.prev:before {
    content: '\f104';
    font-family: "FontAwesome";
    font-size: 18px;
    color: #ffffff;
}
.customNavigation a.next:before {
    content: '\f105';
    font-family: "FontAwesome";
    font-size: 18px;
    color: #ffffff;
}
.customNavigation a:hover{
    background: #e86236;
}
.customNavigation a:hover:before{ color: #fff; }

.customNavigation a.prev {
  left: 0;
}

.customNavigation a.next {
  right: 0
}

.customNavigation a.prev:hover{

}

.customNavigation a.next:hover{
}
@media (max-width: 479px) {
    .lastest_block .customNavigation {
    left: 0;
    right: 0;
    margin: 0 auto;
    top: -30px;
  }
}

@media (max-width: 991px) {
    
}

#czleftbanner img,
#czrightbanner img{
    max-width: 100%;
}   
#czleftbanner li, #czrightbanner li {
    display: inline-block;
}
#czleftbanner li a, #czrightbanner li a {
    display: inline-block;
    overflow: hidden;
    position: relative;
}
#czleftbanner li a:before, #czrightbanner li a:before {
    background: rgba(255,255,255,.5) none repeat scroll 0 0;
    bottom: 20px;
    content: "";
    left: 20px;
    opacity: 1;
    position: absolute;
    right: 20px;
    top: 20px;
    opacity: 1;
    filter: alpha(opacity=100);
    -webkit-transform: scale(0);
    -ms-transform: scale(0);
    transform: scale(0);
}

#czleftbanner li:hover a:before, #czrightbanner li:hover a:before {
    opacity: 0;
    filter: alpha(opacity=0);
    -webkit-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
    -webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    transition: all 1s ease;
}

.top_button {
    background: #e86236 ;
    bottom: 20px;
    cursor: pointer;
    height: 40px;
    padding: 7px 16px;
    position: fixed;
    right: 20px;
    text-align: center;
    width: 40px;
    z-index: 99;
     -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    -o-border-radius: 3px;
    border-radius: 3px;
    border: 1px solid #e86236;
}
.top_button:hover {
    background: #e86236;
}
.top_button:hover::before{
  color: #fff;
}
.top_button::before {
    color: #ffffff;
    content: "\f176";
    font-family: "FontAwesome";
    font-size: 18px;
    line-height: 25px;
}
/* Flexslider */

#index .spinner {
    background: url(../img/codezeel/loading.gif) no-repeat center center #ffffff;
    width: 100%;
    position: fixed;
    height: 100%;
    top: 0;
    left: 0;
    z-index: 100000;
}




</style>
<div class="container-fluid">
    <div class="section-header">
        <h3>Deal Of The Week</h3>
    </div>
</div>
    <section class="special-products mt-100">
        {{-- <h1 class="h1 products-section-title text-uppercase">
            
        </h1> --}} 
        <div class="special-products-wrapper">
            <div class="products">
                 <!-- Define Number of product for SLIDER -->
                <ul id="special-carousel" class="cz-carousel product_list owl-carousel 
                owl-theme" style="opacity: 1; display: block;">
                     <div class="owl-wrapper-outer">
                        <div class="owl-wrapper" style="width: 6140px; left: 0px; display: block;">
                            @foreach ($products as $product)
                            
                            @include('public.products.partials.spcial_product_card')
                            @endforeach

                        </div>
                    </div>

                    <div class="owl-controls clickable">
                        <div class="owl-pagination">
                            <div class="owl-page active">
                                <span class=""></span>
                            </div>
                            <div class="owl-page">
                                <span class=""></span>
                            </div>
                            <div class="owl-page">
                                <span class=""></span>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>
        </div>
    </section>
  
<script>
// Set the date we're counting down to
    var countDownDate = new Date("Jan 5, 2021 15:37:25").getTime();
    
    // Update the count down every 1 second
    var x = setInterval(function() {
    
      // Get today's date and time
      var now = new Date().getTime();
        
      // Find the distance between now and the count down date
      var distance = countDownDate - now;
        
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
      // Output the result in an element with id="countDownTimer"
      //document.getElementById("countDownTimer").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
      
      
      var a = document.getElementsByClassName("counterDayID");
      a[0].innerHTML =  days;
      var b = document.getElementsByClassName("counterHourID");
      b[0].innerHTML =  hours;
      var c = document.getElementsByClassName("counterMinutesID");
      c[0].innerHTML =  minutes;
      var d = document.getElementsByClassName("counterSecID");
      d[0].innerHTML =  seconds;
      
      var e = document.getElementsByClassName("counterDayID");
      e[1].innerHTML =  days;
      var f = document.getElementsByClassName("counterHourID");
      f[1].innerHTML =  hours;
      var g = document.getElementsByClassName("counterMinutesID");
      g[1].innerHTML =  minutes;
      var h = document.getElementsByClassName("counterSecID");
      h[1].innerHTML =  seconds;
        
      // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("countDownTimer").innerHTML = "EXPIRED";
      }
    }, 1000);
</script>