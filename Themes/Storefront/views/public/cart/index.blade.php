
@extends('public.layout')

@section('title', trans('storefront::cart.cart'))

@push('meta')
    <meta name="cart-has-shipping-method" content="{{ $cart->hasShippingMethod() }}">
@endpush

@section('content')
    <div class="row">
        <div class="cart-list-wrapper clearfix">
            @if ($cart->isEmpty())
                <h2 class="text-center">{{ trans('storefront::cart.your_cart_is_empty') }}</h2>
            @else
                <div class="col-md-8">
                    <div class="box-wrapper clearfix">
                        <div class="box-header">
                            <h4>{{ trans('storefront::cart.cart') }}</h4>
                        </div>

                        <div class="cart-list">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        @foreach ($cart->items() as $cartItem)
                                            <tr class="cart-item">
                                                <td>

                                                    @if (! $cartItem->product->base_image->exists)
                                                        <div class="image-placeholder">
                                                            <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                    @else
                                                        <div class="image-holder">
                                                            <img src="{{ $cartItem->product->base_image->path }}">
                                                        </div>
                                                    @endif
                                                </td>

                                                <td>
                                                    <h5>
                                                        <a href="{{ route('products.show', $cartItem->product->slug) }}">{{ $cartItem->product->name }}</a>
                                                    </h5>

                                                    <div class="option">
                                                        @php $additional_custom_price    = 0; @endphp
                                                        @foreach ($cartItem->options as $option)
                                                            <span>{{ $option->name }}: <span>{{ $option->values->implode('label', ', ') }}</span></span>
                                                            @php

                                                                $additional_custom_price    = 0;
                                                             $additional_custom_price   =   $option->values->implode('price', ', '); @endphp
                                                            @php
                                                                $valueName  =   $option->values->implode('label', ', ');
                                                                $getOptionColorImgs =   DB::table('option_images')
                                                                    ->where('product_id',$cartItem->product->id )
                                                                    ->where('option_value_tr', $valueName)
                                                                    ->where('option_id',$option->id)->first();
                                                                    if(isset($getOptionColorImgs)){
                                                                        $maxQty =   $getOptionColorImgs->option_color_qty;
                                                                    }
                                                            @endphp
                                                        @endforeach
                                                    </div>
                                                </td>

                                                <td>
                                                    <label>{{ trans('storefront::cart.price') }}:</label>
                                                    <span>{{ $cartItem->unitPrice()->convertToCurrentCurrency( $cartItem->product->id )->format() }} + {{ number_format((float)$additional_custom_price, 2, '.', '') }}</span>
                                                </td>

                                                <td class="clearfix">
                                                    <div class="quantity pull-left clearfix">
                                                        <div class="input-group-quantity pull-left clearfix">
                                                            <input type="text" name="qty" value="{{ $cartItem->qty }}" class="input-number input-quantity pull-left {{ "qty-{$loop->index}"  }}" min="1" max="@if(isset($maxQty)){{ $maxQty }}@else{{ $cartItem->product->manage_stock ? $cartItem->product->qty : '' }}@endif">

                                                            <span class="pull-left btn-wrapper">
                                                                <button type="button" class="btn btn-number btn-plus" data-type="plus"> + </button>
                                                                <button type="button" class="btn btn-number btn-minus" data-type="minus" {{ $cartItem->qty === 1 ? 'disabled' : '' }}> &#8211; </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </td>
                                                @php
                                                    $additional_custom_price    =   number_format((float)$additional_custom_price, 2, '.', '');
                                                 $valueTotal =  $cartItem->total()->convertToCurrentCurrency( $cartItem->product->id )->format();$valueTotal = ltrim($valueTotal,'$'); $valueTotal = (float)$valueTotal + $additional_custom_price; @endphp
                                                <td>
                                                    <label>{{ trans('storefront::cart.total') }}:</label>
                                                    <span>{{ $valueTotal  }}</span>
                                                </td>

                                                <td>
                                                    <button data-toggle="tooltip" data-placement="top" title="{{ trans('storefront::cart.update') }}" class="btn-update" data-id={{ encrypt($cartItem->id) }} data-quantity-field="{{ ".qty-{$loop->index}" }}">
                                                        <i class="fa fa-refresh" aria-hidden="true"></i>
                                                    </button>

                                                    <form method="POST" action="{{ route('cart.items.destroy', encrypt($cartItem->id)) }}" onsubmit="return confirm('{{ trans('cart::messages.confirm') }}');">
                                                        {{ csrf_field() }}
                                                        {{ method_field('delete') }}

                                                        <button type="submit" class="btn-close" data-toggle="tooltip" data-placement="top" title="{{ trans('storefront::cart.remove') }}">
                                                            &times;
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="cart-list-bottom">
                            <form method="POST" action="{{ route('cart.coupon.store') }}" id="coupon-apply-form" class="clearfix">
                                {{ csrf_field() }}

                                <div class="form-group pull-left">
                                    <input type="text" name="coupon" class="form-control" id="coupon" value="{{ old('coupon') }}">

                                    <button type="submit" class="btn btn-primary" id="coupon-apply-submit" data-loading>
                                        {{ trans('storefront::cart.apply_coupon') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="order-review cart-list-sidebar">
                        <div class="cart-total">
                            <h3>{{ trans('storefront::cart.cart_totals') }}</h3>

                            <span class="item-amount">
                                {{ trans('storefront::cart.subtotal') }}
                                <span>{{ $cart->subTotal()->convertToCurrentCurrency( 'subTotal' )->format() }}</span>
                            </span>

                            @if ($cart->hasAvailableShippingMethod())
                                <div class="available-shipping-methods">
                                    <span>{{ trans('storefront::cart.shipping_method') }}</span>

                                    <div class="form-group">
                                        @foreach ($cart->availableShippingMethods() as $name => $shippingMethod)
                                        @if(($name == 'free_shipping' && $cart->total()->amount >= 500) || $name == 'local_pickup' || ($name == 'flat_rate' && $cart->total()->amount < 500))
                                            <div class="radio">
                                                <input type="radio" name="shipping_method" class="shipping-method" value="{{ $name }}" id="{{ $name }}" {{ $cart->shippingMethod()->name() === $shippingMethod->name || $loop->first ? 'checked' : '' }}>
                                                <label for="{{ $name }}">{{ $shippingMethod->label }}</label>
                                                <span class="pull-right">{{ $shippingMethod->cost->convertToCurrentCurrency( 'subTotal' )->format() }}</span>
                                            </div>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>

                                @if ($errors->has('shipping_method'))
                                    <span class="error-message text-center">
                                        {!! $errors->first('shipping_method', '<span class="help-block">:message</span>') !!}
                                    </span>
                                @endif
                            @endif

                            @if ($cart->hasCoupon())
                                <span class="item-amount">
                                    {{ trans('storefront::cart.coupon') }} (<span class="coupon-code">{{ $cart->coupon()->code() }}</span>)
                                    <span id="coupon-value">&#8211;{{ $cart->coupon()->value()->convertToCurrentCurrency( 'subTotal' )->format() }}</span>
                                </span>
                            @endif

                            @foreach ($cart->taxes() as $tax)
                                <span class="item-amount">
                                    {{ $tax->name() }}
                                    <span>{{ $tax->amount()->convertToCurrentCurrency( 'subTotal' )->format() }}</span>
                                </span>
                            @endforeach

                            <span class="total">
                                {{ trans('storefront::cart.total') }}
                                <span id="total-amount">{{ $cart->total()->convertToCurrentCurrency( 'subTotal' )->format() }}</span>
                            </span>

                            @if ($cart->hasNoAvailableShippingMethod())
                                <span class="error-message text-center">{{ trans('storefront::cart.no_shipping_method_is_available') }}</span>
                            @endif

                            <a href="{{ route('checkout.create') }}" class="btn btn-primary btn-checkout {{ $cart->hasNoAvailableShippingMethod() ? 'disabled' : '' }}" data-loading>
                                {{ trans('storefront::cart.checkout') }}
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    @include('public.products.partials.landscape_products', [
        'title' => trans('storefront::product.you_might_also_like'),
        'products' => $cart->crossSellProducts()
    ])
@endsection
