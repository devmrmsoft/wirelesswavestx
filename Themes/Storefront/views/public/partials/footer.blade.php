{{-- <footer class="footer">
    <div class="container">
        <div class="footer-top p-tb-50 clearfix">
            <div class="row">
                <div class="col-md-4">
                    <a href="{{ route('home') }}" class="footer-logo">
                        @if (is_null($footerLogo))
                            <h2>{{ setting('store_name') }}</h2>
                        @else
                            <img src="{{ $footerLogo }}" class="img-responsive" alt="footer-logo">
                        @endif
                    </a>

                    <div class="clearfix"></div>

                    <p class="footer-brief">{{ setting('store_tagline') }}</p>

                    @if (setting('store_phone') || setting('store_email') || setting('storefront_footer_address'))
                        <div class="contact">
                            <ul class="list-inline">
                                @if (setting('store_phone'))
                                    <li>
                                        <i class="fa fa-phone-square" aria-hidden="true"></i>
                                        <span class="contact-info">{{ setting('store_phone') }}</span>
                                    </li>
                                @endif

                                @if (setting('store_email'))
                                    <li>
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <span class="contact-info" style="display: none;">{{ setting('store_email') }}</span>
                                        <span class="contact-info">info@wireless.com</span>
                                    </li>
                                @endif

                                @if (setting('storefront_footer_address'))
                                    <li>
                                        <i class="fa fa-location-arrow" aria-hidden="true"></i>
                                        <span class="contact-info" style="display: none;">{{ setting('storefront_footer_address') }}</span>
                                        <span class="contact-info">7800 Harwin Drive Suit A4,<br> Houston Texas 77036</span>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="col-md-8">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="links">
                                <div class="mobile-collapse">
                                    <h4>{{ trans('storefront::account.links.my_account') }}</h4>
                                </div>

                                <ul class="list-inline">
                                    <li><a href="{{ route('account.dashboard.index') }}">{{ trans('storefront::account.links.dashboard') }}</a></li>
                                    <li><a href="{{ route('account.orders.index') }}">{{ trans('storefront::account.links.my_orders') }}</a></li>
                                    <li><a href="{{ route('account.reviews.index') }}">{{ trans('storefront::account.links.my_reviews') }}</a></li>
                                    <li><a href="{{ route('account.profile.edit') }}">{{ trans('storefront::account.links.my_profile') }}</a></li>

                                    @auth
                                        <li><a href="{{ route('logout') }}">{{ trans('storefront::account.links.logout') }}</a></li>
                                    @endauth
                                </ul>
                            </div>
                        </div>
                    </div>

                    @if ($footerMenu->isNotEmpty())
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="links">
                                    <div class="mobile-collapse">
                                        <h4>{{ setting('storefront_footer_menu_title') }}</h4>
                                    </div>

                                    <ul class="list-inline">
                                        @foreach ($footerMenu as $menuItem)
                                            <li><a href="{{ $menuItem->url() }}">{{ $menuItem->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        @if ($socialLinks->isNotEmpty())
            <div class="footer-middle p-tb-30 clearfix">
                <ul class="social-links list-inline">
                    @foreach ($socialLinks as $icon => $link)
                        @if (! is_null($link))
                            <li><a href="{{ $link }}"><i class="fa fa-{{ $icon }}" aria-hidden="true"></i></a></li>
                        @endif
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="footer-bottom p-tb-20 clearfix">
        <div class="container">
            <div class="copyright text-center">
                {!! $copyrightText !!}
            </div>
        </div>
    </div>
</footer>
--}}

    <style type="text/css">
                
        .__footer00 {
            background-color: #323f48;
            padding-top: 35px;
            padding-bottom: 35px;
            display: block;
            justify-content: center;
            align-items: center;
            border-bottom: 2px solid #3e4b54;
        }
        .__footer01 {
            background-color: #323f48;
            padding-top: 15px;
            padding-bottom: 15px;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        ._footcol1 a img {
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .__footer00-col {
            display: block;
            justify-content: center;
            align-items: center;
        }
        ._footcol1 p {
            color: #aeb5bd;
            font-weight: 600;
            margin: 0;
            line-height: 24px;
            font-size: 14px;
        }
        ._navfooter h1 {
            margin: 0;
            padding: 0;
            font-size: 18px;
            padding-bottom: 10px;
            color: #fff;
            margin-top: 15px;
        }
        ._navfooter .nav li a {
            padding: 4px 0;
            color: #aeb5bd;
            font-weight: 600;
            font-size: 12px;
        }
        ._navfooter .nav li a:hover{
            background-color: transparent;
        }
        .__footer01-col {
            display: flex;
            justify-content: center;
            align-items: center;
        }
        ._blow-footer p {
            margin: 0;
            padding: 0;
            font-size: 13px;
            color: #aeb5bd;
        }
        ._blow-footer p span {
            color: #337ab7;
        }
        ._blow-footer a i {
            color: #aeb5bd;
            font-size: 18px;
            padding-right: 15px;
        }
        ._blow-footer.blow-footer-anchor{
            padding-top: 4px;
        }
        .newsletter-form{
            padding-top:10px;
        }
    </style>

    <footer>
        <div class="__footer00">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 __footer00-col">
                        <div class="_footcol1">
                            <a href="{{ route('home') }}" class="footer-logo">
                                @if (is_null($footerLogo))
                                    <h2>{{ setting('store_name') }}</h2>
                                @else
                                    <img src="{{ $footerLogo }}" class="img-responsive" width="205" height="93" alt="Footer Logo">
                                @endif
                            </a>
                            <p>Welcome to Wireless Waves; your one-stop Wireless Solution. Established in 2015, we take pride in helping our customer base of small wireless retailers with access to wide variety of products.</p>
                        </div>
                    </div>
                    {{-- <div class="col-md-2 __footer00-col">
                        <div class="_navfooter">
                            <h1>My Account</h1>
                            <ul class="nav">
                                <li><a href="#">Categories</a></li>
                                <li><a href="#">Dashboard</a></li>
                                <li><a href="#">My Orders</a></li>
                                <li><a href="#">My Reviews</a></li>
                                <li><a href="#">My Profile</a></li>
                            </ul>
                        </div>
                    </div> --}}
                    <div class="col-md-3 __footer00-col">
                        <div class="_navfooter">
                            <h1>Information</h1>
                            <ul class="nav">
                                <li><a href="#">New Products</a></li>
                                <li><a href="#">Best Sellers</a></li>
                                <li><a href="#">Our Stores</a></li>
                                <li><form method="GET" action="#" class="newsletter-form" style="display:flex;">
        <input type="email" name="EMAIL" id="email" placeholder=" Subscribe to our newsletter" class="news-field" style="width:230px;">
        <button type="submit" class="button"><i class="fa fa-send pull-left" id="refresh-captcha" aria-hidden="true"></i></button>
      </form></li>
                            </ul>
                        </div>
                    </div>
                {{--<div class="col-md-3 __footer00-col">
                        <div class="_navfooter">
                            <h1>Quick Links</h1>
                            <ul class="nav">
                                <li><a href="#">Blogs</a></li>
                                <li><a href="{{ route('contact.create') }}">Contact us</a></li>
                                <li><a href="#">About us</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms Of Use</a></li>
                            </ul>
                        </div>
                    </div> --}}
                    

                    @if ($footerMenu->isNotEmpty())
                        <div class="col-md-3 __footer00-col">
                            <div class="_navfooter">
                                <h1>{{ setting('storefront_footer_menu_title') }}</h1>
                                <ul class="nav">
                                    @foreach ($footerMenu as $menuItem)
                                        <li><a href="{{ $menuItem->url() }}">{{ $menuItem->name }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-3 __footer00-col">
                        <div class="_navfooter">
                            <h1>My Account</h1>
                            <ul class="nav">
                                <li><a href="#">Dashboard</a></li>
                                <li><a href="#">My Orders</a></li>
                                <li><a href="#">My Reviews</a></li>
                                <li><a href="#">My Profile</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <footer class="footer-below">
        <div class="__footer01">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 __footer01-col">
                        <div class="_blow-footer">
                            <p>Copyright &copy; <a href="{{ route('home') }}"><span>Wireless Waves</span></a> 2020. All rights reserved.</p>
                        </div>
                    </div>
                    <div class="col-md-3 __footer01-col">
                        <div class="_blow-footer blow-footer-anchor">
                            <a href="#" class=""><i class="fab fa-cc-paypal"></i></a>
                            <a href="#" class=""><i class="fab fa-cc-visa"></i></a>
                            <a href="#" class=""><i class="fab fa-cc-mastercard"></i></a>
                            <a href="#" class=""><i class="fab fa-cc-amex"></i></a>
                        </div>
                    </div>
                    <div class="col-md-3 __footer01-col">
                        <div class="_blow-footer">
                            <p>Designed By MRM-Soft</p>
                        </div>
                    </div>
                    <div class="col-md-2 __footer01-col">
                        <div class="_blow-footer">
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://kit.fontawesome.com/906be8c28d.js" crossorigin="anonymous"></script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5f6e39d2f0e7167d0013d262/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->