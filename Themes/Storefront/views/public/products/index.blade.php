@extends('public.layout')

@section('title')
    @if (request()->has('query'))
        {{ trans('storefront::products.search_results_for') }}: "{{ request('query') }}"
    @else
        {{ trans('storefront::products.shop') }}
    @endif
@endsection

@section('content')
    <section class="product-list">
        <div class="row">
            @include('public.products.partials.filter')

            <div class="col-md-9 col-sm-12 mt-30">
                <div class="product-list-header clearfix">
                    <div class="search-result-title pull-left">
                        <?php if(isset($_GET['category']))
                        {
                            $catName    =   DB::table('categories')->
                                                where('slug',  $_GET['category'])->first();
                            $catName = DB::table('categories')
                                            ->join('category_translations', 'category_translations.category_id', '=', 'categories.id')
                                            ->where('slug', $_GET['category'])
                                            ->first();
                        } ?>
                        @if (request()->has('query'))
                            <h3>{{ trans('storefront::products.search_results_for') }}: "{{ request('query') }}"</h3>
                        @else
                            <h3>{{ $catName->name }}</h3>
                        @endif

                        <span>{{ intl_number($products->total()) }} {{ trans_choice('storefront::products.products_found', $products->total()) }}</span>
                    </div>

                    <div class="search-result-right pull-right">
                        <ul class="nav nav-tabs">
                            <li class="view-mode {{ ($viewMode = request('viewMode', 'grid')) === 'grid' ? 'active' : '' }}">
                                <a href="{{ $viewMode === 'grid' ? '#' : request()->fullUrlWithQuery(['viewMode' => 'grid']) }}" title="{{ trans('storefront::products.grid_view') }}">
                                    <i class="fa fa-th-large" aria-hidden="true"></i>
                                </a>
                            </li>

                            <li class="view-mode {{ $viewMode === 'list' ? 'active' : '' }}">
                                <a href="{{ $viewMode === 'list' ? '#' : request()->fullUrlWithQuery(['viewMode' => 'list']) }}" title="{{ trans('storefront::products.list_view') }}">
                                    <i class="fa fa-th-list" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>

                        <div class="form-group">
                            <select class="custom-select-black" onchange="location = this.value">
                                <option value="{{ request()->fullUrlWithQuery(['sort' => 'relevance']) }}" {{ ($sortOption = request()->query('sort')) === 'relevance' ? 'selected' : '' }}>
                                    {{ trans('storefront::products.sort_options.relevance') }}
                                </option>

                                <option value="{{ request()->fullUrlWithQuery(['sort' => 'alphabetic']) }}" {{ ($sortOption = request()->query('sort')) === 'alphabetic' ? 'selected' : '' }}>
                                    {{ trans('storefront::products.sort_options.alphabetic') }}
                                </option>

                                <option value="{{ request()->fullUrlWithQuery(['sort' => 'topRated']) }}" {{ $sortOption === 'topRated' ? 'selected' : '' }}>
                                    {{ trans('storefront::products.sort_options.top_rated') }}
                                </option>

                                <option value="{{ request()->fullUrlWithQuery(['sort' => 'latest']) }}" {{ $sortOption === 'latest' ? 'selected' : '' }}>
                                    {{ trans('storefront::products.sort_options.latest') }}
                                </option>

                                <option value="{{ request()->fullUrlWithQuery(['sort' => 'priceLowToHigh']) }}" {{ $sortOption === 'priceLowToHigh' ? 'selected' : '' }}>
                                    {{ trans('storefront::products.sort_options.price_low_to_high') }}
                                </option>

                                <option value="{{ request()->fullUrlWithQuery(['sort' => 'priceHighToLow']) }}" {{ $sortOption === 'priceHighToLow' ? 'selected' : '' }}>
                                    {{ trans('storefront::products.sort_options.price_high_to_low') }}
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <select class="custom-select-black" onchange="location = this.value">
                                <option value="{{ request()->fullUrlWithQuery(['productsPerPage' => '10']) }}" {{ ($sortOption = request()->query('productsPerPage')) === '10' ? 'selected' : '' }}>10</option>
                                <option value="{{ request()->fullUrlWithQuery(['productsPerPage' => '20']) }}" {{ ($sortOption = request()->query('productsPerPage')) === '20' ? 'selected' : '' }}>20</option>
                                <option value="{{ request()->fullUrlWithQuery(['productsPerPage' => '30']) }}" {{ ($sortOption = request()->query('productsPerPage')) === '30' ? 'selected' : '' }}>30</option>
                                <option value="{{ request()->fullUrlWithQuery(['productsPerPage' => '40']) }}" {{ ($sortOption = request()->query('productsPerPage')) === '40' ? 'selected' : '' }}>40</option>
                                <option value="{{ request()->fullUrlWithQuery(['productsPerPage' => '50']) }}" {{ ($sortOption = request()->query('productsPerPage')) === '50' ? 'selected' : '' }}>50</option>
                                <option value="{{ request()->fullUrlWithQuery(['productsPerPage' => '60']) }}" {{ ($sortOption = request()->query('productsPerPage')) === '60' ? 'selected' : '' }}>60</option>
                                <option value="{{ request()->fullUrlWithQuery(['productsPerPage' => '70']) }}" {{ ($sortOption = request()->query('productsPerPage')) === '70' ? 'selected' : '' }}>70</option>
                                <option value="{{ request()->fullUrlWithQuery(['productsPerPage' => '80']) }}" {{ ($sortOption = request()->query('productsPerPage')) === '80' ? 'selected' : '' }}>80</option>
                                <option value="{{ request()->fullUrlWithQuery(['productsPerPage' => '90']) }}" {{ ($sortOption = request()->query('productsPerPage')) === '90' ? 'selected' : '' }}>90</option>
                                <option value="{{ request()->fullUrlWithQuery(['productsPerPage' => '100']) }}" {{ ($sortOption = request()->query('productsPerPage')) === '100' ? 'selected' : '' }}>100</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="product-list-result clearfix">
                    <div class="tab-content">
                        <div id="grid-view" class="tab-pane {{ ($viewMode = request('viewMode', 'grid')) === 'grid' ? 'active' : '' }}">
                            <div class="row">
                                <div class="grid-products separator">
                                    @if ($viewMode === 'grid')
                                        @forelse ($products as $product)
                                            @include('public.products.partials.product_card')
                                        @empty
                                            <h3>{{ trans('storefront::products.no_products_were_found') }}</h3>
                                        @endforelse
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div id="list-view" class="tab-pane {{ $viewMode === 'list' ? 'active' : '' }}">
                            @if ($viewMode === 'list')
                                @forelse ($products as $product)
                                    @include('public.products.partials.list_view_product_card')
                                @empty
                                    <h3>{{ trans('storefront::products.no_products_were_found') }}</h3>
                                @endforelse
                            @endif
                        </div>
                    </div>
                </div>

                <div class="pull-right">
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection
