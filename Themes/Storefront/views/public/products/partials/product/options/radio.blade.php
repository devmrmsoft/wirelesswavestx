@push('styles')

<style type="text/css">

</style>

@endpush

<div class="form-group row {{ $errors->has("options.{$option->id}") ? 'has-error' : '' }}">
    <label class="col-sm-12 mb-20">
        {{ option_name($option) }}
    </label>

    <div class="col-sm-12 row">
        @foreach ($option->values as $value)
        <div class="col-md-3 p-0">
            
            <div class="radio check-this radio-option-image">
                <input type="radio"
                    name="options[{{ $option->id }}]"
                    value="{{ $value->id }}"
                    id="option-{{ $option->id }}-value-{{ $value->id }}"
                    {{ old("options.{$option->id}") == $value->id ? 'checked' : '' }}
                >

                <?php

                    $getOptionmultipleImages    =   DB::table('option_images')
                                        ->where('option_id',$option->id )
                                        ->where('option_value_id',$value->id)->first();
                    if(isset($getOptionmultipleImages->option_image_path)){
                        if($getOptionmultipleImages->option_image_path != null){
                            $multipleImages =   explode('|', $getOptionmultipleImages->option_image_path);
                            //echo $multipleImages[0];
                        }
                    }

                    $getOptionColorImgs =   DB::table('option_images')
                                            ->where('product_id',$product->id )
                                            ->where('option_value_tr',custom_option_value_tr($product, $value))->first();
                    if($getOptionColorImgs != NULL){
                ?>
                <img class="color_image_path" id="{{ $option->id }}" src="<?php
                    if(isset($multipleImages[0])){ if($multipleImages[0] != null){ echo asset('/public/assets/options/color/images/').'/'.$multipleImages[0]; } }else{
                ?>{{ asset('/public/assets/options/color/images/') }}/{{ $getOptionColorImgs->option_image_path }}<?php } ?>" data-id="option-{{ $option->id }}-value-{{ $value->id }}" data-sku="{{ $getOptionColorImgs->option_color_sku }}" onclick="loadColorImages(this, {{ $option->id }}, {{ $value->id }});changeImagePath(this);">
            <?php  }else{  $text_center = 'text-center'; } ?>
                <label for="option-{{ $option->id }}-value-{{ $value->id }}" class="p-l-0 option-color-label tex @isset($text_center) {{ $text_center }} @endisset">
                    {{ option_value($product, $value) }}
                </label>
            </div>
        </div>
        @endforeach

        {!! $errors->first("options.{$option->id}", '<span class="help-block">:message</span>') !!}
    </div>
</div>
