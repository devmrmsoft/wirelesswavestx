<div class="col-lg-5 col-md-6 col-sm-5 col-xs-7" id="load_color_images">
    <div class="product-image">
        <div class="base-image">
            @if (! $product->base_image->exists)
                <div class="image-placeholder">
                    <i class="fa fa-picture-o"></i>
                </div>
            @else
                <a class="base-image-inner" href="{{ $product->base_image->path }}">
                    <img id="base_image_path" src="{{ $product->base_image->path }}">
                    <span><i class="fa fa-search-plus" aria-hidden="true"></i></span>
                </a>
            @endif

            @foreach ($product->additional_images as $additionalImage)
                @if (! $additionalImage->exists)
                    <div class="image-placeholder">
                        <i class="fa fa-picture-o"></i>
                    </div>
                @else
                    <a class="base-image-inner" href="{{ $additionalImage->path }}">
                        <img src="{{ $additionalImage->path }}">
                        <span><i class="fa fa-search-plus" aria-hidden="true"></i></span>
                    </a>
                @endif
            @endforeach
        </div>

        <div class="additional-image">
            @if (! $product->base_image->exists)
                <div class="image-placeholder">
                    <i class="fa fa-picture-o"></i>
                </div>
            @else
                <div class="thumb-image">
                    <img src="{{ $product->base_image->path }}">
                </div>
            @endif

            @foreach ($product->additional_images as $additionalImage)
                @if (! $additionalImage->exists)
                    <div class="image-placeholder">
                        <i class="fa fa-picture-o"></i>
                    </div>
                @else
                    <div class="thumb-image">
                        <img src="{{ $additionalImage->path }}">
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>

@push('scripts')

<script type="text/javascript">    
    function loadColorImages(el, option_id, value_id){
        
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        
        $.ajax({
            url: '{{ route("get-product-color-images") }}',
            type: 'GET',
            data: {_token: CSRF_TOKEN, option_id: option_id, value_id: value_id},
            dataType: 'JSON',
            success: function(data){
            
            
            
            //console.log(data.html);
            $('#load_color_images').html(data.html);
            
             $('.base-image').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
              asNavFor: '.additional-image'
            });
            $('.additional-image').slick({
              slidesToShow: 3,
              slidesToScroll: 1,
              asNavFor: '.base-image',
              dots: true,
              centerMode: true,
              focusOnSelect: true
            });
            }
        });
    }
</script>

@endpush