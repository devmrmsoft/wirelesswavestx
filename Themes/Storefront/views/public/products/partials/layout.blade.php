<!DOCTYPE html>
<html lang="{{ locale() }}">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>
            @hasSection('title')
                @yield('title') - {{ setting('store_name') }}
            @else
                {{ setting('store_name') }}
            @endif
        </title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        @stack('meta')

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Rubik:400,500" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato|Raleway|Roboto|Simonetta&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

        @if (is_rtl())
            <link rel="stylesheet" href="{{ v(Theme::url('public/css/app.rtl.css')) }}">
        @else
            <link rel="stylesheet" href="{{ v(Theme::url('public/css/app.css')) }}">
            <link rel="stylesheet" href="{{ v(Theme::url('public/css/custom.css')) }}">
        @endif

        <link rel="shortcut icon" href="{{ $favicon }}" type="image/x-icon">

        @stack('styles')

        {!! setting('custom_header_assets') !!}

        <script>
            window.FleetCart = {
                csrfToken: '{{ csrf_token() }}',
                stripePublishableKey: '{{ setting("stripe_publishable_key") }}',
                langs: {
                    'storefront::products.loading': '{{ trans("storefront::products.loading") }}',
                },
            };
        </script>

        @stack('globals')

        @routes

        <style type="text/css">
            .newsletter-suscription-form-input{
                font-size: 12px !important;
                color: #aeaeae !important;
                padding: 10px 0 !important;
                margin-bottom: 15px !important;
                background: transparent !important;
                border: none !important;
                border-bottom: 1px solid #aeaeae !important;
                height: 44px!important;
            }
            .newsletter-subscription-form-button-subscribe{
                text-transform: uppercase;
                -webkit-transition-duration: 0.2s;
                transition-duration: 0.2s;
                -webkit-transition-timing-function: ease;
                transition-timing-function: ease;
                border: 1px solid #fff;
                background: #fff;
                color: #fff;
                font-size: 12px;
                font-family: "PT Sans",Arial,Helvetica,sans-serif;
                -webkit-transition-duration: 0.2s;
                transition-duration: 0.2s;
                -webkit-transition-timing-function: ease;
                transition-timing-function: ease;
                padding: 10px 20px;
                color: #383838;
                width: auto;
                height: 44px;
                margin-bottom: 10px;
            }
        </style>
    </head>

    <body class="{{ $theme }} {{ storefront_layout() }} {{ is_rtl() ? 'rtl' : 'ltr' }}">
        <!--[if lt IE 8]>
            <p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="main">
            <div class="wrapper">
                @include('public.partials.sidebar')
                @include('public.partials.top_nav')
                @include('public.partials.header')
                @include('public.partials.navbar')

                <div class="content-wrapper clearfix {{ request()->routeIs('cart.index') ? 'cart-page' : '' }}">
                    @isset($slider)
                        @unless (is_null($slider))
                            @if (storefront_layout() === 'default')
                        
                                <div class="col-lg-12 col-lg-offset-3 col-md-9 col-md-offset-3">
                                    <div class="row home-slider-row">
                                        @include('public.home.sections.slider')
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            @elseif (storefront_layout() === 'slider_with_banners')
                                <div class="row home-slider-row">
                                    
                                        @include('public.home.sections.slider')
                                    
                    
                                    <!-- <div class="col-md-3 hidden-sm hidden-xs">
                                        @include('public.home.sections.slider_banners')
                                    </div> -->
                                </div>
                            @endif
                        @endunless
                    @endisset
                    <div class="container">
                        @include('public.partials.breadcrumb')

                        @unless (request()->routeIs('home') || request()->routeIs('login') || request()->routeIs('register') || request()->routeIs('reset') || request()->routeIs('reset.complete'))
                            @include('public.partials.notification')
                        @endunless

                        @yield('content')
                    </div>
                </div>

                @if (setting('storefront_brand_section_enabled') && $brands->isNotEmpty() && request()->routeIs('home'))
                    <div class="container">
                        <div class="section-header">
                            <h3>Our Brands</h3>
                        </div>
                    </div>
                    <section class="brands-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="brands">
                                    @foreach ($brands as $brand)
                                        <div class="col-md-3">
                                            <a href="@if($brand->id == '11'){{url('products?category=phones-model-iphone')}}@elseif($brand->id == '12'){{url('products?category=phones-model-samsung-galaxy')}}@elseif($brand->id == '157'){{url('products?category=phones-model-lg')}}@elseif($brand->id == '546'){{url('products?category=phones-carrier-t-mobile')}}@elseif($brand->id == '545'){{url('products?category=')}}@endif" class="img-responsive">
                                                <div class="brand-image">
                                                    <img src="{{ $brand->path }}">
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </section>
                @endif

                <!-- <section class="brands-wrapper" style="background: #e2e3e4;">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-6">
                                    <h4>Subscribe to Get Exclusive Offers & Deals to Your Inbox!</h4>
                                </div>
                                <div class="col-md-4">
                                    <div style="display: flex;margin-top: -10px;"><input name="email" id="email" type="email" class="newsletter-suscription-form-input" placeholder="username@domain.com" style="padding-right: 10%;width: 50%;"><button type="submit" class="newsletter-subscription-form-button-subscribe" style="background-color: #e9420b;color: white;border: #e9420b;margin-left: 6%;"> Subscribe </button></div>
                                </div>
                            </div>
                        </div>
                    </section> -->
                @include('public.partials.footer_newsletter')
                @include('public.partials.new_designed_footer')

                <a class="scroll-top" href="#">
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </a>
            </div>

            @unless (json_decode(Cookie::get('cookie_bar_accepted')))
                <div class="cookie-bar-wrapper">
                    <div class="container">
                        <div class="cookie clearfix">
                            <div class="cookie-text">
                                <span>{{ trans('storefront::layout.cookie_bar_text') }}</span>
                            </div>

                            <div class="cookie-button">
                                <button type="submit" class="btn btn-primary btn-cookie">
                                    {{ trans('storefront::layout.got_it') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endunless
        </div>

        @include('public.partials.quick_view_modal')

        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
        <script src="{{ v(Theme::url('public/js/app.js')) }}"></script>

        @stack('scripts')

        {!! setting('custom_footer_assets') !!}
        <script type="text/javascript">
            
            function changeImagePath(diss){
                alert('ok');
                        var newSrc  =   $(diss).attr('src');
                        var sku  =   $(diss).attr('color-sku');
                        console.log(newSrc);
                        $(diss).siblings('.label').trigger("click");
                        $("#base_image_path").attr('src',newSrc);
                        $('#sku').text(sku);
                        $(".slick-current > img").attr('src',newSrc);
                }
        </script>
    </body>
</html>
