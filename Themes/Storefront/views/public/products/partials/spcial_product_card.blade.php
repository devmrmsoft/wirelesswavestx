<a href="{{ route('products.show', $product->slug) }}" class="product-card">
    <div class="owl-item" style="width: 600px;">
        <li class="item">
            <div class="product-miniature js-product-miniature" data-id-product="27" data-id-product-attribute="205" itemscope="" itemtype="http://schema.org/Product">
                <div class="thumbnail-container col-xs-12 col-md-6">                   
                    <a href="#" class="thumbnail product-thumbnail">
                        <img src="{{ $product->base_image->path }}" alt="">
                        <img class="fliper_image img-responsive" src="{{ $product->base_image->path }}" data-full-size-image-url="{{ $product->base_image->path }}" alt="">
                    </a>
                    <ul class="product-flags">
                        <li class="on-sale">On sale!</li>
                        <li class="new">New</li>
                    </ul>
                </div>
            
                <div class="product-description col-xs-12 col-md-6">
                    <span class="h3 product-title" itemprop="name">
                        <a href="{{ route('products.show', $product->slug) }}">{{ $product->name }}</a>
                    </span>
                    <div class="comments_note">
                        <div class="star_content clearfix">
                             <div class="star star_on"></div>
                             <div class="star star_on"></div>
                             <div class="star star_on"></div>
                             <div class="star star_on"></div>
                             <div class="star star_on"></div>
                        </div>
                        
                    </div>

                    <div class="product-price-and-shipping">              
                         <span class="regular-price">$99.00</span>
                         <span itemprop="price" class="price">{{ product_price($product) }}</span>
                    </div>
                                      
                    
                                 
                    <div class="highlighted-informations">
                          
                        <div class="variant-links">
                              <a href="#" class="color" title="White" style="background-color: #ffffff">
                                <span class="sr-only">White</span>
                              </a>
                              <a href="#" class="color" title="Red" style="background-color: #E84C3D">
                                <span class="sr-only">Red</span>
                              </a>
                              <a href="#" class="color" title="Blue" style="background-color: #5D9CEC">
                                <span class="sr-only">Blue</span>
                              </a>
                            <span class="js-count count"></span>
                        </div>                 
                    </div>
                    
                    <div class="hurryup-text">
                        Hurry Up Deals!!
                    </div>

                    <div class="product-counter">
                        <div class="psproductcountdown buttons_bottom_block" data-to="1653170400000">
                            <div class="pspc-main days-diff-866 weeks-diff-123 ">
                                <div id="countDownTimer">
                                    
                                </div>
                                <div class="time days flip">
                                    <span class="count next top counterDayID" id="counterDayID" >866</span>
                                    <span class="label">days</span>
                                </div>
                                <div class="time hours flip">
                                    <span class="count next top counterHourID" id="counterHourID"  >08</span>
                                    <span class="label">hours</span>
                                </div>
                                <div class="time minutes flip">
                                    <span class="count next top counterMinutesID" id="counterMinutesID">02</span>
                                    <span class="label">min</span>
                                </div>
                                <div class="time seconds flip">
                                    <span class="count next top counterSecID" id="counterSecID">09</span>
                                    <span class="label">sec</span>
                                </div>
                            </div>
                            <input type="hidden" class="pspc-checker">
                        </div>
                    </div>
                    
                    <div class="product-actions">
                        <button type="submit" class="add-to-cart btn btn-primary pull-left" data-loading=""> Add to cart </button>
                    </div>
                </div>
            </div>
        </li>
    </div>
</a>
{{--
<a href="{{ route('products.show', $product->slug) }}" class="product-card">
    <div class="product-card-inner">
        <div class="product-image clearfix">
            <ul class="product-ribbon list-inline">
                @if ($product->isOutOfStock())
                    <li><span class="ribbon bg-red">{{ trans('storefront::product_card.out_of_stock') }}</span></li>
                @endif

                @if ($product->isNew())
                    <li><span class="ribbon bg-green">{{ trans('storefront::product_card.new') }}</span></li>
                @endif
            </ul>

            @if (! $product->base_image->exists)
                <div class="image-placeholder">
                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                </div>
            @else
                <div class="image-holder">
                    <img src="{{ $product->base_image->path }}">
                </div>
            @endif

            <div class="quick-view-wrapper" data-toggle="tooltip" data-placement="top" title="{{ trans('storefront::product_card.quick_view') }}">
                <button type="button" class="btn btn-quick-view" data-slug="{{ $product->slug }}">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                </button>
            </div>
        </div>

        <div class="product-content clearfix">
            <span class="product-price">{{ product_price($product) }}</span>
            <span class="product-name">{{ $product->name }}</span>
        </div>

        <div class="add-to-actions-wrapper">
            <form method="POST" action="{{ route('wishlist.store') }}">
                {{ csrf_field() }}

                <input type="hidden" name="product_id" value="{{ $product->id }}">

                <button type="submit" class="btn btn-wishlist" data-toggle="tooltip" data-placement="{{ is_rtl() ? 'left' : 'right' }}" title="{{ trans('storefront::product_card.add_to_wishlist') }}">
                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                </button>
            </form>

            @if ($product->options_count > 0)
                <button class="btn btn-default btn-add-to-cart" onClick="location = '{{ route('products.show', ['slug' => $product->slug]) }}'">
                    {{ trans('storefront::product_card.view_details') }}
                </button>
            @else
                <form method="POST" action="{{ route('cart.items.store') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                    <input type="hidden" name="qty" value="1">

                    <button class="btn btn-default btn-add-to-cart" {{ $product->isOutOfStock() ? 'disabled' : '' }}>
                        {{ trans('storefront::product_card.add_to_cart') }}
                    </button>
                </form>
            @endif

            <form method="POST" action="{{ route('compare.store') }}">
                {{ csrf_field() }}

                <input type="hidden" name="product_id" value="{{ $product->id }}">

                <button type="submit" class="btn btn-compare" data-toggle="tooltip" data-placement="{{ is_rtl() ? 'right' : 'left' }}" title="{{ trans('storefront::product_card.add_to_compare') }}">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i>
                </button>
            </form>
        </div>
    </div>
</a>

--}}