
    <?php
        $userID             =   Auth::user()->id;

        $updateUserRole     =   DB::table('user_roles')->where('user_id',$userID)
                                    ->where('role_id', '3')
                                    ->first();
        // dd($updateUserRole);
        /*foreach ($updateUserRole as $roleID) {
            // $roleID->user_id;
            $roleID->role_id;
        }*/
        //exit;
        if(isset($updateUserRole->user_id)){
            $userMeta           =   DB::table('user_meta')->where('user_id',$updateUserRole->user_id)->first();
            //dd($userMeta->tax_id);
        }
    ?>

@extends('public.account.layout')

@section('title', trans('storefront::account.links.my_profile'))

@section('account_breadcrumb')
    <li class="active">{{ trans('storefront::account.links.my_profile') }}</li>
@endsection
@section('content_right')
    <form method="POST" action="{{ route('account.profile.update') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('put') }}

        <div class="account-details">
            <div class="account clearfix">
                <h4>{{ trans('storefront::account.profile.account') }}</h4>

                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group {{ $errors->has('first_name') ? 'has-error': '' }}">
                            <label for="first-name">
                                {{ trans('storefront::account.profile.first_name') }}<span>*</span>
                            </label>

                            <input type="text" name="first_name" id="first-name" class="form-control" value="{{ old('first_name', $my->first_name) }}">

                            {!! $errors->first('first_name', '<span class="error-message">:message</span>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('last_name') ? 'has-error': '' }}">
                            <label for="last-name">
                                {{ trans('storefront::account.profile.last_name') }}<span>*</span>
                            </label>

                            <input type="text" name="last_name" id="last-name" class="form-control" value="{{ old('last_name', $my->last_name) }}">

                            {!! $errors->first('last_name', '<span class="error-message">:message</span>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                            <label for="">
                                {{ trans('storefront::account.profile.email') }}<span>*</span>
                            </label>

                            <input type="text" name="email" id="email" class="form-control" value="{{ old('email', $my->email) }}">

                            {!! $errors->first('email', '<span class="error-message">:message</span>') !!}
                        </div>
                        <?php if(isset($updateUserRole->user_id)){ ?>

                            <input type="hidden" name="update_user_meta_id" value="{{ $userMeta->id }}" >

                        <div class="form-group {{ $errors->has('business_name') ? 'has-error': '' }}">
                            <label for="business-name">Business Name<span>*</span></label>

                            <input type="text" name="business_name" value="{{ $userMeta->business_name }}" class="form-control" id="business-name">

                            {!! $errors->first('business_name', '<span class="error-message">:message</span>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('business_address') ? 'has-error': '' }}">
                            <label for="business-address">Business Address<span>*</span></label>

                            <input type="text" name="business_address" value="{{ $userMeta->business_address }}" class="form-control" id="business-address">

                            {!! $errors->first('business_address', '<span class="error-message">:message</span>') !!}
                        </div>
                        


                        <div class="form-group {{ $errors->has('shipping_address') ? 'has-error': '' }}">
                            <label for="shipping-address">Shipping Address<span>*</span></label>

                            <input type="text" name="shipping_address" value="{{ $userMeta->shipping_address }}" class="form-control" id="shipping-address">

                            {!! $errors->first('shipping_address', '<span class="error-message">:message</span>') !!}
                        </div>
                        
                        <!-- Add by Shahrukh -->
                        
                        <div class="form-group {{ $errors->has('city') ? 'has-error': '' }}">
                            <label for="city">City<span>*</span></label>

                            <input type="text" name="city" value="{{ $userMeta->city }}" class="form-control" id="city">

                            {!! $errors->first('city', '<span class="error-message">:message</span>') !!}
                        </div>
                        
                        <div class="form-group {{ $errors->has('state') ? 'has-error': '' }}">
                            <label for="state">State<span>*</span></label>

                            <input type="text" name="state" value="{{ $userMeta->state }}" class="form-control" id="state">

                            {!! $errors->first('state', '<span class="error-message">:message</span>') !!}
                        </div>
                        
                        <div class="form-group {{ $errors->has('zip') ? 'has-error': '' }}">
                            <label for="zip">Zip Code<span>*</span></label>

                            <input type="text" name="zip" value="{{ $userMeta->zip }}" class="form-control" id="zip">

                            {!! $errors->first('zip', '<span class="error-message">:message</span>') !!}
                        </div>


                        <div class="form-group {{ $errors->has('contact_number') ? 'has-error': '' }}">
                            <label for="contact-number">Contact number<span>*</span></label>

                            <input type="text" name="contact_number" value="{{ $userMeta->contact_number }}" class="form-control" id="contact-number">

                            {!! $errors->first('contact_number', '<span class="error-message">:message</span>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('business_number') ? 'has-error': '' }}">
                            <label for="business-number">Business number<span>*</span></label>

                            <input type="text" name="business_number" value="{{ $userMeta->business_number }}" class="form-control" id="business-number">

                            {!! $errors->first('business_number', '<span class="error-message">:message</span>') !!}
                        </div>
        
                        <div class="form-group">
                            <label>Tax ID:</label>
                            @if($userMeta->tax_id != NULL)

                            <img src="{{ asset('public/public/assets/users/tax-id') }}/{{ $userMeta->tax_id }}" class="img img-responsive">

                            @else
                            <br>
                            <label>Upload Your Tax ID To Complete Your Authentication Process:</label>
                            @endif
                            <br>
                            <input type="file" name="tax_id" class="form-control-file">
                        </div>

                        <div class="form-group">
                            <label>Driver License / ID:</label>
                            @if($userMeta->personal_id != NULL)
                            <img src="{{ asset('public/public/assets/users/personal-id') }}/{{ $userMeta->personal_id }}" class="img img-responsive">
                            @else
                            <br>
                            <label>Upload Your Personal ID To Complete Your Authentication Process:</label>
                            @endif
                            <br>
                            <input type="file" name="personal_id" class="form-control-file">
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="password">
                <h4>{{ trans('storefront::account.profile.password') }}</h4>

                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group {{ $errors->has('password') ? 'has-error': '' }}">
                            <label for="new-password">
                                {{ trans('storefront::account.profile.new_password') }}
                            </label>

                            <input type="password" name="password" id="new-password" class="form-control">

                            {!! $errors->first('password', '<span class="error-message">:message</span>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error': '' }}">
                            <label for="confirm-password">
                                {{ trans('storefront::account.profile.confirm_password') }}
                            </label>

                            <input type="password" name="password_confirmation" id="confirm-password" class="form-control">

                            {!! $errors->first('password_confirmation', '<span class="error-message">:message</span>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary" data-loading>
            {{ trans('storefront::account.profile.save_changes') }}
        </button>
    </form>
@endsection
