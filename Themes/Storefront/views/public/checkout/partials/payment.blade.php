<pre style="display:none">
@php 
use Modules\Cart\Facades\Cart;
$cart = Cart::instance();
print_r($gateways);
if(Auth::user() && Auth::user()->ret_list != 0):
$usermeta = DB::table('user_meta')->where('user_id', Auth::user()->id)->first();
$data = [];
$pays = json_decode($usermeta->extra_details);
if(isset($pays->wholesaler_non_secure_payment)):
$data = array_merge($data, $pays->wholesaler_non_secure_payment);
endif;
if(isset($pays->wholesaler_secure_payment)):
$data = array_merge($data, $pays->wholesaler_secure_payment);
endif;
print_r($data);
endif;
$con = 0;
echo $cart->total()->amount;
@endphp
@if(Auth::user() && Auth::user()->ret_list != 0)
{{Auth::user()->id}}
@endif
</pre>


<div id="payment" class="tab-pane" role="tabpanel">
    <div class="box-wrapper payment clearfix">
        <div class="box-header">
            <h4>{{ trans('storefront::checkout.tabs.payment.payment_method') }}</h4>
        </div>

        <ul class="list-inline payment-method clearfix">
            
            @forelse ($gateways as $name => $gateway)
                

                @if(!Auth::user() || (Auth::user() && Auth::user()->ret_list == 0))
                        @if($name == 'paypal_express' || $name == 'credit_card')
            		    <li>
            		        
                            <div class="form-group radio">
                                <input type="radio" name="payment_method" value="{{ $name }}" id="{{ $name }}" {{ $con == 0 ? 'checked' : '' }} {{ old('payment_method') === $name ? 'checked' : '' }}>
                                <label for="{{ $name }}">{{ $gateway->label }}</label>
                            </div>
                            @if($name == 'paypal_express')
                            <p>{{ $gateway->description }}</p>
                            @elseif($name == 'bank_transfer')
                            <div class="payment-gateway" data-key="bank_transfer" style="display:{{ $con == 0 ? 'block' : 'none' }}">
                                <div class="form-group">
                                    <div class="block">
                                        <label>* Transaction ID</label>
                                    </div>
                                    <input type="text" {{ $con == 0 ? 'required' : '' }} class="input-form form-control" name="transaction_id" placeholder="Transaction ID">
                                </div>
                            </div>
                            @elseif($name == 'credit_card')
                            <div class="creditdebit_form payment-gateway" data-key="credit_card" style="display:{{ $con == 0 ? 'block' : 'none' }}">
                                <label>Credit / Debit Card Details</label>
                                </br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="block">
                                                <label>* Card Number</label>
                                            </div>
                                            <input type="text" {{ $con == 0 ? 'required' : '' }} id="ccnum" class="input-form form-control" name="cardNumber" placeholder="Card Number">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="block">
                                                <label>* Expiration Date</label>
                                            </div>
                                            <input type="text" {{ $con == 0 ? 'required' : '' }} id="expiry" class="input-form form-control" name="expirationDate" placeholder="MM/YY">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="block">
                                                <label>* Security Code (CVV)</label>
                                            </div>
                                            <input type="text" {{ $con == 0 ? 'required' : '' }} id="cvc" class="input-form form-control" name="cardCode" placeholder="Security Code CVV">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="block">
                                                <label>* Card Holder Name</label>
                                            </div>
                                            <input type="text" {{ $con == 0 ? 'required' : '' }} class="input-form form-control" name="cardName" placeholder="Card Holder Name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </li>
                        @php 
                        $con++;
                        @endphp
            		    @endif
                @else
                    @php
                    $val = ($name == 'bank_transfer') ? 'wire_transfer' : $name;
                    @endphp
                    @if(array_search($val, $data) !== false)
                        <li>
            		        
                            <div class="form-group radio">
                                <input type="radio" name="payment_method" value="{{ $name }}" id="{{ $name }}" {{ $con == 0 ? 'checked' : '' }} {{ old('payment_method') === $name ? 'checked' : '' }}>
                                <label for="{{ $name }}">{{ $gateway->label }}</label>
                            </div>
                            @if($name == 'paypal_express')
                            <p>{{ $gateway->description }}</p>
                            @elseif($name == 'bank_transfer')
                            <div class="payment-gateway" data-key="bank_transfer"  style="display:{{ $con == 0 ? 'block' : 'none' }}">
                                <div class="form-group">
                                    <div class="block">
                                        <label>* Transaction ID</label>
                                    </div>
                                    <input type="text" {{ $con == 0 ? 'required' : '' }} class="input-form form-control" name="transaction_id" placeholder="Transaction ID">
                                </div>
                            </div>
                            @elseif($name == 'credit_card')
                            <div class="creditdebit_form payment-gateway" data-key="credit_card" style="display:{{ $con == 0 ? 'block' : 'none' }}">
                                <label>Credit / Debit Card Details</label>
                                </br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="block">
                                                <label>* Card Number</label>
                                            </div>
                                            <input type="text" {{ $con == 0 ? 'required' : '' }} id="ccnum" class="input-form form-control" name="cardNumber" placeholder="Card Number">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="block">
                                                <label>* Expiration Date</label>
                                            </div>
                                            <input type="text" {{ $con == 0 ? 'required' : '' }} id="expiry" class="input-form form-control" name="expirationDate" placeholder="MM/YY">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="block">
                                                <label>* Security Code (CVV)</label>
                                            </div>
                                            <input type="text" {{ $con == 0 ? 'required' : '' }} id="cvc" class="input-form form-control" name="cardCode" placeholder="Security Code CVV">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="block">
                                                <label>* Card Holder Name</label>
                                            </div>
                                            <input type="text" {{ $con == 0 ? 'required' : '' }} class="input-form form-control" name="cardName" placeholder="Card Holder Name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </li>
                        @php 
                        $con++;
                        @endphp
                    @endif
                @endif

            @empty
                <p class="error-message">{{ trans('storefront::checkout.tabs.payment.no_payment_method') }}</p>
            @endforelse

            {!! $errors->first('payment_method', '<span class="error-message">:message</span>') !!}
        </ul>
        
        @if ($cart->hasAvailableShippingMethod())
            <div class="available-shipping-methods">
                <div class="box-header">
                    <span style="font-weight:700">{{ trans('storefront::cart.shipping_method') }}</span>
                </div>
                

                <div class="form-group" style="padding-left:2%;padding-right:5%;padding-top:10px;">
                        @foreach ($cart->availableShippingMethods() as $name => $shippingMethod)
                        <span style="display:none">{{$name}}</span>
                        @if(($name == 'free_shipping' && $cart->total()->amount >= 500) || $name == 'local_pickup' || ($name == 'flat_rate' && $cart->total()->amount < 500))
                        <div class="radio">
                                <input type="radio" name="shipping_method" class="shipping-method" value="{{ $name }}" id="{{ $name }}" {{ $cart->shippingMethod()->name() === $shippingMethod->name || $loop->first ? 'checked' : '' }}>
                                <label for="{{ $name }}">{{ $shippingMethod->label }}</label>
                            <span class="pull-right">{{ $shippingMethod->cost->convertToCurrentCurrency('subTotal')->format() }}</span>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>

            @if ($errors->has('shipping_method'))
                <span class="error-message text-center">
                    {!! $errors->first('shipping_method', '<span class="help-block">:message</span>') !!}
                </span>
            @endif
        @endif
        
        <div class="checkout-terms checkbox text-center">
            <input type="checkbox" name="terms_and_conditions" id="terms-and-conditions">

            <label for="terms-and-conditions">
                {{ trans('storefront::checkout.i_agree_to_the') }} <a href="{{ $termsPageURL }}">{{ trans('storefront::checkout.terms_and_conditions') }}</a>
            </label>
        </div>
        
        <div class="checkout-terms checkbox text-center">
        <button type="submit" style="margin-top:15px;" class="btn btn-primary btn-checkout {{ $cart->hasNoAvailableShippingMethod() ? 'disabled' : '' }} {{ $gateways->isEmpty() ? 'disabled' : '' }}" data-loading disabled>
            {{ trans('storefront::checkout.place_order') }}
        </button>
        </div>

        
    </div>
</div>
