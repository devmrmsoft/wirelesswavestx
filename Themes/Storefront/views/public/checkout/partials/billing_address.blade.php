@php if (Auth::check())
{//echo Auth::user()->email;
$usermeta   =   DB::table('user_meta')->where('user_id',Auth::user()->id)->first();
//dd($usermeta);
} @endphp
<div class="billing-address clearfix">
    <h5>{{ trans("storefront::checkout.tabs.billing_address") }}</h5>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('billing.first_name') ? 'has-error': '' }}">
            <label for="billing-first-name">
                {{ trans('storefront::checkout.tabs.attributes.billing.first_name') }}<span>*</span>
            </label>

            <input type="text" required name="billing[first_name]" value="@if(Auth::user()){{Auth::user()->first_name}}@endif" class="form-control" id="billing-first-name">

            {!! $errors->first('billing.first_name', '<span class="error-message">:message</span>') !!}
        </div>
    </div>
 
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('billing.last_name') ? 'has-error': '' }}">
            <label for="billing-last-name">
                {{ trans('storefront::checkout.tabs.attributes.billing.last_name') }}<span>*</span>
            </label>

            <input type="text" required name="billing[last_name]" value="@if(Auth::user()){{Auth::user()->last_name}}@endif" class="form-control" id="billing-last-name">

            {!! $errors->first('billing.last_name', '<span class="error-message">:message</span>') !!}
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group {{ $errors->has('billing.address_1') ? 'has-error': '' }}">
            <label for="billing-1">
                {{ trans('storefront::checkout.tabs.attributes.billing.address_1') }}<span>*</span>
            </label>

            <input type="text" required name="billing[address_1]" value="@isset($usermeta->shipping_address)@if($usermeta->shipping_address != null){{$usermeta->shipping_address}}@endif @endisset" class="form-control" id="billing-address-1">
            
            {!! $errors->first('billing.address_1', '<span class="error-message">:message</span>') !!}
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group {{ $errors->has('billing.address_2') ? 'has-error': '' }}">
            <label for="billing-2">
                {{ trans('storefront::checkout.tabs.attributes.billing.address_2') }}
            </label>

            <input type="text" name="billing[address_2]" value="@isset($usermeta->business_address_2)@if($usermeta->business_address_2 != null){{$usermeta->business_address_2}}@endif @endisset" class="form-control" id="billing-address-2">

            {!! $errors->first('billing.address_2', '<span class="error-message">:message</span>') !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('billing.city') ? 'has-error': '' }}">
            <label for="billing-city">
                {{ trans('storefront::checkout.tabs.attributes.billing.city') }}<span>*</span>
            </label>

            <input type="text" required name="billing[city]" value="@isset($usermeta->city)@if($usermeta->city != null){{$usermeta->city}}@endif @endisset" class="form-control" id="billing-city">

            {!! $errors->first('billing.city', '<span class="error-message">:message</span>') !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('billing.zip') ? 'has-error': '' }}">
            <label for="billing-zip">
                {{ trans('storefront::checkout.tabs.attributes.billing.zip') }}<span>*</span>
            </label>

            <input type="text" required name="billing[zip]" value="@isset($usermeta->zip)@if($usermeta->zip != null){{$usermeta->zip}}@endif @endisset" class="form-control" id="billing-zip">

            {!! $errors->first('billing.zip', '<span class="error-message">:message</span>') !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('billing.country') ? 'has-error': '' }}">
            <label for="billing-country">
                {{ trans('storefront::checkout.tabs.attributes.billing.country') }}<span>*</span>
            </label>

            <select name="billing[country]" required class="custom-select-black" id="billing-country">
                @foreach ($countries as $code => $name)
                    <option value="{{ $code }}" {{ old('billing.country') === $code ? 'selected' : '' }}>
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            {!! $errors->first('billing.country', '<span class="error-message">:message</span>') !!}
        </div>
    </div>

    <div class="col-md-6 Test-aurangzeb" @isset($usermeta->state)@if($usermeta->state != null){{$usermeta->state}}@endif @endisset>
        <div class="form-group {{ $errors->has('billing.state') ? 'has-error': '' }}">
            <label for="billing-state">
                {{ trans('storefront::checkout.tabs.attributes.billing.state') }}<span>*</span>
            </label>

            <input type="text" required name="billing[state]" value="@isset($usermeta->state)@if($usermeta->state != null){{$usermeta->state}}@endif @endisset" class="form-control" id="@isset($usermeta->state)@if($usermeta->state != null)billing-states @else billing-state @endif{{ old('billing.state') }}@endif">

            {!! $errors->first('billing.state', '<span class="error-message">:message</span>') !!}
        </div>
    </div>
</div>
