<!DOCTYPE html>
<html lang="{{ locale() }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>
            @hasSection('title')
                @yield('title') - {{ setting('store_name') }}
            @else
                {{ setting('store_name') }}
            @endif
        </title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        @stack('meta')

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Rubik:400,500" rel="stylesheet">

        @if (is_rtl())
            <link rel="stylesheet" href="{{ v(Theme::url('public/css/app.rtl.css')) }}">
        @else
            <link rel="stylesheet" href="{{ v(Theme::url('public/css/app.css')) }}">
            <link rel="stylesheet" href="{{ v(Theme::url('public/css/custom.css')) }}">
        @endif

        <link rel="shortcut icon" href="{{ $favicon }}" type="image/x-icon">

        @stack('styles')

        {!! setting('custom_header_assets') !!}

        <script>
            window.FleetCart = {
                csrfToken: '{{ csrf_token() }}',
                stripePublishableKey: '{{ setting("stripe_publishable_key") }}',
                langs: {
                    'storefront::products.loading': '{{ trans("storefront::products.loading") }}',
                },
            };
        </script>

        @stack('globals')

        @routes

        <style type="text/css">
            .newsletter-suscription-form-input{
                font-size: 12px !important;
                color: #aeaeae !important;
                padding: 10px 0 !important;
                margin-bottom: 15px !important;
                background: transparent !important;
                border: none !important;
                border-bottom: 1px solid #aeaeae !important;
                height: 44px!important;
            }
            .newsletter-subscription-form-button-subscribe{
                text-transform: uppercase;
                -webkit-transition-duration: 0.2s;
                transition-duration: 0.2s;
                -webkit-transition-timing-function: ease;
                transition-timing-function: ease;
                border: 1px solid #fff;
                background: #fff;
                color: #fff;
                font-size: 12px;
                font-family: "PT Sans",Arial,Helvetica,sans-serif;
                -webkit-transition-duration: 0.2s;
                transition-duration: 0.2s;
                -webkit-transition-timing-function: ease;
                transition-timing-function: ease;
                padding: 10px 20px;
                color: #383838;
                width: auto;
                height: 44px;
                margin-bottom: 10px;
            }
            .newsletter-modal-content {
                 background-image:url('https://wirelesswavestx.com/storage/media/8YX1FzpGeximasWyVYJrP4hr76VI5PJUf4YQRNtE.jpeg');
                 background-size: cover !important;
                 
                    max-width: 100%;
             }
                    
             
            .newsletter-modal-dialog{
                max-width: 80% !important; 
                max-height: 90% !important;
                width: 100% !important;
                display: flex;
                align-items: center;
                height: 100%;
            }
            .newsletter-modal-header{
                padding: 10px !important;
                border: none!important;
            }
            .newsletter-modal-footer{
                padding: 10px !important;
                border: none!important;
            }
            .newsletter-close {
                padding: 1rem 1rem !important;
                margin: -1rem -1rem -1rem auto !important;
                display: block; 
                right: -1px !important;
                line-height: 25px !important;
                text-align: center !important;
                color: #333333 !important;
                font-size: 12px !important;
                border: none !important;
                cursor: pointer !important;
                background-color: #ffffff !important;
            }
            .newsletter-modal-content {
                    position: relative;
                    display: flex;
                    -ms-flex-direction: column;
                    flex-direction: column;
                    width: 100%;
                    pointer-events: auto;
                    background-color: #fff;
                    background-clip: padding-box;
                    border: 1px solid rgba(0, 0, 0, .2);
                    border-radius: .3rem;
                    outline: 0;
                }
        </style>
    </head>

    <body class="{{ $theme }} {{ storefront_layout() }} {{ is_rtl() ? 'rtl' : 'ltr' }}">
        <!--[if lt IE 8]>
            <p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        @unless (json_decode(Cookie::get('newsletter_removed')))
            @unless(\Request::route()->getName() == 'ask-register')
           
            @endunless
        @endunless
        <div class="main">
            <div class="wrapper">
                @include('public.partials.sidebar')
                @include('public.partials.top_nav')
                @include('public.partials.header')
                @include('public.partials.navbar')

                <div class="content-wrapper clearfix {{ request()->routeIs('cart.index') ? 'cart-page' : '' }}">
                    @php if(isset($slider)){ @endphp
                    <div class="container-fluid">
                            @unless (is_null($slider))
                                @if (storefront_layout() === 'default')
                                    <div class="col-lg-9 col-lg-offset-3 col-md-9 col-md-offset-3">
                                        <div class="row">
                                            @include('public.home.sections.slider')
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                @elseif (storefront_layout() === 'slider_with_banners')
                                    <div class="row">
                                        <div class="col-md-12">
                                            @include('public.home.sections.slider')
                                        </div>
                                        {{-- 
                                        <div class="col-md-3 hidden-sm hidden-xs">
                                            @include('public.home.sections.slider_banners')
                                        </div>
                                        --}}
                                    </div>
                                @endif
                            @endunless
                    </div>
                    @php } @endphp
                    <div class="container">
                        @include('public.partials.breadcrumb')

                        @unless (request()->routeIs('home') || request()->routeIs('login') || request()->routeIs('register') || request()->routeIs('reset') || request()->routeIs('reset.complete'))
                            @include('public.partials.notification')
                        @endunless

                        @yield('content')
                    </div>
                </div>

                @if (setting('storefront_brand_section_enabled') && $brands->isNotEmpty() && request()->routeIs('home'))
                    <section class="brands-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="brands">
                                    @foreach ($brands as $brand)
                                        <div class="col-md-3">
                                            <div class="brand-image">
                                                <img src="{{ $brand->path }}">
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </section>
                @endif
{{--
                <section class="brands-wrapper" style="background: #e2e3e4;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-6">
                                <h4>Subscribe to Get Exclusive Offers & Deals to Your Inbox!</h4>
                            </div>
                            <div class="col-md-4">
                                <div style="display: flex;margin-top: -10px;"><input name="email" id="email" type="email" class="newsletter-suscription-form-input" placeholder="username@domain.com" style="padding-right: 10%;width: 50%;"><button type="submit" class="newsletter-subscription-form-button-subscribe" style="background-color: #e9420b;color: white;border: #e9420b;margin-left: 6%;"> Subscribe </button></div>
                            </div>
                        </div>
                    </div>
                </section>
--}}
                @include('public.partials.footer')

                <a class="scroll-top" href="#">
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </a>
            </div>

            @unless (json_decode(Cookie::get('cookie_bar_accepted')))
                <div class="cookie-bar-wrapper">
                    <div class="container">
                        <div class="cookie clearfix">
                            <div class="cookie-text">
                                <span>{{ trans('storefront::layout.cookie_bar_text') }}</span>
                            </div>

                            <div class="cookie-button">
                                <button type="submit" class="btn btn-primary btn-cookie">
                                    {{ trans('storefront::layout.got_it') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endunless
        </div>

        @include('public.partials.quick_view_modal')

        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
        <script src="{{ v(Theme::url('public/js/app.js')) }}"></script>

        @stack('scripts')

        {!! setting('custom_footer_assets') !!}
        <script type="text/javascript">
            
            function changeImagePath(diss){
                        var newSrc  =   $(diss).attr('src');
                        $('.option-color-label').removeClass('orange-text');
                        $(diss).siblings().addClass('orange-text');
                        $("#base_image_path").attr('src',newSrc);
                        $(".slick-current > img").attr('src',newSrc);
                        var getSku  =   $(diss).attr('data-sku');
                        $('.sku > span').text(getSku);
                        
                        var label   =   $(diss).siblings('label').trigger('click');
                }
                
    function loadColorImages(el, option_id, value_id){
        
        if(typeof(value_id) != "undefined" && value_id !== null) {
            var value_id = value_id;
        }else{
            var value_id = el.value;
        }
        
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        
        $.ajax({
            url: '{{ route("get-product-color-images") }}',
            type: 'GET',
            data: {_token: CSRF_TOKEN, option_id: option_id, value_id: value_id},
            dataType: 'JSON',
            success: function(data){
            
            
            
            //console.log(data.html);
            $('#load_color_images').html(data.html);
            $('.sku > span').text(data.sku);
            $('.left-in-stock > .green').text(data.qty);
            console.log(data.sku);
            
             $('.base-image').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
              asNavFor: '.additional-image'
            });
            $('.additional-image').slick({
              slidesToShow: 3,
              slidesToScroll: 1,
              asNavFor: '.base-image',
              dots: true,
              centerMode: true,
              focusOnSelect: true
            });
            }
        });
    }
    
       $(document).ready(function(){
            $('#exampleModal').modal('show');
        }); 
        </script>
    </body>
</html>
