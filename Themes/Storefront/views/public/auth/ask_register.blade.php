@extends('public.layout')

@section('title', trans('user::auth.login'))

@push('styles')

<style type="text/css">
    .form-wrapper{
        width: 460px;
    }
</style>

@endpush
@section('content')

    <div class="row mt-70">
        <div class="col-md-6 mt-30">
            <div class="ask-register-left-box">
                <a href="{{ route('register') }}?r=r" class="text-center">
                    <h1 class="text-white">Register as a Retailer?</h1>
                </a>
            </div>
        </div>
        <div class="col-md-6 mt-30">
            <div class="ask-register-left-box">
                <a href="{{ route('register') }}" class="text-center">
                    <h1 class="text-white">Register as a Customer?</h1>
                </a>
            </div>
        </div>
    </div>

@endsection