@extends('public.layout')

@section('title', trans('user::auth.register'))

@section('content')
    <div class="register-wrapper clearfix">
        <div class="col-lg-6 col-md-7 col-sm-10">
            <div class="row">
                @include('public.partials.notification')

                <form method="POST" action="{{ route('register.post') }}" enctype="multipart/form-data" name="registration" id="registration">
                    {{ csrf_field() }}

                    <div class="box-wrapper register-form">
                        <div class="box-header">
                            @isset($_GET['r'])
                                @if($_GET['r'] == 'r')
                            
                            <h4>{{ trans('user::auth.register') }} <a href="{{ route('register') }}" class="pull-right">Register as a Customer ?</a></h4>
                                @endif
                            @else
                            <h4>{{ trans('user::auth.register') }} <a href="{{ route('register') }}?r=r" class="pull-right">Register as a Retailer ?</a></h4>
                            @endif
                        </div>

                        <div class="form-inner clearfix">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('first_name') ? 'has-error': '' }}">
                                    <label for="first-name">{{ trans('user::auth.first_name') }}<span>*</span></label>

                                    <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control" id="first-name" autofocus>

                                    {!! $errors->first('first_name', '<span class="error-message">:message</span>') !!}
                                </div>

                                <div class="form-group {{ $errors->has('last_name') ? 'has-error': '' }}">
                                    <label for="last-name">{{ trans('user::auth.last_name') }}<span>*</span></label>

                                    <input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control" id="last-name">

                                    {!! $errors->first('last_name', '<span class="error-message">:message</span>') !!}
                                </div>

                                <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                                    <label for="email">{{ trans('user::auth.email') }}<span>*</span></label>

                                    <input type="text" name="email" value="{{ old('email') }}" class="form-control" id="email">

                                    {!! $errors->first('email', '<span class="error-message">:message</span>') !!}
                                </div>
                                @isset($_GET['r'])
                                    @if($_GET['r'] == 'r')
                                    <input type="hidden" name="reg_retailer" value="retailer">
                                <div class="form-group {{ $errors->has('business_name') ? 'has-error': '' }}">
                                    <label for="business-name">Business Name<span>*</span></label>

                                    <input type="text" name="business_name" value="{{ old('business_name') }}" class="form-control" id="business-name">

                                    {!! $errors->first('business_name', '<span class="error-message">:message</span>') !!}
                                </div>
                                <div class="form-group {{ $errors->has('business_address') ? 'has-error': '' }}">
                                    <label for="business-address">Business Address<span>*</span></label>

                                    <input type="text" name="business_address" value="{{ old('business_address') }}" class="form-control" id="business-address">

                                    {!! $errors->first('business_address', '<span class="error-message">:message</span>') !!}
                                </div>

                                @endisset
                                @endif

                                {{-- <div class="form-group {{ $errors->has('shipping_address') ? 'has-error': '' }}">
                                    <label for="shipping-address">Shipping Address<span>*</span></label>

                                    <input type="text" name="shipping_address" value="{{ old('shipping_address') }}" class="form-control" id="shipping-address">

                                    {!! $errors->first('shipping_address', '<span class="error-message">:message</span>') !!}
                                </div> --}}
                                
                                <!-- edit by shahrukh -->
                                <div class="form-group {{ $errors->has('city') ? 'has-error': '' }}">
                                    <label for="city">City<span>*</span></label>

                                    <input type="text" name="city" value="{{ old('city') }}" class="form-control" id="city">

                                    {!! $errors->first('city', '<span class="error-message">:message</span>') !!}
                                </div>
                                
                                <div class="form-group {{ $errors->has('state') ? 'has-error': '' }}">
                                    <label for="state">State<span>*</span></label>

                                    <input type="text" name="state" value="{{ old('state') }}" class="form-control" id="state">

                                    {!! $errors->first('state', '<span class="error-message">:message</span>') !!}
                                </div>
                                
                                <div class="form-group {{ $errors->has('zip') ? 'has-error': '' }}">
                                    <label for="zip">Zip Code<span>*</span></label>

                                    <input type="text" name="zip" value="{{ old('zip') }}" class="form-control" id="zip">

                                    {!! $errors->first('zip', '<span class="error-message">:message</span>') !!}
                                </div>


                                <div class="form-group {{ $errors->has('contact_number') ? 'has-error': '' }}">
                                    <label for="contact-number">Contact number<span>*</span></label>

                                    <input type="text" name="contact_number" value="{{ old('contact_number') }}" class="form-control" id="contact-number">

                                    {!! $errors->first('contact_number', '<span class="error-message">:message</span>') !!}
                                </div>
                                
                                <div class="form-group {{ $errors->has('referral_code') ? 'has-error': '' }}">
                                    <label for="referral_code">Referral code (optional)<span></span></label>

                                    <input type="text" name="referral_code" value="{{ old('referral_code') }}" class="form-control" id="referral_code">

                                    {!! $errors->first('referral_code', '<span class="error-message">:message</span>') !!}
                                </div>

                                @isset($_GET['r'])
                                    @if($_GET['r'] == 'r')

                                <div class="form-group {{ $errors->has('business_number') ? 'has-error': '' }}">
                                    <label for="business-number">Business number<span>*</span></label>

                                    <input type="text" name="business_number" value="{{ old('business_number') }}" class="form-control" id="business-number">

                                    {!! $errors->first('business_number', '<span class="error-message">:message</span>') !!}
                                </div>



                                <div class="form-group {{ $errors->has('tax_id') ? 'has-error': '' }}">
                                    <label for="tax_id">Tax ID<span>*</span></label>

                                    <input type="file" name="tax_id" class="form-control tax_id" id="tax_id">
                                    <span class="error-tax_id"></span>
                                    {!! $errors->first('tax_id', '<span class="error-tax_id">:message</span>') !!}
                                </div>


                                <div class="form-group {{ $errors->has('personal_id') ? 'has-error': '' }}">
                                    <label for="personal-id">Driver License / ID:<span>*</span></label>
                                    <input type="file" name="personal_id" class="form-control" id="personal_id">
                                    <span class="error-personal_id"></span>
                                    {!! $errors->first('personal_id', '<span class="error-message">:message</span>') !!}
                                </div>



                                @endisset
                                @endif

                                <div class="form-group {{ $errors->has('password') ? 'has-error': '' }}">
                                    <label for="password">{{ trans('user::auth.password') }}<span>*</span></label>

                                    <input type="password" name="password" class="form-control" id="password">

                                    {!! $errors->first('password', '<span class="error-message">:message</span>') !!}
                                </div>

                                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error': '' }}">
                                    <label for="confirm-password">{{ trans('user::auth.password_confirmation') }}<span>*</span></label>

                                    <input type="password" name="password_confirmation" class="form-control" id="confirm-password">

                                    {!! $errors->first('password_confirmation', '<span class="error-message">:message</span>') !!}
                                </div>

                                <div class="form-group {{ $errors->has('captcha') ? 'has-error': '' }}">
                                    @captcha
                                    <input type="text" name="captcha" id="captcha" class="captcha-input">

                                    {!! $errors->first('captcha', '<span class="error-message">:message</span>') !!}
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="checkbox">
                                <input type="checkbox" name="privacy_policy" id="privacy">

                                <label for="privacy">
                                    {{ trans('user::auth.i_agree_to_the') }} <a href="{{ $privacyPageURL }}">{{ trans('user::auth.privacy_policy') }}</a>
                                </label>

                                {!! $errors->first('privacy_policy', '<span class="error-message">:message</span>') !!}
                            </div>

                            <button type="submit" class="btn btn-primary btn-register pull-right" data-loading>
                                {{ trans('user::auth.register') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script type="text/javascript">


// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[id='registration']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      tax_id: {
          required: true,
          extension: "png|jpeg|jpg"
        },
      personal_id: {
          required: true,
          extension: "png|jpeg|jpg"
        }
      
    },
    // Specify validation error message
    messages: {
      tax_id: {
        extension: "Please Upload Valid Image"
      },
      personal_id: {
        extension: "Please Upload Valid Image"
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});


</script>

@endpush
