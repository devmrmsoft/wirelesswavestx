    <div class="product-image">
        <div class="base-image">
                <a class="base-image-inner" href="{{ asset('/public//public/assets/options/color/images/') }}/{{ $multipleImages[0] }}">
                    <img id="base_image_path" src="{{ asset('/public//public/assets/options/color/images/') }}/{{ $multipleImages[0] }}">
                    <span><i class="fa fa-search-plus" aria-hidden="true"></i></span>
                </a>

            @foreach ($multipleImages as $index => $additionalImage)
                
                    <a class="base-image-inner" href="{{ asset('/public//public/assets/options/color/images/') }}/{{ $additionalImage }}">
                        <img src="{{ asset('/public//public/assets/options/color/images/') }}/{{ $additionalImage }}">
                        <span><i class="fa fa-search-plus" aria-hidden="true"></i></span>
                    </a>
            @endforeach
        </div>

        <div class="additional-image">
            <div class="thumb-image">
                <img src="{{ asset('/public//public/assets/options/color/images/') }}/{{ $multipleImages[0] }}">
            </div>

            @foreach ($multipleImages as $index => $additionalImage)
            
                <div class="thumb-image">
                    <img src="{{ asset('/public//public/assets/options/color/images/') }}/{{ $additionalImage }}">
                </div>
            @endforeach
        </div>
    </div>