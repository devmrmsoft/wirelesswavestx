@foreach ($modelCats as $category)
    <li class="{{ request('category') === $category->slug ? 'active' : '' }} parent">
        <div class="form-group">
            <div class="checkbox">
                <input type="checkbox"
                    name="multiModelCats[]"
                    class="phone-checkbox"
                    value="{{ mb_strtolower($category->id) }}"
                    onchange='addActiveClass(this);'
                    id="multicat-{{ $category->id }}"
                    {{ is_filtering($category->slug) ? 'checked' : '' }} >
                <label for="multicat-{{ $category->id }}">{{ $category->name }}</label>
            </div>
        </div>
        <!-- <ul>
        </ul> -->
    </li>
@endforeach