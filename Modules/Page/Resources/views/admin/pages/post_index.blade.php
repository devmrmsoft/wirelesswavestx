@extends('admin::layout')

<?php $posts    =   DB::table('blog_posts')->get(); ?>

@component('admin::components.page.header')
    @slot('title', 'Posts')

    <li class="active">posts</li>
@endcomponent

@component('admin::components.page.index_table')
    @slot('resource', 'pages')
    @slot('name', 'Posts')

<div class="table-responsive">
    <table class="table table-striped table-hover" id="pages-table">
        <thead>
            <tr>
                <th>Serial</th>
                <th>Post Title</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php $serialNum   =   1; ?>
            @foreach($posts as $post)
            <tr>
                <td>#<?=$serialNum?></td>
                <td>{{ $post->post_title }}</td>

                <td> 
                    <a href="{{ route('admin.users.retailer_categories') }}/{{ $post->id }}" onclick="return confirm('are you sure?')"><i class="fa fa-trash"></i></a>
                    <a href="{{ url('/admin/pages/posts/') }}/{{ $post->id }}/edit" ><i class="fa fa-edit"></i></a>
                </td>
            </tr>
            <?php $serialNum   += 1; ?>
            @endforeach
        </tbody>

        @isset($tfoot)
            <tfoot>{{ $tfoot }}</tfoot>
        @endisset
    </table>
</div>

@endcomponent
@push('scripts')
    <script>

        $(document).ready( function () {
            $('#pages-table .table').DataTable();
        } );
    </script>
@endpush
