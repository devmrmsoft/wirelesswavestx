@extends('admin::layout')

@push('styles')
<style type="text/css">
	.m-top-2{
		margin-top: 20px;
	}
    .m-bot-2{
        margin-bottom: 20px;
    }
	.d-none{
		display: none;
	}
    .order-wrapper {
        background: #fff;
        padding: 15px;
        border-radius: 3px;
    }
</style>

@endpush

@component('admin::components.page.header')
    @slot('title', 'Post Create')

    <li><a href="{{ route('admin.pages.index') }}">{{ trans('page::pages.pages') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('page::pages.page')]) }}</li>
@endcomponent

@section('content')
    <div class="order-wrapper">
    	@isset($posts)
        <form method="POST" action="{{ route('admin.pages.post_update') }}" id="checkout-formt">
        	<input type="hidden" name="post_id" value="{{ $posts->id }}">
        @else
        <form method="POST" action="{{ route('admin.pages.post_store') }}" id="checkout-formt">
        @endisset
            {{ csrf_field() }}
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Post Title</label>
                    <div class="col-md-9">
                        <input type="text" name="post_title" class="form-control" value="@isset($posts){{ $posts->post_title }}@endisset" required="">
                    </div>
                </div>
            </div>
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Post Title</label>
                    <div class="col-md-9">
            			<textarea name="body" class="form-control  wysiwyg" id="body" rows="10" cols="10" labelcol="2" aria-hidden="true" required="">@isset($posts) {{ $posts->post_content }} @endisset</textarea>
                    </div>
                </div>
            </div>
            <div class="row m-top-2 form-group">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

        </form>

    </div>
@endsection

@push('scripts')


@endpush