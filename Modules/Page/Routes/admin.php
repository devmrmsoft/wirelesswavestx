<?php

/** @var Illuminate\Routing\Router $router */
Route::get('pages', [
    'as' => 'admin.pages.index',
    'uses' => 'PageController@index',
    'middleware' => 'can:admin.pages.index',
]);

Route::get('pages/create', [
    'as' => 'admin.pages.create',
    'uses' => 'PageController@create',
    'middleware' => 'can:admin.pages.create',
]);

Route::get('pages/posts', [
    'as' => 'admin.pages.post_index',
    'uses' => 'PageController@post_index',
    'middleware' => 'can:admin.pages.index',
]);

Route::get('pages/posts/create', [
    'as' => 'admin.pages.post_create',
    'uses' => 'PageController@post_create',
    'middleware' => 'can:admin.pages.index',
]);

Route::post('pages/posts/create', [
    'as' => 'admin.pages.post_store',
    'uses' => 'PageController@post_store',
    'middleware' => 'can:admin.pages.index',
]);

Route::get('pages/posts/{id}/edit', [
    'as' => 'admin.pages.post_edit',
    'uses' => 'PageController@post_edit',
    'middleware' => 'can:admin.pages.index',
]);

Route::get('pages/posts/{id}/delete', [
    'as' => 'admin.pages.post_delete',
    'uses' => 'PageController@post_delete',
    'middleware' => 'can:admin.pages.index',
]);

Route::post('pages/posts/edit', [
    'as' => 'admin.pages.post_update',
    'uses' => 'PageController@post_update',
    'middleware' => 'can:admin.pages.edit',
]);

Route::post('pages', [
    'as' => 'admin.pages.store',
    'uses' => 'PageController@store',
    'middleware' => 'can:admin.pages.create',
]);

Route::get('pages/{id}/edit', [
    'as' => 'admin.pages.edit',
    'uses' => 'PageController@edit',
    'middleware' => 'can:admin.pages.edit',
]);

Route::put('pages/{id}/edit', [
    'as' => 'admin.pages.update',
    'uses' => 'PageController@update',
    'middleware' => 'can:admin.pages.edit',
]);

Route::delete('pages/{ids?}', [
    'as' => 'admin.pages.destroy',
    'uses' => 'PageController@destroy',
    'middleware' => 'can:admin.pages.destroy',
]);
