<?php

namespace Modules\Page\Http\Controllers\Admin;

use Modules\Page\Entities\Page;
use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Page\Http\Requests\SavePageRequest;
use Illuminate\Http\Request;
use DB;

class PageController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Page::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'page::pages.page';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'page::admin.pages';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SavePageRequest::class;

    public function post_index(){
        return view('page::admin.pages.post_index');
    }

    public function post_create(){
        return view('page::admin.pages.post_create');
    }

    public function post_store(Request $request){

        $blog_posts   =   DB::table('blog_posts')->insert([
                                'post_title'        =>  $request->post_title,
                                'post_content'      =>  $request->body,
                                'created_at'        =>  DB::raw('CURRENT_TIMESTAMP'),
                                'updated_at'        => DB::raw('CURRENT_TIMESTAMP'),
                            ]);
        return redirect()->route('admin.pages.post_index');
    }
    
    public function post_delete($id){
        
        $posts  =   DB::table('blog_posts')->where('id', $id)->delete();     

        return view('page::admin.pages.post_index' );
    }

    public function post_edit($id){
        // dd($id);
        $posts  =   DB::table('blog_posts')->where('id', $id)->first();

        $data   =   [

                        'posts' =>  $posts,

                    ];

        return view('page::admin.pages.post_create', $data );
    }

    public function post_update(Request $request){
        $postUpdate     =   DB::table('blog_posts')
                                    ->where('id', $request->post_id)
                                    ->update([
                                        'post_title'   =>  $request->post_title,
                                        'post_content'   =>  $request->body,
                                    ]);
        return redirect()->route('admin.pages.post_index');
    }
}
