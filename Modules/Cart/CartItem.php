<?php

namespace Modules\Cart;

use Modules\Support\Money;
use Auth;
use DB;

class CartItem
{
    public $id;
    public $qty;
    public $product;
    public $options;

    public function __construct($item)
    {
        $this->id = $item->id;
        $this->qty = $item->quantity;
        $this->product = $item->attributes['product'];
        $this->options = $item->attributes['options'];
        $this->aurang   =   $item->attributes['product'];
    }

    public function unitPrice()
    {
        //dd($this->product->selling_price);
        /*if (Auth::check()){

            $listID = Auth::user()->ret_list;
            if($listID >= '1'){

                $retListTable   =   DB::table('retailer_lists')->where('id', $listID )->first();
                //return $retListTable->list_price;https://thevideobee.to/cxtth46l1in7.html
                $amount =   $retListTable->list_price;
                $price  =   $this->product->selling_price;
                //return $price;
                // $price  =   substr($price, 1);
                $price  =   $price * ((100- $amount) / 100);

            }else{
                $price =    $this->product->selling_price;
            }

        }else{
            $price =    $this->product->selling_price;
        }*/
        //return $this->product->selling_price;
        return $this->product->selling_price->add($this->optionsPrice());
    }

    public function total()
    {
        return $this->unitPrice()->multiply($this->qty);
    }

    public function optionsPrice()
    {
        return Money::inDefaultCurrency($this->calculateOptionsPrice());
    }

    public function calculateOptionsPrice()
    {
        return $this->options->sum(function ($option) {
            return $this->valuesSum($option->values);
        });
    }

    private function valuesSum($values)
    {
        return $values->sum(function ($value) {
            if ($value->price_type === 'fixed') {
                return $value->price->amount();
            }

            return ($value->price / 100) * $this->product->selling_price->amount();
        });
    }

    public function isAvailable()
    {
        return $this->fresh()->product->hasStockFor($this->qty);
    }

    public function isNotAvailable()
    {
        return ! $this->isAvailable();
    }

    public function fresh()
    {
        $this->product = $this->product->refresh();

        return $this;
    }

    public function findTax($addresses)
    {
        return $this->product->taxClass->findTaxRate($addresses);
    }
}
