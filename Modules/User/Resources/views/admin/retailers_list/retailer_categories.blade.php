@extends('admin::layout')

<?php $retailer_categories	=	DB::table('retailer_lists')->get(); ?>

@component('admin::components.page.header')
    @slot('title', 'Retailers')

    <li class="active">{{ trans('user::users.users') }}</li>
@endcomponent

@component('admin::components.page.index_table')
    @slot('buttons', ['create'])
    @slot('resource', 'users')
    @slot('name', trans('user::users.user'))



<div class="table-responsive">
    <table class="table table-striped table-hover" id="users-table">
        <thead>
        	<tr>
	            <th>Serial</th>
	            <th>Category Name</th>
	            <th>Discount</th>
	            <th data-sort>Action</th>
        	</tr>
        </thead>

        <tbody>
        	@foreach($retailer_categories as $retCat)
        	<tr>
	        	<td>#{{ $retCat->id }}</td>
	        	<td>{{ $retCat->list_name }}</td>
	        	<td>{{ $retCat->list_price }} %</td>
	        	<td> 
	        		<a href="{{ route('admin.users.retailer_categories') }}/{{ $retCat->id }}" onclick="return confirm('are you sure?')"><i class="fa fa-trash"></i></a>
	        		<a href="{{ url('/admin/users/list/') }}/{{ $retCat->id }}/edit" ><i class="fa fa-edit"></i></a>
	        	</td>
        	</tr>
        	@endforeach
        </tbody>

        @isset($tfoot)
            <tfoot>{{ $tfoot }}</tfoot>
        @endisset
    </table>
</div>

@endcomponent
@push('scripts')
    <script>
        new DataTable('#users-table');
    </script>
@endpush
