@extends('admin::layout')

<?php   

$check = DB::table('users')
            ->join('user_roles', 'users.id', '=', 'user_roles.user_id')
            ->select('users.id','users.first_name','user_roles.role_id')
            ->where('user_roles.user_id',Auth::user()->id )
            ->first();
            
if($check->role_id == 5)
{
    $retailer_categories	=	DB::table('users')->where('sales_person', Auth::user()->id)
                                        ->get();
}
else
{
    $retailer_categories	=	DB::table('users')->where('ret_list', '!=', '0')
                                        ->where('id', '!=', '1')
                                        ->get();
}

 
                                        
?>

@component('admin::components.page.header')
    @slot('title', 'Retailers')

    <li class="active">{{ trans('user::users.users') }}</li>
@endcomponent


@component('admin::components.page.index_table')
    @slot('buttons', ['create'])
    @slot('resource', 'users')
    @slot('name', trans('user::users.user'))



<div class="table-responsive">
    <table class="table table-striped table-hover" id="users-table">
        <thead>
        	<tr>
	            <th>Serial</th>
	            <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
	            <th>Category</th>
	            <th data-sort>Action</th>
        	</tr>
        </thead>

        <tbody>
            <?php $serialNum   =   1; ?>
        	@foreach($retailer_categories as $retCat)
        	<tr>
	        	<td>#<?=$serialNum?></td>
	        	<td>{{ $retCat->first_name }}</td>
                <td>{{ $retCat->last_name }}</td>
	        	<td>{{ $retCat->email }}</td>
	        	<?php $retailer_categories    =   DB::table('retailer_lists')->where('id',$retCat->ret_list)->first(); ?>
                <td>{{ $retailer_categories->list_name }}</td>
                <td> 
                    <a href="{{ url('/admin/retailers/') }}/{{ $retCat->id }}/retailer-edit" ><i class="fa fa-edit"></i></a>
                    <a href="{{ route('admin.retailers.destroy') }}/{{ $retCat->id }}" onclick="return confirm('are you sure?')"><i class="fa fa-trash"></i></a>
                </td>
        	</tr>
            <?php $serialNum   += 1; ?>
        	@endforeach
        </tbody>

        @isset($tfoot)
            <tfoot>{{ $tfoot }}</tfoot>
        @endisset
    </table>
</div>

@endcomponent
@push('scripts')
    <script>

        $(document).ready( function () {
            $('#users-table .table').DataTable();
        } );
    </script>
@endpush
