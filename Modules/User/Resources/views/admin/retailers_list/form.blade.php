
    <div class="row">
        <div class="col-md-8">
        
        @if(isset($retailerList->id))
            <input type="hidden" name="user_list_id" value="{{ $retailerList->id }}">
        @endif
            <div class="form-group ">
                <label for="list_name" class="col-md-3 control-label text-left">List Name</label>
                <div class="col-md-9">

                    <input type="text" name="list_name" value="@isset($retailerList){{ $retailerList->list_name }}@endif" class="form-control" id="list_name" placeholder="Class A">
                </div>
            </div>
            <div class="form-group ">
                <label for="list_price" class="col-md-3 control-label text-left">List Discount Price</label>
                <div class="col-md-9">

                    <input type="number" name="list_price" value="@isset($retailerList){{ $retailerList->list_price }}@endif" class="form-control" id="list_price" placeholder="The value shall be considered as percent of total price">
                </div>
            </div>
        </div>
    </div>