@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', 'Create List')

    <li><a href="{{ route('admin.users.list_index') }}">List</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => 'List']) }}</li>
@endcomponent


@push('styles')

    <link media="all" type="text/css" rel="stylesheet" href="{{ asset('/modules/admin/css/admin.css?v=1.1.5') }}">

@endpush

@isset($id)
<?php

    $retailerList   =   DB::table('retailer_lists')->find($id);
    //print_r( $userMeta );
?>
@endisset


@section('content')

	@if(isset($retailerList->id))
        <form method="POST" action="{{ route('admin.users.list_update') }}" class="form-horizontal" id="user-create-form" novalidate>
    @else
    	<form method="POST" action="{{ route('admin.users.list_store') }}" class="form-horizontal" id="user-create-form" novalidate>
    @endif
        {{ csrf_field() }}
        @include('user::admin.retailers_list.form')

        <div class="form-group">
		    <div class="{{ ($buttonOffset ?? true) ? 'col-md-offset-2' : '' }} col-md-10">
		        <button type="submit" class="btn btn-primary" data-loading>
		        	@isset($id)
		        	Update
		            @else
		            {{ trans('admin::admin.buttons.save') }}
		            @endisset
		        </button>
		    </div>
		</div>
    </form>
@endsection

@include('user::admin.users.partials.shortcuts')
