@extends('admin::layout')

<?php $retailer_categories	=	DB::table('users')->where('ret_list', '0')
                                        ->where('id', '!=', '1')
                                        ->get(); 
                                        //dd($retailer_categories);
                                        ?>

@component('admin::components.page.header')
    @slot('title', 'Retailers')

    <li class="active">{{ trans('user::users.users') }}</li>
@endcomponent

@component('admin::components.page.index_table')
    @slot('buttons', ['create'])
    @slot('resource', 'users')
    @slot('name', trans('user::users.user'))



<div class="table-responsive">
    <table class="table table-striped table-hover" id="users-table">
        <thead>
        	<tr>
	            <th>Serial</th>
	            <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
	            <th>Category</th>
	            <th data-sort>Action</th>
        	</tr>
        </thead>

        <tbody>
            <?php $serialNum   =   1; ?>
        	@foreach($retailer_categories as $retCat)
        	<tr>
	        	<td>#<?= $serialNum ?></td>
	        	<td>{{ $retCat->first_name }}</td>
                <td>{{ $retCat->last_name }}</td>
	        	<td>{{ $retCat->email }}</td>
	        	<?php $retailer_lists    =   DB::table('retailer_lists')->where('id',$retCat->ret_list)->first();  ?>
                <td>@isset($retailer_lists->list_name) {{ $retailer_lists->list_name }} @else Tier 0 @endisset</td>
                <td>
                    <a href="{{ url('/admin/retailers/') }}/{{ $retCat->id }}/retailer-edit" ><i class="fa fa-edit"></i></a>
                </td>
        	</tr>
            <?php $serialNum   += 1; ?>
        	@endforeach
        </tbody>

        @isset($tfoot)
            <tfoot>{{ $tfoot }}</tfoot>
        @endisset
    </table>
</div>

@endcomponent
@push('scripts')
    <script>
        new DataTable('#users-table');
    </script>
@endpush
