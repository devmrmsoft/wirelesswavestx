@push('styles')

<style>
    .d-none{
        display:none;
    }
</style>

@endpush

@if(isset($user->id))
    <div class="row">
        <div class="col-md-8">
        <?php
            $userMeta   =   DB::table('user_meta')->where('user_id',$user->id)->first();
            //$userss   =   DB::table('users')->where('id',$user->id)->first();
            $retLists   =   DB::table('retailer_lists')->get();
            if(isset( $userMeta->extra_details )){

                $extraDetails   =   json_decode($userMeta->extra_details, true);
            }
            if(isset( $extraDetails['wholesaler_secure_payment'] )){

                foreach ($extraDetails['wholesaler_secure_payment'] as $key => $value) {
                    $extraDetails['wholesaler_secure_payment'][$value]  =   $value;
                }

            }
            if(isset( $extraDetails['wholesaler_non_secure_payment'] )){

                foreach ($extraDetails['wholesaler_non_secure_payment'] as $key => $value) {
                    $extraDetails['wholesaler_non_secure_payment'][$value]  =   $value;
                }

            }
        ?>
        @if(isset($userMeta->id))
            <input type="hidden" name="user_meta_id" value="{{ $userMeta->id }}">
        @endif
            <div class="form-group ">
                <label for="business-name" class="col-md-3 control-label text-left">Business Name</label>
                <div class="col-md-9">

                    <input type="text" name="business_name" value="@isset($userMeta->business_name){{ $userMeta->business_name }}@endisset" class="form-control" id="business-name">
                </div>
            </div>


            <div class="form-group ">
                <label for="business-address" class="col-md-3 control-label text-left">Address Line 1</label>
                <div class="col-md-9">
                    <input type="text" name="business_address" value="@isset($userMeta->business_address){{ $userMeta->business_address }}@endisset" class="form-control" id="business-address">
                </div>
            </div>

            <div class="form-group ">
                <label for="address-2" class="col-md-3 control-label text-left">Address Line 2</label>
                <div class="col-md-9">
                    <input type="text" name="business_address_2" value="@isset($userMeta->business_address_2){{ $userMeta->business_address_2 }}@endisset" class="form-control" id="address-2">
                </div>
            </div>
            <div class="form-group ">
                <label for="address-city" class="col-md-3 control-label text-left">City</label>
                <div class="col-md-9">
                    <input type="text" name="city" value="@isset($userMeta->city){{ $userMeta->city }}@endisset" class="form-control" id="address-city">
                </div>
            </div>
            <div class="form-group">
                <label for="address-state" class="col-md-3 control-label text-left">State/Provice/Region</label>
                <div class="col-md-9">
                    <input type="text" name="state" value="@isset($userMeta->state){{ $userMeta->state }}@endisset" class="form-control" id="address-state">
                </div>
            </div>
            <div class="form-group">
                <label for="address-zip" class="col-md-3 control-label text-left">ZIP</label>
                <div class="col-md-9">
                    <input type="text" name="zip" value="@isset($userMeta->zip){{ $userMeta->zip }}@endisset" class="form-control" id="address-zip">
                </div>
            </div>


            <div class="form-group ">
                <label for="contact_number" class="col-md-3 control-label text-left">Contact number</label>
                <div class="col-md-9">
                    <input type="text" name="contact_number" value="@isset($userMeta->contact_number){{ $userMeta->contact_number }}@endisset" class="form-control" id="contact_number">
                </div>
            </div>
            
            <div class="form-group ">
                <label for="user_code" class="col-md-3 control-label text-left">User Code</label>
                <div class="col-md-9">
                    <input type="text" name="user_code" value="@isset($userMeta->user_code){{ $userMeta->user_code }}@endisset" class="form-control" id="user_code">
                </div>
            </div>
            <div class="form-group ">
                <label for="referral_code" class="col-md-3 control-label text-left">Referral Code</label>
                <div class="col-md-9">
                    <input type="text" name="referral_code" value="@isset($userMeta->referral_code){{ $userMeta->referral_code }}@endisset" class="form-control" id="referral_code">
                </div>
            </div>
            
            <div class="form-group ">
                <label for="business_number" class="col-md-3 control-label text-left">Business number</label>
                <div class="col-md-9">
                <input type="text" name="business_number" value="@isset($userMeta->business_number){{ $userMeta->business_number }}@endisset" class="form-control" id="business_number">
                </div>
            </div>

            <div class="form-group ">
                <label for="ret_list" class="col-md-3 control-label text-left">Retailer List</label>
                <div class="col-md-9">
                    <select name="ret_list" class="form-control custom-select-black " id="ret_list">
                        <option value="0">Select List</option>
                        @foreach($retLists as $retList)
                        <option  <?php if(isset($user->ret_list) && $user->ret_list == $retList->id ){ echo 'selected'; } ?> value="{{ $retList->id }}">{{ $retList->list_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group ">
                <label for="credit_usage" class="col-md-3 control-label text-left">Credit Usage</label>
                <div class="col-md-9">
                    <input type="text" name="credit_usage" value="@isset($user->credit_usage){{ $user->credit_usage }}@endisset" class="form-control" id="credit_usage">
                </div>
            </div>

            <div class="form-group ">
                <label for="yes_credit_line" class="col-md-3 control-label text-left">Credit Line</label>
                <div class="col-md-9">
                    <div class="checkbox">                    
                        <input type="checkbox" name="extra_details[yes_credit_line]" class="" id="yes_credit_line" value="1" <?php if(isset($extraDetails['yes_credit_line']) && $extraDetails['yes_credit_line'] == '1'){ echo ' checked=""'; } ?> onchange='creditLineToggle();'>
                        <label for="yes_credit_line">Activated</label>
                    </div>
                </div>
            </div>

            <div class="form-group credit-limit  <?php if(isset($extraDetails['yes_credit_line']) && $extraDetails['yes_credit_line'] == '1'){ echo ''; }else{ echo 'd-none'; }  ?>">
                <label for="credit_line" class="col-md-3 control-label text-left">Credit Line In Days</label>
                <div class="col-md-3">
                    <select name="" class="form-control custom-select-black " id="credit_line">
                        <option value="1">1</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <input type="text" name="extra_details[credit_line]" value="<?php if(isset($extraDetails['credit_line'])){ echo $extraDetails['credit_line']; } ?>" class="form-control" id="business_number" placeholder="Type number of days">
                </div>
            </div>

            <div class="form-group credit-limit   <?php if(isset($extraDetails['yes_credit_line']) && $extraDetails['yes_credit_line'] == '1'){ echo ''; }else{ echo 'd-none'; } ?>">
                <label for="credit_limit" class="col-md-3 control-label text-left">Credit Limit</label>
                <div class="col-md-9">

                    <input type="text" name="extra_details[credit_limit]" value="<?php if(isset($extraDetails['credit_limit'])){ echo $extraDetails['credit_limit']; } ?>" class="form-control" id="credit_limit">

                </div>
            </div>

            <div class="form-group ">
                <label for="no_credit_line" class="col-md-3 control-label text-left">No Credit Line</label>
                <div class="col-md-9">
                    <div class="checkbox">                    
                        <input type="checkbox" name="extra_details[no_credit_line]" class="" id="no_credit_line" value="1" <?php if(isset($extraDetails['no_credit_line']) && $extraDetails['no_credit_line'] == '1'){ echo ' checked=""'; } ?>
                         onchange='noCreditLineToggle();'>
                        <label for="no_credit_line">Activated</label>
                    </div>
                </div>
            </div>
      <!--       <div class="form-group ">

                <label for="business-number" class="col-md-3 control-label text-left">Credit Line</label>

                <div class="col-md-9">
                    <select name="credit_line" class="form-control selectize prevent-creation " multiple id="credit_line">
                        <option value="1">1-10 Days</option>
                        <option value="2">1-20 Days</option>
                        <option value="3">1-30 Days</option>

                    </select>
                </div>
            </div>
     -->
            <div class="form-group">
                <label for="wholesaler_secure_payment" class="col-md-3 control-label text-left">Secure Payment</label>

                <div class="col-md-9">
                    <select name="extra_details[wholesaler_secure_payment][]" class="form-control selectize prevent-creation " multiple id="wholesaler_secure_payment" onChange="checkIfSecurePaymentIsSelected(this)">

                        <option <?php 


                        if(isset($extraDetails['wholesaler_secure_payment']['wire_transfer']))
                        {

                            if($extraDetails['wholesaler_secure_payment']['wire_transfer'] == 'wire_transfer'){
                                echo 'selected';
                            }

                          }?>  value="wire_transfer" >Wire Transfer</option>
                        <option <?php 


                        if(isset($extraDetails['wholesaler_secure_payment']['credit_card']))
                        {

                            if($extraDetails['wholesaler_secure_payment']['credit_card'] == 'credit_card'){
                                echo 'selected';
                            }

                          }?>   value="credit_card" >Credit Card</option>


                        <!-- <option  <?php 


                        if(isset($extraDetails['wholesaler_secure_payment']['bank_transfer']))
                        {

                            if($extraDetails['wholesaler_secure_payment']['bank_transfer'] == 'bank_transfer'){
                                //echo 'selected';
                            }

                          }?> value="bank_transfer">Bank Transfer</option> -->

                        <option  <?php 


                        if(isset($extraDetails['wholesaler_secure_payment']['paypal_express']))
                        {

                            if($extraDetails['wholesaler_secure_payment']['paypal_express'] == 'paypal_express'){
                                echo 'selected';
                            }

                          }?> value="paypal_express">Paypal Express</option>
                        <!-- <option <?php 


                        if(isset($extraDetails['wholesaler_secure_payment']['bank_transfer']))
                        {

                            if($extraDetails['wholesaler_secure_payment']['bank_transfer'] == 'bank_transfer'){
                                //echo 'selected';
                            }

                          }?> value="bank_transfer">Stripe Payment</option>-->
                        <option <?php 


                        if(isset($extraDetails['wholesaler_secure_payment']['check_payment']))
                        {

                            if($extraDetails['wholesaler_secure_payment']['check_payment'] == 'check_payment'){
                                echo 'selected';
                            }

                          }?> value="check_payment">Check / Money Order</option>

                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="wholesaler_non_secure_payment" class="col-md-3 control-label text-left">Non Secure Payment</label>

                <div class="col-md-9">
                    <select name="extra_details[wholesaler_non_secure_payment][]" class="form-control selectize prevent-creation " multiple id="wholesaler_non_secure_payment">
                        <option <?php 


                        if(isset($extraDetails['wholesaler_non_secure_payment']['company_cheque']))
                        {

                            if($extraDetails['wholesaler_non_secure_payment']['company_cheque'] == 'company_cheque'){
                                echo 'selected';
                            }

                          }?>  value="company_cheque">COD Company Check</option>
                        <option <?php 


                        if(isset($extraDetails['wholesaler_non_secure_payment']['personal_cheque']))
                        {

                            if($extraDetails['wholesaler_non_secure_payment']['personal_cheque'] == 'personal_cheque'){
                                echo 'selected';
                            }

                          }?> value="personal_cheque">COD Certified Check</option>
                        <option <?php 


                        if(isset($extraDetails['wholesaler_non_secure_payment']['open_shipment']))
                        {

                            if($extraDetails['wholesaler_non_secure_payment']['open_shipment'] == 'open_shipment'){
                                echo 'selected';
                            }

                          }?> value="open_shipment">Credit Account</option>
                    </select>
                </div>
            </div>

            @isset($userMeta->tax_id)
            <div class="form-group">
                <label for="" class="col-md-3 control-label text-left">Tax ID:</label>
                <div class="col-md-9">
                    @if($userMeta->tax_id != NULL)

                    <img src="{{ asset('/public/public/assets/users/tax-id') }}/{{ $userMeta->tax_id }}" class="img img-responsive">

                    @else
                    <br>
                    <label>User does not provided his/her TAX ID card:</label>
                    @endif
                </div>
            </div>
            @endisset
            @isset($userMeta->personal_id)
            <div class="form-group">
                <label for="" class="col-md-3 control-label text-left">Driver License / ID:</label>
                <div class="col-md-9">
                    @if($userMeta->personal_id != NULL)
                        <img src="{{ asset('/public/public/assets/users/personal-id') }}/{{ $userMeta->personal_id }}" class="img img-responsive">
                        @else
                        <br>
                        <label>User does not provided his/her PERSONAL ID card:</label>
                    @endif
                </div>
            </div>
            @endisset
        </div>
    </div>
@endif

@push('scripts')

<script type="text/javascript">
    function creditLineToggle(){
        var yesCreditLine = document.getElementById('yes_credit_line');
        var noCreditLine = document.getElementById('no_credit_line');
        /*creditLine.checked = creditLine.checked;*/

        if(yesCreditLine.checked == false) {
            noCreditLine.checked = true;
            $('.credit-limit').addClass('d-none');
        }
        else {
            if(yesCreditLine.checked == true) {
                noCreditLine.checked = false;
                $('.credit-limit').removeClass('d-none');
             }
        }
    }
    function noCreditLineToggle(){
        var yesCreditLine = document.getElementById('yes_credit_line');
        var noCreditLine = document.getElementById('no_credit_line');
        /*creditLine.checked = creditLine.checked;*/

        if(noCreditLine.checked == false) {
            yesCreditLine.checked = true; 
        }
        else {
            if(noCreditLine.checked == true) {
                yesCreditLine.checked = false;
             }
        }
        
        if(yesCreditLine.checked == false) {
            noCreditLine.checked = true;
            $('.credit-limit').addClass('d-none');
        }
        else {
            if(yesCreditLine.checked == true) {
                noCreditLine.checked = false;
                $('.credit-limit').removeClass('d-none');
             }
        }

    }
    
    function checkIfSecurePaymentIsSelected(data){
        if($(data).val()[0]){
            $('#wholesaler_non_secure_payment')[0].selectize.disable();
            } else {
                /*alert('unselected');*/
                $('#wholesaler_non_secure_payment')[0].selectize.enable();
            }
    }
    
    $(document).ready(function(){
        
       if($("#wholesaler_secure_payment").val()[0]){
                $('#wholesaler_non_secure_payment')[0].selectize.disable();
            } else {
                $('#wholesaler_non_secure_payment')[0].selectize.enable();
            }
    });
</script>
@endpush