<?php
    $sales_person = DB::table('users')
            ->join('user_roles', 'users.id', '=', 'user_roles.user_id')
            ->select('users.id','users.first_name','users.last_name')
            ->where('user_roles.role_id', 5)
            ->get();
            
    $person = DB::table('users')->select('first_name','last_name')->where('id',$user->sales_person)->first();
    
?>
<div class="row">
    <div class="col-md-8">
        {{ Form::text('first_name', trans('user::attributes.users.first_name'), $errors, $user, ['required' => true]) }}
        {{ Form::text('last_name', trans('user::attributes.users.last_name'), $errors, $user, ['required' => true]) }}
        {{ Form::email('email', trans('user::attributes.users.email'), $errors, $user, ['required' => true]) }}
        {{ Form::select('roles', trans('user::attributes.users.roles'), $errors, $roles, $user, ['multiple' => true, 'required' => true, 'class' => 'selectize prevent-creation']) }}
        
        @if($user->ret_list !=0)
        <div class="form-group ">
        	<label for="tax_class_id" class="col-md-3 control-label text-left">Sales Person</label>
        	<div class="col-md-9">
        		<select class="form-control"  name="sales_person" id="sales_person" >
        		    @isset($person)
        		    <option value="{{$user->sales_person}}">{{$person->first_name}} {{$person->last_name}}</option>
        		    @endisset
        			@foreach($sales_person as $sp)
        			<option value="{{$sp->id}}">{{$sp->first_name}} {{$sp->last_name}}</option>
        		    @endforeach
        		</select>
        	</div>
        </div>
        @endif
        

        @if (request()->routeIs('admin.users.create'))
            {{ Form::password('password', trans('user::attributes.users.password'), $errors, null, ['required' => true]) }}
            {{ Form::password('password_confirmation', trans('user::attributes.users.password_confirmation'), $errors, null, ['required' => true]) }}
        @endif
        @if (request()->routeIs('admin.retailers.retailer_edit'))
            <input type="hidden" name="return_retailer_route" value="1">
        @endif
        @if (request()->routeIs('admin.users.edit') || request()->routeIs('admin.retailers.retailer_edit'))
            {{ Form::checkbox('activated', trans('user::attributes.users.activated'), trans('user::users.form.activated'), $errors, $user, ['disabled' => $user->id === $currentUser->id, 'checked' => old('activated', $user->isActivated())]) }}
        @endif
    </div>
</div>
