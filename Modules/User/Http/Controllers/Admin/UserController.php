<?php

namespace Modules\User\Http\Controllers\Admin;

use Modules\User\Entities\User;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Admin\Traits\HasCrudActions;
use Modules\User\Http\Requests\SaveUserRequest;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Modules\User\Mail\Welcome;
use Illuminate\Support\Facades\Mail;
use DB;

class UserController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'user::users.user';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'user::admin.users';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveUserRequest::class;

    /**
     * Store a newly created resource in storage.
     *
     * @param \Modules\User\Http\Requests\SaveUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveUserRequest $request)
    {
        
        $request->merge(['password' => bcrypt($request->password)]);

        $user = User::create($request->all());

        $user->roles()->attach($request->roles);

        Activation::complete($user, Activation::create($user)->code);

        return redirect()->route('admin.users.index')
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => trans('user::users.user')]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param \Modules\User\Http\Requests\SaveUserRequest $request
     * @return \Illuminate\Http\Response
     */
     
    public function update($id, SaveUserRequest $request)
    {
        $data           =   $request->all();
        //dd($data);
        if(isset( $data['user_meta_id'] )){
                
            $userMeta   =   DB::table('user_meta')
                                ->where('user_id', $id)
                                ->update([
                                'business_name'         =>  $data['business_name'],
                                'business_address'      =>  $data['business_address'],
                                'business_address_2'    =>  $data['business_address_2'],
                                'city'                  =>  $data['city'],
                                'state'                 =>  $data['state'],
                                'zip'                   =>  $data['zip'],
                                'contact_number'        =>  $data['contact_number'],
                                'user_code'             =>  $data['user_code'],
                                'referral_code'         =>  $data['referral_code'],
                                'business_number'       =>  $data['business_number'],
                                'extra_details'         =>  json_encode( $data['extra_details'] ),

                                ]);
        } 

        
        $user = User::findOrFail($id);

        if (is_null($request->password)) {
            unset($request['password']);
        } else {
            $request->merge(['password' => bcrypt($request->password)]);
        }
        
        $user->update($request->all());
        if(isset($data['sales_person']))
        { 
            DB::table('users')->where('id', $id)->update([
                                'sales_person'      =>  $data['sales_person'],
                                ]);
        }
        
        if($data['credit_usage'] == 0)
        {
            DB::table('users')->where('id', $id)->update([
                                'ret_list'      =>  $data['ret_list'],
                                'credit_usage'  =>  $data['credit_usage'],
                                'usage_date'    =>  null,
                                'last_usage_date' => null,
                                ]);
        }
        else
        {
            DB::table('users')->where('id', $id)->update([
                                'ret_list'      =>  $data['ret_list'],
                                'credit_usage'  =>  $data['credit_usage'],
                                ]);
        }
        
        $user->roles()->sync($request->roles);

        if (! Activation::completed($user) && $request->activated === '1') {
            Activation::complete($user, Activation::create($user)->code);

            if (setting('confirm_user_email')) {
                Mail::to($request->email)
                    ->send(new Welcome($request->first_name,'activation'));
            }
        }

        if (Activation::completed($user) && $request->activated === '0') {
            Activation::remove($user);
        }

        if(isset( $request->return_retailer_route ) && $request->return_retailer_route == '1'){
            return redirect()->route('admin.users.list_index')
                ->withSuccess('Retaler Information Has Been Saved');
            }
        return redirect()->route('admin.users.index')
            ->withSuccess(trans('admin::messages.resource_saved', ['resource' => trans('user::users.user')]));
    }

    public function list_index(){
        
        return view('user::admin.retailers_list.index');

    }

    public function retailer_categories(){
        //dd('test');
        return view('user::admin.retailers_list.retailer_categories');

    }

    public function ret_requests(){

        return view('user::admin.retailers_list.ret_requests');

    }

    public function list_create(){
        return view('user::admin.retailers_list.create');
    }

    public function list_store(Request $request){
        
        $data       =   $request->all();
        $userMeta   =   DB::table('retailer_lists')->insert([
                            'list_name'     =>  $data['list_name'],
                            'list_price'  =>  $data['list_price'],
                            ]);
        return redirect()->route('admin.users.list_index')->withSuccess('List has been created.');
    }

    public function list_edit($id){

        $data   =   [

                        'id'    =>  $id,

                    ];
        return view('user::admin.retailers_list.create', $data);
    }

    public function list_update(Request $request){

        $data       =   $request->all();
        $userMeta   =   DB::table('retailer_lists')
                            ->where('id', $data['user_list_id'])
                            ->update([
                                'list_name'     =>  $data['list_name'],
                                'list_price'  =>  $data['list_price'],

                                ]);
        return redirect()->route('admin.users.list_index')->withSuccess('List has been saved.');
    }
    public function list_destroy($id){

        DB::table('retailer_lists')->delete($id);
        return redirect()->route('admin.users.list_store')->withSuccess('List has been deleted.');
    }

    public function destroyRetailers($id){
        DB::table('users')->delete($id);
        return redirect()->route('admin.users.list_index')->withSuccess('Retailer has been deleted.');
    }
}