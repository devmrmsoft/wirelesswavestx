<?php

namespace Modules\User\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Modules\Media\Entities\File;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendToAdmin extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $userEmail;
    private $activation;
    public $heading;
    public $text;

    /**
     * Create a new instance.
     *
     * @param string $userEmail
     * @return void
     */
    public function __construct($userEmail,$activation = null)
    {
        $this->firstName = $userEmail;
        $this->activation = $activation;
        $this->heading = 'New User Registered';
        $this->text = trans('user::mail.new_user_created', ['email' => $userEmail]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {//dd($this->activation);
        return $this->subject('New User Registered!')
            ->view("emails.{$this->getViewName()}", [
                'logo' => File::findOrNew(setting('storefront_mail_logo'))->path,
            ]);
    }

    private function getViewName()
    {
        return 'sendToAdmin';
    }
}
