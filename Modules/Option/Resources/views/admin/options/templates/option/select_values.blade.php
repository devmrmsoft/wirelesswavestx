<script type="text/html" id="option-select-values-template">
    <tr class="option-row" onclick="getOptionsData(this)">
        <td class="text-center">
            <span class="drag-icon">
                <i class="fa">&#xf142;</i>
                <i class="fa">&#xf142;</i>
            </span>
        </td>
        <td>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input
                type="hidden"
                <% if (optionId === undefined) { %>
                    name="values[<%- valueId %>][id]"
                    id="values-<%- valueId %>-id"
                <% } else { %>
                    name="options[<%- optionId %>][values][<%- valueId %>][id]"
                    id="option-<%- optionId %>-values-<%- valueId %>-id"
                <% } %>

                value="<%- value.id %>"
            >

            <input
                type="text"
                <% if (optionId === undefined) { %>
                    name="values[<%- valueId %>][label]"
                    id="values-<%- valueId %>-label"
                <% } else { %>
                    name="options[<%- optionId %>][values][<%- valueId %>][label]"
                    id="option-<%- optionId %>-values-<%- valueId %>-label"
                <% } %>

                class="form-control"
                value="<%- value.label %>"
            >
        </td>
        <td>
            <input
                type="number"
                <% if (optionId === undefined) { %>
                    name="values[<%- valueId %>][price]"
                    id="values-<%- valueId %>-price"
                <% } else { %>
                    name="options[<%- optionId %>][values][<%- valueId %>][price]"
                    id="option-<%- optionId %>-values-<%- valueId %>-price"
                <% } %>
                class="form-control"
                value="<%- _.isObject(value.price) ? value.price.amount : value.price %>"
                step="0.01"
                min="0"
            >
        </td>
        <td>
            <select
                <% if (optionId === undefined) { %>
                    name="values[<%- valueId %>][price_type]"
                    id="values-<%- valueId %>-price_type"
                <% } else { %>
                    name="options[<%- optionId %>][values][<%- valueId %>][price_type]"
                    id="option-<%- optionId %>-values-<%- valueId %>-price_type"
                <% } %>
                class="form-control custom-select-black"
            >
                <option value="fixed"
                    <%= value.price_type === 'fixed' ? 'selected' : '' %>
                >
                    {{ trans('option::options.form.price_types.fixed') }}
                </option>

                <option value="percent"
                    <%= value.price_type === 'percent' ? 'selected' : '' %>
                >
                    {{ trans('option::options.form.price_types.percent') }}
                </option>
            </select>
        </td>
        <td class="qty-box"><input type="number"
            <% if (optionId === undefined) { %>
                    name="values[<%- valueId %>][qty]"
                    id="values-<%- valueId %>-qty"
                <% } else { %>
                    name="options[<%- optionId %>][values][<%- valueId %>][qty]"
                    id="option-<%- optionId %>-values-<%- valueId %>-qty"
                <% } %> value=""
             onkeyup="sumQty(this)"class="form-control w-200 option-qty" data-value-label="<%- value.label %>"  data-value-id="<%- value.id %>"  ></td>
        <td class="sku-box"><input type="text"
            <% if (optionId === undefined) { %>
                    name="values[<%- valueId %>][sku]"
                    id="values-<%- valueId %>-sku"
                <% } else { %>
                    name="options[<%- optionId %>][values][<%- valueId %>][sku]"
                    id="option-<%- optionId %>-values-<%- valueId %>-sku"
                <% } %>
            class="form-control w-200 option-sku" placeholder="0123456"></td>
        <td class="img-box">
            {{-- <textarea rows="4"
            <% if (optionId === undefined) { %>
                    name="values[<%- valueId %>][multiple_image]"
                    id="values-<%- valueId %>-multiple_image" required="required"
                <% } else { %>
                    name="options[<%- optionId %>][values][<%- valueId %>][multiple_image]"
                    id="option-<%- optionId %>-values-<%- valueId %>-multiple_image"
                <% } %>
            ></textarea> --}}
            <input type='file' class="imgInp" onload="readURL(this)"
            <% if (optionId === undefined) { %>
                    name="values[<%- valueId %>][image][]"
                    id="values-<%- valueId %>-image" required="required"
                <% } else { %>
                    name="options[<%- optionId %>][values][<%- valueId %>][image][]"
                    id="option-<%- optionId %>-values-<%- valueId %>-image"
                <% } %>

             multiple>
                <% if (optionId === undefined) { %>
                <% } else { %>
                <img src="" class="option-image" width="50" height="50">
                <% } %>

                <input type="hidden"
                <% if (optionId === undefined) { %>
                    name="values[<%- valueId %>][image_id]"
                    id="values-<%- valueId %>-image_id"
                <% } else { %>
                    name="options[<%- optionId %>][values][<%- valueId %>][image_id]"
                    id="option-<%- optionId %>-values-<%- valueId %>-image_id"
                <% } %>
                value="<%- value.label %>"
            >
            <input type="button" name="checkUpload" value="upload" onclick="uploadBeforeProduct(this)">
        </td>
        <td class="text-center">
            <button type="button" class="btn btn-default delete-row" data-toggle="tooltip" title="{{ trans('option::options.form.delete_row') }}">
                <i class="fa fa-trash"></i>
            </button>
        </td>
    </tr>
</script>
@push ('scripts')

<script type="text/javascript">

/*
    function readURL(diss) {
        var newSrc  =   $(diss).attr('src');
        $(diss).siblings().attr('src',newSrc);
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(this).siblings().attr('src', e.target.result);
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }*/
/*
    $("#inputFile").change(function () {
        readURL(this);
    });*/

/*
    $('form#product-edit-form').submit( function(event) {
        alert('before');
        var _token  =   'asd';
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        console.log(formData);
        $.ajax({
            url: '{{ route("admin.products.option_image") }}',
            type: 'POST',
            cache: false,
            data: { '_token': _token, formData: formData }, //see the $_token
            datatype: 'html',
            beforeSend: function() {
                //something before send
            },
            success: function(data) {

                    console.log(data);
            },
            error: function(xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
        alert('after');
    });*/

/*
   $("form").submit(function(evt){   
       $.ajax({
           url: '{{ route("admin.products.option_image") }}',
           type: 'POST',
           data: $('form#product-edit-form').serialize(),
           async: false,
           cache: false,
           contentType: false,
           enctype: 'multipart/form-data',
           processData: false,
           success: function (response) {
             alert(response);
           }
       });
   });
*/

   /*

   $("form").submit(function(evt){   
        

    var Upload = function (file) {
    this.file = file;
    };

    Upload.prototype.getType = function() {
        return this.file.type;
    };
    Upload.prototype.getSize = function() {
        return this.file.size;
    };
    Upload.prototype.getName = function() {
        return this.file.name;
    };
    Upload.prototype.doUpload = function () {
        var that = this;
        var formData = new FormData();

        // add assoc key values, this will be posts values
        formData.append("file", this.file, this.getName());
        formData.append("upload_file", true);

        $.ajax({
            type: "POST",
            url: '{{ route("admin.products.option_image") }}',
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener('progress', that.progressHandling, false);
                }
                return myXhr;
            },
            success: function (data) {
                // your callback here
            },
            error: function (error) {
                // handle error
            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        });
    };

    Upload.prototype.progressHandling = function (event) {
        var percent = 0;
        var position = event.loaded || event.position;
        var total = event.total;
        var progress_bar_id = "#progress-wrp";
        if (event.lengthComputable) {
            percent = Math.ceil(position / total * 100);
        }
        // update progressbars classes so it fits your code
        $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
        $(progress_bar_id + " .status").text(percent + "%");
    };
});*/


 /*   $(document).ready(function (e) {
     $("#product-edit-form").on('submit',(function(e) {
      e.preventDefault();
      $.ajax({
        url: '{{ route("admin.products.option_image") }}',
       type: "PUT",
       data:  new FormData(this),
       contentType: false,
        cache: false,
       processData:false,
       beforeSend : function()
       {
        //$("#preview").fadeOut();
        $("#err").fadeOut();
       },
       success: function(data)
          {
            console.log(data);
          },
         error: function(e) 
          {
        $("#err").html(e).fadeIn();
          }          
        });
     }));
    });*/

/*    $(document).ready(function(){
        
      $('#product-edit-form').on('submit', function(e){
        
        e.preventDefault();
        var form = e.target;
        var data = new FormData(form);
        $.ajax({
          url: '{{ route("admin.products.option_image") }}',
          method: form.method,
          processData: false,
          contentType: false,
          data: data,
          processData: false,
          success: function(data){
            console.log(data);
          }
        });
      });

      $('#product-create-form').on('submit', function(e){
        e.preventDefault();
        var form = e.target;
        var data = new FormData(form);
        $.ajax({
          url: '{{ route("admin.products.create_option_image") }}',
          method: form.method,
          processData: false,
          contentType: false,
          data: data,
          processData: false,
          success: function(data){
            console.log(data);
          }
        })
      })
    });*/
</script>

@endpush