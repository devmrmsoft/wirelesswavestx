@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('report::admin.reports'))

    <li class="active">{{ trans('report::admin.reports') }}</li>
@endcomponent

@section('content')
    <div class="box box-primary report-wrapper">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <div class="report-result">
                        @yield('report_result')
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="filter-report clearfix">
                        <h3 class="tab-content-title">{{ trans('report::admin.filter') }}</h3>

                        <form method="GET" action="{{ route('admin.reports.index') }}">
                            <div class="form-group">
                                <label for="report-type">{{ trans('report::admin.filters.report_type') }}</label>

                                <select name="type" id="report-type" class="custom-select-black">
                                    @foreach (trans('report::admin.filters.report_types') as $type => $label)
                                        <option value="{{ $type }}" {{ $request->type === $type ? 'selected' : '' }}>
                                            {{ $label }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            @yield('filters')

                            <button type="submit" class="btn btn-default pull-right" data-loading>
                                {{ trans('report::admin.filter') }}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

<script type="text/javascript">
    function exportTableToExcel(tableID, filename = ''){
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(tableID);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        
        // Specify file name
        filename = filename?filename+'.xls':'excel_data.xls';
        
        // Create download link element
        downloadLink = document.createElement("a");
        
        document.body.appendChild(downloadLink);
        
        if(navigator.msSaveOrOpenBlob){
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob( blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
        
            // Setting the file name
            downloadLink.download = filename;
            
            //triggering the function
            downloadLink.click();
        }
    }
</script>