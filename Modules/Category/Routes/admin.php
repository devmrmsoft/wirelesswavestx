<?php

Route::get('categories/tree', [
    'as' => 'admin.categories.tree',
    'uses' => 'CategoryTreeController@index',
    'middleware' => 'can:admin.categories.index',
]);

Route::put('categories/tree', [
    'as' => 'admin.categories.tree.update',
    'uses' => 'CategoryTreeController@update',
    'middleware' => 'can:admin.categories.edit',
]);

Route::get('categories', [
    'as' => 'admin.categories.index',
    'uses' => 'CategoryController@index',
    'middleware' => 'can:admin.categories.index',
]);

Route::post('categories', [
    'as' => 'admin.categories.store',
    'uses' => 'CategoryController@store',
    'middleware' => 'can:admin.categories.create',
]);

Route::post('categories/save', [
    'as' => 'admin.categories.getCategoryPhoneModel',
    'uses' => 'CategoryController@getCategoryPhoneModel',
    'middleware' => 'can:admin.categories.create',
]);

Route::get('categories/{id}', [
    'as' => 'admin.categories.show',
    'uses' => 'CategoryController@show',
    'middleware' => 'can:admin.categories.edit',
]);

Route::post('categories/first_child', [
    'as' => 'admin.categories.first_child',
    'uses' => 'CategoryController@getChild',
    'middleware' => 'can:admin.categories.edit',
]);

Route::post('categories/get_child_for_phone', [
    'as' => 'admin.categories.get_child_for_phone',
    'uses' => 'CategoryController@getChildForPhone',
    'middleware' => 'can:admin.categories.edit',
]);

Route::post('categories/acc_brand', [
    'as' => 'admin.categories.acc_brand',
    'uses' => 'CategoryController@getAccBrandChild',
    'middleware' => 'can:admin.categories.edit',
]);

Route::post('categories/get_model_for_brand', [
    'as' => 'admin.categories.get_model_for_brand',
    'uses' => 'CategoryController@getModelForBrand',
    'middleware' => 'can:admin.categories.edit',
]);


Route::put('categories/{id}', [
    'as' => 'admin.categories.update',
    'uses' => 'CategoryController@update',
    'middleware' => 'can:admin.categories.edit',
]);

Route::delete('categories/{id}', [
    'as' => 'admin.categories.destroy',
    'uses' => 'CategoryController@destroy',
    'middleware' => 'can:admin.categories.destroy',
]);
