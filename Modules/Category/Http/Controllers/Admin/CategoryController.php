<?php

namespace Modules\Category\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Modules\Category\Entities\Category;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Category\Http\Requests\SaveCategoryRequest;
use Illuminate\Http\Request;
use DB;

class CategoryController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'category::categories.category';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'category::admin.categories';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveCategoryRequest::class;

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Category::withoutGlobalScope('active')->find($id);
    }

    public function getChild(Request $request){

       //dd($request->all());
        /*if($request->id == '133'){

           $childCats       =   Category::where('parent_id', '162')->get();
           $parentCat       =   Category::find('162');
           $mainRootCatId   =   Category::find('162');

        }else{
        }*/
           $childCats       =   Category::where('parent_id', $request->id)->get();
           $parentCat       =   Category::find($request->id);
           $mainRootCatId   =   Category::find($request->mainRootCatId);
       //root_cat_id
       foreach($childCats as $childCat){
        $ifHasChild     =   Category::where('parent_id', $childCat->id)->get();
            if($ifHasChild->total() >=  1){
                $hasChild = true;
           }else{
           }
        }
        if(! isset( $hasChild )){
            $hasChild = 'no-child';
        }else{
            $hasChild = 'has-child';
        }
        if(isset($request->type)){
            if($request->type == 'headphones'){
                $headphone_label    =   'Headphone Type';
            }else{
                $headphone_label = '0';
            }
        }
        $next_ele_count =   $request->ele_count +1;

        $previous_el    =   $next_ele_count - 1;
       $data    =   [
                        'childCats'         =>  $childCats,
                        'parentCat'         =>  $parentCat,
                        'id'                =>  $request->id,
                        'ele_count'         =>  $next_ele_count,
                        'hasChild'          =>  $hasChild,
                        'mainRootCatId'     =>  $mainRootCatId,
                        'headphone_label'   =>  $headphone_label
                    ];

        $returnHTML = view('category::ajax_layouts.get_cat_options', $data )->render();

        //return response()->json(array('success' => true, 'html'=>$returnHTML, 'parent'=>$request->el_id, 'child_el'=>$next_ele_count));
        if($childCats->total() == 0 ){
            return response()->json(array('success' => 'empty', 'html'=>$returnHTML));
        }else{
        return response()->json(array('success' => true, 'html'=>$returnHTML, 'parent'=>$request->el_id, 'child_el'=>$next_ele_count));
        }

    }


    public function getChildForPhone(Request $request){

       //dd($request->all());
       $childCats       =   Category::where('parent_id', $request->id)->get();
       $parentCat       =   Category::find($request->id);
       $mainRootCatId   =   Category::find($request->mainRootCatId);
       //root_cat_id
       foreach($childCats as $childCat){
        $ifHasChild     =   Category::where('parent_id', $childCat->id)->get();
            if($ifHasChild->total() >=  1){
                $hasChild = true;
           }else{
           }
        }
        if(! isset( $hasChild )){
            $hasChild = 'no-child';
        }else{
            $hasChild = 'has-child';
        }
        $next_ele_count =   $request->ele_count +1;

        $previous_el    =   $next_ele_count - 1;
       $data    =   [
                        'childCats'     =>  $childCats,
                        'parentCat'     =>  $parentCat,
                        'id'            =>  $request->id,
                        'ele_count'     =>  $next_ele_count,
                        'hasChild'      =>  $hasChild,
                        'mainRootCatId' =>  $mainRootCatId,
                    ];

        $returnHTML = view('category::ajax_layouts.get_cat_options_for_phones', $data )->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML, 'parent'=>$request->el_id, 'child_el'=>$next_ele_count));
        //return response()->json(array('success' => true, 'html'=>$returnHTML, 'parent'=>$request->el_id, 'child_el'=>$next_ele_count));
        if($childCats->total() == 0 ){
            return response()->json(array('success' => 'empty', 'html'=>$returnHTML));
        }else{
        return response()->json(array('success' => true, 'html'=>$returnHTML, 'parent'=>$request->el_id, 'child_el'=>$next_ele_count));
        }

    }
    public function getAccBrandChild(Request $request){

       //dd($request->all());
       $childCats       =   Category::where('parent_id', $request->id)->get();
       $parentCat       =   Category::find($request->id);
       //root_cat_id
       foreach($childCats as $childCat){
        $ifHasChild     =   Category::where('parent_id', $childCat->id)->get();
            if($ifHasChild->total() >=  1){
                $hasChild = true;
           }else{
           }
        }
        if(! isset( $hasChild )){
            $hasChild = 'no-child';
        }else{
            $hasChild = 'has-child';
        }
        $next_ele_count =   $request->ele_count +1;

        $previous_el    =   $next_ele_count - 1;
       $data    =   [
                        'childCats'     =>  $childCats,
                        'parentCat'     =>  $parentCat,
                        'id'            =>  $request->id,
                        'ele_count'     =>  $next_ele_count,
                        'hasChild'      =>  $hasChild,
                    ];

        $returnHTML = view('category::ajax_layouts.get_acc_brand_options', $data )->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML, 'parent'=>$request->el_id, 'child_el'=>$next_ele_count));     
        if($childCats->total() == 0 ){
            return response()->json(array('success' => 'empty', 'html'=>$returnHTML));
        }else{
            return response()->json(array('success' => true, 'html'=>$returnHTML, 'parent'=>$request->el_id, 'child_el'=>$next_ele_count));
        }

    }

    public function getModelForBrand(Request $request){

       //dd($request->all());
       $childCats       =   Category::where('parent_id', $request->id)->get();
       $parentCat       =   Category::find($request->id);
       //root_cat_id
       foreach($childCats as $childCat){
        $ifHasChild     =   Category::where('parent_id', $childCat->id)->get();
            if($ifHasChild->total() >=  1){
                $hasChild = true;
           }else{
           }
        }
        if(! isset( $hasChild )){
            $hasChild = 'no-child';
        }else{
            $hasChild = 'has-child';
        }
        $next_ele_count =   $request->ele_count +1;

        $previous_el    =   $next_ele_count - 1;
       $data    =   [
                        'childCats'     =>  $childCats,
                        'parentCat'     =>  $parentCat,
                        'id'            =>  $request->id,
                        'ele_count'     =>  $next_ele_count,
                        'hasChild'      =>  $hasChild,
                    ];

        $returnHTML = view('category::ajax_layouts.get_model_brand_options', $data )->render();

        return response()->json(array('success' => true, 'html'=>$returnHTML, 'parent'=>$request->el_id, 'child_el'=>$next_ele_count));     
        if($childCats->total() == 0 ){
            return response()->json(array('success' => 'empty', 'html'=>$returnHTML));
        }else{
            return response()->json(array('success' => true, 'html'=>$returnHTML, 'parent'=>$request->el_id, 'child_el'=>$next_ele_count));
        }

    }

    public function getCategoryPhoneModel(Request $request){

        $getMainParent  =   Category::find($request->parent_id);
        if(isset($request->parent_id)){
            $getCategory    =   Category::where('parent_id', $request->parent_id)->orderBy('created_at', 'desc')->limit(1)->first();
        }else{
            $getCategory    =   Category::orderBy('created_at', 'desc')->limit(1)->first();
        }
        /*dd($getCategory->id);*/
        if(isset($getMainParent->parent_id)){
            if($getMainParent->parent_id != 392){
                return response()->json(array('success' => true, 'id'=>$getCategory->id, 'name'=>$getCategory->name,'p_id' => 'no_hidden' ));
            }else{
                return response()->json(array('success' => true, 'id'=>$getCategory->id, 'name'=>$getCategory->name,'p_id' => 'make_hidden' ));
            }
        }else{
            return response()->json(array('success' => true, 'id'=>$getCategory->id, 'name'=>$getCategory->name ));
        }
        /*$form   =   '';
        $form   .=  '<form method="post" action="'.route('admin.categories.store').'" id="makeCategory">';
        $form   .=  '<input type="text" name="name" value="'.$request->name.'" >';
        $form   .=  '<input type="text" name="parent_id" value="'.$request->parent_id.'" >';
        $form   .=  '<input type="text" name="is_active" value="'.$request->is_active.'" >';
        $form   .=  '<input type="text" name="is_searchable" value="'.$request->is_searchable.'" >';
        $form   .=  '</form>';
        $checkformaction = "<script type=\"text/javascript\"> 
                document.getElementById('makeCategory').submit();
       </script>";
       echo $checkformaction;*/
    }

    /**
     * Destroy resources by given ids.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::withoutGlobalScope('active')
            ->findOrFail($id)
            ->delete();

        return back()->withSuccess(trans('admin::messages.resource_deleted', ['resource' => $this->getLabel()]));
    }
}
