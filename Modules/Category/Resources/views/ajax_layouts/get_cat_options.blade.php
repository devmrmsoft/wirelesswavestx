<div class="form-group " id="categories-{{$id}}" data-count="{{$ele_count}}">
	<!-- <label for="tax_class_id" class="col-md-3 control-label text-left">@if($ele_count == '3') @if($headphone_label != '0') {{ $headphone_label }} @else {{ ucwords($mainRootCatId->name) }} Brand @endif @else {{ ucwords($parentCat->name) }} @endif</label> -->
	<label for="tax_class_id" class="col-md-3 control-label text-left">{{ ucwords($parentCat->name) }}</label>
	<div class="col-md-8">
		<select name="categories[]" class="form-control custom-select-black @if($hasChild == 'no-child') create-model-category @endif" @if($hasChild == 'no-child') onchange="getChildForRootCat(this);addNewCat(this);addHiddenFieldForPhoneModel(this);" @else  onchange="getChildForRootCat(this);createHeadphoneBluetooth(this, {{ $parentCat->id }});addNewCat(this, {{ $parentCat->id }} );" @endif>
			<option value="">Please Select</option>
			@foreach($childCats as $cat)
			<option value="{{ $cat->id }}">{{ $cat->name }}</option>
			@endforeach

			@if($hasChild == 'no-child')
			<!--<option value="create-model">Add New</option>-->
			@else
			<!--<option value="create-model-{{ $parentCat->id }}">Add New</option>-->
			@endif

		</select>
	</div>
	<div class="col-md-1">
		<button type="button" class="btn btn-default delete-category pull-right" @if($hasChild == 'no-child')  onclick="savePhoneModel(this , {{$id}} )"  @else onclick="removeThisCategory(this)" @endif data-toggle="tooltip" title=""> @if($hasChild == 'no-child') <i class="fa fa-plus"></i> @else  <i class="fa fa-trash"></i> @endif</button>
	</div>
</div>