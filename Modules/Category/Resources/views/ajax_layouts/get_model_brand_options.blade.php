<div class="form-group " id="categories-{{$id}}" data-count="{{$ele_count}}">
	<label for="tax_class_id" class="col-md-3 control-label text-left">Brand Models</label>
	<div class="col-md-8">
		<select @forelse($childCats as $cat) name="categories[]" @empty @endforelse class="form-control example-basic-multiple @if($hasChild == 'no-child') create-model-category @endif" @if($hasChild == 'no-child') onchange="addNewBrand(this)" @else  onchange="getChildForAccBrand(this);addNewBrand(this, {{ $parentCat->id }} );" @endif  multiple="multiple" id="brand_phone_models">
			<option value="">Please Select</option>
			@foreach($childCats as $cat)
			<option value="{{ $cat->id }}">{{ $cat->name }}</option>
			@endforeach
			
			@if($hasChild == 'no-child')
			<!--<option value="create-model">Add New</option>-->
			@else
			<!--<option value="create-model-{{ $parentCat->id }}">Add New</option>-->
			@endif
			
		</select>
	</div>
	<div class="col-md-1">
		<button type="button" class="btn btn-default delete-category pull-right" @if($hasChild == 'no-child')  onclick="saveBrandModel(this , {{$id}} )"  @else onclick="removeThisCategory(this)" @endif data-toggle="tooltip" title=""> @if($hasChild == 'no-child') <i class="fa fa-plus"></i> @else  <i class="fa fa-trash"></i> @endif</button>
	</div>
</div>
<input type="hidden" id="new_brand_phone_models" name="new_brand_phone_models[]" value="">