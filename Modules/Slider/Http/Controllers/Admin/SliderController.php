<?php

namespace Modules\Slider\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Modules\Slider\Entities\Slider;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Slider\Http\Requests\SaveSliderRequest;
use Illuminate\Http\Request;
use DB;

class SliderController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Slider::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'slider::sliders.slider';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'slider::admin.sliders';

    /**
     * Form requests for the resource.
     *
     * @var array
     */
    protected $validation = SaveSliderRequest::class;

    public function popup_index(){

        $popup  =   DB::table('popups')->first();
        $data   =   [

                        'popup'  =>  $popup,

                        ];
        return view('slider::admin.sliders.popup_index', $data );
    }

    public function popup_update(Request $request){
        // dd($request->all());
        $postUpdate     =   DB::table('popups')
                                    ->where('id', $request->popup_id)
                                    ->update([
                                        'popup_title'   =>  $request->popup_title,
                                        'popup_content'   =>  $request->popup_content,
                                    ]);
        
        $popup  =   DB::table('popups')->first();
        $data   =   [

                        'popup'  =>  $popup,

                        ];
        return view('slider::admin.sliders.popup_index', $data );
        return view('slider::admin.sliders.popup_index', $data );
    }
}
