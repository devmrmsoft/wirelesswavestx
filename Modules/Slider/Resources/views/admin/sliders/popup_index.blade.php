@extends('admin::layout')

@push('styles')
<style type="text/css">
    .m-top-2{
        margin-top: 20px;
    }
    .m-bot-2{
        margin-bottom: 20px;
    }
    .d-none{
        display: none;
    }
    .order-wrapper {
        background: #fff;
        padding: 15px;
        border-radius: 3px;
    }
</style>

@endpush

@component('admin::components.page.header')
    @slot('title', 'Pop Up')

    <li><a href="{{ route('admin.sliders.index') }}">{{ trans('slider::sliders.sliders') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('slider::sliders.slider')]) }}</li>
@endcomponent

@section('content')
    <div class="order-wrapper">
        @isset($popup)
        <form method="POST" action="{{ route('admin.sliders.popup_update') }}" id="checkout-formt">
            <input type="hidden" name="popup_id" value="{{ $popup->id }}">

            {{ csrf_field() }}
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Pop Up Title</label>
                    <div class="col-md-9">
                        <input type="text" name="popup_title" class="form-control" value="@isset($popup){{ $popup->popup_title }}@endisset" required="">
                    </div>
                </div>
            </div>
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Pop Up Body</label>
                    <div class="col-md-9">
                        <textarea name="popup_content" class="form-control  wysiwyg" id="body" rows="10" cols="10" labelcol="2" aria-hidden="true" required="">@isset($popup){{ $popup->popup_content }}@endisset</textarea>
                    </div>
                </div>
            </div>
            <div class="row m-top-2">
                <div class="form-group ">
                    <label for="is_active" class="col-md-3 control-label text-left">Status</label>
                    <div class="col-md-9">
                        <div class="checkbox">
                            <input type="hidden" value="0" name="is_active">
                            <input type="checkbox" name="is_active" class="" id="is_active" value="1">
                            <label for="is_active">Enable the Pop Up</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-top-2 form-group">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

        </form>
        @endisset
    </div>
@endsection

@push('scripts')


@endpush