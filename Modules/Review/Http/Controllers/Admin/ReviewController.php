<?php

namespace Modules\Review\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Modules\Review\Entities\Review;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Admin\Ui\Facades\TabManager;
use Modules\Review\Http\Requests\UpdateReviewRequest;
use DB;

class ReviewController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Review::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'review::reviews.review';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'review::admin.reviews';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = UpdateReviewRequest::class;

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review = Review::withoutGlobalScope('approved')->findOrFail($id);

        $tabs = TabManager::get('reviews');

        return view('review::admin.reviews.edit', compact('review', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $review = Review::withoutGlobalScope('approved')->findOrFail($id);

        $review->update(request()->all());

        return back()->withSuccess(trans('admin::messages.resource_saved', ['resource' => $this->getLabel()]));
    }

    public function fIndex(){
        if(isset( $_GET['s'] )){
            if($_GET['s'] == 'pending'){
                $status =   '0';
            }else if($_GET['s'] == 'approved'){
                $status =   '1';
            }else{
                $status =   '3';
            }
            $reviews    =   DB::table('reviews')
                                ->join('product_translations','product_translations.product_id', '=', 'reviews.product_id')
                                ->where('is_approved', $status)
                                ->get();
            $data       =   [

                                'reviews'   =>  $reviews,

                            ];
           // dd($reviews);
            return view('review::admin.reviews.filter_index', $data );

        }
    }
}
