@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('review::reviews.reviews'))

    <li class="active">{{ trans('review::reviews.reviews') }}</li>
@endcomponent

@push('styles')

<style type="text/css">
    .filtertag-active {
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #0068e1;
        padding: 10px 15px;
        border: none;
        display: block;
        text-decoration: none;
    }
    .filtertag:hover{
        text-decoration: none;
        color: #fff;
    }
    .padding-0{
        padding-right:0;
        padding-left:0;
    }    
    .padding-r-0{
        padding-right:0;
    }
    .filtertag-active{
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #e9420b;
        padding: 10px 15px;
        border-right: 2px solid;
        display: block;
        text-decoration: none;
    }
    .filtertag{
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #0068e1;
        padding: 10px 15px;
        border: none;
        display: block;
        text-decoration: none;
        border-right: 2px solid;
    }
</style>

@endpush

@section('filter-form')
    <div class="row">
        <div class="col-md-2 padding-r-0">
            <a href="{{ route('admin.reviews.index') }}" class=" @isset($_GET['s']) filtertag @else filtertag-active @endisset">All Reviews</a>
        </div>

        <div class="col-md-2 padding-0">
            <a href="{{ route('admin.reviews.f_index') }}?s=pending" class=" @if( isset($_GET['s']) && $_GET['s']=='pending') filtertag-active @else filtertag @endif">Pending Reviews</a>
        </div>
        <div class="col-md-2 padding-0">
            <a href="{{ route('admin.reviews.f_index') }}?s=approved" class=" @if( isset($_GET['s']) && $_GET['s']=='approved') filtertag-active @else filtertag @endif">Approved Reviews</a>
        </div>

    </div>
@endsection

@component('admin::components.page.index_table')
    @slot('resource', 'reviews')
    @slot('name', trans('review::reviews.review'))

<div class="table-responsive">
    <table class="table table-striped table-hover" id="reviews-table">
        <thead>
            <tr>
                <th>Serial</th>
                <th>Product</th>
                <th>Reviewer Name</th>
                <th>Rating</th>
            </tr>
        </thead>

        <tbody>
            <?php $serialNum   =   1; ?>
            @foreach($reviews as $review)
            <tr>
                <td>#<?=$serialNum?></td>
                <td>{{ $review->name }}</td>
                <td>{{ $review->reviewer_name }}</td>
                <td>{{ $review->rating }}</td>
            </tr>
            <?php $serialNum   += 1; ?>
            @endforeach
        </tbody>

        @isset($tfoot)
            <tfoot>{{ $tfoot }}</tfoot>
        @endisset
    </table>
</div>

@endcomponent
@push('scripts')
    <script>

        $(document).ready( function () {
            $('#reviews-table .table').DataTable();
        } );
    </script>
@endpush
