@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('review::reviews.reviews'))

    <li class="active">{{ trans('review::reviews.reviews') }}</li>
@endcomponent

@push('styles')

<style type="text/css">
    .filtertag-active {
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #0068e1;
        padding: 10px 15px;
        border: none;
        display: block;
        text-decoration: none;
    }
    .filtertag:hover{
        text-decoration: none;
        color: #fff;
    }
    .padding-0{
        padding-right:0;
        padding-left:0;
    }    
    .padding-r-0{
        padding-right:0;
    }
    .filtertag-active{
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #e9420b;
        padding: 10px 15px;
        border-right: 2px solid;
        display: block;
        text-decoration: none;
    }
    .filtertag{
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #0068e1;
        padding: 10px 15px;
        border: none;
        display: block;
        text-decoration: none;
        border-right: 2px solid;
    }
</style>

@endpush

@section('filter-form')
    <div class="row">
        <div class="col-md-2 padding-r-0">
            <a href="{{ route('admin.reviews.index') }}" class=" @isset($_GET['s']) filtertag @else filtertag-active @endisset">All Reviews</a>
        </div>

        <div class="col-md-2 padding-0">
            <a href="{{ route('admin.reviews.f_index') }}?s=pending" class=" @if( isset($_GET['s']) && $_GET['s']=='pending') filtertag-active @else filtertag @endif">Pending Reviews</a>
        </div>
        <div class="col-md-2 padding-0">
            <a href="{{ route('admin.reviews.f_index') }}?s=approved" class=" @if( isset($_GET['s']) && $_GET['s']=='approved') filtertag-active @else filtertag @endif">Approved Reviews</a>
        </div>

    </div>
@endsection

@component('admin::components.page.index_table')
    @slot('resource', 'reviews')
    @slot('name', trans('review::reviews.review'))

    @slot('thead')
        <tr>
            @include('admin::partials.table.select_all')

            <th>{{ trans('review::reviews.table.product') }}</th>
            <th>{{ trans('review::reviews.table.reviewer_name') }}</th>
            <th>{{ trans('review::reviews.table.rating') }}</th>
            <th>{{ trans('review::reviews.table.approved') }}</th>
            <th data-sort>{{ trans('admin::admin.table.date') }}</th>
        </tr>
    @endslot
@endcomponent

@push('scripts')
    <script>
        new DataTable('#reviews-table .table', {
            columns: [
                { data: 'checkbox', orderable: false, searchable: false, width: '3%' },
                { data: 'product', name: 'product.price', orderable: false, searchable: false, defaultContent: '' },
                { data: 'reviewer_name' },
                { data: 'rating' },
                { data: 'status', name: 'is_approved', searchable: false },
                { data: 'created', name: 'created_at' },
            ],
        });
    </script>
@endpush
