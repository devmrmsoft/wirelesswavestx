<?php

namespace Modules\Support;

use NumberFormatter;
use JsonSerializable;
use InvalidArgumentException;
use Modules\Currency\Currency;
use Modules\Currency\Entities\CurrencyRate;
use Auth;
use DB;
use Modules\Cart\Facades\Cart;

class Money implements JsonSerializable
{
    public $amount;
    private $currency;

    public function __construct($amount, $currency)
    {
        /*if (Auth::check()){

            $listID = Auth::user()->ret_list;
            if($listID >= '1'){

                $retListTable   =   DB::table('retailer_lists')->where('id', $listID )->first();

                $retailer_discount_amount   =   $retListTable->list_price;
                $price  =   $amount;

                //$price  =   $price * (('100' - $retailer_discount_amount) / '100');
                $price  =   $price - $retailer_discount_amount;

            }else{
                $price =    $amount;
            }

        }else{
            $price =    $amount;
        }*/

        $this->amount = $amount;
     }

    public static function inDefaultCurrency($amount)
    {   
        return new self($amount, setting('default_currency'));
    }

    private function newInstance($amount)
    {
        return new self($amount, $this->currency);
    }

    public function amount()
    {
        /*changes for retailer*/
        /*if (Auth::check()){

            $listID = Auth::user()->ret_list;
            if($listID >= '1'){

                $retListTable   =   DB::table('retailer_lists')->where('id', $listID )->first();
                //return $retListTable->list_price;https://thevideobee.to/cxtth46l1in7.html
                $amount =   $retListTable->list_price;
                $price  =   $this->amount;
                //return $price;
                // $price  =   substr($price, 1);
                $price  =   $price * ((100- $amount) / 100);

            }else{
                $price =    $this->amount;
            }

        }else{
            $price =    $this->amount;
        }
        return $price;*/
        return $this->amount;
    }

    public function currency()
    {
        return $this->currency;
    }

    public function isZero()
    {
        return $this->amount == 0;
    }

    public function add($addend)
    {
        $addend = $this->convertToSameCurrency($addend);

        return $this->newInstance($this->amount + $addend->amount);
    }

    public function subtract($subtrahend)
    {
        $subtrahend = $this->convertToSameCurrency($subtrahend);

        return $this->newInstance($this->amount - $subtrahend->amount);
    }

    public function multiply($multiplier)
    {
        return $this->newInstance($this->amount * $multiplier);
    }

    public function divide($divisor)
    {
        return $this->newInstance($this->amount / $divisor);
    }

    public function lessThan($other)
    {
        return $this->amount < $other->amount;
    }

    public function lessThanOrEqual($other)
    {
        return $this->amount <= $other->amount;
    }

    public function greaterThan($other)
    {
        return $this->amount > $other->amount;
    }

    public function greaterThanOrEqual($other)
    {
        return $this->amount >= $other->amount;
    }

    private function convertToSameCurrency($other)
    {
        if ($this->isNotSameCurrency($other)) {
            $other = $other->convertToDefaultCurrency();
        }

        $this->assertSameCurrency($other);

        return $other;
    }

    public function isSameCurrency($other)
    {
        return $this->currency === $other->currency;
    }

    public function isNotSameCurrency($other)
    {
        return ! $this->isSameCurrency($other);
    }

    private function assertSameCurrency($other)
    {
        if ($this->isNotSameCurrency($other)) {
            throw new InvalidArgumentException('Mismatch money currency.');
        }
    }

    public function convertToDefaultCurrency()
    {
        $currencyRate = CurrencyRate::for($this->currency);

        if (is_null($currencyRate)) {
            throw new InvalidArgumentException('Cannot convert the money to the default currency.');
        }

        return new self($this->amount / $currencyRate, setting('default_currency'));
    }

    public function convertToCurrentCurrency($pId = null, $currencyRate = null)
    {
        return $this->convert(currency(), $currencyRate, $pId);
    }

    public function convert($currency, $currencyRate = null, $pId = null)
    {
        //$pId    =   0;
        $cart = Cart::instance();
        if($pId == 'cart'){
            // $this->amount  = '20';
            foreach ($cart->items() as $cartItem) {
                echo '<pre>'; print_r( $cartItem->product->id ); echo '</pre>';
            }
        }else{
        //dd($cart->items());
        if (Auth::check()){

            $listID = Auth::user()->ret_list;
            if($listID >= '1' && $listID != '4'){

                $retListTable   =   DB::table('retailer_lists')->where('id', $listID )->first();
                //dd($pId);

                $productPrices      =   DB::table('products')->where('id', $pId)->first();
                if(isset( $productPrices->retailer_prices )){
                        
                    $productPrices      =   $productPrices->retailer_prices;
                    //$productPrices        =   explode(',', $productPrices);
                    $productPrices      =   json_decode($productPrices, true);

                    //return $retListTable->list_price;
                    $amount =   $retListTable->list_price;
                    //dd($cart);
                    /*foreach ($cart->items() as $cartItem) {
                        echo '<pre>'; print_r( $cartItem->qty ); echo '</pre>';
                    }*/
                    if(isset($cartItem->qty)){
                        if($cartItem->qty > 0 || $cartItem->qty > '0'){
                            $productPrices[$listID] =   $productPrices[$listID] * $cartItem->qty;
                        }
                    }
                    $this->amount  =   $productPrices[$listID];
                }else{
                    $this->amount =    $this->amount;
                    /*foreach ($cart->items() as $cartItem) {
                        echo '<pre>'; print_r( $cartItem->qty ); echo '</pre>';
                    }*/
                }

                /*//return $retListTable->list_price;
                $amount =   $retListTable->list_price;
                
                //return $price;
                // $price  =   substr($price, 1);
                $this->amount  =   $price * ((100- $amount) / 100);*/

            }else{
                $this->amount =    $this->amount;
            }

        }else{
            $this->amount =    $this->amount;
        }
        }
        $currencyRate = $currencyRate ?: CurrencyRate::for($currency);

        if (is_null($currencyRate)) {
            throw new InvalidArgumentException("Cannot convert the money to currency [$currency].");
        }

        return new self($this->amount * $currencyRate, $currency);
    }

    public function round($precision = null, $mode = null)
    {
        if (is_null($precision)) {
            $precision = Currency::subunit($this->currency);
        }

        $amount = round($this->amount, $precision, $mode);

        return $this->newInstance($amount);
    }

    public function format($currency = null, $locale = null, $customer_id = null, $product_id = null, $pro_qty = null)
    {
        // change price to MSRP product prices
            if($customer_id != null && $product_id != null){
                $msrp_customer = DB::table('users')->find($customer_id);
                $listID = $msrp_customer->ret_list;
                if($listID >= '1' && $listID != '4'){
                    $retListTable   =   DB::table('retailer_lists')->where('id', $listID )->first();
                    $productPrices      =   DB::table('products')->where('id', $product_id)->first();
                    if(isset( $productPrices->retailer_prices )){
                            
                        $productPrices      =   $productPrices->retailer_prices;

                        $productPrices      =   json_decode($productPrices, true);

                        $amount =   $retListTable->list_price;

                        if(isset($cartItem->qty)){
                            if($cartItem->qty > 0 || $cartItem->qty > '0'){
                                $productPrices[$listID] =   $productPrices[$listID] * $cartItem->qty;
                            }
                        }
                        $msrp_amount  =   $productPrices[$listID];
                        if($pro_qty != null){
                            $msrp_amount = $msrp_amount * $pro_qty;
                        }
                    }else{
                        $msrp_amount =    $this->amount;
                    }

                }else{
                    $msrp_amount =    $this->amount;
                }


            }else{
                $msrp_amount =    $this->amount;
                }
        
        // end change MSRP product prices

        $currency = $currency ?: currency();
        $locale = $locale ?: locale();

        $numberFormatter = new NumberFormatter($locale, NumberFormatter::CURRENCY);

        $amount = $numberFormatter->formatCurrency($msrp_amount, $currency);

        /**
         * Fix: Hungarian Forint outputs wrong currency format.
         *
         * @see https://github.com/MehediDracula/FleetCart/issues/18
         */
        if (currency() === 'HUF') {
            $amount = str_replace(',00', '', $amount);
        }

        return $amount;
    }

    public function jsonSerialize()
    {
        return [
            'amount' => $this->amount,
            'formatted' => $this->format(),
            'currency' => $this->currency,
        ];
    }

    public function __toString()
    {
        return (string) $this->amount;
    }
}
