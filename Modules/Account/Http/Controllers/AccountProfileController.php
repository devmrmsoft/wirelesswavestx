<?php

namespace Modules\Account\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\User\Http\Requests\UpdateProfileRequest;
use DB;

class AccountProfileController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $my = auth()->user();

        return view('public.account.profile.edit', compact('my'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Modules\User\Http\Requests\UpdateProfileRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request)
    {
        $this->bcryptPassword($request);

        auth()->user()->update($request->all());

        $data   =   array();

        if(isset( $request->update_user_meta_id ) ){

            if($request->file("personal_id")){
                $uploadID       =   $this->uploadID($request->file("personal_id"));
                $data['personal_id']    =   $uploadID;
            }
            if($request->file("tax_id")){
                $uploadTaxId        =   $this->uploadTaxId($request->file("tax_id"));
                $data['tax_id']     =   $uploadTaxId;
            }

            $data['business_name']      =   $request->business_name;
            $data['business_address']   =   $request->business_address;
            $data['shipping_address']   =   $request->shipping_address;
            $data['city']               =   $request->city;
            $data['state']              =   $request->state;
            $data['zip']                =   $request->zip;
            $data['contact_number']     =   $request->contact_number;
            $data['business_number']    =   $request->business_number;


            $userMeta   =   DB::table('user_meta')
                                ->where('id', $request->update_user_meta_id)
                                ->update($data);
        }



        return back()->withSuccess(trans('account::messages.profile_updated'));
    }



    private function uploadTaxId($file){
        if(file_exists($file)){
            //Move File
            $path="public/assets/users/tax-id";
            $custom=date('joYhis')."_user_";
            $file->move($path,$custom.$file->getClientOriginalName());
            return $custom.$file->getClientOriginalName();
        }else{
            return "";
        }
    }
    private function uploadID($file){
        if(file_exists($file)){
            //Move File
            $path="public/assets/users/personal-id";
            $custom=date('joYhis')."_user_";
            $file->move($path,$custom.$file->getClientOriginalName());
            return $custom.$file->getClientOriginalName();
        }else{
            return "";
        }
    }
    /**
     * Bcrypt user password.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    private function bcryptPassword($request)
    {
        if ($request->filled('password')) {
            return $request->merge(['password' => bcrypt($request->password)]);
        }

        unset($request['password']);
    }
}
