<?php

namespace Modules\Account\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use DB;

class AccountDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $my = auth()->user();
        $current_credit = Auth::user()->credit_usage;
        $last_usage_date = Auth::user()->last_usage_date;
        $recentOrders = auth()->user()->recentOrders(5);

        //USER CREDIT LIMIT AND DURATION
        
        if(Auth::user()->ret_list != 0)
        {
            $usermeta   =   DB::table('user_meta')->select('extra_details','user_code')->where('user_id',Auth::user()->id)->first();
            $decode = json_decode($usermeta->extra_details);
            
            if($decode->credit_line != null && $decode->credit_limit != null && isset($decode->yes_credit_line))
            {
                $user_code = $usermeta->user_code;
                $duration = intval($decode->credit_line);
                $limit = intval($decode->credit_limit);
    
                return view('public.account.dashboard.index', compact('my', 'recentOrders','current_credit','last_usage_date','duration','limit','user_code'));
            }
            else
            {
                $user_code = $usermeta->user_code;
                $current_credit = 0;
                $last_usage_date = "Not set";
                $duration = "0";
                $limit = "Not set";
                return view('public.account.dashboard.index', compact('my', 'recentOrders','current_credit','last_usage_date','duration','limit','user_code'));   
            }    
        }
        else
            {
                $current_credit = 0;
                $last_usage_date = "Not set";
                $duration = "0";
                $limit = "Not set";
                return view('public.account.dashboard.index', compact('my', 'recentOrders','current_credit','last_usage_date','duration','limit'));   
            }
        
        
    }
}
 