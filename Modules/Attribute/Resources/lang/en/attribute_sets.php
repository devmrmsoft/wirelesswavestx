<?php

return [
    'attribute_set' => 'Filter Set',
    'attribute_sets' => 'Filter Sets',
    'table' => [
        'name' => 'Name',
    ],
    'tabs' => [
        'group' => [
            'attribute_set_information' => 'Filter Set Information',
        ],
        'general' => 'General',
    ],
];
