<?php

return [
    'attributes' => [
        'attribute_set_id' => 'Filter Set',
        'name' => 'Name',
        'categories' => 'Categories',
        'is_filterable' => 'Filterable',
    ],
    'attribute_sets' => [
        'name' => 'Name',
    ],
    'product_attributes' => [
        'attributes.*.attribute_id' => 'Filter',
        'attributes.*.values' => 'Values',
    ],
];
