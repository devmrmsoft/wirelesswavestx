<?php

namespace Modules\Checkout\Http\Controllers;

use Exception;
use Modules\Support\Country;
use Modules\Cart\Facades\Cart;
use Modules\Page\Entities\Page;
use Illuminate\Routing\Controller;
use Modules\Payment\Facades\Gateway;
use Modules\Checkout\Events\OrderPlaced;
use Modules\User\Services\CustomerService; 
use Modules\Checkout\Services\OrderService;
use Modules\Order\Http\Requests\StoreOrderRequest;
use Illuminate\Support\Facades\Auth;
use Modules\User\Entities\User;
use Illuminate\Http\Request;
use DB;
class CheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['cart_not_empty', 'check_stock', 'check_coupon_usage_limit']);
    }
    
    public function shipstation()
    {
        return view('order_ship');
    }
    
   public function fetch_store(Request $req)
    {
        $stores = DB::table('retailer_stores')->where('id',$req->id)->first();
   
        return json_encode($stores);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//dd(Gateway::all());
    
        $cart = Cart::instance();
        $countries = Country::supported();
        $gatewayss = Gateway::all();
        $termsPageURL = Page::urlForPage(setting('storefront_terms_page'));
        $check = 0;

        if(Auth::user() && Auth::user()->ret_list != 0)
        {
            
            //Extract Duration and Limit from User Meta
            $usermeta   =   DB::table('user_meta')->select('extra_details')->where('user_id',Auth::user()->id)->first();
            $decode = json_decode($usermeta->extra_details);
            
            //$duration = intval($decode->yes_credit_line.$decode->credit_line);
            $limit = intval($decode->credit_limit);
            $last_usage_date = (array)  DB::table('users')->select('last_usage_date')->where('id',Auth::user()->id)->first();
            $current_date = date('Y-m-d');
            $methods = ['paypal_express','cod','bank_transfer','check_payment','wire_transfer','company_cheque','open_shipment','personal_cheque','credit_card'];
            
            

            $credit_usage =  Auth::user()->credit_usage;
            if($credit_usage >= $limit || $current_date > $last_usage_date['last_usage_date'] && $last_usage_date['last_usage_date'] != null )
            {
                
                //$gateways = collect([(object) $gatewayss['paypal_express']]);
                
                $gateways = $gatewayss;
                $check = 1;
                return view('public.checkout.create', compact('cart', 'countries', 'gateways', 'termsPageURL','check'));
            }
            else
            {
                //dd($gatewayss);
                if(isset($decode->wholesaler_secure_payment))
                {
                    $filter = $decode->wholesaler_secure_payment;

                    foreach ($filter as $key => $value) {
                        # code...
                        if (($key = array_search($value, $methods)) !== false) {
                            unset($methods[$key]);
                        }

                    }
                    
                    foreach ($methods as $value) 
                    {
                         $gatewayss->forget($value);
                    }

                }
                
                if(isset($decode->wholesaler_non_secure_payment))
                {
                    $filter = $decode->wholesaler_non_secure_payment;

                    foreach ($filter as $key => $value) {
                        # code...
                        if (($key = array_search($value, $methods)) !== false) {
                            unset($methods[$key]);
                        }

                    }
                    
                    foreach ($methods as $value) 
                    {
                         $gatewayss->forget($value);
                    }
                    
                }
                
                //dd($gatewayss);

                $gateways = $gatewayss;
                $check = 0;
                return view('public.checkout.create', compact('cart', 'countries', 'gateways', 'termsPageURL','check'));   
            
            }
        }
        else
        {
            
            $gateways = $gatewayss;
            $check = 0;
            return view('public.checkout.create', compact('cart', 'countries', 'gateways', 'termsPageURL','check'));
        }

            
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Modules\Order\Http\Requests\StoreOrderRequest $request
     * @param \Modules\User\Services\CustomerService $customerService
     * @param \Modules\Checkout\Services\OrderService $orderService
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CustomerService $customerService, OrderService $orderService)
    {
        if (auth()->guest() && $request->create_an_account) {
            $customerService->register($request)->login();
        }
    if($request->payment_method == 'credit_card'){
        
        $curl = curl_init();
        $card = str_replace(' ', '', $request->cardNumber);
        $code = str_replace(' ', '', $request->cardCode);
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.authorize.net/xml/v1/request.api",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>"
              {\r\n    \"createTransactionRequest\": 
                  {\r\n        \"merchantAuthentication\": 
                      {\r\n            \"name\": \"654gCG9gu\",
                      \r\n            \"transactionKey\": \"35Q6EVL33qq7qc3w\"\r\n        
                          
                      },
                      \r\n        \"refId\": \"123456\",
                      \r\n        \"transactionRequest\": 
                          {\r\n            \"transactionType\": \"authCaptureTransaction\",
                          \r\n            \"amount\": \"5\",
                          \r\n            \"payment\": 
                              {\r\n                \"creditCard\": 
                                  {\r\n                    \"cardNumber\": \"{$card}\",
                                  \r\n                    \"expirationDate\": \"{$request->expirationDate}\",
                                  \r\n                    \"cardCode\": \"{$code}\"\r\n                }\r\n
                              }\r\n
                        }\r\n
                      
                  }\r\n
                  
              }",
          
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));
        
        $response = curl_exec($curl);
        /*var_dump($response);
        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200){
            dd('error');
        }else{
            dd('success');
        }*/
        $res = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true );
        /*$tmp_bom = pack('H*','EFBBBF');
        $result = preg_replace("/^$tmp_bom/", '', $response);
        unset($tmp_bom);
        $result = json_decode($result);*/
        //dd($res);
        curl_close($curl);
        echo $request->cardCode;
        echo '<pre>';print_r($res);echo '</pre>';die;
        if(!isset($res['transactionResponse'])){
            return back()->withInput()->withError('Card details invalid');
        }
        if($res['transactionResponse']['responseCode'] != '1'){
            dd($res['transactionResponse']['errors'][0]['errorText']);
        }
    }
        // Limit check and show payment options according to limit By Shahrukh

        $order = $orderService->create($request);
        
        // // start send order info to shipstation via curl API
        
        
        //                 $cartt = Cart::items();
                        
        //                 $sendDataToShipStation  =   [];
        //                 $sendDataToShipStation['orderId']   =   $order->id ;
        //                 $sendDataToShipStation['orderNumber']   =   $order->id ;
        //                 $sendDataToShipStation['orderDate']   =   $order->created_at->format('m-d-Y');
        //                 $sendDataToShipStation['orderStatus']   =   "awaiting_payment" ;
        //                 $sendDataToShipStation['customerUsername']   =   '"'.$order->customer_first_name.' '.$order->customer_last_name.'"';
        //                 $sendDataToShipStation['customerEmail']   =   $order->customer_email ;
        //                 $sendDataToShipStation['billTo']['name']   =   '"'.$order->billing_first_name.' '.$order->billing_last_name.'"';
        //                 $sendDataToShipStation['billTo']['street1']   =   $order->billing_address_1;
        //                 $sendDataToShipStation['billTo']['city']   =   $order->billing_city;
        //                 $sendDataToShipStation['billTo']['state']   =   $order->billing_state;
        //                 $sendDataToShipStation['billTo']['postalCode']   =   $order->billing_zip;
        //                 $sendDataToShipStation['billTo']['country']   =   null;
        //                 $sendDataToShipStation['billTo']['phone']   =   $order->customer_phone;
        //                 $sendDataToShipStation['shipTo']['name']   =   $order->shipping_first_name.' '.$order->shipping_last_name;
        //                 $sendDataToShipStation['shipTo']['street1']   =   $order->shipping_address_1;
        //                 $sendDataToShipStation['shipTo']['city']   =   $order->shipping_city;
        //                 $sendDataToShipStation['shipTo']['state']   =   $order->shipping_state;
        //                 $sendDataToShipStation['shipTo']['postalCode']   =   $order->shipping_zip;
        //                 $sendDataToShipStation['shipTo']['country']   =   null;
        //                 $sendDataToShipStation['shipTo']['phone']   =   $order->customer_phone;
                        
                        
        //                 $orderItemsForShipstation   =   [];
        //                 $updateIndex    =   0;
                        
        //                 foreach($cartt as $key=>$item)
        //                 {
        //                     $sendProductName    =   $item->product['slug'];
        //                     $sendProductSku    =   $item->product['sku'];
        //                     $sendQuantity   =   $item->qty;
        //                     $sendPrice      =   $item->product['price'];
        //                     $sendProductId    =   $item->product['id'];
                            
        //                     $senditemsToShipStation[$updateIndex]['lineItemKey']    =   $sendProductSku;
        //                     $senditemsToShipStation[$updateIndex]['sku']    =   $sendProductSku;
        //                     $senditemsToShipStation[$updateIndex]['name']    =   $sendProductName;
        //                     $senditemsToShipStation[$updateIndex]['productId']    =   $sendProductId;
        //                     $senditemsToShipStation[$updateIndex]['quantity']    =   $sendQuantity;
        //                     $senditemsToShipStation[$updateIndex]['unitPrice']    =   "$sendPrice";
        //                     $updateIndex++;
        //                 }
                        
        //                 $sendDataToShipStation['items'] =  $senditemsToShipStation  ;
        //                 $senditemsToShipStation  =   json_encode( (array)$sendDataToShipStation );
        //                 /*print_r($senditemsToShipStation);die;*/
        //                 $curl = curl_init();
        //                 $username = '708b9f30a65a463cb9b2219b9b459413';
        //                 $password   =  '01d30413573d4a9da8a052a56b75f6e5';
        //                 $check = [];
                        
        //                     curl_setopt_array($curl, array(
        //                       CURLOPT_URL => "https://ssapi.shipstation.com/orders/createorder",
        //                       CURLOPT_RETURNTRANSFER => true,
        //                       CURLOPT_ENCODING => "",
        //                       CURLOPT_MAXREDIRS => 10,
        //                       CURLOPT_TIMEOUT => 0,
        //                       CURLOPT_FOLLOWLOCATION => true,
        //                       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            
        //                     CURLOPT_POSTFIELDS =>$senditemsToShipStation,
        //                       CURLOPT_HTTPHEADER => array(
        //                         "Host: ssapi.shipstation.com",
        //                         "Authorization: Basic " . base64_encode($username . ":" . $password),
        //                         "Content-Type: application/json"
        //                       ),
        //                     ));
                            
        //                     $response = curl_exec($curl);
                        
            
        //     curl_close($curl);
       
        // // end send order info to shipstation via curl API
        $money = $order->total;

        $gateway = Gateway::get($request->payment_method);

        //dd($limit);
        
        if(Auth::user() && Auth::user()->ret_list != 0)
        {
            $user = DB::table('users')->where('id',Auth::user()->id)->first();

            $usermeta = DB::table('user_meta')->select('extra_details')->where('user_id',Auth::user()->id)->first();
            $decode = json_decode($usermeta->extra_details);
            $duration = intval($decode->credit_line);
            $limit = intval($decode->credit_limit);
            
            if($request->payment_method != 'paypal_express')
            {   
                //For  Retailer not paying with Paypal
                
                if($user->credit_usage < $limit)
                {
    
                    if($money->amount+$user->credit_usage <= $limit)
                    {
    
                        $new_credit_usage = $money->amount+$user->credit_usage;
                        
                        DB::table('users')->where('id',Auth::user()->id)->update(['credit_usage'=>$new_credit_usage]);
                        
                        if($user->credit_usage == 0)
                        {
                            $new_usage_date = date('Y-m-d');
                            $new_last_usage_date = date('Y-m-d', strtotime('+'.$duration.' day'));
                            DB::table('users')->where('id',Auth::user()->id)->update(['usage_date'=> $new_usage_date,'last_usage_date'=> $new_last_usage_date]);
                        } 
                        
                        // start send order info to shipstation via curl API
        
        
                        $cartt = Cart::items();
                        
                        $sendDataToShipStation  =   [];
                        $sendDataToShipStation['orderId']   =   $order->id ;
                        $sendDataToShipStation['orderNumber']   =   $order->id ;
                        $sendDataToShipStation['orderDate']   =   $order->created_at->format('m-d-Y');
                        $sendDataToShipStation['orderStatus']   =   "awaiting_payment" ;
                        $sendDataToShipStation['customerUsername']   =   '"'.$order->customer_first_name.' '.$order->customer_last_name.'"';
                        $sendDataToShipStation['customerEmail']   =   $order->customer_email ;
                        $sendDataToShipStation['billTo']['name']   =   '"'.$order->billing_first_name.' '.$order->billing_last_name.'"';
                        $sendDataToShipStation['billTo']['street1']   =   $order->billing_address_1;
                        $sendDataToShipStation['billTo']['city']   =   $order->billing_city;
                        $sendDataToShipStation['billTo']['state']   =   $order->billing_state;
                        $sendDataToShipStation['billTo']['postalCode']   =   $order->billing_zip;
                        $sendDataToShipStation['billTo']['country']   =   null;
                        $sendDataToShipStation['billTo']['phone']   =   $order->customer_phone;
                        $sendDataToShipStation['shipTo']['name']   =   $order->shipping_first_name.' '.$order->shipping_last_name;
                        $sendDataToShipStation['shipTo']['street1']   =   $order->shipping_address_1;
                        $sendDataToShipStation['shipTo']['city']   =   $order->shipping_city;
                        $sendDataToShipStation['shipTo']['state']   =   $order->shipping_state;
                        $sendDataToShipStation['shipTo']['postalCode']   =   $order->shipping_zip;
                        $sendDataToShipStation['shipTo']['country']   =   null;
                        $sendDataToShipStation['shipTo']['phone']   =   $order->customer_phone;
                        
                        
                        $orderItemsForShipstation   =   [];
                        $updateIndex    =   0;
                        
                        foreach($cartt as $key=>$item)
                        {
                            $sendProductName    =   $item->product['slug'];
                            $sendProductSku    =   $item->product['sku'];
                            $sendQuantity   =   $item->qty;
                            $sendPrice      =   $item->product['price'];
                            $sendProductId    =   $item->product['id'];
                            
                            $senditemsToShipStation[$updateIndex]['lineItemKey']    =   $sendProductSku;
                            $senditemsToShipStation[$updateIndex]['sku']    =   $sendProductSku;
                            $senditemsToShipStation[$updateIndex]['name']    =   $sendProductName;
                            $senditemsToShipStation[$updateIndex]['productId']    =   $sendProductId;
                            $senditemsToShipStation[$updateIndex]['quantity']    =   $sendQuantity;
                            $senditemsToShipStation[$updateIndex]['unitPrice']    =   "$sendPrice";
                            $updateIndex++;
                        }
                        
                        $sendDataToShipStation['items'] =  $senditemsToShipStation  ;
                        $senditemsToShipStation  =   json_encode( (array)$sendDataToShipStation );
                        /*print_r($senditemsToShipStation);die;*/
                        $curl = curl_init();
                        $username = '708b9f30a65a463cb9b2219b9b459413';
                        $password   =  '01d30413573d4a9da8a052a56b75f6e5';
                        $check = [];
                        
                            curl_setopt_array($curl, array(
                              CURLOPT_URL => "https://ssapi.shipstation.com/orders/createorder",
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_ENCODING => "",
                              CURLOPT_MAXREDIRS => 10,
                              CURLOPT_TIMEOUT => 0,
                              CURLOPT_FOLLOWLOCATION => true,
                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            
                            CURLOPT_POSTFIELDS =>$senditemsToShipStation,
                              CURLOPT_HTTPHEADER => array(
                                "Host: ssapi.shipstation.com",
                                "Authorization: Basic " . base64_encode($username . ":" . $password),
                                "Content-Type: application/json"
                              ),
                            ));
                            
                            $response = curl_exec($curl);
                        
            
                        curl_close($curl);
                   
                    //  end send order info to shipstation via curl API
    
                        try {
                            $response = $gateway->purchase($order, $request);
                        } catch (Exception $e) {
                            $orderService->delete($order);
    
                            return back()->withInput()->withError($e->getMessage());
                        }
    
                        if ($response->isRedirect()) {
                            return redirect($response->getRedirectUrl());
                        } elseif ($response->isSuccessful()) {
                            $order->storeTransaction($response);
    
                            event(new OrderPlaced($order));
    
                            return redirect()->route('checkout.complete.show');
                        }
    
                        $orderService->delete($order);
    
                        return back()->withInput()->withError($response->getMessage());
                        
                    }
                    else{
                           
                            return back()->withInput()->withError("Your Order total exceeds your credit limit. kindly use PayPal for this transaction.");
                    }        
                        
                }
                else
                {
                    //dd('else part 2');
                    return back()->withInput()->withError($response->getMessage("Your Order total exceeds your credit limit. kindly use PayPal for this transaction.."));
                }
                
    
            }
            else
            {  
                //For  Retailer paying with Paypal
                // start send order info to shipstation via curl API
        
        
                        $cartt = Cart::items();
                        
                        $sendDataToShipStation  =   [];
                        $sendDataToShipStation['orderId']   =   $order->id ;
                        $sendDataToShipStation['orderNumber']   =   $order->id ;
                        $sendDataToShipStation['orderDate']   =   $order->created_at->format('m-d-Y');
                        $sendDataToShipStation['orderStatus']   =   "awaiting_shipment" ;
                        $sendDataToShipStation['customerUsername']   =   '"'.$order->customer_first_name.' '.$order->customer_last_name.'"';
                        $sendDataToShipStation['customerEmail']   =   $order->customer_email ;
                        $sendDataToShipStation['billTo']['name']   =   '"'.$order->billing_first_name.' '.$order->billing_last_name.'"';
                        $sendDataToShipStation['billTo']['street1']   =   $order->billing_address_1;
                        $sendDataToShipStation['billTo']['city']   =   $order->billing_city;
                        $sendDataToShipStation['billTo']['state']   =   $order->billing_state;
                        $sendDataToShipStation['billTo']['postalCode']   =   $order->billing_zip;
                        $sendDataToShipStation['billTo']['country']   =   null;
                        $sendDataToShipStation['billTo']['phone']   =   $order->customer_phone;
                        $sendDataToShipStation['shipTo']['name']   =   $order->shipping_first_name.' '.$order->shipping_last_name;
                        $sendDataToShipStation['shipTo']['street1']   =   $order->shipping_address_1;
                        $sendDataToShipStation['shipTo']['city']   =   $order->shipping_city;
                        $sendDataToShipStation['shipTo']['state']   =   $order->shipping_state;
                        $sendDataToShipStation['shipTo']['postalCode']   =   $order->shipping_zip;
                        $sendDataToShipStation['shipTo']['country']   =   null;
                        $sendDataToShipStation['shipTo']['phone']   =   $order->customer_phone;
                        
                        
                        $orderItemsForShipstation   =   [];
                        $updateIndex    =   0;
                        
                        foreach($cartt as $key=>$item)
                        {
                            $sendProductName    =   $item->product['slug'];
                            $sendProductSku    =   $item->product['sku'];
                            $sendQuantity   =   $item->qty;
                            $sendPrice      =   $item->product['price'];
                            $sendProductId    =   $item->product['id'];
                            
                            $senditemsToShipStation[$updateIndex]['lineItemKey']    =   $sendProductSku;
                            $senditemsToShipStation[$updateIndex]['sku']    =   $sendProductSku;
                            $senditemsToShipStation[$updateIndex]['name']    =   $sendProductName;
                            $senditemsToShipStation[$updateIndex]['productId']    =   $sendProductId;
                            $senditemsToShipStation[$updateIndex]['quantity']    =   $sendQuantity;
                            $senditemsToShipStation[$updateIndex]['unitPrice']    =   "$sendPrice";
                            $updateIndex++;
                        }
                        
                        $sendDataToShipStation['items'] =  $senditemsToShipStation  ;
                        $senditemsToShipStation  =   json_encode( (array)$sendDataToShipStation );
                        /*print_r($senditemsToShipStation);die;*/
                        $curl = curl_init();
                        $username = '708b9f30a65a463cb9b2219b9b459413';
                        $password   =  '01d30413573d4a9da8a052a56b75f6e5';
                        $check = [];
                        
                            curl_setopt_array($curl, array(
                              CURLOPT_URL => "https://ssapi.shipstation.com/orders/createorder",
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_ENCODING => "",
                              CURLOPT_MAXREDIRS => 10,
                              CURLOPT_TIMEOUT => 0,
                              CURLOPT_FOLLOWLOCATION => true,
                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            
                            CURLOPT_POSTFIELDS =>$senditemsToShipStation,
                              CURLOPT_HTTPHEADER => array(
                                "Host: ssapi.shipstation.com",
                                "Authorization: Basic " . base64_encode($username . ":" . $password),
                                "Content-Type: application/json"
                              ),
                            ));
                            
                            $response = curl_exec($curl);
                        
            
                        curl_close($curl);
                   
                    // end send order info to shipstation via curl API
                
                
            
                try {
                        
                        $response = $gateway->purchase($order, $request);
                        } catch (Exception $e) {
                            $orderService->delete($order);
    
                            return back()->withInput()->withError($e->getMessage());
                        }
    
                        if ($response->isRedirect()) {
                            return redirect($response->getRedirectUrl());
                        } elseif ($response->isSuccessful()) {
                            $order->storeTransaction($response);
    
                            event(new OrderPlaced($order));
    
                            return redirect()->route('checkout.complete.show');
                        }
    
                        $orderService->delete($order);
    
                        return back()->withInput()->withError($response->getMessage());
    
            }    
            
            
        }
        else
        {
            // For normal user paying with Paypal
            // start send order info to shipstation via curl API
        
        
                        $cartt = Cart::items();
                        
                        $sendDataToShipStation  =   [];
                        $sendDataToShipStation['orderId']   =   $order->id ;
                        $sendDataToShipStation['orderNumber']   =   $order->id ;
                        $sendDataToShipStation['orderDate']   =   $order->created_at->format('m-d-Y');
                        $sendDataToShipStation['orderStatus']   =   "awaiting_shipment" ;
                        $sendDataToShipStation['customerUsername']   =   '"'.$order->customer_first_name.' '.$order->customer_last_name.'"';
                        $sendDataToShipStation['customerEmail']   =   $order->customer_email ;
                        $sendDataToShipStation['billTo']['name']   =   '"'.$order->billing_first_name.' '.$order->billing_last_name.'"';
                        $sendDataToShipStation['billTo']['street1']   =   $order->billing_address_1;
                        $sendDataToShipStation['billTo']['city']   =   $order->billing_city;
                        $sendDataToShipStation['billTo']['state']   =   $order->billing_state;
                        $sendDataToShipStation['billTo']['postalCode']   =   $order->billing_zip;
                        $sendDataToShipStation['billTo']['country']   =   null;
                        $sendDataToShipStation['billTo']['phone']   =   $order->customer_phone;
                        $sendDataToShipStation['shipTo']['name']   =   $order->shipping_first_name.' '.$order->shipping_last_name;
                        $sendDataToShipStation['shipTo']['street1']   =   $order->shipping_address_1;
                        $sendDataToShipStation['shipTo']['city']   =   $order->shipping_city;
                        $sendDataToShipStation['shipTo']['state']   =   $order->shipping_state;
                        $sendDataToShipStation['shipTo']['postalCode']   =   $order->shipping_zip;
                        $sendDataToShipStation['shipTo']['country']   =   null;
                        $sendDataToShipStation['shipTo']['phone']   =   $order->customer_phone;
                        
                        
                        $orderItemsForShipstation   =   [];
                        $updateIndex    =   0;
                        
                        foreach($cartt as $key=>$item)
                        {
                            $sendProductName    =   $item->product['slug'];
                            $sendProductSku    =   $item->product['sku'];
                            $sendQuantity   =   $item->qty;
                            $sendPrice      =   $item->product['price'];
                            $sendProductId    =   $item->product['id'];
                            
                            $senditemsToShipStation[$updateIndex]['lineItemKey']    =   $sendProductSku;
                            $senditemsToShipStation[$updateIndex]['sku']    =   $sendProductSku;
                            $senditemsToShipStation[$updateIndex]['name']    =   $sendProductName;
                            $senditemsToShipStation[$updateIndex]['productId']    =   $sendProductId;
                            $senditemsToShipStation[$updateIndex]['quantity']    =   $sendQuantity;
                            $senditemsToShipStation[$updateIndex]['unitPrice']    =   "$sendPrice";
                            $updateIndex++;
                        }
                        
                        $sendDataToShipStation['items'] =  $senditemsToShipStation  ;
                        $senditemsToShipStation  =   json_encode( (array)$sendDataToShipStation );
                        /*print_r($senditemsToShipStation);die;*/
                        $curl = curl_init();
                        $username = '708b9f30a65a463cb9b2219b9b459413';
                        $password   =  '01d30413573d4a9da8a052a56b75f6e5';
                        $check = [];
                        
                            curl_setopt_array($curl, array(
                              CURLOPT_URL => "https://ssapi.shipstation.com/orders/createorder",
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_ENCODING => "",
                              CURLOPT_MAXREDIRS => 10,
                              CURLOPT_TIMEOUT => 0,
                              CURLOPT_FOLLOWLOCATION => true,
                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            
                            CURLOPT_POSTFIELDS =>$senditemsToShipStation,
                              CURLOPT_HTTPHEADER => array(
                                "Host: ssapi.shipstation.com",
                                "Authorization: Basic " . base64_encode($username . ":" . $password),
                                "Content-Type: application/json"
                              ),
                            ));
                            
                            $response = curl_exec($curl);
                        
            
                        curl_close($curl);
                   
                    // end send order info to shipstation via curl API
            try {
                        
                        $response = $gateway->purchase($order, $request);
                        } catch (Exception $e) {
                            $orderService->delete($order);
    
                            return back()->withInput()->withError($e->getMessage());
                        }
    
                        if ($response->isRedirect()) {
                            return redirect($response->getRedirectUrl());
                        } elseif ($response->isSuccessful()) {
                            $order->storeTransaction($response);
    
                            event(new OrderPlaced($order));
    
                            return redirect()->route('checkout.complete.show');
                        }
    
                        $orderService->delete($order);
    
                        return back()->withInput()->withError($response->getMessage());
            
        }
        

        
    }
}
