<?php

namespace Modules\Order\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Order\Entities\Order;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\Order\Mail\OrderStatusChanged;
use DB;

class OrderStatusController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param \Modules\Order\Entities\Order $request
     * @return \Illuminate\Http\Response
     */
    public function update(Order $order)
    {
        //dd(request('status'));
        $order->update(['status' => request('status')]);

        $message = trans('order::messages.status_updated');

        if (setting('order_status_email')) {
            Mail::to($order->customer_email)
                ->send(new OrderStatusChanged($order));
        }

        return $message;
    }
    
    public function createNewOrder(){
        return view('order::admin.orders.create-order');
    }
    public function storeNewOrder(Request $request){
        
        //dd($request->all());
        if(isset( $request->add_user )){
            $user   =   DB::table('users')->insertGetId([
                                'first_name'    =>  $request->first_name,
                                'last_name'     =>  $request->last_name,
                                'email'         =>  $request->email,
                                'password'      =>  bcrypt('123456'),
                            ]);
            $data   =   [
                        'recently_added_user_id'  =>  $user,
                        ];
            return view('order::admin.orders.create-order', $data);

        }else{

            $userDetail =   DB::table('users')->where('id', $request->customer)->first();
            $userMeta   =   DB::table('user_meta')->where('user_id', $userDetail->id)->first();
            //dd($userMeta);
            if( $userMeta == NULL){
                $contact_number = '---';
                $userMeta->shipping_address =   '---';
                
            }else{
                if($userMeta->contact_number == NULL){
                    $contact_number = '---';
                }else{
                    $contact_number =   $userMeta->contact_number;
                }
            }
            if($request->shipping_address_1 == NULL){
                $request->billing_address_1;
            }
            // dd($request->all());
            $orderMeta  =   DB::table('orders')->insertGetId([
                            'customer_id'           =>  $userDetail->id,
                            'customer_email'        =>  $userDetail->email,
                            'customer_phone'        =>  $contact_number,
                            'customer_first_name'   =>  $userDetail->first_name,
                            'customer_last_name'    =>  $userDetail->last_name,
                            'billing_first_name'    =>  $userDetail->first_name,
                            'billing_last_name'     =>  $userDetail->last_name,
                            'billing_address_1'     =>  $userMeta->shipping_address,
                            'billing_address_2'     =>  NULL,
                            'shipping_first_name'   =>  $userDetail->first_name,
                            'shipping_last_name'    =>  $userDetail->last_name,
                            'billing_city'          =>     '---',
                            'billing_state'         =>  '---',
                            'billing_zip'           =>  '---',
                            'billing_country'       =>  'US',
                            'shipping_address_1'    =>  $userMeta->shipping_address,
                            'shipping_address_2'    =>  NULL,
                            'shipping_city'         =>  '--',
                            'shipping_state'        =>  '--',
                            'shipping_zip'          =>  '--',
                            'shipping_country'      =>  'US',
                            'sub_total'             =>  $request->sub_total,
                            'shipping_method'       =>  'free_shipping',
                            'shipping_cost'         =>  '0.0000',
                            'coupon_id'             =>  NULL,
                            'discount'              =>  '0.0000',
                            'total'                 =>  $request->total_amount,
                            'payment_method'        =>  'cod',
                            'currency'              =>  'USD',
                            'currency_rate'         =>  '1.0000',
                            'locale'                =>  'en',
                            'status'                =>  'pending',
                            'created_at'            =>  DB::raw('CURRENT_TIMESTAMP'),
                            'updated_at'            =>  DB::raw('CURRENT_TIMESTAMP'),

                        ]);
            //dd($orderMeta);


            foreach ($request->products as $key => $value) {
                    $qty    =   $request->qty;
                    $price  =   $request->price;

                $orderProductMeta   =   DB::table('order_products')->insert([
                                'order_id'      =>  $orderMeta,
                                'product_id'    =>  $value,
                                'unit_price'    =>  $price[$key],
                                'qty'           =>  $qty[$key],
                                'line_total'    =>  $request->total_amount,
                            ]);
            }
            
            return redirect()->route('admin.orders.index');
        }

    }

    public function createNewUser(){
        $data   =   [

                    'create_new_user'  =>  'create_new_user',
                    ];
        return view('order::admin.orders.create-order', $data);
    }

    public function storeNewUser(Request $request){

    }

    public function fIndex(){

        if(isset( $_GET['s'] )){
            if($_GET['s'] == 'pending'){
                $status =   'pending';
            }else if($_GET['s'] == 'processing'){
                $status =   'processing';
            }else if($_GET['s'] == 'completed'){
                $status =   'completed';
            }else{
                $status =   '';
            }

            $orders     =   Order::where('status', $status)->get();

            $data       =   [

                                'orders'    =>  $orders

                            ];
            return view('order::admin.orders.filter_index', $data);
        }
    }

    public function getItemRow(){
        if(isset( $_GET['product_id'] )){
            $id     =   $_GET['product_id'];
            $product    =   DB::table('products')->where('id', $id)->first();
            //$returnHTML = view('order::admin.orders.ajax_response.get_product_row')->render();
            return response()->json(array('success' => true, 'product'=>$product));

        }else if(isset($_GET['serialNum'])){
            $serialNum  =   $_GET['serialNum'];
            $data   =   [

                            'serialNum' =>  $serialNum,

                        ];
            $returnHTML = view('order::admin.orders.ajax_response.get_product_row', $data)->render();
            return response()->json(array('success' => true, 'html'=>$returnHTML));

        }
        /*$returnHTML = view('order::admin.orders.filter_index')->with('userjobs', $userjobs)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));*/
    }
}
