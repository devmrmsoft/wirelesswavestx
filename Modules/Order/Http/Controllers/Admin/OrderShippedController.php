<?php

namespace Modules\Order\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Order\Entities\Order;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\Order\Mail\OrderStatusChanged;
use DB;

class OrderShippedController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param \Modules\Order\Entities\Order $request
     * @return \Illuminate\Http\Response
     */
    public function updateShippingCourier(Request $req)
    {
        //dd($req->data1[0]);
        
        $order = new Order; 
        $order->where('id', $req->data1[1])->update(['shipping_courier' => $req->data1[0]]);
        

        
        
        $message = trans('order::messages.status_updated');

        return $message;
    }
    
    public function updateTrackingNumber(Request $req)
    {
        //dd($req->all());
        
        $order = new Order; 
        $order->where('id', $req->data2[1])->update(['tracking_number' => $req->data2[0]]);
       
        
        $message = trans('order::messages.status_updated');

        return $message;
    }
    
}
