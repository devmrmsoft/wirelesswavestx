<?php 
    $products   =   DB::table('products')->get();
 ?>
  <tr id='addr3'>
    <td class="serial">{{ $serialNum + 1 }}</td>
    <td style="width: 40%;">                        
        <select name="products[]" onchange="getProductDetail(this)" class="form-control selectize prevent-creation user-select">
            <option>Select Product</option>
            @foreach($products as $prod)
            <option class="select-product-option" value="{{ $prod->id }}">{{ ucwords($prod->slug ) }}</option>
            @endforeach
        </select>
    </td>
    <td><input type="number" name='qty[]' placeholder='Enter Qty' class="form-control qty" step="0" min="0"/></td>
    <td><input type="text" name='price[]' placeholder='Enter Unit Price' class="form-control price" step="0.00" min="0"/></td>
    <td><input type="number" name='total[]' placeholder='0.00' class="form-control total" readonly/></td>
    <td>
        <button type="button" id="dlteBtn" onclick="dThisRow(this)" class="pull-right btn btn-default"> <i class="fa fa-trash"></i> </button>
    </td>
  </tr>