<div class="items-ordered-wrapper">
    <h3 class="section-title">{{ trans('order::orders.items_ordered') }}</h3>

    <div class="row">
        <div class="col-md-12">
            <div class="items-ordered">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>UPC</th>
                                <th>{{ trans('order::orders.product') }}</th>
                                <th>{{ trans('order::orders.unit_price') }}</th>
                                <th>{{ trans('order::orders.quantity') }}</th>
                                <th>{{ trans('order::orders.line_total') }}</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($order->products as $product)
                               
                            @php
                                $productSku =   DB::table('products')->find($product->product_id);
                                $check = DB::select('SELECT option_images.option_value_tr, option_images.option_color_sku
                                    FROM option_images
                                    LEFT JOIN option_translations
                                    ON option_translations.option_id = option_images.option_id
                                    WHERE option_images.product_id ='.$product->product_id);

                                  if ($product->hasAnyOption()){
                                    foreach ($product->options as $option) {
                                        $color = $option->values->implode('label', ', ');
                                        foreach ($check as  $value) {
                                            $id = $value->option_value_tr;
                                            if (strstr($value->option_value_tr, $color)) {
                                               $colorSKU = $value->option_color_sku;
                                            }
                                        }
                                     }
                                    }
                                    else{
                                        $colorSKU = $productSku->sku;
                                    }
                                                      
// dd($colorSKU);

                            @endphp
                                <tr>
                                    <td>{{ $colorSKU }}</td>
                                    <td>
                                        @if ($product->trashed())
                                            {{ $product->name }}
                                        @else
                                            <a href="{{ route('admin.products.edit', $product->product->id) }}">{{ $product->name }}</a>
                                        @endif

                                        @if ($product->hasAnyOption())
                                            <br>
                                            @foreach ($product->options as $option)
                                            @php
                                // dd($option->values);
                                            @endphp
                                                <span>
                                                    {{ $option->name }}:
                                                    <span>{{ $option->values->implode('label', ', ') }}</span>
                                                </span>
                                            @endforeach
                                        @endif
                                    </td>

                                    <td>
                                        {{ $product->unit_price->format($order->currency,null,$order->customer_id, $product->product->id, null) }}
                                    </td>

                                    <td>{{ intl_number($product->qty) }}</td>

                                    <td>
                                        {{ $product->line_total->format($order->currency,null,$order->customer_id, $product->product->id, $product->qty) }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
