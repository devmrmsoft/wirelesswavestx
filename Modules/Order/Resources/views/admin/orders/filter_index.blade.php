@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('order::orders.orders'))

    <li class="active">{{ trans('order::orders.orders') }}</li>
@endcomponent


@push('styles')

<style type="text/css">
    .filtertag-active {
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #0068e1;
        padding: 10px 15px;
        border: none;
        display: block;
        text-decoration: none;
    }
    .filtertag:hover{
        text-decoration: none;
        color: #fff;
    }
    .padding-0{
        padding-right:0;
        padding-left:0;
    }    
    .padding-r-0{
        padding-right:0;
    }
    .filtertag-active{
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #e9420b;
        padding: 10px 15px;
        border-right: 2px solid;
        display: block;
        text-decoration: none;
    }
    .filtertag{
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #0068e1;
        padding: 10px 15px;
        border: none;
        display: block;
        text-decoration: none;
        border-right: 2px solid;
    }
</style>

@endpush

@section('filter-form')
    <div class="row">
        <div class="col-md-2 padding-r-0">
            <a href="{{ route('admin.orders.index') }}" class=" @isset($_GET['s']) filtertag @else filtertag-active @endisset">All Orders</a>
        </div>

        <div class="col-md-2 padding-0">
            <a href="{{ route('admin.orders.f_index') }}?s=pending" class=" @if( isset($_GET['s']) && $_GET['s']=='pending') filtertag-active @else filtertag @endif">Pending Orders</a>
        </div>
        <div class="col-md-2 padding-0">
            <a href="{{ route('admin.orders.f_index') }}?s=processing" class=" @if( isset($_GET['s']) && $_GET['s']=='processing') filtertag-active @else filtertag @endif">Processing Orders</a>
        </div>
        <div class="col-md-2 padding-0">
            <a href="{{ route('admin.orders.f_index') }}?s=completed" class=" @if( isset($_GET['s']) && $_GET['s']=='completed') filtertag-active @else filtertag @endif">Completed Orders</a>
        </div>

    </div>
@endsection

@component('admin::components.page.index_table')
    @slot('resource', 'orders')
    @slot('name', 'Orders')

<div class="table-responsive">
    <table class="table table-striped table-hover" id="orders-table">
        <thead>
            <tr>
                <th>Serial</th>
                <th>Customer Name</th>
                <th>Customer Email</th>
                <th>Status</th>
                <th>Total</th>
            </tr>
        </thead>

        <tbody>
            <?php $serialNum   =   1; ?>
            @foreach($orders as $order)
            <tr>
                <td>#<?=$serialNum?></td>
                <td>{{ $order->customer_first_name }} {{ $order->customer_last_name }}</td>
                <td>{{ $order->customer_email }}</td>
                <td>{{ $order->status }}</td>
                <td>{{ $order->total }}</td>
            </tr>
            <?php $serialNum   += 1; ?>
            @endforeach
        </tbody>

        @isset($tfoot)
            <tfoot>{{ $tfoot }}</tfoot>
        @endisset
    </table>
</div>

@endcomponent
@push('scripts')
    <script>

        $(document).ready( function () {
            $('#orders-table .table').DataTable();
        } );
    </script>
@endpush
