@extends('admin::layout')
<?php
	$customers	=	DB::table('users')->get();
	$products	=	DB::table('products')->get();
?>

@push('styles')
<style type="text/css">
	.m-top-2{
		margin-top: 20px;
	}
    .m-bot-2{
        margin-bottom: 20px;
    }
	.d-none{
		display: none;
	}
    .order-wrapper {
        background: #fff;
        padding: 15px;
        border-radius: 3px;
    }
</style>

@endpush

@component('admin::components.page.header')
    @slot('title', 'Create Order')

    <li><a href="{{ route('admin.orders.index') }}">{{ trans('order::orders.orders') }}</a></li>
    <li class="active">{{ trans('admin::resource.show', ['resource' => trans('order::orders.order')]) }}</li>
@endcomponent

@section('content')
    <div class="order-wrapper">
        @isset($create_new_user)

        <form method="POST" action="{{ route('admin.orders.new_index_store') }}">
            {{ csrf_field() }}
            <input type="hidden" name="add_user" value="1">
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">First Name</label>
                    <div class="col-md-9">
                        <input type="text" name="first_name" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Last Name</label>
                    <div class="col-md-9">
                        <input type="text" name="last_name" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Email</label>
                    <div class="col-md-9">
                        <input type="email" name="email" class="form-control">
                    </div>
                </div>
            </div>

            <div class="row m-top-2 form-group">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>

        @else
        <form method="POST" action="{{ route('admin.orders.new_index_store') }}" id="checkout-formt">
            {{ csrf_field() }}
        	<div class="row">
        		
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Client</label>
                    <div class="col-md-6">
                        <select name="customer" class="form-control selectize prevent-creation user-select" id="">
                        	<option>Select User</option>
                        	@foreach($customers as $cust)
                        	<option value="{{ $cust->id }}" @isset($recently_added_user_id) @if($recently_added_user_id == $cust->id) selected @endif @endisset> {{ ucwords($cust->first_name ) }} {{ ucwords($cust->last_name ) }}</option>
                        	@endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ route('admin.orders.new_user') }}" class="btn btn-primary">Create New User</a>
                    </div>
                </div>
        	</div>
        	<div class="row m-top-2">
                <div class="append-element">
                    
                </div>
                <div class="product-items form-group">

                    <label for="wholesaler_secure_payment" class="col-md-3 control-label text-left">Product</label>
                    <div class="col-md-9">
                        <select name="products[]" class="form-control selectize prevent-creation product-select" multiple id="wholesaler_secure_payment">
                        	<option>Select Product</option>
                        	@foreach($products as $prod)
                        	<option class="select-product-option" value="{{ $prod->id }}">{{ ucwords($prod->slug ) }}</option>
                        	@endforeach
                        </select>
                    </div>
                </div>
        	</div>
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Price</label>
                    <div class="col-md-9">
                        <input type="text" name="price" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Billing Address</label>
                    <div class="col-md-9">
                        <input type="text" name="billing_address_1" required="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Shipping Address</label>
                    <div class="col-md-9">
                        <input type="text" name="shipping_address_1" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row m-top-2 form-group">
                <label for="" class="col-md-3 control-label text-left">Order Status</label>
                <div class="col-lg-9 col-md-10 col-sm-10">
                    <select id="order-status" name="order_status" class="form-control custom-select-black" data-id="6">
                        <option value="canceled">Canceled</option>
                        <option value="completed"> Completed</option>
                        <option value="on_hold"> On Hold</option>
                        <option value="pending" selected=""> Pending</option>
                        <option value="pending_payment"> Pending Payment</option>
                        <option value="processing"> Processing</option>
                        <option value="refunded"> Refunded</option>
                    </select>
                </div>
            </div>
            <div class="row m-top-2 form-group">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
        @endisset
    </div>
@endsection

@push('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		var selectUserText	=	$('.user-select > .has-items > span > .item').text();
		var selectProductText		=	$('.product-select > .has-items > span > .item').text();

		if(selectProductText == 'Select Product'){
			$('.product-select > .has-items > span > .remove-single').addClass('d-none');
		}
		if(selectUserText == 'Select User'){
			$('.user-select > .has-items > span > .remove-single').addClass('d-none');
		}
	});
    function addItem(){
        var mySecondDiv = $('<div id="mySecondDiv"><button class="btn btn-primary">test</button></div>');
        $('.append-element').append(mySecondDiv);
        console.log(mySecondDiv);
    }
</script>

@endpush