@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('order::orders.orders'))

    <li class="active">{{ trans('order::orders.orders') }}</li>
@endcomponent


@push('styles')

<style type="text/css">
    .filtertag-active {
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #0068e1;
        padding: 10px 15px;
        border: none;
        display: block;
        text-decoration: none;
    }
    .filtertag:hover{
        text-decoration: none;
        color: #fff;
    }
    .padding-0{
        padding-right:0;
        padding-left:0;
    }    
    .padding-r-0{
        padding-right:0;
    }
    .filtertag-active{
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #e9420b;
        padding: 10px 15px;
        border-right: 2px solid;
        display: block;
        text-decoration: none;
    }
    .filtertag{
        z-index: 3;
        color: #fff;
        cursor: pointer;
        background-color: #0068e1;
        padding: 10px 15px;
        border: none;
        display: block;
        text-decoration: none;
        border-right: 2px solid;
    }
</style>

@endpush

@section('filter-form')
    <div class="row">
        <div class="col-md-2 padding-r-0">
            <a href="{{ route('admin.orders.index') }}" class=" @isset($_GET['s']) filtertag @else filtertag-active @endisset">All Orders</a>
        </div>

        <div class="col-md-2 padding-0">
            <a href="{{ route('admin.orders.f_index') }}?s=pending" class=" @if( isset($_GET['s']) && $_GET['s']=='pending') filtertag-active @else filtertag @endif">Pending Orders</a>
        </div>
        <div class="col-md-2 padding-0">
            <a href="{{ route('admin.orders.f_index') }}?s=processing" class=" @if( isset($_GET['s']) && $_GET['s']=='processing') filtertag-active @else filtertag @endif">Processing Orders</a>
        </div>
        <div class="col-md-2 padding-0">
            <a href="{{ route('admin.orders.f_index') }}?s=completed" class=" @if( isset($_GET['s']) && $_GET['s']=='completed') filtertag-active @else filtertag @endif">Completed Orders</a>
        </div>

    </div>
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-body index-table" id="orders-table">
            @component('admin::components.table')
                @slot('thead')
                    <tr>
                        <th>{{ trans('order::orders.table.order_id') }}</th>
                        <th>{{ trans('order::orders.table.customer_name') }}</th>
                        <th>{{ trans('order::orders.table.customer_email') }}</th>
                        <th>{{ trans('admin::admin.table.status') }}</th>
                        <th>{{ trans('order::orders.table.total') }}</th>
                        <th data-sort>{{ trans('admin::admin.table.created') }}</th>
                    </tr>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        DataTable.setRoutes('#orders-table .table', {
            index: '{{ "admin.orders.index" }}',
            show: '{{ "admin.orders.show" }}',
        });

        new DataTable('#orders-table .table', {
            columns: [
                { data: 'id' },
                { data: 'customer_name', orderable: false, searchable: false },
                { data: 'customer_email' },
                { data: 'status' },
                { data: 'total' },
                { data: 'created', name: 'created_at' },
            ],
        });
    </script>
@endpush
