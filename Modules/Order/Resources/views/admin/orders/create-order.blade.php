@extends('admin::layout')
<?php
// facebook pixelID: 369797037249649
	$customers	=	DB::table('users')->get();
	$products	=	DB::table('products')->get();
?>

@push('styles')
<style type="text/css">
	.m-top-2{
		margin-top: 20px;
	}
    .m-bot-2{
        margin-bottom: 20px;
    }
	.d-none{
		display: none;
	}
    .order-wrapper {
        background: #fff;
        padding: 15px;
        border-radius: 3px;
    }
    .selectize-input.items.full.has-options.has-items{
        padding: 0 8px;
    }
</style>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

@endpush

@component('admin::components.page.header')
    @slot('title', 'Create Order')

    <li><a href="{{ route('admin.orders.index') }}">{{ trans('order::orders.orders') }}</a></li>
    <li class="active">{{ trans('admin::resource.show', ['resource' => trans('order::orders.order')]) }}</li>
@endcomponent

@section('content')
    <div class="order-wrapper">
        
        @isset($create_new_user)

        <form method="POST" action="{{ route('admin.orders.new_index_store') }}">
            {{ csrf_field() }}
            <input type="hidden" name="add_user" value="1">
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">First Name</label>
                    <div class="col-md-9">
                        <input type="text" name="first_name" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Last Name</label>
                    <div class="col-md-9">
                        <input type="text" name="last_name" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row m-top-2">
                <div class="form-group">
                    <label for="" class="col-md-3 control-label text-left">Email</label>
                    <div class="col-md-9">
                        <input type="email" name="email" class="form-control">
                    </div>
                </div>
            </div>

            <div class="row m-top-2 form-group">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>

        @else
        <div class="container">
            <form method="POST" action="{{ route('admin.orders.new_index_store') }}" id="checkout-formt">
                {{ csrf_field() }}

                <div class="row m-bot-2">
                    <div class="col-md-3">                        
                        <div class="form-group">
                            <label for="" class="col-md-3 control-label text-left">Client</label>
                        </div>
                    </div>
                        <div class="col-md-6">
                            <select name="customer" class="form-control selectize prevent-creation">
                                <option>Select User</option>
                                @foreach($customers as $cust)
                                <option value="{{ $cust->id }}" @isset($recently_added_user_id) @if($recently_added_user_id == $cust->id) selected @endif @endisset> {{ ucwords($cust->first_name ) }} {{ ucwords($cust->last_name ) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('admin.orders.new_user') }}" class="btn btn-primary">Create New User</a>
                        </div>
                </div>
                <div class="row clearfix">
                    <div class="col-md-12">
                        <table class="table table-bordered table-hover" id="tab_logic">
                            <thead>
                              <tr>
                                <th class="text-center"> # </th>
                                <th class="text-center"> Product </th>
                                <th class="text-center"> Qty </th>
                                <th class="text-center"> Price </th>
                                <th class="text-center"> Total </th>
                                <th class="text-center"> Action </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr id='addr0'>
                                <td class="serial">1</td>
                                <td style="width: 40%;">
                                    <select name="products[]" onchange="getProductDetail(this)" class="form-control selectize prevent-creation">
                                        <option>Select Product</option>
                                        @foreach($products as $prod)
                                        <option class="select-product-option" value="{{ $prod->id }}">{{ ucwords($prod->slug ) }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><input type="number" name='qty[]' placeholder='Enter Qty' class="form-control qty" step="0" min="0"/></td>
                                <td><input type="text" name='price[]' placeholder='Enter Unit Price' class="form-control price" step="0.00" min="0"/></td>
                                <td><input type="number" name='total[]' placeholder='0.00' class="form-control total" readonly/></td>
                                <td>
                                </td>
                              </tr>
                              <!-- <tr id='addr1'></tr> -->
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-md-12">
                      <button type="button" id="add_row" class="btn btn-default pull-left">Add Row</button>
                      <button type="button" id='delete_row' class="pull-right btn btn-default">Delete Row</button>
                    </div>
                </div>
                <div class="row clearfix" style="margin-top:20px">
                    <div class="pull-right col-md-4">
                      <table class="table table-bordered table-hover" id="tab_logic_total">
                        <tbody>
                          <tr>
                            <th class="text-center">Sub Total</th>
                            <td class="text-center"><input type="number" name='sub_total' placeholder='0.00' class="form-control" id="sub_total" readonly/></td>
                          </tr>
                          <tr>
                            <th class="text-center">Tax</th>
                            <td class="text-center"><div class="input-group mb-2 mb-sm-0">
                                <input type="number" class="form-control" id="tax" placeholder="0">
                                <div class="input-group-addon">%</div>
                              </div></td>
                          </tr>
                          <tr>
                            <th class="text-center">Tax Amount</th>
                            <td class="text-center"><input type="number" name='tax_amount' id="tax_amount" placeholder='0.00' class="form-control" readonly/></td>
                          </tr>
                          <tr>
                            <th class="text-center">Grand Total</th>
                            <td class="text-center"><input type="number" name='total_amount' id="total_amount" placeholder='0.00' class="form-control" readonly/></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </div>

                <div class="row m-top-2 form-group">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-primary pull-right">Create Invoice</button>
                    </div>
                </div>
            </form>
        </div>
        @endisset
    </div>
@endsection

@push('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		var selectUserText	=	$('.user-select > .has-items > span > .item').text();
		var selectProductText		=	$('.product-select > .has-items > span > .item').text();

		if(selectProductText == 'Select Product'){
			$('.product-select > .has-items > span > .remove-single').addClass('d-none');
		}
		if(selectUserText == 'Select User'){
			$('.user-select > .has-items > span > .remove-single').addClass('d-none');
		}
	});
    function addItem(){
        var mySecondDiv = $('<div id="mySecondDiv"><button class="btn btn-primary">test</button></div>');
        $('.append-element').append(mySecondDiv);
        console.log(mySecondDiv);
    }

    $(document).ready(function(){

        $("#add_row").click(function(){

            getItemRow();
        });
        $("#delete_row").click(function(){

            var table   =   document.getElementById('tab_logic');
            var lastRow =   table.rows[ table.rows.length - 1 ];
            var serial  =   $(lastRow).find('.serial').text();
            if(serial>1){

                $(lastRow).remove();
            }


            calc();
        });
        
        $('#tab_logic tbody').on('keyup change',function(){
            calc();
        });
        $('#tax').on('keyup change',function(){
            calc_total();
        });
        

    });
    function dThisRow(disRow){
        /*var elementRow  =   ddis.parent().parent();
        $(this).parent().parent().remove();*/
        var parDiv      =   disRow.parentNode.parentNode;
        $(parDiv).remove();
        //console.log(parDiv);
    }
    function calc()
    {
        $('#tab_logic tbody tr').each(function(i, element) {
            var html = $(this).html();
            if(html!='')
            {
                var qty = $(this).find('.qty').val();
                var price = $(this).find('.price').val();
                $(this).find('.total').val(qty*price);
                
                calc_total();
            }
        });
    }

    function calc_total()
    {
        total=0;
        $('.total').each(function() {
            total += parseInt($(this).val());
        });
        $('#sub_total').val(total.toFixed(2));
        tax_sum=total/100*$('#tax').val();
        $('#tax_amount').val(tax_sum.toFixed(2));
        $('#total_amount').val((tax_sum+total).toFixed(2));
    }


    function getItemRow() {
        $_token = "{{ csrf_token() }}";
        //b=i+1;
        var table   =   document.getElementById('tab_logic');
        var lastRow = table.rows[ table.rows.length - 1 ];
        var b   =   $(lastRow).find('.serial').text();
        console.log(lastRow);
        $.ajax({
            url: "{{ route('admin.orders.getItemRow') }}",
            type: 'GET',
            cache: false,
            data: { '_token': $_token, serialNum: b }, //see the $_token
            datatype: 'html',
            beforeSend: function() {
                //something before send
            },
            success: function(data) {
                //console.log('success');
                console.log(data);
                //success
                //var data = $.parseJSON(data);
                if(data.success == true) {
                    //$('#addr'+i).html($('#addr'+b).html()).find('td:first-child').html(i+1);
                    $('#tab_logic').append(data.html);
                    calc();
                  console.log('working');
                } else {
                    console.log('error');
                }
            },
            error: function(xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }
    function getProductDetail(sel){
        $_token = "{{ csrf_token() }}";
        var product_id =  sel.value;
        parentDiv = sel.parentNode.parentNode;
        console.log(parentDiv);
        $.ajax({
            url: "{{ route('admin.orders.getItemRow') }}",
            type: 'GET',
            cache: false,
            data: { '_token': $_token, product_id: product_id }, //see the $_token
            datatype: 'html',
            beforeSend: function() {
                //something before send
            },
            success: function(data) {
                if (data.product !== null){
                    product =   data.product;
                    
                    $(parentDiv).find('.qty').val('1');
                    $(parentDiv).find('.price').val(product['price']);
                    $(parentDiv).find('.total').val(product['price']);
                    calc();
                    console.log(parentDiv);
                }

            },
            error: function(xhr,textStatus,thrownError) {
                alert(xhr + "\n" + textStatus + "\n" + thrownError);
            }
        });
    }
</script>

@endpush