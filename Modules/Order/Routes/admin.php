<?php

Route::get('orders', [
    'as' => 'admin.orders.index',
    'uses' => 'OrderController@index',
    'middleware' => 'can:admin.orders.index',
]);


Route::get('orders-filter', [
    'as' => 'admin.orders.f_index',
    'uses' => 'OrderStatusController@fIndex',
    'middleware' => 'can:admin.orders.index',
]);

Route::get('get-item-row', [
    'as' => 'admin.orders.getItemRow',
    'uses' => 'OrderStatusController@getItemRow',
    'middleware' => 'can:admin.orders.index',
]);


Route::get('orders/new-order', [
    'as' => 'admin.orders.new_index',
    'uses' => 'OrderStatusController@createNewOrder',
    'middleware' => 'can:admin.orders.index',
]);

Route::get('orders/new-user', [
    'as' => 'admin.orders.new_user',
    'uses' => 'OrderStatusController@createNewUser',
    'middleware' => 'can:admin.orders.index',
]);

Route::post('orders/new-user', [
    'as' => 'admin.orders.order_store_user',
    'uses' => 'OrderStatusController@storeNewUser',
    'middleware' => 'can:admin.orders.index',
]);

Route::post('orders/new-order', [
    'as' => 'admin.orders.new_index_store',
    'uses' => 'OrderStatusController@storeNewOrder',
    'middleware' => 'can:admin.orders.index',
]);

Route::get('orders/{id}', [
    'as' => 'admin.orders.show',
    'uses' => 'OrderController@show',
    'middleware' => 'can:admin.orders.show',
]);

Route::put('orders/{order}/status', [
    'as' => 'admin.orders.status.update',
    'uses' => 'OrderStatusController@update',
    'middleware' => 'can:admin.orders.edit',
]);

Route::post('orders/{order}/email', [
    'as' => 'admin.orders.email.store',
    'uses' => 'OrderEmailController@store',
    'middleware' => 'can:admin.orders.show',
]);

Route::get('orders/{order}/print', [
    'as' => 'admin.orders.print.show',
    'uses' => 'OrderPrintController@show',
    'middleware' => 'can:admin.orders.show',
]);

Route::get('shippingcourier','OrderShippedController@updateShippingCourier')->name('shippingcourier');
Route::get('trackingnumber','OrderShippedController@updateTrackingNumber')->name('trackingnumber');
