<?php

namespace Modules\Product\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Modules\Product\Entities\Product;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Product\Http\Requests\SaveProductRequest;
use Illuminate\Http\Request;
use DB;

class ProductController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Product::class;


    //dd($model);
    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'product::products.product';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'product::admin.products';
    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveProductRequest::class;
    /*function check(){
        dd($validation);
    }*/
  /*  public function __construct(SaveProductRequest $validation)
        {
            dd(Product::class);
            dd('test validation');
        }*/
    //protected $validation = echo '<pre>'; print_r( $validation ); exit;
    public function filterProduct(){
        $catFilterID    =   $_GET['cat'];
        $urlInfo        =   explode(',', $_GET['cat']);
        $catFilterID    =   $urlInfo[0];
        $catSlug        =   $urlInfo[1];
        $proCats    =   DB::table('product_categories')->where('category_id', $catFilterID)->get();

        $proIds     =   array();
        foreach ($proCats as $key => $value) {
            $proIds[$key]   =   $value->product_id;
        }
        $products   =   DB::table('products')->whereIn('id', $proIds)->get();
        /*$entity_files   =   DB::table('entity_files')->whereIn('entity_id', $proIds)
                                ->where('entity_type', 'Modules\Product\Entities\Product')
                                ->where('zone', 'base_image')
                                ->get();*/
        $products   =   DB::table('products')
                            ->whereIn('products.id', $proIds)
                            ->join('product_translations', 'product_translations.product_id', '=', 'products.id')
                            ->join('entity_files','entity_files.entity_id', '=', 'products.id')
                            ->join('files', 'files.id', '=', 'entity_files.file_id')
                            ->where('entity_files.entity_type', '=', 'Modules\Product\Entities\Product')
                            ->where('entity_files.zone','=', 'base_image')
                            ->select('files.id AS fileID', 'entity_files.entity_id AS proId', 'files.path AS imagePath','products.id AS productID','entity_files.entity_type as Etype', 'entity_files.id AS entityID','products.*','product_translations.*')
                            ->get();

        $data   =   [

                        'products'  =>  $products,
                        'catSlug'   =>  $catSlug,

                    ];
        return view('product::admin.products.filtered_products', $data );

    }

    public function optionImage(Request $request){
        $value  =   $request->all();
        dd($value);
            echo '<pre>';
        foreach( $request->options as $option ){
            /*print_r( $option->values );*/
            
            foreach ($option['values'] as $value) {
                echo $option['id'];
                echo '<br>';
                echo $value['label'];
                echo '<br>';
                echo $value['image_id'];
                echo '<br>';
                echo  $value['id'];
                echo '<br>';
                print_r( $value );
                echo '<br>';

                if(!isset($value['image'])){
                    if($value['id'] != NULL){
                        $optionImagesData   =   DB::table('option_images')
                                                    ->where('option_id', $option['id'])
                                                    ->where('option_value_tr', $value['image_id'])
                                                    ->first();
                        if($optionImagesData == NULL ){
                            $optionImage    =   $this->uploadOptionImage($value['image']);
                        }else{
                            $optionImage    =   $optionImagesData->option_image_path;
                        }
                    }else{
                        $optionImage    =   $this->uploadOptionImage($value['image']);
                    }
                }else{
                    $optionImage    =   $this->uploadOptionImage($value['image']);
                }
                if($value['id'] == NULL){
                $optionsImages  =   DB::table('option_images')->insert([
                                        'option_id'             =>  $option['id'],
                                        'option_name'          =>  $option['name'],
                                        'product_slug'          =>  $request->slug,
                                        'option_value_tr'       =>  $value['label'],
                                        'option_image_path'     =>  $optionImage,
                                    ]);
                }else{
                    $optionImagesData   =   DB::table('option_images')
                                                    ->where('option_id', $option['id'])
                                                    ->where('option_value_tr', $value['image_id'])
                                                    ->first();
                    if($optionImagesData){

                        DB::table('option_images')->where('option_id', $option['id'])
                                                    ->where('option_value_tr', $value['image_id'])
                                                    ->update([
                                                        'option_value_tr'       =>  $value['label'],
                                                        'option_image_path'     =>  $optionImage,
                                                    ]);
                    }else{
                        $optionsImages  =   DB::table('option_images')->insert([
                                                'option_id'             =>  $option['id'],
                                                'option_name'          =>  $option['name'],
                                                'product_slug'          =>  $request->slug,
                                                'option_value_tr'       =>  $value['label'],
                                                'option_image_path'     =>  $optionImage,
                                            ]);

                    }

                }
            }
        }
        dd($request->all());
        return $value;
    }


    public function productInventoryOptionImagesUpload(Request $request){
        $optionImage    =   $this->uploadMultipeOptionImages($request->photo);
        $optionImage    =   implode('|', $optionImage);
            //dd($optionImage);

            return response()->json(array('success' => true,'inputFieldValue' => $optionImage));
    }

    private function uploadMultipeOptionImages($files){
        
            //Move File
            $images=array();
            foreach($files as $file){
                $path="public/assets/options/color/images";
                $custom=date('joYhis')."_color_".rand(10,10000);
                $file->move($path,$custom.$file->getClientOriginalName());
                $images[]   =  $custom.$file->getClientOriginalName();
            }
            return $images;
        
    }

    public function getOptionsData(){
        //dd($_GET['option_value_id']);
        $getOptionTrId =   DB::table('option_value_translations')->where('option_value_id',$_GET['option_value_id'])->first();
        //dd($getOptionTrId->option_value_id);
        $getOptionColorImgs =   DB::table('option_images')
            ->where('product_id',$_GET['product_id'] )
            ->where('option_value_id', $getOptionTrId->id)->first();
                //dd($getOptionColorImgs->option_image_path);
            if($getOptionColorImgs->option_image_path != null){
                $firstImage =   explode('|',$getOptionColorImgs->option_image_path);
                $image  =   asset('/public/public/assets/options/color/images/').'/'.$firstImage[0];
            }
            else{
                $image  =   '';
            }
            return response()->json(array('success' => true, 'qty'=>$getOptionColorImgs->option_color_qty, 'sku'=>$getOptionColorImgs->option_color_sku, 'image'=>$image));
    }


    private function uploadOptionImage($file){
        if(file_exists($file)){
            //Move File
            $path="public/assets/options/color/images";
            $custom=date('joYhis')."_color_";
            $file->move($path,$custom.$file->getClientOriginalName());
            return $custom.$file->getClientOriginalName();
        }else{
            return "";
        }
    }

    public function productsDisable(Request $request){
        //dd($request->all());
        foreach ($request->id as $key => $value) {
            //echo $value;
            Product::where('id',$value)->update(['is_active'=>0]);
        }
        return 'done';
    }

    public function productsEnable(Request $request){
        //dd($request->all());
        foreach ($request->id as $key => $value) {
            echo $value;
            $productquery = DB::table('products')->where('id',$value)->update(['is_active'=>1]);

        }
        return 'done';
    }

    public function indexDisabled(){
        $products = Product::all();
        //return $products;
        return response()->json(array('data' => $products));
    }
}
