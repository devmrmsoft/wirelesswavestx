<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Product\Entities\Product;
use Modules\Product\Events\ProductViewed;
use Modules\Product\Filters\ProductFilter;
use Modules\Product\Events\ShowingProductList;
use Modules\Product\Http\Middleware\SetProductSortOption;
use Illuminate\Http\Request;
use DB;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(SetProductSortOption::class)->only('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Modules\Product\Entities\Product $model
     * @param \Modules\Product\Filters\ProductFilter $productFilter
     * @return \Illuminate\Http\Response
     */
    public function index(Product $model, ProductFilter $productFilter)
    {
        $productIds = [];

        if (request()->has('query')) {
            $model = $model->search(request('query'));
            $productIds = $model->keys();
        }

        $query = $model->filter($productFilter);

        if (request()->has('category')) {
            $productIds = (clone $query)->select('products.id')->resetOrders()->pluck('id');
        }

        if (request()->has('productsPerPage')) {
            $products = $query->paginate(request('perPage', request('productsPerPage')))
                ->appends(request()->query());
        }else{
            
            $products = $query->paginate(request('perPage', 15))
                ->appends(request()->query());
        }

        if (request()->wantsJson()) {
            return response()->json($products);
        }

        event(new ShowingProductList($products));

        return view('public.products.index', compact('products', 'productIds'));
    }

    /**
     * Show the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Product::findBySlug($slug);
        $relatedProducts = $product->relatedProducts()->forCard()->get();
        $upSellProducts = $product->upSellProducts()->forCard()->get();
        $reviews = $this->getReviews($product);
        $checkPhone = DB::select('SELECT categories.*
        FROM categories
        LEFT JOIN product_categories
        ON categories.id = product_categories.category_id
        WHERE product_categories.product_id = '.$product->id);
        if (setting('reviews_enabled')) {
            $product->load('reviews:product_id,rating');
        }

        event(new ProductViewed($product));

        return view('public.products.show', compact('product', 'relatedProducts', 'upSellProducts', 'reviews','checkPhone'));
    }

    /**
     * Get reviews for the given product.
     *
     * @param \Modules\Product\Entities\Product $product
     * @return \Illuminate\Support\Collection
     */
    private function getReviews($product)
    {
        if (! setting('reviews_enabled')) {
            return collect();
        }

        return $product->reviews()->paginate(15, ['*'], 'reviews');
    }

    public function getPhoneModels(Request $request){

        //dd($request->all());
        $values =   [];
        if(isset($request->val)){
            $values =   $request->val;
        }
        $data   =   DB::table('categories')
                        ->join('category_translations', 'category_translations.category_id', '=', 'categories.id')
                        ->whereIn('categories.parent_id', $values)
                        ->select('categories.id as catId','category_translations.name as name', 'categories.*')
                        ->get();
        //dd($data);

        $returnHTML = view('public.ajax-layouts.get-phone-models')->with('modelCats', $data)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public function getBrandModels(Request $request){
        
        $values =   [];
        if(isset($request->val)){
            $values =   $request->val;
        }
        $data   =   DB::table('categories')
                        ->join('category_translations', 'category_translations.category_id', '=', 'categories.id')
                        ->whereIn('categories.parent_id', $values)
                        ->select('categories.id as catId','category_translations.name as name', 'categories.*')
                        ->get();
        //dd($data);

        $returnHTML = view('public.ajax-layouts.get-phone-models')->with('modelCats', $data)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public function getAccessoryBrandModels(Request $request){
        
        $values =   [];
        if(isset($request->val)){
            $values =   $request->val;
        }
        $data   =   DB::table('categories')
                        ->join('category_translations', 'category_translations.category_id', '=', 'categories.id')
                        ->whereIn('categories.parent_id', $values)
                        ->select('categories.id as catId','category_translations.name as name', 'categories.*')
                        ->get();
        //dd($data);

        $returnHTML = view('public.ajax-layouts.get-accessory-models')->with('modelCats', $data)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }
    
    public function getProductColorImages(){
        
        
        $getOptionValueTranslations =   DB::table('option_value_translations')
                                    ->where('option_value_id',$_GET['value_id'])->first();
                                    
        $getOptionColorImgs =   DB::table('option_images')
                                    ->where('option_id',$_GET['option_id'] )
                                    ->where('option_value_id',$getOptionValueTranslations->id)->first();
                        // dd($getOptionValueTranslations->id);
        $multipleImages =   explode('|', $getOptionColorImgs->option_image_path);
        $optionColorSku =   $getOptionColorImgs->option_color_sku;
        $optionColorQty =   $getOptionColorImgs->option_color_qty;
        //below code was begfore dropdown  color updateoption_color_qty
        /*$getOptionColorImgs =   DB::table('option_images')
                                    ->where('option_id',$_GET['option_id'] )
                                    ->where('option_value_id',$_GET['value_id'])->first();
        $multipleImages =   explode('|', $getOptionColorImgs->option_image_path);*/
        
        /*foreach($multipleImages as $index => $multipleImages){
            echo 'index: '.$index.' value: '.$multipleImages.'<br>';
        }exit;*/
        $returnHTML = view('public.ajax-layouts.get-product-color-images')->with('multipleImages', $multipleImages)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML, 'sku' =>  $optionColorSku,'qty' => $optionColorQty));
    }
}
