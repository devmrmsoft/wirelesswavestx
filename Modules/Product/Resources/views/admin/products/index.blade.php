@php

    use Modules\Category\Entities\Category;
    use FleetCart\productPhoneModel;
    $categoryTreeLists  =   Category::categoryTreeList();

@endphp

@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('product::products.products'))

    <li class="active">{{ trans('product::products.products') }}</li>
@endcomponent
@section('filter-form')

<?php
    $categories =   Category::all();
?>
    <div class="row">
        <div class="col-md-12">                
            <form method="GET" action="{{ route('admin.products.products_filter') }}">
                {{ csrf_field() }}
                <div class="row m-top-2 form-group">
                    <label for="" class="col-md-3 control-label text-left">Search By Category</label>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <select id="order-status" name="cat" class="form-control custom-select-black" data-id="6">
                            @foreach($categories as $cat)
                            <option value="{{ $cat->id }},{{ $cat->slug }}">{{ $cat->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@component('admin::components.page.index_table')

    @slot('buttons', ['create'])
    @slot('resource', 'products')
    @slot('name', trans('product::products.product'))

    @slot('thead')
        @include('product::admin.products.partials.thead', ['name' => 'products-index'])
    @endslot
@endcomponent

@push('scripts')
    <script>
        new DataTable('#products-table .table', {
            columns: [
                { data: 'checkbox', orderable: false, searchable: false, width: '3%' },
                { data: 'thumbnail', orderable: false, searchable: false, width: '10%' },
                { data: 'name', name: 'translations.name', orderable: false, defaultContent: '' },
                { data: 'price', searchable: false },
                { data: 'status', name: 'is_active', searchable: false },
                { data: 'created', name: 'created_at' },
            ],
        });
    </script>
@endpush
