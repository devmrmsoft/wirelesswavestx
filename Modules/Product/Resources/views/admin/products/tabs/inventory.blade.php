<?php

use Modules\Option\Entities\Option;
$gOptions =	Option::globals()->get();
?>

@push('styles')

        <link href="css/bootstrap-imageupload/bootstrap-imageupload.css" rel="stylesheet">
        <style type="text/css">
            .imageupload {
                margin: 20px 0;
            }
        </style>
@endpush
<div class="row">
    <div class="col-md-8">
        {{ Form::text('sku', trans('product::attributes.sku'), $errors, $product) }}
        {{ Form::select('manage_stock', trans('product::attributes.manage_stock'), $errors, trans('product::products.form.manage_stock_states'), $product) }}

        <div class="{{ old('manage_stock', $product->manage_stock) ? '' : 'hide' }}" id="qty-field">
            {{ Form::number('qty', trans('product::attributes.qty'), $errors, $product, ['required' => true, 'class' => 'total-qty'] ) }}
        </div>

        {{ Form::select('in_stock', trans('product::attributes.in_stock'), $errors, trans('product::products.form.stock_availability_states'), $product) }}
    </div>
</div>
<div id="options-group">
    {{--  Options will be added here dynamically using JS  --}}
</div>

<div class="box-footer no-border p-t-0">
    <div class="form-group pull-left">
        <div class="col-md-10">
            <button type="button" class="btn btn-default m-r-10" id="add-new-option">
                {{ trans('option::options.form.add_new_option') }}
            </button>
        </div>
    </div>

    @hasAccess('admin.options.index')
        @if ($gOptions->isNotEmpty())
            <div class="add-global-option clearfix pull-right">
                <div class="form-group pull-left">
                    <select class="form-control custom-select-black" id="global-option">
                        <option value="">{{ trans('option::options.select_global_option') }}</option>

                        @foreach ($gOptions as $globalOption)
                            <option ss value="{{ $globalOption->id }}">{{ $globalOption->name }}</option>
                        @endforeach
                    </select>
                </div>

                <button type="button" class="btn btn-default" id="add-global-option" data-loading>
                    {{ trans('option::options.form.add_global_option') }}
                </button>
            </div>
        @endif
    @endHasAccess
</div>
@push ('scripts')

    <script type="text/javascript">

        $(document).ready(function (e) {
            $('.option-row').trigger('click');

        });

        function getOptionsData(data){
            var token               =   $('input[name="_token"]').val();
            var product_id          =   $('input[name="p_id"]').val();
            var qtyBox = $(data).find('td.qty-box');
            var skuBox = $(data).find('td.sku-box');
            var imgBox = $(data).find('td.img-box');
            var option_value_text =  $(qtyBox).find('.option-qty').attr('data-value-label');
            var option_value_id =  $(qtyBox).find('.option-qty').attr('data-value-id');
            console.log(option_value_id+'qtyBox'+qtyBox+'skuBox'+skuBox+'imgBox'+imgBox+'option_value_text'+option_value_text);
             $.ajax({
                url: '{{ route("admin.products.get_options_data") }}',
                type: 'GET',
                data: {_token: token, product_id,option_value_text,option_value_id},
                dataType: 'JSON',
                success: function(response){
                console.log(data.html);
                $(qtyBox).find('.option-qty').val(response.qty);
                $(skuBox).find('.option-sku').val(response.sku);
                $(imgBox).find('.option-image').attr('src', response.image);
                }
            }); 

            
        }


    </script>

@endpush
@push('globals')
    <script>
        function uploadBeforeProduct(fileValue){
            var imgInpupt   =   $(fileValue).siblings('.imgInp');


            var ajaxData = new FormData();


            var inputName   =   imgInpupt.attr("id");
            var inputNameText   =   imgInpupt.attr("name");
            console.log(imgInpupt+'inputname:'+inputName);

            ajaxData.append( 'action','uploadImages');
            $.each($("#"+inputName), function(i, obj) {
                    $.each(obj.files,function(j, file){
                        ajaxData.append('photo['+j+']', file);
                    })
            });
            console.log(imgInpupt);
            //var colorInputValues    =   imgInpupt.val();
            
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            
            $.ajax({
                url: '{{ route("admin.products.inventory_option_images_upload") }}',
                type: 'POST',
                data: ajaxData,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType:'json',
                success: function(data){
                    $("#"+inputName).after('<input type="text" id="'+inputName+'" name="'+inputNameText+'" value="'+data.inputFieldValue+'">');
                    console.log(inputNameText);
                    $(fileValue).siblings('.imgInp').remove();
                }
            });
        }


    function sumQty(numberElement){
        var sum = 0;
        $(".option-qty").each(function(){
            sum += +$(this).val();
        });
        $(".total-qty").val(sum);
        console.log(sum);
    }
        FleetCart.data['product.options'] = {!! old_json('options', $product->options) !!};
        FleetCart.errors['product.options'] = @json($errors->get('options.*'), JSON_FORCE_OBJECT);
    </script>
@endpush

@include('option::admin.options.templates.product_option')
