@php
if(isset( $product->id )){
        		$productPrices		=	DB::table('products')->where('id', $product->id)->first();
        		$productPrices		=	$productPrices->retailer_prices;
        		//dd($productPrices);
        		//$productPrices		=	explode(',', $productPrices);
        		$productPrices		=	json_decode($productPrices, true);
        	}
@endphp


<div class="row">
    <div class="col-md-8">
        {{ Form::number('price', trans('product::attributes.price'), $errors, $product, ['min' => 0, 'required' => true]) }}
        {{ Form::number('special_price', trans('product::attributes.special_price'), $errors, $product, ['min' => 0]) }}
        {{ Form::text('special_price_start', trans('product::attributes.special_price_start'), $errors, $product, ['class' => 'datetime-picker']) }}
        {{ Form::text('special_price_end', trans('product::attributes.special_price_end'), $errors, $product, ['class' => 'datetime-picker']) }}
        <?php 
        	$retailer_categories	=	DB::table('retailer_lists')->get();
        	if(isset( $product->id )){
        		$productPrices		=	DB::table('products')->where('id', $product->id)->first();
        		$productPrices		=	$productPrices->retailer_prices;
        		//$productPrices		=	explode(',', $productPrices);
        		$productPrices		=	json_decode($productPrices, true);
        	}

         ?>
         @php if(isset( $product->id )){ @endphp
         <input type="hidden" name="p_id" value="{{ $product->id }}" >
         @php } @endphp
        @foreach($retailer_categories as $retailer_category)
	        <div class="form-group ">
	        	<label for="price-{{$retailer_category->id}}" class="col-md-3 control-label text-left">{{$retailer_category->list_name}}</label>
	        	<div class="col-md-9">
	        		@isset($productPrices)
	        		<?php 
	        		if (array_key_exists($retailer_category->id,$productPrices)){
	        			?>
	        		<input name="retailer_prices[{{ $retailer_category->id }}]" class="form-control " id="price-{{$retailer_category->id}}" min="0" type="number" value="{{ $productPrices[$retailer_category->id] }}">
	        			<?php
	        		}else{
	        			?>
	        		<input name="retailer_prices[{{ $retailer_category->id }}]" class="form-control " id="price-{{$retailer_category->id}}" min="0" type="number" value="">
	        			<?php
	        		}

	        		 ?>
	        		 @else

	        		<input name="retailer_prices[{{ $retailer_category->id }}]" class="form-control " id="price-{{$retailer_category->id}}" min="0" type="number" value="">
	        		@endisset
	        		
	        	</div>
	        </div>
        @endforeach

    </div>
</div>
