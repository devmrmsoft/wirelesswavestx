@php

	use Modules\Category\Entities\Category;
	use FleetCart\productPhoneModel;
	$categoryTreeLists 	=	Category::categoryTreeList();
	$model_cat		=	Category::where('parent_id', null)->get();
	$phone_model	=	Category::where('parent_id', '392')->get();
	$get_brands		=	Category::where('parent_id', '392')->get();
	$get_carriers		=	Category::where('parent_id', '4')->get();
	$get_accBrand		=	Category::where('parent_id', '427')->get();

	if(isset($product->id)){
		$product_categories	=	DB::table('product_categories')->where('product_id', $product->id)->get();
		$phoneModels	=	productPhoneModel::where('product_id', $product->id)->get();
		$findThisArray  =   [];
		foreach($phoneModels as $phoneModelforeachArray){
		    $findThisArray[$phoneModelforeachArray->id]    =   $phoneModelforeachArray->cat_id;
		}
		$phoneModelsReadOnly	=	Category::whereIn('id',$findThisArray)->get();
		//dd($phoneModelsReadOnly);
		$objArray 		=	[];
		foreach($product_categories as $index => $pro_cat){
			$pro_catArray = (array) $pro_cat;

			$objArray[$index] = $pro_catArray['category_id'];
		}
		$getSavedCategories	=	[];
		foreach($categoryTreeLists as $id => $categoryTreeList){

			if( in_array( $id ,$objArray ) )
				{
				    $getSavedCategories[$id] = $id;
				}
		}
	 //dd($getSavedCategories);
	}

@endphp

        	<?php
        		 /*echo '<pre>'; print_r($getSavedCategories); echo '</pre>';
        		exit;*/
        	?>
@push('styles')



@endpush

{{ Form::text('name', trans('product::attributes.name'), $errors, $product, ['labelCol' => 2, 'required' => true]) }}
{{ Form::wysiwyg('description', trans('product::attributes.description'), $errors, $product, ['labelCol' => 2, 'required' => true]) }}

<div class="row">
    <div class="col-md-8">
        <!-- {{ Form::select('categories', trans('product::attributes.categories'), $errors, $categories, $product, ['class' => 'selectize prevent-creation', 'multiple' => true]) }} -->


        <div class="form-group " data-count="1" id="root_cat_idtest">
        	<label for="main_root_cat_label" class="col-md-3 control-label text-left">Root Category</label>
        	<div class="col-md-9" style="display: flex;">
        		<select name="categories[]" class="form-control custom-select-black test product-root-type" select-attr="root-cat" id="main_root_cat_label" onchange="getChildForRootCat(this);createRootCategory(this);">
        			<option value="">Please Select</option>
        			@foreach($model_cat as $cat)
        			<option value="{{ $cat->id }}"
        				@php
        				if(isset($product_categories)){
        				if($product_categories->contains('category_id',$cat->id))
							{
							  echo "selected";
							}
						}

        				@endphp

        			 >{{ $cat->name }}</option>
        			@endforeach
        			<!--<option value="main_root_cat_label">Add New</option>-->
        		</select>
        	</div>
        </div>

        @php if(isset($product->id)){  $data_count = 2; @endphp
        @foreach($product_categories as $index => $pro_cat)

        <?php ?>
        @php $pro_catArray = (array) $pro_cat;

        $next = $product_categories->get(++$index);
        	if(is_object($next)){
		        		$nextArray = get_object_vars($next);
		        }else{
		    }
        $update_cat		=	Category::where('parent_id', $pro_cat->category_id)->get();
         //dd($product_categories);
		    //print_r($next);
        $hasChild 		=	null;
        foreach($update_cat as $childCat){
			
        $ifHasChild     =   Category::where('parent_id', $childCat->id)->get();

            if($ifHasChild->total() >=  1 || in_array( $childCat->id ,$getSavedCategories )){
				/*foreach($ifHasChild as $te){
				 
			}*/
				if(isset($phoneModels->cat_id) && $phoneModels->cat_id == $childCat->id){
				continue;
			}
                $hasChild = true;
           }else{

           }
        }
        if( $hasChild == null){

        	continue;

        }else{

	    }
        @endphp
        
		@foreach($update_cat as $cat)
		    
		    <?php if( in_array( $cat->id ,$getSavedCategories ) ){
		        $getMultipleModelsOnlyForSelect2Class[$cat->id] =  $cat->name ;
		        } 
		        if(isset($getMultipleModelsOnlyForSelect2Class)){
		            $getNumberOfValuesInArray   =   count($getMultipleModelsOnlyForSelect2Class);
		        }
			?> 
		@endforeach
		
		
		
        <div class="form-group " data-count="{{ $data_count }}" id="root_cat_{{ $data_count }}">
        	<label for="root_cat_label" class="col-md-3 control-label text-left">Root category</label>
        	<div class="col-md-9">
        		<select name="categories[]" class="form-control custom-select-black root-category <?php if(isset($getNumberOfValuesInArray) && $getNumberOfValuesInArray >= 2){ echo 'example-basic-multiple'; } ?>"  <?php if(isset($getNumberOfValuesInArray) && $getNumberOfValuesInArray >= 2){ echo 'multiple="multiple"'; } ?> select-attr="root-cat" id="root_cat_label" onchange="getChildForRootCat(this);">
        			<option value="">Please Select</option>
        			@foreach($update_cat as $cat)
        			<option value="{{ $cat->id }}"
        				<?php if( in_array( $cat->id ,$getSavedCategories ) )
						{ echo 'selected'; } ?>  {{ $pro_cat->category_id }} >{{ $cat->name }}</option>
        			@endforeach
        			<!--<option value="create-model">Add New</option>-->
        		</select>
        	</div>
        </div>
        <?php $data_count = $data_count+1; 
            if(isset($getMultipleModelsOnlyForSelect2Class)){
		        unset($getMultipleModelsOnlyForSelect2Class);
		        $getMultipleModelsOnlyForSelect2Class = array();
            }
		        ?>
        @endforeach
        @php } @endphp
        
        @isset($phoneModelsReadOnly)
            @if($phoneModelsReadOnly->count() > 0)
            <div class="form-group " id="">
            	<label for="phone_model" class="col-md-3 control-label text-left">Phone Models</label>
            	<div class="col-md-9">
            		<input type="hidden" name="categories_old_test[]" value="">
            		<input type="hidden" name="phone_model_old_test" value="">
            		<input type="text" readonly="" name="" class="form-control " value="@foreach($phoneModelsReadOnly as $phMoRe){{ $phMoRe->name }},@endforeach">        	      		
            	</div>
            </div>
            @endif
        @endisset

        <div class="form-group " data-count="19" id="accessory_brand_main">
        	<label for="root_cat_label" class="col-md-3 control-label text-left">Accessory Brands</label>
        	<div class="col-md-9" style="display: flex;">
        		<select name="categories[]" class="form-control custom-select-black " id="root_cat_label" onchange="getChildForAccBrand(this);createChildCatForAccessoryBrands(this);"  select-attr="root-cat">
					<option value="">Please Select</option>
        			@foreach($get_accBrand as $cat)
        			<option value="{{ $cat->id }}"
        				@php
        				if(isset($product_categories)){
        				if($product_categories->contains('category_id',$cat->id))
							{
							  echo "selected";
							}
						}

        				@endphp

        			 >{{ $cat->name }}</option>
        			@endforeach
        			<!--<option value="create-child-cat-for-acc-brand-427">Add New</option>-->
        		</select>
        	</div>
        </div>
        <div class="form-group " data-count="100" id="root_cat_id">
        	<label for="root_cat_label" class="col-md-3 control-label text-left">Brands</label>
        	<div class="col-md-9"  style="display: flex;">
        		<select name="categories[]" class="form-control custom-select-black " id="root_cat_label" onchange="getChildForBrandCat(this);createChildCatForBrands(this, '392' );"  select-attr="root-cat">
					<option value="">Please Select</option>
        			@foreach($phone_model as $cat)
        			<option value="{{ $cat->id }}"
        				@php
        				if(isset($product_categories)){
        				if($product_categories->contains('category_id',$cat->id))
							{
							  echo "selected";
							}
						}

        				@endphp

        			 >{{ $cat->name }}</option>
        			@endforeach
        			<!--<option value="create-child-cat-for-brand-392">Add New</option>-->
        		</select>
        	</div>
        </div>
        
        
    	{{--<div class="form-group ">
        	<label for="tax_class_id" class="col-md-3 control-label text-left">First Child</label>
        	<div class="col-md-9">
        		<select name="phone_model" class="form-control custom-select-black " id="first_child_cat_id" onchange="getSecondChildForFirstChild(this)">
	        		@foreach($phone_model as $cat)
	    			<option value="{{ $cat->id }}" @isset($phoneModels) @if($phoneModels->cat_id == $cat->id) selected @endif @endisset>{{ $cat->name }}</option>
	    			@endforeach
        		</select>
        	</div>
        </div> --}}

        {{--
    	<div class="form-group ">
        	<label for="tax_class_id" class="col-md-3 control-label text-left">Second Child</label>
        	<div class="col-md-9">
        		<select name="categories[]" class="form-control custom-select-black " id="second_child_cat_id" onchange="getSecondChildForFirstChild(this)">        			
        		</select>
        	</div>
        </div>
        <div class="form-group ">
        	<label for="tax_class_id" class="col-md-3 control-label text-left">Third Child</label>
        	<div class="col-md-9">
        		<select name="categories[]" class="form-control custom-select-black " id="third_child_cat_id" onchange="getThirdChildForSecondChild(this)">
        		</select>
        	</div>
        </div> --}}
    @isset($get_carriers)
        @foreach($get_carriers as $cat)
			<?php if(isset($getSavedCategories)){  if( in_array( $cat->id ,$getSavedCategories ) )
			{ $attrname = 'name='; $attrvalue = 'categories'; $attrArr = '[]'; $carriername = $attrname.$attrvalue.$attrArr; continue; }else{} continue;  }else{ }?>
		@endforeach
	@endisset
        <div class="form-group ">
        	<label for="tax_class_id" class="col-md-3 control-label text-left">Carriers</label>
        	<div class="col-md-9">
        		<select class="form-control custom-select-black selectize prevent-creation" @isset($carriername) {{ $carriername }} @endisset multiple='1'  onchange="addNameToCarrier(this)" id="carriers_select">
        			<option value="select-carrier">Please Select</option>
        			@foreach($get_carriers as $cat)
	    			<option value="{{ $cat->id }}"
	    				<?php if(isset($getSavedCategories)){  if( in_array( $cat->id ,$getSavedCategories ) )
						{ echo 'selected'; } }?>
	    				>{{ $cat->name }}</option>
	    			@endforeach
        		</select>
        	</div>
        </div>

        {{ Form::select('tax_class_id', trans('product::attributes.tax_class_id'), $errors, $taxClasses, $product) }}
        {{ Form::checkbox('is_active', trans('product::attributes.is_active'), trans('product::products.form.enable_the_product'), $errors, $product) }}
    </div>
</div>


@push('scripts')
<script type="text/javascript">

    $(document).ready(function(){
       var checkRootValue = $('.product-root-type').val();
       if(checkRootValue == '1'){
            $('#accessory_brand_main').html('');
            $('#root_cat_id').html('');
       }
       /*if(checkRootValue == '133'){
            $('#root_cat_2').parent().append('<input name="categories[]" class="form-control " id="create-cat-133" value="162" type="hidden">');
            $('#root_cat_2').html('');
       }*/
       
       var selected_option = $('#carriers_select option:selected');
       if (selected_option != "") {
        console.log('selected_option: '+selected_option);
           
       }
       
    });
	function getChildForPhoneCat(t){
        var CSRF_TOKEN 		= 	$('meta[name="csrf-token"]').attr('content');
        var val 			=	t.value
        var el_id			=	$(t).parent().parent().attr('id');
        p_element 			=	$(t).parent().parent();
        var ele_count		=	$(t).parent().parent().attr('data-count');
        var mainRootCatId	=	$('#main_root_cat_label').val();
        if(t.value == '1'){
            $('#accessory_brand_main').html('');
        }
        console.log(ele_count);
        $.ajax({
            url: '{{ route("admin.categories.get_child_for_phone") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN, id:val, el_id: el_id, ele_count: ele_count,mainRootCatId: mainRootCatId},
            dataType: 'JSON',
            success: function(data){
            console.log(data.html);
            if(data.success == 'empty'){
            }else{
            	 
            	 $('[data-count='+data.child_el+']').html('');
            	$(data.html).insertAfter('#'+data.parent);
            }
        }
        });
	}
	function getChildForRootCat(t,type){
        var CSRF_TOKEN 		= 	$('meta[name="csrf-token"]').attr('content');
        var val 			=	t.value
        var el_id			=	$(t).parent().parent().attr('id');
        p_element 			=	$(t).parent().parent();
        var ele_count		=	$(t).parent().parent().attr('data-count');
        var mainRootCatId	=	$('#main_root_cat_label').val();
        if(typeof(type) != "undefined" && type !== null) {
            var type = type;
        }else{
            var type = 'not-defined';
        }
        if(t.value == '1'){
            $('#accessory_brand_main').html('');
            $('#root_cat_id').html('');
            getChildForPhoneCat(t);
        }else{
            console.log(ele_count);
            $.ajax({
                url: '{{ route("admin.categories.first_child") }}',
                type: 'POST',
                data: {_token: CSRF_TOKEN, id:val, el_id: el_id, ele_count: ele_count,mainRootCatId: mainRootCatId, type:type},
                dataType: 'JSON',
                success: function(data){
                console.log(data.html);
                if(data.success == 'empty'){
                }else{
                	 
                	 $('[data-count='+data.child_el+']').html('');
                	$(data.html).insertAfter('#'+data.parent);
                }
            }
            });
        }
	}
	function getSecondChildForFirstChild(t){
        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
        var val 		=	t.value
        console.log(t.value);
        $.ajax({
            url: '{{ route("admin.categories.first_child") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN, id:val},
            dataType: 'JSON',
            success: function(data){
            console.log(data.html);
            $(this).parent().parent().append(data.html);
            
            }
        });
	}
	function removeThisCategory(t){
		$(t).parent().parent().remove();
	}

	function addNewCat(t,id){
		console.log(t.value);
		/*$(t).attr('select-attr');*/
		var thisValue	=	'create-model-'+id;
		var mainDivId	=	'categories-'+id;
		console.log(t.value );
        if(t.value == 'create-model'){
        	$(t).parent().append('<input name="create_phone_model" class="form-control " id="create-model-category" type="text">');
			$(t).remove();
			var main_el 	=	$(t).parent().parent().attr('class');
			console.log(main_el);
			//$( main_el +'> .col-md-1 > button > i').addClass('test');
        }else if(t.value == thisValue){
        	$(t).parent().append('<input name="categories[]" class="form-control " id="'+'create-cat-'+id+'" type="text">');
        	$('#'+mainDivId+' > .col-md-1 > .btn-default').removeAttr('onclick');
        	var getThis 		=	'this';
        	var saveFunction 	=	'saveCategoryPhoneModel('+getThis+','+id+')';
        	$('#'+mainDivId+' > .col-md-1 > .btn-default').attr('onclick',saveFunction);
        	$('#'+mainDivId+' > .col-md-1 > .btn-default > .fa').removeClass('fa-trash');
        	$('#'+mainDivId+' > .col-md-1 > .btn-default > .fa').addClass('fa-plus');
			$(t).remove();
        }

		/*
		$(t).parent().parent().append('<input name="phone_model" class="form-control " id="phone_model" type="text">');
		$(t).parent().remove();
*/
	}

	function savePhoneModel(t , id){
		var cat_name 	=	$('#create-model-category').val();
		/*var cat_name 	=	$('#create-cat-'+id).val();*/
        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
        setTimeout(function(){ getCategoryPhoneModel(id) }, 3000);
        $.ajax({
            url: '{{ route("admin.categories.store") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN, name:cat_name, parent_id: id, is_active:'1', is_searchable: '0'},
            dataType: 'JSON',
            success: function(data){
            	getCategoryPhoneModel(id);
            },
            error: function(){
            	getCategoryPhoneModel(id);
            },

        });
	}
	function saveCategoryPhoneModel(t , id){
		/*var cat_name 	=	$('#create-model-category').val();create-cat-*/
		var cat_name 	=	$('#create-cat-'+id).val();
        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
        setTimeout(function(){ getCategorySaved(id) }, 3000);
        $.ajax({
            url: '{{ route("admin.categories.store") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN, name:cat_name, parent_id: id, is_active:'1', is_searchable: '0'},
            dataType: 'JSON',
            success: function(data){
            	getCategoryPhoneModel(id);
            },
            error: function(){
            	getCategoryPhoneModel(id);
            },

        });
	}

	function createRootCategory(t){
		/*var cat_name 	=	$('#create-model-category').val();*/
        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
        
        if(t.value == 'main_root_cat_label'){
        	$(t).parent().append('<input name="create_phone_model" class="form-control " id="create-main_root_cat_label" type="text"><button type="button" class="btn btn-default pull-right" onclick="saveRootCategory()" data-toggle="tooltip" title="" data-original-title="">  <i class="fa fa-plus"></i> </button>');
			$(t).remove();
			var main_el 	=	$(t).parent().parent().attr('class');
			console.log(main_el);
        }
        /*setTimeout(function(){ getCategoryPhoneModel(id) }, 3000);
        $.ajax({
            url: '{{ route("admin.categories.store") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN, name:cat_name, parent_id: id, is_active:'1', is_searchable: '0'},
            dataType: 'JSON',
            success: function(data){
            	getCategoryPhoneModel(id);
            },
            error: function(){
            	getCategoryPhoneModel(id);
            },
			<button type="button" class="btn btn-default pull-right" onclick="saveCategoryPhoneModel()" data-toggle="tooltip" title="" data-original-title="">  <i class="fa fa-plus"></i> </button>
        });*/
	}

	function saveRootCategory(t){

			var cat_name 	=	$('#create-main_root_cat_label').val();
	        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
	        setTimeout(function(){ getSavedRootCategory() }, 3000);
	        $.ajax({
	            url: '{{ route("admin.categories.store") }}',
	            type: 'POST',
	            data: {_token: CSRF_TOKEN, name:cat_name, parent_id: null, is_active:'1', is_searchable: '0'},
	            dataType: 'JSON',
	            success: function(data){

	            },
	            error: function(){

	            },

	        });
	}

	function getSavedRootCategory(){
        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '{{ route("admin.categories.getCategoryPhoneModel") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            success: function(data){
            	$('#appended_with_root_category').remove();
            	$('#create-main_root_cat_label').append('<input type="hidden" name="categories[]" value="'+data.id+'" >');
            	$('#appended_with_root_category').val(data.name);
            }
        });
	}

	function getCategoryPhoneModel(id){
        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '{{ route("admin.categories.getCategoryPhoneModel") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN, parent_id: id},
            dataType: 'JSON',
            success: function(data){
            	if(data.p_id == 'make_hidden'){

	            	$('#appended_category').remove();
	            	$('#phone_model-'+data.id+'').remove();
	            	$('#create-model-category').parent().append('<input id="phone_model-'+data.id+'" type="hidden" name="phone_model" value="'+data.id+'" >');
	            	$('#root_cat_idtest').append('<input type="hidden" id="appended_category" name="categories[]" value="'+data.id+'" >');
	            	$('#create-model-category').val(data.name);
            	}else{
            		$('#create_cate-'+data.id+'').remove();
	            	$('#root_cat_idtest').append('<input id="create_cate-'+data.id+'" type="hidden" name="categories[]" value="'+data.id+'" >');
            	}
            }
        });
	}

	function getCategorySaved(id){
        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
        console.log(id);
        $.ajax({
            url: '{{ route("admin.categories.getCategoryPhoneModel") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN, parent_id: id},
            dataType: 'JSON',
            success: function(data){
            	$('#appended_category'+id).remove();
            	$('#root_cat_idtest').append('<input type="hidden" id="appended_category'+id+'" name="categories[]" value="'+data.id+'" >');
            	$('#create-cat-'+id).val(data.name);
            }
        });
	}
	$('.example-basic-multiple').select2({});
</script>
@include('product::admin.products.partials.script')

@endpush