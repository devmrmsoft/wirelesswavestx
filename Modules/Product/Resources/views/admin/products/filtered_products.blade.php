@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('product::products.products'))

    <li class="active">{{ trans('product::products.products') }}</li>
@endcomponent

@push('styles')

<style type="text/css">
	.thumbnail-holder {
	    position: relative;
	    border: 1px solid #d9d9d9;
	    background: #fff;
	    height: 50px;
	    width: 50px;
	    border-radius: 3px;
	    overflow: hidden!important;
	}
	.thumbnail-holder>img {
	    position: absolute;
	    top: 50%;
	    left: 50%;
	    -webkit-transform: translate(-50%,-50%);
	    transform: translate(-50%,-50%);
	}
	.thumbnail-holder>img {
	    max-height: 100%;
	    max-width: 100%;
	}
</style>

@endpush

@section('filter-form')
<?php
    $categories =   DB::table('categories')->get();
?>
    <div class="row">
        <div class="col-md-12">                
            <form method="GET" action="{{ route('admin.products.products_filter') }}">
                {{ csrf_field() }}
                <div class="row m-top-2 form-group">
                    <label for="" class="col-md-3 control-label text-left">Search By Category</label>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <select id="order-status" name="cat" class="form-control custom-select-black" data-id="6">
                            @foreach($categories as $cat)
                            <option value="{{ $cat->id }},{{ $cat->slug }}">{{ $cat->slug }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@component('admin::components.page.index_table')

    @slot('buttons', ['create'])
    @slot('resource', 'products')
    @slot('name', trans('product::products.product'))

<div class="table-responsive">
    <table class="table table-striped table-hover" id="products-table">
        <thead>
        	<tr>
	            <th>Serial</th>
	            <th>Thumbnail</th>
                <th>Name</th>
                <th>Price</th>
	            <th>Category</th>
        	</tr>
        </thead>

        <tbody>
            <?php $serialNum   =   1; ?>
        	@foreach($products as $product)
        	<tr>
	        	<td>#<?=$serialNum?></td>
	        	<td>
	        		<div class="thumbnail-holder">
	        			<img src="{{ asset('/storage/') }}/{{ $product->imagePath }}">
	        		</div>
	        	</td>
                <td>{{ $product->name }}</td>
	        	<td>{{ substr($product->price, '-1') }}</td>
                <td>{{ ucfirst($catSlug) }}</td>
        	</tr>
            <?php $serialNum   += 1; ?>
        	@endforeach
        </tbody>

        @isset($tfoot)
            <tfoot>{{ $tfoot }}</tfoot>
        @endisset
    </table>
</div>

@endcomponent
@push('scripts')
    <script>

        $(document).ready( function () {
            $('#products-table .table').DataTable();
        } );
    </script>
@endpush
