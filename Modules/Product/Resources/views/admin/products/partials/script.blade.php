@push('scripts')
<script type="text/javascript">
	function getChildForAccBrand(t){
        var CSRF_TOKEN 		= $('meta[name="csrf-token"]').attr('content');
        var val 			=	t.value
        var el_id			=	$(t).parent().parent().attr('id');//accessory_brand_main
        p_element 			=	$(t).parent().parent();
        var ele_count		=	$(t).parent().parent().attr('data-count');//19
        console.log(ele_count);
        $.ajax({
            url: '{{ route("admin.categories.acc_brand") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN, id:val, el_id: el_id, ele_count: ele_count},
            dataType: 'JSON',
            success: function(data){
            console.log(data.html);
            if(data.success == 'empty'){
            }else{
            	 $('[data-count='+data.child_el+']').html('');
            	$(data.html).insertAfter('#'+data.parent);
            }
        }
        });
	}
	function getChildForBrandCat(t){	
        var CSRF_TOKEN 		= $('meta[name="csrf-token"]').attr('content');
        var val 			=	t.value
        var el_id			=	$(t).parent().parent().attr('id');//accessory_brand_main
        p_element 			=	$(t).parent().parent();
        var ele_count		=	$(t).parent().parent().attr('data-count');//19
        console.log(ele_count);
        $.ajax({
            url: '{{ route("admin.categories.get_model_for_brand") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN, id:val, el_id: el_id, ele_count: ele_count},
            dataType: 'JSON',
            success: function(data){
            console.log(data.html);
            if(data.success == 'empty'){
            }else{
                $('[data-count='+data.child_el+']').html('');
            	$(data.html).insertAfter('#'+data.parent);
            	$('.example-basic-multiple').select2({});
            }
        }
        });
	}
	function addNameToCarrier(t){
		if(t.value == 'select-carrier' || t.value == ''){
			$(t).removeAttr('name');
		}else{
			$(t).attr('name','categories[]');
		}
	}


	function createChildCatForBrands(t){
        
        if(t.value == 'create-child-cat-for-brand-392'){
        	$(t).parent().append('<input name="create-child-cat-for-brand-392" class="form-control " id="create-child-cat-for-brand-392" type="text"><button type="button" class="btn btn-default pull-right" onclick="saveChildCatForBrands(this)" data-toggle="tooltip" title="" data-original-title="">  <i class="fa fa-plus"></i> </button>');
			$(t).remove();
			var main_el 	=	$(t).parent().parent().attr('class');
			console.log(main_el);
        }
	}

	function saveChildCatForBrands(t){

			var cat_name 	=	$('#create-child-cat-for-brand-392').val();
	        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
	        setTimeout(function(){ getChildCatForBrands () }, 3000);
	        $.ajax({
	            url: '{{ route("admin.categories.store") }}',
	            type: 'POST',
	            data: {_token: CSRF_TOKEN, name:cat_name, parent_id: '392', is_active:'1', is_searchable: '0'},
	            dataType: 'JSON',
	            success: function(data){

	            },
	            error: function(){

	            },

	        });
	}

	function getChildCatForBrands(){
        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '{{ route("admin.categories.getCategoryPhoneModel") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            success: function(data){
            	$('#create-child-cat-for-brand-392').val('');
            	$('#create-main_root_cat_label').append('<input type="hidden" name="categories[]" value="'+data.id+'" >');
            	$('#create-child-cat-for-brand-392').val(data.name);
            }
        });
	}
    function createChildCatForAccessoryBrands(t){
        
        if(t.value == 'create-child-cat-for-acc-brand-427'){
            $(t).parent().append('<input name="create-child-cat-for-acc-brand-427" class="form-control " id="createChildCatForAccBrand427" type="text"><button type="button" class="btn btn-default pull-right" onclick="saveChildCatForAccessoryBrands(this)" data-toggle="tooltip" title="" data-original-title="">  <i class="fa fa-plus"></i> </button>');
            $(t).remove();
            var main_el     =   $(t).parent().parent().attr('class');
            console.log(main_el);
        }
    }

    function saveChildCatForAccessoryBrands(t){

            var cat_name    =   $('#createChildCatForAccBrand427').val();
            alert(cat_name);
            var CSRF_TOKEN  = $('meta[name="csrf-token"]').attr('content');
            setTimeout(function(){ getChildCatForAccessoryBrands () }, 3000);
            $.ajax({
                url: '{{ route("admin.categories.store") }}',
                type: 'POST',
                data: {_token: CSRF_TOKEN, name:cat_name, parent_id: '427', is_active:'1', is_searchable: '0'},
                dataType: 'JSON',
                success: function(data){

                },
                error: function(){

                },

            });
    }

    function getChildCatForAccessoryBrands(){
        var CSRF_TOKEN  = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '{{ route("admin.categories.getCategoryPhoneModel") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN},
            dataType: 'JSON',
            success: function(data){
                $('#createChildCatForAccBrand427').val('');
                $('#create-main_root_cat_label').append('<input type="hidden" name="categories[]" value="'+data.id+'" >');
                $('#createChildCatForAccBrand427').val(data.name);
            }
        });
    }
	function addHiddenFieldForPhoneModel(t){
		var phoneModelValue = $(t).val();
				$('.create-model-category').parent().append('<input id="phone_model-'+phoneModelValue+'" type="hidden" name="phone_model" value="'+phoneModelValue+'" >');
	}
	function addNewBrand(t,id){
		console.log(t.value);
		/*$(t).attr('select-attr');*/
		var thisValue	=	'create-model-'+id;
		var mainDivId	=	'categories-'+id;
		console.log(t.value );
        if(t.value == 'create-model'){
        	$(t).parent().append('<input name="create_phone_model" class="form-control " id="create-brand-model-category" type="text">');
			$(t).remove();
			var main_el 	=	$(t).parent().parent().attr('class');
			console.log(main_el);
			//$( main_el +'> .col-md-1 > button > i').addClass('test');
        }else if(t.value == thisValue){
        	$(t).parent().append('<input name="categories[]" class="form-control " id="'+'create-cat-'+id+'" type="text">');
        	$('#'+mainDivId+' > .col-md-1 > .btn-default').removeAttr('onclick');
        	var getThis 		=	'this';
        	var saveFunction 	=	'saveCategoryPhoneModel('+getThis+','+id+')';
        	$('#'+mainDivId+' > .col-md-1 > .btn-default').attr('onclick',saveFunction);
        	$('#'+mainDivId+' > .col-md-1 > .btn-default > .fa').removeClass('fa-trash');
        	$('#'+mainDivId+' > .col-md-1 > .btn-default > .fa').addClass('fa-plus');
			$(t).remove();
        }

		/*
		$(t).parent().parent().append('<input name="phone_model" class="form-control " id="phone_model" type="text">');
		$(t).parent().remove();
*/
	}


	function saveBrandModel(t , id){
		var cat_name 	=	$('#create-brand-model-category').val();
		/*var cat_name 	=	$('#create-cat-'+id).val();*/
        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
        setTimeout(function(){ getBrandPhoneModel(id) }, 3000);
        $.ajax({
            url: '{{ route("admin.categories.store") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN, name:cat_name, parent_id: id, is_active:'1', is_searchable: '0'},
            dataType: 'JSON',
            success: function(data){
            },
            error: function(){
            },

        });
	}

	function getBrandPhoneModel(id){
        var CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '{{ route("admin.categories.getCategoryPhoneModel") }}',
            type: 'POST',
            data: {_token: CSRF_TOKEN, parent_id: id},
            dataType: 'JSON',
            success: function(data){
            	if(data.p_id == 'make_hidden'){
	            	$('#appended_category').remove();
	            	$('#phone_model-'+data.id+'').remove();
	            	$('#create-brand-model-category').parent().append('<input id="phone_model-'+data.id+'" type="hidden" name="phone_model" value="'+data.id+'" >');
	            	$('#root_cat_idtest').append('<input type="hidden" id="appended_category" name="categories[]" value="'+data.id+'" >');
	            	$('#create-brand-model-category').val(data.name);
            	}else{
            		$('#create_cate-'+data.id+'').remove();
	            	$('#root_cat_idtest').append('<input id="create_cate-'+data.id+'" type="hidden" name="categories[]" value="'+data.id+'" >');
            	}
            }
        });
	}
    function createAccessoryBrand(){

    }
    
    function createHeadphoneBluetooth(el, p_id){
        if(el.value == '59'){
            getChildForRootCat(el,'headphones');
        }
    }
    setInterval(function(){ $('#new_brand_phone_models').val($('#brand_phone_models').val()); }, 5000);
</script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endpush