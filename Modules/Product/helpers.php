<?php

use Illuminate\Support\HtmlString;

if (! function_exists('product_price')) {
    /**
     * Get the selling price of the given product.
     *
     * @param \Modules\Product\Entities\Product $product
     * @param string $class
     * @return \Illuminate\Support\HtmlString|string
     */
    function product_price($product, $class = 'previous-price')
    {
        //dd($product->price);
        if (! $product->hasSpecialPrice()) {
            return $product->price->convertToCurrentCurrency($product->id)->format();

            /*if (Auth::check()){

                $listID = Auth::user()->ret_list;
                if($listID >= '1'){

                    $retListTable   =   DB::table('retailer_lists')->where('id', $listID )->first();
                    //return $retListTable->list_price;
                    $amount =   $retListTable->list_price;
                    $price  =   $product->price->convertToCurrentCurrency()->format();
                    //return $price;
                    $price  =   substr($price, 1);
                    $price =    $price * ((100- $amount) / 100);

                    //$price = (int) $price * ((100- 5) / 100);

                    return '$'.$price;
                }else{
                    return $product->price->convertToCurrentCurrency()->format();
                }

            }else{
                return $product->price->convertToCurrentCurrency()->format();
            }*/
            //return $product->price->convertToCurrentCurrency()->format();
        }

        if (Auth::check())
        {

          /*  $price = Auth::user();
            dd($price);
            $price = '1042.00';
            $amount =   Auth::user()->ret_list;
            $specialPrice = $specialPrice * ((100-$amount) / 100);

            */
            /*$price = $product->price->convertToCurrentCurrency()->format();
            $specialPrice = $product->special_price->convertToCurrentCurrency()->format();*/

            $price = '1235';
            $specialPrice = '12312';
            $price = $product->price->convertToCurrentCurrency()->format();
            $specialPrice = $product->price->convertToCurrentCurrency()->format();

        }else{
            $price = $product->price->convertToCurrentCurrency()->format();

            $specialPrice = $product->special_price->convertToCurrentCurrency()->format();

        }



        //$price = $product->price->convertToCurrentCurrency()->format();
        // $specialPrice = $product->special_price->convertToCurrentCurrency()->format();

        return new HtmlString("{$specialPrice} <span class='{$class}'>{$price}</span>");
    }
}
