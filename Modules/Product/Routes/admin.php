<?php

Route::get('products', [
    'as' => 'admin.products.index',
    'uses' => 'ProductController@index',
    'middleware' => 'can:admin.products.index',
]);

Route::get('products/disabled', [
    'as' => 'admin.products.index_disabled',
    'uses' => 'ProductController@indexDisabled',
    'middleware' => 'can:admin.products.index',
]);

Route::get('products/filter', [
    'as' => 'admin.products.products_filter',
    'uses' => 'ProductController@filterProduct',
    'middleware' => 'can:admin.products.index',
]);


Route::post('products/option/image-create', [
    'as' => 'admin.products.create_option_image',
    'uses' => 'ProductController@optionImage',
    'middleware' => 'can:admin.products.index',
]);


Route::get('get-options-data', [
    'as' => 'admin.products.get_options_data',
    'uses' => 'ProductController@getOptionsData',
    'middleware' => 'can:admin.products.index',
]);


Route::put('products/option/image', [
    'as' => 'admin.products.option_image',
    'uses' => 'ProductController@optionImage',
    'middleware' => 'can:admin.products.index',
]);


Route::post('products/inventory/option/images/upload', [
    'as' => 'admin.products.inventory_option_images_upload',
    'uses' => 'ProductController@productInventoryOptionImagesUpload',
    'middleware' => 'can:admin.products.index',
]);


Route::get('products/create', [
    'as' => 'admin.products.create',
    'uses' => 'ProductController@create',
    'middleware' => 'can:admin.products.create',
]);

Route::post('products', [
    'as' => 'admin.products.store',
    'uses' => 'ProductController@store',
    'middleware' => 'can:admin.products.create',
]);

Route::get('products/{id}/edit', [
    'as' => 'admin.products.edit',
    'uses' => 'ProductController@edit',
    'middleware' => 'can:admin.products.edit',
]);

Route::put('products/{id}', [
    'as' => 'admin.products.update',
    'uses' => 'ProductController@update',
    'middleware' => 'can:admin.products.edit',
]);

Route::POST('products/products-update_disable', [
    'as' => 'admin.products.products_disable',
    'uses' => 'ProductController@productsDisable',
    'middleware' => 'can:admin.products.edit',
]);

Route::POST('products/products-update_enable', [
    'as' => 'admin.products.products_enable',
    'uses' => 'ProductController@productsEnable',
    'middleware' => 'can:admin.products.edit',
]);

Route::delete('products/{ids}', [
    'as' => 'admin.products.destroy',
    'uses' => 'ProductController@destroy',
    'middleware' => 'can:admin.products.destroy',
]);
