<?php

Route::get('products', 'ProductController@index')->name('products.index');
Route::get('products/{slug}', 'ProductController@show')->name('products.show');
Route::post('get-phone-models', 'ProductController@getPhoneModels')->name('get-phone-models');
Route::post('get-brand-models', 'ProductController@getBrandModels')->name('get-brand-models');
Route::post('get-accessory-brand-models', 'ProductController@getAccessoryBrandModels')->name('get-accessory-brand-models');

Route::get('get-product-color-images', 'ProductController@getProductColorImages')->name('get-product-color-images');
