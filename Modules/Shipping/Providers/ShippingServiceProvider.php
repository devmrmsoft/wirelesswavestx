<?php

namespace Modules\Shipping\Providers;
use Modules\Cart\Facades\Cart;
use Modules\Shipping\Method;
use Illuminate\Support\ServiceProvider;
use Modules\Shipping\Facades\ShippingMethod;

class ShippingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (! config('app.installed')) {
            return;
        }

        $this->registerFreeShipping();
        $this->registerLocalPickup();
        $this->registerFlatRate();
    }

    private function registerFreeShipping()
    {
        // $cart = Cart::instance();
        // print_r($cart->total()->amount);
        if (! setting('free_shipping_enabled')) {
            return;
        }
        // echo '1';
        ShippingMethod::register('free_shipping', function () {
            return new Method('free_shipping', setting('free_shipping_label'), 0);
        });
    }

    private function registerLocalPickup()
    {
        if (! setting('local_pickup_enabled')) {
            return;
        }
        // echo '2';
        ShippingMethod::register('local_pickup', function () {
            return new Method('local_pickup', setting('local_pickup_label'), setting('local_pickup_cost'));
        });
    }

    private function registerFlatRate()
    {
        // $cart = Cart::instance();
        if (! setting('flat_rate_enabled')) {
            return;
        }
        // echo '3';
        ShippingMethod::register('flat_rate', function () {
            return new Method('flat_rate', setting('flat_rate_label'), setting('flat_rate_cost'));
        });
    }
}
