<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Modules\User\Entities\User;
use Modules\Order\Entities\Order;
use Illuminate\Routing\Controller;
use Modules\Review\Entities\Review;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\SearchTerm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class DashboardController extends Controller
{
    /**
     * Display the dashboard with its widgets.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset($_REQUEST['from'])){

            $from  =   $_GET['from'];
            $to  =   $_GET['to'];
            return view('admin::dashboard.index', [
                'totalSales' => Order::totalSalesFiltered(),
                'totalOrders' => Order::whereBetween('created_at', [$from, $to])->count(),
                'totalProducts' => Product::withoutGlobalScope('active')->whereBetween('created_at', [$from, $to])->count(),
                'totalCustomers' => $this->totalCustomersFilter(),
                'latestSearchTerms' => $this->getLatestSearchTerms(),
                'latestOrders' => $this->getLatestOrders(),
                'latestReviews' => $this->getLatestReviews(),
            ]);
        }
        return view('admin::dashboard.index', [
            'totalSales' => Order::totalSales(),
            'totalOrders' => Order::count(),
            'totalProducts' => Product::withoutGlobalScope('active')->count(),
            'totalCustomers' => User::totalCustomers(),
            'latestSearchTerms' => $this->getLatestSearchTerms(),
            'latestOrders' => $this->getLatestOrders(),
            'latestReviews' => $this->getLatestReviews(),
        ]);
    }
    

    public function adminStoreIndex()
    {
        return view('admin::dashboard.index-store');
    }
    
    public function save_store(Request $req)
    {
        DB::table('retailer_stores')->insert(['user_id'=> Auth::user()->id ,'store_name'=> $req->store_name,'first_name'=> $req->first_name, 'last_name'=> $req->last_name, 'store_address' => $req->store_address ,'store_postcode'=> $req->store_postcode, 'store_city'=> $req->store_city , 'store_state'=> $req->store_state, 'store_country'=> $req->store_country]);

        return view('admin::dashboard.index-store');
    }

    public function delete_store($id)
    {
        
        DB::table('retailer_stores')->where('id',$id)->delete();
        return view('admin::dashboard.index-store');
    }

    private function getLatestSearchTerms()
    {
        return SearchTerm::latest('updated_at')->take(5)->get();
    }


    private function totalCustomersFilter()
    {
        $from  =   $_GET['from'];
        $to  =   $_GET['to'];
        $result = User::whereBetween('created_at', [$from, $to])->count();
        return $result;
        
    }

    /**
     * Get latest five orders.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getLatestOrders()
    {
        return Order::select([
            'id',
            'customer_first_name',
            'customer_last_name',
            'total',
            'status',
            'created_at',
        ])
        ->latest()
        ->take(5)
        ->get();
    }

    /**
     * Get latest five reviews.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getLatestReviews()
    {
        return Review::select('id', 'product_id', 'reviewer_name', 'rating')
            ->has('product')
            ->with('product:id')
            ->limit(5)
            ->get();
    }
}
