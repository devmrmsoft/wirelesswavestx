<?php

Route::get('/', 'DashboardController@index')->name('admin.dashboard.index');

Route::post('save-store','DashboardController@save_store')->name('admin.save-store');

Route::get('/delete-store/{id}','DashboardController@delete_store')->name('admin.delete-store');

Route::get('admin-store-index', 'DashboardController@adminStoreIndex')->name('admin.stores.index');

Route::get('/sales-analytics', [
    'as' => 'admin.sales_analytics.index',
    'uses' => 'SalesAnalyticsController@index',
    'middleware' => 'can:admin.orders.index',
]);
