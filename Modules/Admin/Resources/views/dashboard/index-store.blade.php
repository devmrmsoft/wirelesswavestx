<?php
    $stores = DB::table('retailer_stores')->where('user_id', Auth::user()->id)->get();
?>
@extends('admin::layout')

@section('title', trans('admin::dashboard.dashboard'))

@push('styles')
<style type="text/css">
    .filter-report{
        padding: 15px;
        background: #fff;
        border-radius: 3px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.1);
        box-shadow: 0 1px 2px rgba(0,0,0,.1);
    }
    .grid-info{
        background-color: #8862e0;
        padding: 25px 20px 20px 20px;
    }

    .grid-success{
        background-color: #19d895;
        padding: 25px 20px 20px 20px;
    }
    .text-white{
        color: #ffffff;
    }

    .mb-3{
        margin-bottom: 1rem !important;
    }

    .d-flex{
        display: flex !important;
    }
    .font-weight-semibold {
        font-weight: 600;
    }
    .font-weight-bold {
        font-weight: 700 !important;
    }
    .img-sm {
        width: 43px;
        min-width: 43px;
        height: 43px;
        position: relative;
        overflow: hidden;
        margin-left: auto !important;
        align-items: center !important;
        -webkit-box-pack: center !important;
        justify-content: center !important;
        background-color: #ffffff;
        border-radius: 100%;
    } 

    .img-sm-icon{
        color: #8862e0 !important;
        font-size: 20px;

    }
    .img-sm-icon-success{
        color: #19d895 !important;
        font-size: 20px;

    }


    .img-sm-icon-tProducts{
        color: #2196f3 !important;
        font-size: 20px;

    }    

    .grid-tProducts{ 
        background-color: #2196f3;
        padding: 25px 20px 20px 20px;
    }


    .img-sm-icon-tCustomers{
        color: #ffaf00 !important;
        font-size: 20px;

    }    

    .grid-tCustomers{ 
        background-color: #ffaf00;
        padding: 25px 20px 20px 20px;
    }
    .box{
        height: 75vh auto;
        width: 90%;
        padding-top: 20px;
        padding-bottom: 20px;
    }
    .box1{
        height: auto;
        width: 100%;
        background-color: white;
    }
</style>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />
@endpush

@section('content_header')
    <h2 class="pull-left">Retailer Stores</h2>
@endsection

@section('content')

<div class="container">
    <div class="container box">
        <form action='{{route("admin.save-store")}}' id="store-form" name="store-form" method="post">
        @csrf
        <div class="form-group">
            <label for="first-name">First Name</label>
            <input type="text" class="form-control" placeholder="" name="first_name" required>
          </div>
          <div class="form-group">
            <label for="last-name">Last Name</label>
            <input type="text" class="form-control" placeholder="" name="last_name" required>
          </div>
          <div class="form-group">
            <label for="store-name">Store Name</label>
            <input type="text" class="form-control" placeholder="" name="store_name" required>
          </div>
          <div class="form-group">
            <label for="store-address">Store Address</label>
            <input type="text" class="form-control" placeholder="" name="store_address" required>
          </div>
          <div class="form-group">
            <label for="store-city">City</label>
            <input type="text" class="form-control" placeholder="" name="store_city" required>
          </div>
          <div class="form-group">
            <label for="store-state">State</label>
            <input type="text" class="form-control" placeholder="" name="store_state" required>
          </div>
          <div class="form-group">
            <label for="store-postcode">Zip Code</label>
            <input type="text" class="form-control" placeholder="" name="store_postcode" required>
          </div>
          <div class="form-group">
            <label for="store-country">Country</label>
            <input type="text" class="form-control" placeholder="" name="store_country" required>
          </div>
          
          <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>

<div class="box1">
    <table class="table" id="store_table" >
      <thead>
        <tr>
          <th scope="col">S.No</th>
          <th scope="col">Store Name</th>
          <th scope="col">First Name</th>
          <th scope="col">Last Name</th>
          <th scope="col">Address</th>
          <th scope="col">City</th>
          <th scope="col">State</th>
          <th scope="col">Zip Code</th>
          <th scope="col">Country</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($stores as $store)
            <tr>
              <td>{{$store->id}}</td>
              <td>{{$store->store_name}}</td>
              <td>{{$store->first_name}}</td>
              <td>{{$store->last_name}}</td>
              <td>{{$store->store_address}}</td>
              <td>{{$store->store_city}}</td>
              <td>{{$store->store_state}}</td>
              <td>{{$store->store_postcode}}</td>
              <td>{{$store->store_country}}</td>
              <td><a href="{{route('admin.delete-store',[$store->id])}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
            </tr>
        @endforeach
      </tbody>
    </table>
</div>

@endsection

@push('scripts')

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

    // $(document).ready(function(event){

    //     $('#store_table .table').DataTable();
        
    // });
</script>

@endpush