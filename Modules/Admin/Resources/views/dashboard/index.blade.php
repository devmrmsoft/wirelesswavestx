@extends('admin::layout')

@section('title', trans('admin::dashboard.dashboard'))

@push('styles')
<style type="text/css">
    .filter-report{
        padding: 15px;
        background: #fff;
        border-radius: 3px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.1);
        box-shadow: 0 1px 2px rgba(0,0,0,.1);
    }
    .grid-info{
        background-color: #8862e0;
        padding: 25px 20px 20px 20px;
    }

    .grid-success{
        background-color: #19d895;
        padding: 25px 20px 20px 20px;
    }
    .text-white{
        color: #ffffff;
    }

    .mb-3{
        margin-bottom: 1rem !important;
    }

    .d-flex{
        display: flex !important;
    }
    .font-weight-semibold {
        font-weight: 600;
    }
    .font-weight-bold {
        font-weight: 700 !important;
    }
    .img-sm {
        width: 43px;
        min-width: 43px;
        height: 43px;
        position: relative;
        overflow: hidden;
        margin-left: auto !important;
        align-items: center !important;
        -webkit-box-pack: center !important;
        justify-content: center !important;
        background-color: #ffffff;
        border-radius: 100%;
    } 

    .img-sm-icon{
        color: #8862e0 !important;
        font-size: 20px;

    }
    .img-sm-icon-success{
        color: #19d895 !important;
        font-size: 20px;

    }


    .img-sm-icon-tProducts{
        color: #2196f3 !important;
        font-size: 20px;

    }    

    .grid-tProducts{ 
        background-color: #2196f3;
        padding: 25px 20px 20px 20px;
    }


    .img-sm-icon-tCustomers{
        color: #ffaf00 !important;
        font-size: 20px;

    }    

    .grid-tCustomers{ 
        background-color: #ffaf00;
        padding: 25px 20px 20px 20px;
    }
</style>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
@endpush

@section('content_header')
    <h2 class="pull-left">{{ trans('admin::dashboard.dashboard') }}</h2>
@endsection

@section('content')

    <div class="grid clearfix">
        <div class="row">
            <div class="col-md-3">
                <div class="grid-info">
                    <div class="d-flex">
                      <div class="wraper">
                        <p class="mb-0 font-weight-bold">{{ trans('admin::dashboard.total_sales') }}</p>
                        <h2 class="mb-0 mt-n1 font-weight-semibold text-white">{{ $totalSales->format() }}</h2>
                      </div>
                      <div class="img-sm rounded-circle bg-white d-flex align-items-center justify-content-center ml-auto">
                        <i class="fa fa-dollar img-sm-icon" aria-hidden="true"></i>
                      </div>
                    </div>
                    <canvas id="myChart"></canvas>
                </div>
            </div>
            <div class="col-md-3">
                <div class="grid-success">
                    <div class="d-flex">
                      <div class="wraper">
                        <p class="mb-0 font-weight-bold">{{ trans('admin::dashboard.total_orders') }}</p>
                        <h2 class="mb-0 mt-n1 font-weight-semibold text-white">{{ $totalOrders }}</h2>
                      </div>
                      <div class="img-sm rounded-circle bg-white d-flex align-items-center justify-content-center ml-auto">
                        <i class="fa fa-shopping-cart img-sm-icon-success" aria-hidden="true"></i>
                      </div>
                    </div>
                    <canvas id="totalOrders"></canvas>
                </div>
            </div>
            <div class="col-md-3">
                <div class="grid-tProducts">
                    <div class="d-flex">
                      <div class="wraper">
                        <p class="mb-0 font-weight-bold">{{ trans('admin::dashboard.total_products') }}</p>
                        <h2 class="mb-0 mt-n1 font-weight-semibold text-white">{{ $totalProducts }}</h2>
                      </div>
                      <div class="img-sm rounded-circle bg-white d-flex align-items-center justify-content-center ml-auto">
                        <i class="fa fa-cubes img-sm-icon-tProducts" aria-hidden="true"></i>
                      </div>
                    </div>
                    <canvas id="tProducts"></canvas>
                </div>
            </div>
            <div class="col-md-3">
                <div class="grid-tCustomers">
                    <div class="d-flex">
                      <div class="wraper">
                        <p class="mb-0 font-weight-bold">{{ trans('admin::dashboard.total_customers') }}</p>
                        <h2 class="mb-0 mt-n1 font-weight-semibold text-white">{{ $totalCustomers }}</h2>
                      </div>
                      <div class="img-sm rounded-circle bg-white d-flex align-items-center justify-content-center ml-auto">
                        <i class="fa fa-users img-sm-icon-tCustomers" aria-hidden="true"></i>
                      </div>
                    </div>
                    <canvas id="tCustomers"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="grid clearfix">
        <div class="row">
            @hasAccess('admin.orders.index')
                @include('admin::dashboard.grids.total_sales')
                @include('admin::dashboard.grids.total_orders')
            @endHasAccess

            @hasAccess('admin.products.index')
                @include('admin::dashboard.grids.total_products')
            @endHasAccess

            @hasAccess('admin.users.index')
                @include('admin::dashboard.grids.total_customers')
            @endHasAccess
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9 col-md-8">
            @hasAccess('admin.orders.index')
                @include('admin::dashboard.panels.sales_analytics')
            @endHasAccess

            @hasAccess('admin.orders.index')
                @include('admin::dashboard.panels.latest_orders')
            @endHasAccess


            @include('admin::dashboard.panels.latest_search_terms')

            @hasAccess('admin.reviews.index')
                @include('admin::dashboard.panels.latest_reviews')
            @endHasAccess
        </div>

<?php $retailers    =   DB::table('users')->where('ret_list', '!=', '0')
                                        ->where('id', '!=', '1')
                                        ->orderBy('id', 'desc')->take(5)->get();
                                        //dd($retailers);
                                        ?>
        <div class="col-lg-3 col-md-4">
            <div class="filter-report clearfix" style="margin-top: 30px;">

                <div class="box">
                    <div class="notifications">
                        <i class="fa fa-bell"></i>
                        <span class="num">{{ $totalCustomers }}</span>
                        <ul>
                            @foreach($retailers as $retailer)
                            <li class="icon">
                                <span class="icon"><i class="fa fa-user"></i></span>
                                <a href="{{ url('/admin/retailers/') }}/{{ $retailer->id }}/retailer-edit" class="decoration-none">
                                    <span class="text">{{ $retailer->first_name }} {{ $retailer->last_name }}</span>                                    
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4">
            <div class="filter-report clearfix" style="margin-top: 30px;">

                <!-- <div class="form-group" style="padding-top: 15px;">
                    <label for="from">Date Start</label>
                    <input type="text" class="form-control" name="daterange" id="demo">
                </div> -->


                <h3 class="tab-content-title">{{ trans('report::admin.filter') }}</h3>

                <form method="GET" action="{{ route('admin.dashboard.index') }}">
                    <div class="form-group" style="padding-top: 15px;">
                        <label for="from">Date Start</label>
                        <input type="hidden" name="from" class="form-control datetime-picker flatpickr-input" id="from" data-default-date="">

                    </div>

                    <div class="form-group">
                        <label for="to">Date End</label>
                        <input type="hidden" name="to" class="form-control datetime-picker flatpickr-input" id="to" data-default-date="">
                    </div>

                    <button type="submit" class="btn btn-default pull-right" data-loading>
                        {{ trans('report::admin.filter') }}
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">
    $('#demo').daterangepicker({
        "showWeekNumbers": true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "startDate": "11/02/2019",
        "endDate": "11/08/2019"
    }, function(start, end, label) {
      console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    });

    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'My First dataset',
                backgroundColor: 'rgb(206, 163, 241)',
                borderColor: 'rgb(206, 163, 241)',
                data: [0, 10, 5, 2, 20, 30, 45]
            },{
                label: 'My Second dataset',
                backgroundColor: 'rgb(175, 145, 234)',
                borderColor: 'rgb(175, 145, 234)',
                data: [0, 5, 4, 1, 10, 20, 35]
            }]
        },

        // Configuration options go here
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    }
                }]
            },
            legend: {
                display: false
            },
        }
    });
    var ctx = document.getElementById("totalOrders");
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'My First dataset',
                backgroundColor: 'rgba(255,255,255)',
                borderColor: 'rgba(255,255,255)',
                data: [40, 10, 20, 12, 25, 30, 45]
            },{
                label: 'My Second dataset',
                backgroundColor: 'rgb(167, 243, 216)',
                borderColor: 'rgb(167, 243, 216)',
                data: [30, 5, 15, 20, 10, 20, 35]
            }]
      },
      options: {
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    }
                }]
            },
            legend: {
                display: false
            },
      }
    });

    var ctx = document.getElementById("tProducts");
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'My First dataset',
                backgroundColor: 'rgba(0, 0, 0, 0)',
                borderColor: 'rgb(255,255,255)',
                data: [0, 10, 5, 2, 20, 30, 45]
            },{
                label: 'My Second dataset',
                backgroundColor: 'rgba(0, 0, 0, 0)',
                borderColor: 'rgb(255,255,255)',
                data: [0, 5, 4, 1, 10, 20, 35]
            }]
        },

        // Configuration options go here
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    }
                }]
            },
            legend: {
                display: false
            },
        }
    });
    var ctx = document.getElementById("tCustomers");
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November', 'December'],
            datasets: [{
                label: 'My First dataset',
                backgroundColor: 'rgba(255,255,255)',
                borderColor: 'rgba(255,255,255)',
                data: [40, 30, 20, 12, 25, 30, 45, 28, 38, 44, 32, 24]
            }]
      },
      options: {
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    }
                }]
            },
            legend: {
                display: false
            },
      }
    });

</script>

@endpush