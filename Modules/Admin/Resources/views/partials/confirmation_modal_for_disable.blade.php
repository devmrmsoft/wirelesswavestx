<div class="modal fade" id="confirmation_modal_for_disable-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>

                <h3 class="modal-title">{{ trans('admin::admin.disable.confirmation') }}</h3>
            </div>
<input type="hidden" name="product_update_url" id="product_disable_update_url" value="{{ route('admin.products.products_disable') }}">
            <div class="modal-body">
                <div class="default-message">
                    {{ $message ?? trans('admin::admin.disable.confirmation_message') }}
                </div>
            </div>

            <div class="modal-footer">
                <form method="POST" id="product_disableconfirmation-form">
                    {{ csrf_field() }}
                    {{ method_field('post') }}

                    <button type="button" class="btn btn-default cancel" data-dismiss="modal">
                        {{ trans('admin::admin.buttons.cancel') }}
                    </button>

                    <button type="submit" class="btn btn-danger delete">
                        {{ trans('admin::admin.buttons.disable') }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
