<aside class="main-sidebar">
    <header class="main-header clearfix">
        <a class="logo sidebar-logo-a" href="{{ route('admin.dashboard.index') }}">
            <!-- <span class="logo-lg">Wireless -Admin</span> -->
            <img class="logo-lg" src="{{ asset('storage/media/fRshNnTpFwQync74YxPskBjSwRCdyLShlmCIEyfJ.png') }}">
        </a>

        <a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <i aria-hidden="true" class="fa fa-bars"></i>
        </a>
    </header>

    <section class="sidebar">
        {!! $sidebar !!}
    </section>
</aside>
