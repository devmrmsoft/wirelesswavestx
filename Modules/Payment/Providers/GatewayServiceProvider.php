<?php

namespace Modules\Payment\Providers;

use Modules\Payment\Gateways\COD;
use Modules\Payment\Facades\Gateway;
use Modules\Payment\Gateways\Stripe;
use Illuminate\Support\ServiceProvider;
use Modules\Payment\Gateways\Instamojo;
use Modules\Payment\Gateways\BankTransfer;
use Modules\Payment\Gateways\CheckPayment;
use Modules\Payment\Gateways\PayPalExpress;

use Modules\Payment\Gateways\WireTransfer;
use Modules\Payment\Gateways\CompanyCheque;
use Modules\Payment\Gateways\OpenShipment;
use Modules\Payment\Gateways\PersonalCheque;
use Modules\Payment\Gateways\CreditCard;

class GatewayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (! config('app.installed')) {
            return;
        }

        $this->registerPayPalExpress();
        $this->registerStripe();
        $this->registerInstamojo();
        $this->registerCashOnDelivery();
        $this->registerBankTransfer();
        $this->registerCheckPayment();
        // $this->registerWireTransfer();
        $this->registerCompanyCheque();
        $this->registerOpenShipment();
        $this->registerPersonalCheque();
        $this->registerCreditCard();
    }

    private function enabled($paymentMethod)
    {
        if (app('inBackend')) {
            return true;
        }

        return setting("{$paymentMethod}_enabled");
    }

    private function registerPayPalExpress()
    {
        if ($this->enabled('paypal_express')) {
            Gateway::register('paypal_express', new PayPalExpress);
        }
    }

    private function registerStripe()
    {
        if ($this->enabled('stripe')) {
            Gateway::register('stripe', new Stripe);
        }
    }

    private function registerInstamojo()
    {
        if ((setting('instamojo_enabled') && currency() === 'INR') || app('inBackend')) {
            Gateway::register('instamojo', new Instamojo);
        }
    }

    private function registerCashOnDelivery()
    {
        if ($this->enabled('cod')) {
            Gateway::register('cod', new COD);
        }
    }

    private function registerBankTransfer()
    {
        if ($this->enabled('bank_transfer')) {
            Gateway::register('bank_transfer', new BankTransfer);
        }
    }

    private function registerCheckPayment()
    {
        if ($this->enabled('check_payment')) {
            Gateway::register('check_payment', new CheckPayment);
        }
    }
    
    // private function registerWireTransfer()
    // {

    //         Gateway::register('wire_transfer', new WireTransfer);
            
    // }
    private function registerCompanyCheque()
    {
        
            Gateway::register('company_cheque', new CompanyCheque);
        
    }
    private function registerOpenShipment()
    {

            Gateway::register('open_shipment', new OpenShipment);

    }
    private function registerPersonalCheque()
    {

            Gateway::register('personal_cheque', new PersonalCheque);

    }
    private function registerCreditCard()
    {
            Gateway::register('credit_card', new CreditCard);
    }
}
