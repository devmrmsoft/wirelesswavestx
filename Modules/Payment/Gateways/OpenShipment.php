<?php

namespace Modules\Payment\Gateways;

use Modules\Payment\NullResponse;

class OpenShipment
{
    public $label;
    public $description;

    public function __construct()
    {
        $this->label = 'Credit Account';
        $this->description = 'Its a credit account.';
    }

    public function purchase()
    {
        return new NullResponse;
    }
}
