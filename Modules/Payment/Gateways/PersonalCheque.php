<?php

namespace Modules\Payment\Gateways;

use Modules\Payment\NullResponse;

class PersonalCheque
{
    public $label;
    public $description;

    public function __construct()
    {
        $this->label = 'COD Certified Check';
        $this->description = 'Please send a certified check to our store.';
    }

    public function purchase()
    {
        return new NullResponse;
    }
}
