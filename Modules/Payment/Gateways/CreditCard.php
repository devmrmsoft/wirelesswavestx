<?php

namespace Modules\Payment\Gateways;

use Modules\Payment\NullResponse;

class CreditCard
{
    public $label;
    public $description;

    public function __construct()
    {
        $this->label = 'Credit Card';
        $this->description = 'Please provide information as according given below';
    }

    public function purchase()
    {
        return new NullResponse;
    }
}
