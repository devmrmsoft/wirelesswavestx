<?php

namespace Modules\Payment\Gateways;

use Modules\Payment\NullResponse;

class CompanyCheque
{
    public $label;
    public $description;

    public function __construct()
    {
        $this->label = 'COD Company Check';
        $this->description = 'Please send a COD company check to our store.';
    }

    public function purchase()
    {
        return new NullResponse;
    }
}
