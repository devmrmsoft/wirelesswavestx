<?php

namespace Modules\Payment\Gateways;

use Modules\Payment\NullResponse;

class WireTransfer
{
    public $label;
    public $description;

    public function __construct()
    {
        $this->label = 'Wire Transfer';
        $this->description = 'Send us your payment through wire transfer.';
    }

    public function purchase()
    {
        return new NullResponse;
    }
}
 