@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('transaction::transactions.transactions'))

    <li class="active">{{ trans('transaction::transactions.transactions') }}</li>
@endcomponent

@section('content')
    <div class="box box-primary">
        <div class="box-body index-table" id="transactions-table"">
            @component('admin::components.table')
                @slot('thead')
                    <tr>
                        <th>{{ trans('transaction::transactions.table.order_id') }}</th>
                        <th>{{ trans('transaction::transactions.table.transaction_id') }}</th>
                        <th>{{ trans('transaction::transactions.table.payment_method') }}</th>
                        <th data-sort>{{ trans('admin::admin.table.created') }}</th>
                    </tr>
                @endslot
            @endcomponent
        </div>
    </div>
    <div class="rwo">
        <div class="col-md-12">
            <div class="pull-right">                
                <button onclick="demoFromHTML('products_purchase_report', 'products-purchase-order-report')">Export Table Data To Excel File</button>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
    <script>
        DataTable.setRoutes('#transactions-table .table', {
            index: '{{ "admin.transactions.index" }}',
        });

        new DataTable('#transactions-table .table', {
            columns: [
                { data: 'order_id' },
                { data: 'transaction_id' },
                { data: 'payment_method' },
                { data: 'created', name: 'created_at' },
            ],
        });
        function demoFromHTML() {
            var pdf = new jsPDF('p', 'pt', 'letter');

            pdf.cellInitialize();
            pdf.setFontSize(10);
            $.each( $('#transactions-table tr'), function (i, row){
                $.each( $(row).find("td, th"), function(j, cell){
                    var txt = $(cell).text().trim() || " ";
                    var width = (j==4) ? 40 : 70; //make 4th column smaller
                    pdf.cell(10, 50, width, 30, txt, i);
                });
            });

            pdf.save('transactions.pdf');
        }
    </script>
@endpush
