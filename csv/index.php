<?php
$host      = "localhost";
$user      = "wirelesswavestx_db";
$pass      = "Admin!@#321";
$db_name   = "wirelesswavestx_db";
$exp_table = "table_name"; // Table to export

$mysqli = new mysqli($host, $user, $pass, $db_name);
$mysqli->set_charset("utf8");

if (!$mysqli)
    die("ERROR: Could not connect. " . mysqli_connect_error());

// Create and open new csv file
$csv  = $exp_table . "-" . date('d-m-Y-his') . '.csv';
$file = fopen($csv, 'w');

// Get the table
if (!$mysqli_result = mysqli_query($mysqli, "SELECT
    pt.name,
    p.qty,
    p.price,
    p.retailer_prices,
    concat('https://wirelesswavestx.com/storage/',f.path) as image,
    mo.model,
    cat.cate,
    subcat.cate,
    col.condi as color,
    p.id,
    p.sku,
    pt.description,
    st.stg,
    cond.condi,
    ca.carriers
FROM
    products p
JOIN product_translations pt ON
    pt.product_id = p.id
LEFT JOIN entity_files ef ON
    ef.entity_type LIKE "%Product%" AND ef.entity_id = p.id AND ef.zone = 'base_image'
LEFT JOIN files f ON
    f.id = ef.file_id
LEFT JOIN (select pc.product_id, group_concat(ct.name separator ',') as carriers from product_categories pc join categories c on c.id = pc.category_id and c.parent_id = 4 join category_translations ct on ct.category_id = c.id group by pc.product_id) ca on ca.product_id = p.id
LEFT JOIN (select pa.product_id, group_concat(avt.value separator ',') as stg from product_attributes pa left join product_attribute_values pav on pav.product_attribute_id = pa.id join attribute_value_translations avt on avt.attribute_value_id = pav.attribute_value_id where pa.attribute_id = 2 group by pa.product_id) st on st.product_id = p.id
LEFT JOIN (select pa.product_id, group_concat(avt.value separator ',') as condi from product_attributes pa left join product_attribute_values pav on pav.product_attribute_id = pa.id join attribute_value_translations avt on avt.attribute_value_id = pav.attribute_value_id where pa.attribute_id = 3 group by pa.product_id) cond on cond.product_id = p.id
LEFT JOIN (select pa.product_id, group_concat(avt.value separator ',') as condi from product_attributes pa left join product_attribute_values pav on pav.product_attribute_id = pa.id join attribute_value_translations avt on avt.attribute_value_id = pav.attribute_value_id where pa.attribute_id = 4 group by pa.product_id) col on col.product_id = p.id
LEFT JOIN (select pc.product_id, ct.category_id as brand, c.id from product_categories pc join categories c on c.id = pc.category_id and c.parent_id = 392 join category_translations ct on ct.category_id = c.id group by pc.product_id) ba on ba.product_id = p.id
LEFT JOIN (select pc.product_id, group_concat(ct.name separator ',') as model, c.parent_id from product_categories pc join categories c on c.id = pc.category_id join category_translations ct on ct.category_id = c.id group by pc.product_id,c.parent_id) mo on mo.product_id = p.id and mo.parent_id = ba.id
LEFT JOIN (select pc.product_id, group_concat(ct.name separator ',') as cate from product_categories pc join categories c on c.id = pc.category_id and c.parent_id IS NULL join category_translations ct on ct.category_id = c.id group by pc.product_id) cat on cat.product_id = p.id
LEFT JOIN (select pc.product_id, group_concat(ct.name separator ',') as cate from product_categories pc join categories c on c.id = pc.category_id and c.parent_id IS NOT NULL join category_translations ct on ct.category_id = c.id group by pc.product_id) subcat on cat.product_id = p.id
GROUP BY
    p.id"))
    printf("Error: %s\n", $mysqli->error);

    // Get column names 
    while ($column = mysqli_fetch_field($mysqli_result)) {
        $column_names[] = $column->name;
    }
    
    // Write column names in csv file
    if (!fputcsv($file, $column_names))
        die('Can\'t write column names in csv file');
    
    // Get table rows
    while ($row = mysqli_fetch_row($mysqli_result)) {
        // Write table rows in csv files
        if (!fputcsv($file, $row))
            die('Can\'t write rows in csv file');
    }

fclose($file);

echo "<p><a href=\"$csv\">Download</a></p>\n"; 