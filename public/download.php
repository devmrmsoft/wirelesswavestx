<?php
$remoteHost = 'https://mrm-soft.com/wireless/public-upload-live-updated-files-only.zip';
$rootPath = realpath(__DIR__) . DIRECTORY_SEPARATOR . 'uploaded' . DIRECTORY_SEPARATOR;
$filePath = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, filter_input_array(INPUT_GET)['file']);
$fileName = (basename($filePath));
$remoteFile = $remoteHost . $filePath;
$file = file_get_contents($remoteFile);
$absolutePath = str_replace($fileName, '', $rootPath . $filePath);
if (!is_dir($absolutePath)) {
    mkdir($absolutePath, 0777, true);
}
if (file_put_contents($absolutePath . $fileName, $file, FILE_APPEND)) {
    die('ok');
}
die('nok');