<!DOCTYPE html>
<html lang="<?php echo e(locale()); ?>">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>
            <?php if (! empty(trim($__env->yieldContent('title')))): ?>
                <?php echo $__env->yieldContent('title'); ?> - <?php echo e(setting('store_name')); ?>

            <?php else: ?>
                <?php echo e(setting('store_name')); ?>

            <?php endif; ?>
        </title>

        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <?php echo $__env->yieldPushContent('meta'); ?>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Rubik:400,500" rel="stylesheet">

        <?php if(is_rtl()): ?>
            <link rel="stylesheet" href="<?php echo e(v(Theme::url('public/css/app.rtl.css'))); ?>">
        <?php else: ?>
            <link rel="stylesheet" href="<?php echo e(v(Theme::url('public/css/app.css'))); ?>">
            <link rel="stylesheet" href="<?php echo e(v(Theme::url('public/css/custom.css'))); ?>">
        <?php endif; ?>

        <link rel="shortcut icon" href="<?php echo e($favicon); ?>" type="image/x-icon">

        <?php echo $__env->yieldPushContent('styles'); ?>

        <?php echo setting('custom_header_assets'); ?>


        <script>
            window.FleetCart = {
                csrfToken: '<?php echo e(csrf_token()); ?>',
                stripePublishableKey: '<?php echo e(setting("stripe_publishable_key")); ?>',
                langs: {
                    'storefront::products.loading': '<?php echo e(trans("storefront::products.loading")); ?>',
                },
            };
        </script>

        <?php echo $__env->yieldPushContent('globals'); ?>

        <?php echo app('Tightenco\Ziggy\BladeRouteGenerator')->generate(); ?>

        <style type="text/css">
            .newsletter-suscription-form-input{
                font-size: 12px !important;
                color: #aeaeae !important;
                padding: 10px 0 !important;
                margin-bottom: 15px !important;
                background: transparent !important;
                border: none !important;
                border-bottom: 1px solid #aeaeae !important;
                height: 44px!important;
            }
            .newsletter-subscription-form-button-subscribe{
                text-transform: uppercase;
                -webkit-transition-duration: 0.2s;
                transition-duration: 0.2s;
                -webkit-transition-timing-function: ease;
                transition-timing-function: ease;
                border: 1px solid #fff;
                background: #fff;
                color: #fff;
                font-size: 12px;
                font-family: "PT Sans",Arial,Helvetica,sans-serif;
                -webkit-transition-duration: 0.2s;
                transition-duration: 0.2s;
                -webkit-transition-timing-function: ease;
                transition-timing-function: ease;
                padding: 10px 20px;
                color: #383838;
                width: auto;
                height: 44px;
                margin-bottom: 10px;
            }
            .newsletter-modal-content {
                 background-image:url('https://wirelesswavestx.com/storage/media/8YX1FzpGeximasWyVYJrP4hr76VI5PJUf4YQRNtE.jpeg');
                 background-size: cover !important;
                 
                    max-width: 100%;
             }
                    
             
            .newsletter-modal-dialog{
                max-width: 80% !important; 
                max-height: 90% !important;
                width: 100% !important;
                display: flex;
                align-items: center;
                height: 100%;
            }
            .newsletter-modal-header{
                padding: 10px !important;
                border: none!important;
            }
            .newsletter-modal-footer{
                padding: 10px !important;
                border: none!important;
            }
            .newsletter-close {
                padding: 1rem 1rem !important;
                margin: -1rem -1rem -1rem auto !important;
                display: block; 
                right: -1px !important;
                line-height: 25px !important;
                text-align: center !important;
                color: #333333 !important;
                font-size: 12px !important;
                border: none !important;
                cursor: pointer !important;
                background-color: #ffffff !important;
            }
            .newsletter-modal-content {
                    position: relative;
                    display: flex;
                    -ms-flex-direction: column;
                    flex-direction: column;
                    width: 100%;
                    pointer-events: auto;
                    background-color: #fff;
                    background-clip: padding-box;
                    border: 1px solid rgba(0, 0, 0, .2);
                    border-radius: .3rem;
                    outline: 0;
                }
        </style>
    </head>

    <body class="<?php echo e($theme); ?> <?php echo e(storefront_layout()); ?> <?php echo e(is_rtl() ? 'rtl' : 'ltr'); ?>">
        <!--[if lt IE 8]>
            <p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <?php if (! (json_decode(Cookie::get('newsletter_removed')))): ?>
            <?php if (! (\Request::route()->getName() == 'ask-register')): ?>
           
            <?php endif; ?>
        <?php endif; ?>
        <div class="main">
            <div class="wrapper">
                <?php echo $__env->make('public.partials.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('public.partials.top_nav', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('public.partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('public.partials.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <div class="content-wrapper clearfix <?php echo e(request()->routeIs('cart.index') ? 'cart-page' : ''); ?>">
                    <?php if(isset($slider)){ ?>
                    <div class="container-fluid">
                            <?php if (! (is_null($slider))): ?>
                                <?php if(storefront_layout() === 'default'): ?>
                                    <div class="col-lg-9 col-lg-offset-3 col-md-9 col-md-offset-3">
                                        <div class="row">
                                            <?php echo $__env->make('public.home.sections.slider', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                <?php elseif(storefront_layout() === 'slider_with_banners'): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php echo $__env->make('public.home.sections.slider', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                        </div>
                                        
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                    </div>
                    <?php } ?>
                    <div class="container">
                        <?php echo $__env->make('public.partials.breadcrumb', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                        <?php if (! (request()->routeIs('home') || request()->routeIs('login') || request()->routeIs('register') || request()->routeIs('reset') || request()->routeIs('reset.complete'))): ?>
                            <?php echo $__env->make('public.partials.notification', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <?php endif; ?>

                        <?php echo $__env->yieldContent('content'); ?>
                    </div>
                </div>

                <?php if(setting('storefront_brand_section_enabled') && $brands->isNotEmpty() && request()->routeIs('home')): ?>
                    <section class="brands-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="brands">
                                    <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-md-3">
                                            <div class="brand-image">
                                                <img src="<?php echo e($brand->path); ?>">
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php endif; ?>

                <?php echo $__env->make('public.partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <a class="scroll-top" href="#">
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </a>
            </div>

            <?php if (! (json_decode(Cookie::get('cookie_bar_accepted')))): ?>
                <div class="cookie-bar-wrapper">
                    <div class="container">
                        <div class="cookie clearfix">
                            <div class="cookie-text">
                                <span><?php echo e(trans('storefront::layout.cookie_bar_text')); ?></span>
                            </div>

                            <div class="cookie-button">
                                <button type="submit" class="btn btn-primary btn-cookie">
                                    <?php echo e(trans('storefront::layout.got_it')); ?>

                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <?php echo $__env->make('public.partials.quick_view_modal', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
        <script src="<?php echo e(v(Theme::url('public/js/app.js'))); ?>"></script>

        <?php echo $__env->yieldPushContent('scripts'); ?>

        <?php echo setting('custom_footer_assets'); ?>

        <script type="text/javascript">
            
            function changeImagePath(diss){
                        var newSrc  =   $(diss).attr('src');
                        $('.option-color-label').removeClass('orange-text');
                        $(diss).siblings().addClass('orange-text');
                        $("#base_image_path").attr('src',newSrc);
                        $(".slick-current > img").attr('src',newSrc);
                        var getSku  =   $(diss).attr('data-sku');
                        $('.sku > span').text(getSku);
                        
                        var label   =   $(diss).siblings('label').trigger('click');
                }
                
    function loadColorImages(el, option_id, value_id){
        
        if(typeof(value_id) != "undefined" && value_id !== null) {
            var value_id = value_id;
        }else{
            var value_id = el.value;
        }
        
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        
        $.ajax({
            url: '<?php echo e(route("get-product-color-images")); ?>',
            type: 'GET',
            data: {_token: CSRF_TOKEN, option_id: option_id, value_id: value_id},
            dataType: 'JSON',
            success: function(data){
            
            
            
            //console.log(data.html);
            $('#load_color_images').html(data.html);
            $('.sku > span').text(data.sku);
            $('.left-in-stock > .green').text(data.qty);
            console.log(data.sku);
            
             $('.base-image').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
              asNavFor: '.additional-image'
            });
            $('.additional-image').slick({
              slidesToShow: 3,
              slidesToScroll: 1,
              asNavFor: '.base-image',
              dots: true,
              centerMode: true,
              focusOnSelect: true
            });
            }
        });
    }
    
       $(document).ready(function(){
            $('#exampleModal').modal('show');
        }); 
        </script>
    </body>
</html>
