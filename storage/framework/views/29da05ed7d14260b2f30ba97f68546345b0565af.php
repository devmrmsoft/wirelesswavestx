<?php
    use Modules\Category\Entities\Category;
    use FleetCart\productPhoneModel;
 if(isset($_GET['category']))
    {
        $catName    =   DB::table('categories')->
                            where('slug', $_GET['category'])->first();
        $catName = DB::table('categories')
                        ->join('category_translations', 'category_translations.category_id', '=', 'categories.id')
                        ->where('slug',  $_GET['category'])
                        ->first();
                        
    $filterCategories =   DB::table('categories')->where('parent_id', $catName->id)
                            ->join('category_translations', 'category_translations.category_id', '=', 'categories.id')
                            ->get();
    $brandsCats     =   DB::table('categories')->where('parent_id', '392')
                            ->join('category_translations', 'category_translations.category_id', '=', 'categories.id')
                            ->get(); 
    $carrierCats    =   DB::table('categories')->where('parent_id', '4')
                            ->join('category_translations', 'category_translations.category_id', '=', 'categories.id')
                            ->get(); 
    $modelCats    =   DB::table('categories')->where('parent_id', '3')
                            ->join('category_translations', 'category_translations.category_id', '=', 'categories.id')
                        ->get();

    $filter_categories  =   Category::where('slug',  $_GET['category'])->first();
    $inner_filter_categories    =   Category::where('parent_id', $filter_categories->id)->get();
    $get_accBrand       =   Category::where('parent_id', '427')->get();
    $getParent = new stdClass();
    $getParent->id  =   $filter_categories->parent_id;
        if($filter_categories->parent_id != null){
            $getParent  =   Category::find($filter_categories->parent_id);
            if($getParent->parent_id != null){
                $getParent  =   Category::find($getParent->parent_id);
                if($getParent->parent_id != null){
                    $getParent  =   Category::find($getParent->parent_id);
                    if($getParent->parent_id != null){
                        $getParent  =   Category::find($getParent->parent_id);
                        if($getParent->parent_id != null){
                            $getParent  =   Category::find($getParent->parent_id);
                            if($getParent->parent_id != null){
                                $getParent  =   Category::find($getParent->parent_id);
                                if($getParent->parent_id != null){
                                    $getParent  =   Category::find($getParent->parent_id);
                                }
                            }
                        }
                    }
                }
            }
        }

        if(isset($getParent->id) || isset($filter_categories->id) ){
            if($filter_categories->id == '1' || $filter_categories->id == '3' || $getParent->id == '1'){
                    $brandsCats     =   DB::table('categories')->where('parent_id', '3')
                        ->join('category_translations', 'category_translations.category_id', '=', 'categories.id')
                        ->get();
            }
        }
    }


//    echo '<pre>'; print_r( $categories ); echo '</pre>'; 
?>
<?php $__env->startPush('styles'); ?>
<style type="text/css">
    .clickable{
        cursor: pointer;   
    }

    .panel-heading span {
        margin-top: -25px;
        font-size: 15px;
    }
</style>
<?php $__env->stopPush(); ?>

<?php if($categories->isNotEmpty()): ?>
    <?php if(isset($_GET['category'])){ if($_GET['category'] == 'accessories-main' || $_GET['category'] == 'universal-accessories' ){ ?>
     
    <?php } else{
    ?>
    
    <?php
    
    } } ?>
    <?php if(isset($_GET['category'])): ?>
    <div class="filter-section clearfix ">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Brand</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-down fa fa-angle-down"></i></span>
            </div>
            <div class="panel-body p-0"  style="display:none;">
                <ul class="filter-category list-inline" >
                    <?php $__currentLoopData = $brandsCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li class="<?php echo e(request('category') === $category->slug ? 'active' : ''); ?> parent">
                            <div class="form-group">
                                <div class="checkbox">
                                    <input type="checkbox"
                                        name="multiCat[]"
                                        value="<?php echo e(mb_strtolower($category->id)); ?>"
                                        onchange='takeBrandModels(this);'
                                        id="multicat-<?php echo e($category->id); ?>"
                                        class="brand-checkbox"
                                        <?php echo e(is_filtering($category->slug) ? 'checked' : ''); ?>

                                    >        
                                    <label for="multicat-<?php echo e($category->id); ?>"><?php echo e($category->name); ?></label>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        </div>        
    </div> 
    <div class="filter-section clearfix">       
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Model</h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-down fa fa-angle-down"></i></span>
                </div>
                <div class="panel-body p-0"  style="display:none;">
                    <ul class="filter-category list-inline" id="phonesModelUl">
                    </ul>                        
                </div>
            </div>
    </div>
    <?php if(isset($getParent->id) || isset($filter_categories->id) ){
        if($filter_categories->id != '1'){
        if($getParent->id != '1'){ 
            ?>
    <div class="filter-section clearfix">
       
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php if(isset($getParent->id)): ?> <?php if($getParent->id != '1'): ?> Accessories Brand  <?php endif; ?> <?php else: ?> Accessories Brand <?php endif; ?></h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-down fa fa-angle-down"></i></span>
                </div>
                <div class="panel-body p-0"  style="display:none;">
                    <ul class="filter-category list-inline" id="">
                        <?php $__currentLoopData = $get_accBrand; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="<?php echo e(request('category') === $category->slug ? 'active' : ''); ?> parent">
                                <!-- <i class="fa fa-angle-right pull-left" aria-hidden="true"></i> -->
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox"
                                            name="multiCatss[]"
                                            class="phone-checkbox"
                                            value="<?php echo e(mb_strtolower($category->id)); ?>"
                                            onchange='handleChange(this);'
                                            id="multicat-<?php echo e($category->id); ?>"
                                            <?php echo e(is_filtering($category->slug) ? 'checked' : ''); ?>

                                        >
            
                                        <label for="multicat-<?php echo e($category->id); ?>"><?php echo e($category->name); ?></label>
                                    </div>
                                </div>
                                <!-- <ul>
                                </ul> -->
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                        
                </div>
            </div>
        
    </div>
    <div class="filter-section clearfix">       
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Accessory Type</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-down fa fa-angle-down"></i></span>
            </div>
            <div class="panel-body p-0"  style="display:none;">
                <ul class="filter-category list-inline" id="accessoryTypeUl">
                </ul>                        
            </div>
        </div>
    </div>
<?php }} } ?>
    <?php endif; ?>
    
    <?php if(isset($_GET['category'])){ ?>
    <div class="filter-section clearfix">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Carrier</h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-down fa fa-angle-down"></i></span>
                </div>
                <div class="panel-body p-0" style="display:none;">
                    <ul class="filter-category list-inline">
                        <?php $__currentLoopData = $carrierCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="<?php echo e(request('category') === $category->slug ? 'active' : ''); ?> parent">
                                <!-- <i class="fa fa-angle-right pull-left" aria-hidden="true"></i> -->
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox"
                                            name="multiCat[]"
                                            value="<?php echo e(mb_strtolower($category->id)); ?>"
                                            id="multicat-<?php echo e($category->id); ?>"
                                            <?php echo e(is_filtering($category->slug) ? 'checked' : ''); ?>

                                        >
            
                                        <label for="multicat-<?php echo e($category->id); ?>"><?php echo e($category->name); ?></label>
                                    </div>
                                </div>
                                <!-- <ul>
                                </ul> -->
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>
        
    </div>
    <?php } ?>
<?php endif; ?>

<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
    
    $(document).on('click', '.panel-heading', function(e){
    var $this = $(this);
    console.log($this);
    if($this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
        
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
        
    }
});
    function handleChange(checkbox){
        var val = [];
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $('.phone-checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });
        console.log(val);
        $.ajax({
            url: '<?php echo e(route("get-accessory-brand-models")); ?>',
            type: 'POST',
            data: {_token: CSRF_TOKEN, val},
            dataType: 'JSON',
            success: function(data){
            //console.log(data.html);
            $('#accessoryTypeUl').html(data.html);
            
            }
        });
    }
    
    function takeBrandModels(checkbox){
        var val = [];
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $('.brand-checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });
        console.log(val);
        $.ajax({
            url: '<?php echo e(route("get-phone-models")); ?>',
            type: 'POST',
            data: {_token: CSRF_TOKEN, val},
            dataType: 'JSON',
            success: function(data){
            //console.log(data.html);
            $('#phonesModelUl').html(data.html);
            
            }
        });
    }

      
      function addActiveClass(checkbox){
          
           
          
          if($(checkbox). prop("checked") == true){
              $(checkbox).addClass('checked');
              $(checkbox).attr('checked', true);
          }else{
              $(checkbox).removeClass('checked');
              $(checkbox).attr('checked', false);
              
          }
          var checkboxID    =   $(checkbox).attr('id');
          
          var atLeastOneIsChecked = $(checkboxID+':checkbox:checked').length > 0;
          console.log(atLeastOneIsChecked);
            /*creditLine.checked = creditLine.checked;*/
    
          //console.log(checkbox);
      }
</script>
<?php $__env->stopPush(); ?>