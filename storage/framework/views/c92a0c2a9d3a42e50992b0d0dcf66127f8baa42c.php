<section class="tab-products clearfix">
    <div class="section-header clearfix">
        <h3 class="pull-left"><?php echo e(setting('storefront_product_tabs_section_title')); ?></h3>

        <ul class="nav nav-tabs pull-right">
            <?php if($tabProducts['tab_1']->isNotEmpty()): ?>
                <li class="active">
                    <a data-toggle="tab" href="#tab-1"><?php echo e(setting('storefront_product_tabs_section_tab_1_title')); ?></a>
                </li>
            <?php endif; ?>

            <?php if($tabProducts['tab_2']->isNotEmpty()): ?>
                <li>
                    <a data-toggle="tab" href="#tab-2"><?php echo e(setting('storefront_product_tabs_section_tab_2_title')); ?></a>
                </li>
            <?php endif; ?>

            <?php if($tabProducts['tab_3']->isNotEmpty()): ?>
                <li>
                    <a data-toggle="tab" href="#tab-3"><?php echo e(setting('storefront_product_tabs_section_tab_3_title')); ?></a>
                </li>
            <?php endif; ?>

            <?php if($tabProducts['tab_4']->isNotEmpty()): ?>
                <li>
                    <a data-toggle="tab" href="#tab-4"><?php echo e(setting('storefront_product_tabs_section_tab_4_title')); ?></a>
                </li>
            <?php endif; ?>
        </ul>
    </div>

    <div class="row">
        <div class="tab-content">
            <?php if($tabProducts['tab_1']->isNotEmpty()): ?>
                <div id="tab-1" class="tab-pane fade in active">
                    <div class="tab-product-slider separator clearfix">
                        <?php $__currentLoopData = $tabProducts['tab_1']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-3">
                                <?php echo $__env->make('public.products.partials.product_card', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if($tabProducts['tab_2']->isNotEmpty()): ?>
                <div id="tab-2" class="tab-pane fade in">
                    <div class="tab-product-slider separator clearfix">
                        <?php $__currentLoopData = $tabProducts['tab_2']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-3">
                                <?php echo $__env->make('public.products.partials.product_card', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if($tabProducts['tab_3']->isNotEmpty()): ?>
                <div id="tab-3" class="tab-pane fade in">
                    <div class="tab-product-slider separator clearfix">
                        <?php $__currentLoopData = $tabProducts['tab_3']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-3">
                                <?php echo $__env->make('public.products.partials.product_card', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if($tabProducts['tab_4']->isNotEmpty()): ?>
                <div id="tab-4" class="tab-pane fade in">
                    <div class="tab-product-slider separator clearfix">
                        <?php $__currentLoopData = $tabProducts['tab_4']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-3">
                                <?php echo $__env->make('public.products.partials.product_card', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
