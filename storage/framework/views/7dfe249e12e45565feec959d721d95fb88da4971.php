

    <style type="text/css">
                
        .__footer00 {
            background-color: #323f48;
            padding-top: 35px;
            padding-bottom: 35px;
            display: block;
            justify-content: center;
            align-items: center;
            border-bottom: 2px solid #3e4b54;
        }
        .__footer01 {
            background-color: #323f48;
            padding-top: 15px;
            padding-bottom: 15px;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        ._footcol1 a img {
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .__footer00-col {
            display: block;
            justify-content: center;
            align-items: center;
        }
        ._footcol1 p {
            color: #aeb5bd;
            font-weight: 600;
            margin: 0;
            line-height: 24px;
            font-size: 14px;
        }
        ._navfooter h1 {
            margin: 0;
            padding: 0;
            font-size: 18px;
            padding-bottom: 10px;
            color: #fff;
            margin-top: 15px;
        }
        ._navfooter .nav li a {
            padding: 4px 0;
            color: #aeb5bd;
            font-weight: 600;
            font-size: 12px;
        }
        ._navfooter .nav li a:hover{
            background-color: transparent;
        }
        .__footer01-col {
            display: flex;
            justify-content: center;
            align-items: center;
        }
        ._blow-footer p {
            margin: 0;
            padding: 0;
            font-size: 13px;
            color: #aeb5bd;
        }
        ._blow-footer p span {
            color: #337ab7;
        }
        ._blow-footer a i {
            color: #aeb5bd;
            font-size: 18px;
            padding-right: 15px;
        }
        ._blow-footer.blow-footer-anchor{
            padding-top: 4px;
        }
        .newsletter-form{
            padding-top:10px;
        }
    </style>

    <footer>
        <div class="__footer00">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 __footer00-col">
                        <div class="_footcol1">
                            <a href="<?php echo e(route('home')); ?>" class="footer-logo">
                                <?php if(is_null($footerLogo)): ?>
                                    <h2><?php echo e(setting('store_name')); ?></h2>
                                <?php else: ?>
                                    <img src="<?php echo e($footerLogo); ?>" class="img-responsive" width="205" height="93" alt="Footer Logo">
                                <?php endif; ?>
                            </a>
                            <p>Welcome to Wireless Waves; your one-stop Wireless Solution. Established in 2015, we take pride in helping our customer base of small wireless retailers with access to wide variety of products.</p>
                        </div>
                    </div>
                    
                    <div class="col-md-3 __footer00-col">
                        <div class="_navfooter">
                            <h1>Information</h1>
                            <ul class="nav">
                                <li><a href="#">New Products</a></li>
                                <li><a href="#">Best Sellers</a></li>
                                <li><a href="#">Our Stores</a></li>
                                <li><form method="GET" action="#" class="newsletter-form" style="display:flex;">
        <input type="email" name="EMAIL" id="email" placeholder=" Subscribe to our newsletter" class="news-field" style="width:230px;">
        <button type="submit" class="button"><i class="fa fa-send pull-left" id="refresh-captcha" aria-hidden="true"></i></button>
      </form></li>
                            </ul>
                        </div>
                    </div>
                
                    

                    <?php if($footerMenu->isNotEmpty()): ?>
                        <div class="col-md-3 __footer00-col">
                            <div class="_navfooter">
                                <h1><?php echo e(setting('storefront_footer_menu_title')); ?></h1>
                                <ul class="nav">
                                    <?php $__currentLoopData = $footerMenu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menuItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e($menuItem->url()); ?>"><?php echo e($menuItem->name); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-3 __footer00-col">
                        <div class="_navfooter">
                            <h1>My Account</h1>
                            <ul class="nav">
                                <li><a href="#">Dashboard</a></li>
                                <li><a href="#">My Orders</a></li>
                                <li><a href="#">My Reviews</a></li>
                                <li><a href="#">My Profile</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <footer class="footer-below">
        <div class="__footer01">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 __footer01-col">
                        <div class="_blow-footer">
                            <p>Copyright &copy; <a href="<?php echo e(route('home')); ?>"><span>Wireless Waves</span></a> 2020. All rights reserved.</p>
                        </div>
                    </div>
                    <div class="col-md-3 __footer01-col">
                        <div class="_blow-footer blow-footer-anchor">
                            <a href="#" class=""><i class="fab fa-cc-paypal"></i></a>
                            <a href="#" class=""><i class="fab fa-cc-visa"></i></a>
                            <a href="#" class=""><i class="fab fa-cc-mastercard"></i></a>
                            <a href="#" class=""><i class="fab fa-cc-amex"></i></a>
                        </div>
                    </div>
                    <div class="col-md-3 __footer01-col">
                        <div class="_blow-footer">
                            <p>Designed By MRM-Soft</p>
                        </div>
                    </div>
                    <div class="col-md-2 __footer01-col">
                        <div class="_blow-footer">
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://kit.fontawesome.com/906be8c28d.js" crossorigin="anonymous"></script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5f6e39d2f0e7167d0013d262/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->