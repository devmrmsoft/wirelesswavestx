<div class="col-lg-5 col-md-6 col-sm-5 col-xs-7" id="load_color_images">
    <div class="product-image">
        <div class="base-image">
            <?php if(! $product->base_image->exists): ?>
                <div class="image-placeholder">
                    <i class="fa fa-picture-o"></i>
                </div>
            <?php else: ?>
                <a class="base-image-inner" href="<?php echo e($product->base_image->path); ?>">
                    <img id="base_image_path" src="<?php echo e($product->base_image->path); ?>">
                    <span><i class="fa fa-search-plus" aria-hidden="true"></i></span>
                </a>
            <?php endif; ?>

            <?php $__currentLoopData = $product->additional_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $additionalImage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(! $additionalImage->exists): ?>
                    <div class="image-placeholder">
                        <i class="fa fa-picture-o"></i>
                    </div>
                <?php else: ?>
                    <a class="base-image-inner" href="<?php echo e($additionalImage->path); ?>">
                        <img src="<?php echo e($additionalImage->path); ?>">
                        <span><i class="fa fa-search-plus" aria-hidden="true"></i></span>
                    </a>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>

        <div class="additional-image">
            <?php if(! $product->base_image->exists): ?>
                <div class="image-placeholder">
                    <i class="fa fa-picture-o"></i>
                </div>
            <?php else: ?>
                <div class="thumb-image">
                    <img src="<?php echo e($product->base_image->path); ?>">
                </div>
            <?php endif; ?>

            <?php $__currentLoopData = $product->additional_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $additionalImage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(! $additionalImage->exists): ?>
                    <div class="image-placeholder">
                        <i class="fa fa-picture-o"></i>
                    </div>
                <?php else: ?>
                    <div class="thumb-image">
                        <img src="<?php echo e($additionalImage->path); ?>">
                    </div>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>

<?php $__env->startPush('scripts'); ?>

<script type="text/javascript">    
    function loadColorImages(el, option_id, value_id){
        
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        
        $.ajax({
            url: '<?php echo e(route("get-product-color-images")); ?>',
            type: 'GET',
            data: {_token: CSRF_TOKEN, option_id: option_id, value_id: value_id},
            dataType: 'JSON',
            success: function(data){
            
            
            
            //console.log(data.html);
            $('#load_color_images').html(data.html);
            
             $('.base-image').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
              asNavFor: '.additional-image'
            });
            $('.additional-image').slick({
              slidesToShow: 3,
              slidesToScroll: 1,
              asNavFor: '.base-image',
              dots: true,
              centerMode: true,
              focusOnSelect: true
            });
            }
        });
    }
</script>

<?php $__env->stopPush(); ?>