<style>
@media  screen and (max-width:1024px)
{
    .home-slider
    {
        width:100%!important;
        height:260px;
        margin-left:0!important;
    }
    .home-slider .slider-image
    {
       width:100%!important;
       height:260px;
    }
}
@media  screen and (max-width:767px)
{
    .home-slider
    {
        height:100px;
    }
    .home-slider .slider-image
    {
        height:90px!important;
        
    }   
    /*.home-slider .slick-active .slider-image*/
    /*{*/
    /*    height:100px!important;*/
    /*}*/
}
</style>
<div class="home-slider"
    data-autoplay="<?php echo e($slider->autoplay); ?>"
    data-autoplay-speed="<?php echo e($slider->autoplay_speed); ?>"
    data-arrows="<?php echo e($slider->arrows); ?>"
>
    <?php $__currentLoopData = $slider->slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="slide">
            <div class="slider-image" style="background-image: url(<?php echo e($slide->file->path); ?>);"></div>

            <div class="display-table">
                <div class="display-table-cell">
                    <div class="col-md-9 col-md-offset-1 col-sm-10 col-sm-offset-1">
                        <div class="slider-content clearfix">
                            <div class="display-table">
                                <div class="display-table-cell">
                                    <?php if (! (is_null($slide->caption_1))): ?>
                                        <div class="caption caption-md"
                                            data-delay="<?php echo e($slide->options['caption_1']['delay']); ?>ms"
                                            data-effect="<?php echo e($slide->options['caption_1']['effect']); ?>"
                                        >
                                            <?php echo e($slide->caption_1); ?>

                                        </div>
                                    <?php endif; ?>

                                    <?php if (! (is_null($slide->caption_2))): ?>
                                        <div class="caption caption-lg"
                                            data-delay="<?php echo e($slide->options['caption_2']['delay']); ?>ms"
                                            data-effect="<?php echo e($slide->options['caption_2']['effect']); ?>"
                                        >
                                            <?php echo e($slide->caption_2); ?>

                                        </div>
                                    <?php endif; ?>

                                    <?php if (! (is_null($slide->caption_3))): ?>
                                        <div class="caption caption-sm"
                                            data-delay="<?php echo e($slide->options['caption_3']['delay']); ?>ms"
                                            data-effect="<?php echo e($slide->options['caption_3']['effect']); ?>"
                                        >
                                            <?php echo e($slide->caption_3); ?>

                                        </div>
                                    <?php endif; ?>

                                    <?php if (! (is_null($slide->call_to_action_text))): ?>
                                        <a href="<?php echo e($slide->call_to_action_url); ?>"
                                            class="btn-slider btn btn-primary animate"
                                            target="<?php echo e($slide->options['call_to_action']['target'] ?? '_self'); ?>"
                                            data-delay="<?php echo e($slide->options['call_to_action']['delay']); ?>ms"
                                            data-effect="<?php echo e($slide->options['call_to_action']['effect']); ?>"
                                        >
                                            <?php echo e($slide->call_to_action_text); ?>

                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

