<span class="product-rating">
    <i class="<?php echo e(rating_star_class($rating, 1)); ?>"></i>
    <i class="<?php echo e(rating_star_class($rating, 2)); ?>"></i>
    <i class="<?php echo e(rating_star_class($rating, 3)); ?>"></i>
    <i class="<?php echo e(rating_star_class($rating, 4)); ?>"></i>
    <i class="<?php echo e(rating_star_class($rating, 5)); ?>"></i>
</span>
