<div class="product-variants-select form-group row <?php echo e($errors->has("options.{$option->id}") ? 'has-error' : ''); ?>">
    <label class="col-sm-4" for="option-<?php echo e($option->id); ?>">
        <?php echo e(option_name($option)); ?>

    </label>
            
            <?php
                $getOptionColorImgs =   DB::table('option_images')
                    ->where('product_id',$product->id )
                    ->where('option_id',$option->id )->first();
            ?>

    <div class="col-sm-8">
        <select name="options[<?php echo e($option->id); ?>]<?php echo e($option->type === 'multiple_select' ? '[]' : ''); ?>"
                class="custom-select-black <?php echo e($option->type === 'multiple_select' ? 'selectize' : ''); ?>"
                id="option-<?php echo e($option->id); ?>"
                <?php echo e($option->type === 'multiple_select' ? 'multiple' : ''); ?>

                 <?php if($getOptionColorImgs != NULL){ ?>  onchange="loadColorImages(this, <?php echo e($option->id); ?>);changeImagePath(this);" <?php } ?> 
            >
            <?php if($option->type === 'dropdown' ): ?>
                <option value=""><?php echo e(trans('storefront::product.options.choose_an_option')); ?></option>
            <?php endif; ?>

            <?php $__currentLoopData = $option->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($value->id); ?>" <?php echo e(old("options.{$option->id}") == $value->id ? 'selected' : ''); ?>>
                    <?php echo e(option_value($product, $value, FOR_SELECT_OPTION)); ?>

                </option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>

        <?php echo $errors->first("options.{$option->id}", '<span class="help-block">:message</span>'); ?>

    </div>
</div>
