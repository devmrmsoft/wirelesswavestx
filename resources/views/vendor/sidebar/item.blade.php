<?php
    $check = DB::table('users')
            ->join('user_roles', 'users.id', '=', 'user_roles.user_id')
            ->select('users.id','users.first_name','user_roles.role_id')
            ->where('user_roles.user_id',Auth::user()->id )
            ->first();
?>

@if(Auth::user()->ret_list != 4)
    <li class="">
        <a href="{{ route('admin.stores.index') }}" class="">
            <i class="fa fa-home"></i>
            <span>Stores</span>
        </a>
    </li>
@endif

@if($item->getName() == 'Transactions')
<li class="">
    <a href="{{ route('admin.reports.index') }}" class="">
        <i class="fa fa-bar-chart"></i>
        <span>Reports</span>
    </a>
</li>
<li class="">
    <a href="{{ route('admin.orders.new_index') }}" class="">
        <i class="fa fa-angle-double-right"></i>
        <span>New Invoice</span>
    </a>
</li>
@endif

@if($item->getName() == 'Storefront')
<li class="">
    <a href="{{ route('admin.sliders.popup_index') }}" class="">
        <i class="fa fa-send"></i>
        <span>Pop Up</span>
    </a>
</li>
@endif

{{--
@if($item->getName() == 'Media')
<li class="">
    <a href="{{ route('admin.pages.post_index') }}" class="">
        <i class="fa fa-send"></i>
        <span>Posts</span>
    </a>
</li>
@endif --}}

@if($item->getName() == 'Users' && $item->getIcon() == 'fa fa-users')

<li class="{{ $item->getItemClass() ? $item->getItemClass() : null }}
    {{ $active ? 'active' : null }}
    {{ $item->hasItems() ? 'treeview' : null }}
">
    <a href="{{ $item->getUrl() }}" class="{{ count($appends) > 0 ? 'hasAppend' : null }}"
        @if ($item->getNewTab())
            target="_blank"
        @endif
    >
        <i class="{{ $item->getIcon() }}"></i>
        <span>{{ $item->getName() }}</span>

        @foreach ($badges as $badge)
            {!! $badge !!}
        @endforeach

        @if ($item->hasItems())
            <span class="pull-right-container">
                <i class="{{ $item->getToggleIcon() }} pull-right"></i>
            </span>
        @endif
    </a>

    @foreach ($appends as $append)
        {!! $append !!}
    @endforeach

    @if($check->role_id != 5)
        @if (count($items) > 0)
            <ul class="treeview-menu">
                @foreach ($items as $item)
                    {!! $item !!}
                @endforeach
            </ul>
        @endif
    @endif
    
</li>
<?php 

    $link = $_SERVER['PHP_SELF'];
    $link_array = explode('/',$link);
    $lastPart = end($link_array);

     $url   =   Request::url();
     $keys =    parse_url($url); // parse the url
     $path =    explode("/", $keys['path']); // splitting the path
     $lastPart = end($path);
?>
@if (request()->routeIs('admin.users.list_index') || request()->routeIs('admin.users.ret_requests') || request()->routeIs('admin.retailers.retailer_edit') || request()->routeIs('admin.users.list_create') || request()->routeIs('admin.users.retailer_categories'))

<li class="active treeview selected">

@else

<li class="treeview {{ $lastPart }}">

@endif

<?php //if($lastPart == 'retailers' || $lastPart == 'list-create' || $lastPart == 'retailers-requests'){
        ?>
<!-- <li class="active treeview selected"> -->
        <?php
//    }else{
        ?>
<!-- <li class="treeview"> -->

        <?php
    // }
?>

    <a href="{{ route('admin.users.list_index') }}" class="">
        <i class="fa fa-users"></i>
        <span>Retailers</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
@if (request()->routeIs('admin.users.list_index') || request()->routeIs('admin.retailers.retailer_edit'))
        <li class="active">
@else
        <li class="">
@endif
        
            <a href="{{ route('admin.users.list_index') }}" class="">
                <i class="fa fa-angle-double-right"></i>
                <span>Retailers</span>
            </a>
        </li>

        @if (request()->routeIs('admin.users.retailer_categories'))
        <li class="active">
        @else
        <li class="">
        @endif
            <a href="{{ route('admin.users.retailer_categories') }}" class="">
                <i class="fa fa-angle-double-right"></i>
                <span>Retailers Categories</span>
            </a>
        </li>

        @if($lastPart == 'retailers-requests')
        <li class="active">
        @else
        <li class="">
        @endif
            <a href="{{ route('admin.users.ret_requests') }}" class="">
                <i class="fa fa-angle-double-right"></i>
                <span>Retailers Requests</span>
            </a>
        </li>


        @if($lastPart == 'list-create')
        <li class="active">
        @else
        <li class="">
        @endif
            <a href="{{ route('admin.users.list_create') }}" class="">
                <i class="fa fa-angle-double-right"></i>
                <span>Create Retailers List</span>
            </a>
        </li>
    </ul>
</li>

@else
<li class="{{ $item->getItemClass() ? $item->getItemClass() : null }}
    {{ $active ? 'active' : null }}
    {{ $item->hasItems() ? 'treeview' : null }}
">
    <a href="{{ $item->getUrl() }}" class="{{ count($appends) > 0 ? 'hasAppend' : null }}"
        @if ($item->getNewTab())
            target="_blank"
        @endif
    >
        <i class="{{ $item->getIcon() }}"></i>
        <span>{{ $item->getName() }}</span>

        @foreach ($badges as $badge)
            {!! $badge !!}
        @endforeach

        @if ($item->hasItems())
            <span class="pull-right-container">
                <i class="{{ $item->getToggleIcon() }} pull-right"></i>
            </span>
        @endif
    </a>

    @foreach ($appends as $append)
        {!! $append !!}
    @endforeach

    @if (count($items) > 0)
        <ul class="treeview-menu">
            @foreach ($items as $item)
                {!! $item !!}
            @endforeach
        </ul>
    @endif
</li>
@endif

